<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>MyCurriculum.org Web Application Test</title>
</head>
<body>
    <h1>MyCurriculum.org Web Application Test</h1>
    <p></p>User: <strong><sec:authentication property="principal.username" />!</strong></p>
    <p>Account: <strong>${accountName}</strong></p>
    <p>Authorities (Roles):</p>
    <ul>
        <c:forEach var="authority" items="${authorities}">
        <li><strong><c:out value="${authority.getAuthority()}"></c:out></strong></li>
        </c:forEach>
    </ul>
    <br />
    <h2>Courses</h2>
    <ul>
        <c:forEach var="course" items="${courses}">
        <li><c:out value="${course}"></c:out></li>
        </c:forEach>
    </ul>
</body>
</html>
