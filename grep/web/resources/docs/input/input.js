var com = { qmino : { miredot : {}}};
com.qmino.miredot.restApiSource = {"licenceType":"PRO","miredotRevision":"780bc70fd738","hideLogoOnTop":true,"allowUsageTracking":true,"issuesTabHidden":true,"licenceErrorMessage":null,"miredotVersion":"1.6.0","validLicence":true,"projectTitle":"grep.ndla.no","initialCollapseLevel":0,"jsonDocEnabled":true,"baseUrl":"http:\/\/mycurriculum.ndlap3.seria.net\/v1","projectVersion":"1.0-SNAPSHOT","singlePage":false,"jsonDocHidden":false,"licenceHash":"9136483928772491021","buildSystem":"maven 3","projectName":"mycurriculum","dateOfGeneration":"2015-04-21 10:36:39"};
com.qmino.miredot.restApiSource.tos = {
	org_mycurriculum_data_tms_domain_RolfPsiTextEntry_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfPsiTextEntry_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfPsiTextEntry_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfPsiTextEntry_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLanguage_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLanguage_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLanguage_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLanguage_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUserById_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUserById_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUserById_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUserById_out", "content": [] },
	org_mycurriculum_business_api_RolfDeleteRelationPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_RolfDeleteRelationPayload_in", "content": [] },
	org_mycurriculum_business_api_RolfDeleteRelationPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_RolfDeleteRelationPayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUser_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUser_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUser_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUser_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfRelation_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfRelation_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfRelation_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfRelation_out", "content": [] },
	org_mycurriculum_business_api_StatusNoLinkPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_StatusNoLinkPayload_in", "content": [] },
	org_mycurriculum_business_api_StatusNoLinkPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_StatusNoLinkPayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfBasicResource_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfBasicResource_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfBasicResource_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfBasicResource_out", "content": [] },
	org_mycurriculum_business_api_LinkIdPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_LinkIdPayload_in", "content": [] },
	org_mycurriculum_business_api_LinkIdPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_LinkIdPayload_out", "content": [] },
	org_mycurriculum_business_api_UserPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_UserPayload_in", "content": [] },
	org_mycurriculum_business_api_UserPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_UserPayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfResource_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfResource_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfResource_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfResource_out", "content": [] },
	org_mycurriculum_business_api_RolfCurriculumSetPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_RolfCurriculumSetPayload_in", "content": [] },
	org_mycurriculum_business_api_RolfCurriculumSetPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_RolfCurriculumSetPayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCompetenceAim_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCompetenceAim_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCompetenceAim_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCompetenceAim_out", "content": [] },
	org_mycurriculum_business_api_CoursePayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_CoursePayload_in", "content": [] },
	org_mycurriculum_business_api_CoursePayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_CoursePayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUserById$UserById_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUserById$UserById_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUserById$UserById_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUserById$UserById_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfAccount_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfAccount_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfAccount_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfAccount_out", "content": [] },
	org_mycurriculum_business_api_RelationsPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_RelationsPayload_in", "content": [] },
	org_mycurriculum_business_api_RelationsPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_RelationsPayload_out", "content": [] },
	org_mycurriculum_business_api_CourseMappingPayload$Subject_in: { "type": "complex", "name": "org_mycurriculum_business_api_CourseMappingPayload$Subject_in", "content": [] },
	org_mycurriculum_business_api_CourseMappingPayload$Subject_out: { "type": "complex", "name": "org_mycurriculum_business_api_CourseMappingPayload$Subject_out", "content": [] },
	org_mycurriculum_business_api_ProcessingPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_ProcessingPayload_in", "content": [] },
	org_mycurriculum_business_api_ProcessingPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_ProcessingPayload_out", "content": [] },
	org_mycurriculum_business_api_EducationGroupPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_EducationGroupPayload_in", "content": [] },
	org_mycurriculum_business_api_EducationGroupPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_EducationGroupPayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_out", "content": [] },
	org_mycurriculum_business_api_IndexStatusPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_IndexStatusPayload_in", "content": [] },
	org_mycurriculum_business_api_IndexStatusPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_IndexStatusPayload_out", "content": [] },
	org_mycurriculum_business_api_StatusPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_StatusPayload_in", "content": [] },
	org_mycurriculum_business_api_StatusPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_StatusPayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfName_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfName_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfName_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfName_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculums_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculums_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculums_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculums_out", "content": [] },
	org_mycurriculum_business_api_ResourcePayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_ResourcePayload_in", "content": [] },
	org_mycurriculum_business_api_ResourcePayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_ResourcePayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfNameNoScope_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfNameNoScope_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfNameNoScope_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfNameNoScope_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLink_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLink_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLink_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLink_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculum_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculum_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculum_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculum_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_out", "content": [] },
	org_mycurriculum_business_api_CourseMappingPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_CourseMappingPayload_in", "content": [] },
	org_mycurriculum_business_api_CourseMappingPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_CourseMappingPayload_out", "content": [] },
	org_mycurriculum_business_api_RolfRelationPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_RolfRelationPayload_in", "content": [] },
	org_mycurriculum_business_api_RolfRelationPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_RolfRelationPayload_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLocale_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLocale_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLocale_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLocale_out", "content": [] },
	org_mycurriculum_business_api_CourseMappingPayload$Subjects_in: { "type": "complex", "name": "org_mycurriculum_business_api_CourseMappingPayload$Subjects_in", "content": [] },
	org_mycurriculum_business_api_CourseMappingPayload$Subjects_out: { "type": "complex", "name": "org_mycurriculum_business_api_CourseMappingPayload$Subjects_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfIdAuthor_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfIdAuthor_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfIdAuthor_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfIdAuthor_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculumSet_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculumSet_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCurriculumSet_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCurriculumSet_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUrlAuthor_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUrlAuthor_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfUrlAuthor_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfUrlAuthor_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCourseStructure_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCourseStructure_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCourseStructure_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCourseStructure_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfTextEntry_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfTextEntry_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfTextEntry_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfTextEntry_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCourse_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCourse_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfCourse_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfCourse_out", "content": [] },
	org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_in: { "type": "complex", "name": "org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_in", "content": [] },
	org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_out: { "type": "complex", "name": "org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_out", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLevel_in: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLevel_in", "content": [] },
	org_mycurriculum_data_tms_domain_RolfLevel_out: { "type": "complex", "name": "org_mycurriculum_data_tms_domain_RolfLevel_out", "content": [] },
	org_mycurriculum_business_api_StatusAssociationPayload_in: { "type": "complex", "name": "org_mycurriculum_business_api_StatusAssociationPayload_in", "content": [] },
	org_mycurriculum_business_api_StatusAssociationPayload_out: { "type": "complex", "name": "org_mycurriculum_business_api_StatusAssociationPayload_out", "content": [] }
};

com.qmino.miredot.restApiSource.enums = {

};
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_in"].content = [ 
	{
		"name": "text",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_out"].content = [ 
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "text",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_in"].content = [ 
	{
		"name": "displayLanguage",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_out"].content = [ 
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "displayLanguage",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "self",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "user",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_in"],
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById_out"].content = [ 
	{
		"name": "user",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_out"],
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "self",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfDeleteRelationPayload_in"].content = [ 
	{
		"name": "competenceAimId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "humanId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "resourcePsi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfDeleteRelationPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfDeleteRelationPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfDeleteRelationPayload_out"].content = [ 
	{
		"name": "humanId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "competenceAimId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "resourcePsi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfDeleteRelationPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfDeleteRelationPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUser_in"].content = [ 
	{
		"name": "account",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_in"] },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "userName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUser_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUser_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUser_out"].content = [ 
	{
		"name": "account",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_out"] },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "userName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUser_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUser_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_in"].content = [ 
	{
		"name": "authors",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_in"] },
		"deprecated": false
	},
	{
		"name": "apprenticeRelevant",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "humanId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "competenceAimSetId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "curriculumId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "competenceAim",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_in"],
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_out"].content = [ 
	{
		"name": "competenceAim",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_out"],
		"deprecated": false
	},
	{
		"name": "humanId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "authors",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_out"] },
		"deprecated": false
	},
	{
		"name": "competenceAimSetId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "curriculumId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "apprenticeRelevant",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_out"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_in"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_out"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_in"].content = [ 
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_in"].content = [ 
	{
		"name": "isEnabled",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "firstName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "lastName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "email",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "roles",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "userName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_out"].content = [ 
	{
		"name": "lastName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "isEnabled",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "firstName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "email",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "roles",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "userName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource_in"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "ingress",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_in"],
		"deprecated": false
	},
	{
		"name": "relations",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_in"] },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource_out"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	},
	{
		"name": "ingress",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_out"],
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "relations",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfRelation_out"] },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfCurriculumSetPayload_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "setType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "curriculums",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_in"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfCurriculumSetPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfCurriculumSetPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfCurriculumSetPayload_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "setType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	},
	{
		"name": "curriculums",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfCurriculumSetPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfCurriculumSetPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_in"].content = [ 
	{
		"name": "sortingOrder",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "levels",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_in"] },
		"deprecated": false
	},
	{
		"name": "competenceAimSets",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "upstreamId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "levels",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_out"] },
		"deprecated": false
	},
	{
		"name": "sortingOrder",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	},
	{
		"name": "competenceAimSets",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out"] },
		"deprecated": false
	},
	{
		"name": "upstreamId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_in"].content = [ 
	{
		"name": "defaultLocale",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_in"] },
		"deprecated": false
	},
	{
		"name": "defaultText",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psiTextEntries",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_in"] },
		"deprecated": false
	},
	{
		"name": "languages",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_in"] },
		"deprecated": false
	},
	{
		"name": "localeTextEntries",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_in"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_out"].content = [ 
	{
		"name": "defaultLocale",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_out"] },
		"deprecated": false
	},
	{
		"name": "defaultText",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "languages",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLanguage_out"] },
		"deprecated": false
	},
	{
		"name": "psiTextEntries",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfPsiTextEntry_out"] },
		"deprecated": false
	},
	{
		"name": "localeTextEntries",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource$RolfIngress_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_in"].content = [ 
	{
		"name": "sortingOrder",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "sortingOrder",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAim_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "externalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "educationGroupId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "externalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "educationGroupId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_in"].content = [ 
	{
		"name": "isEnabled",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "firstName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "lastName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "account",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_in"] },
		"deprecated": false
	},
	{
		"name": "email",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "roles",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "userName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "lastName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "isEnabled",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "account",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_out"] },
		"deprecated": false
	},
	{
		"name": "firstName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "email",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "roles",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "userName",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById$UserById_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_in"].content = [ 
	{
		"name": "author",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_in"],
		"deprecated": false
	},
	{
		"name": "subjectMatterId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "relations",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_in"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_out"].content = [ 
	{
		"name": "author",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_out"],
		"deprecated": false
	},
	{
		"name": "subjectMatterId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "relations",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_in"].content = [ 
	{
		"name": "title",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "course_structure",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_out"].content = [ 
	{
		"name": "title",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "course_structure",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_out"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "parentExternalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "externalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "parentExternalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "externalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_in"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumNoAimSets_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_in"].content = [ 
	{
		"name": "error",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "log",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "message",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "trace",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_out"].content = [ 
	{
		"name": "message",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "error",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "log",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "trace",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_out"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "isLanguageNeutral",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "scopes",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "isLanguageNeutral",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "scopes",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculums_in"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculums_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculums_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculums_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculums_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculums_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_in"].content = [ 
	{
		"name": "psi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "ingress",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_in"],
		"deprecated": false
	},
	{
		"name": "authors",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_in"] },
		"deprecated": false
	},
	{
		"name": "type",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_in"] },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "licenses",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_out"].content = [ 
	{
		"name": "type",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_out"] },
		"deprecated": false
	},
	{
		"name": "psi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "ingress",
		"comment": null,
		"fullComment": null,
		"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_out"],
		"deprecated": false
	},
	{
		"name": "authors",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_out"] },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "licenses",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "languageCode",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "isLanguageNeutral",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "languageCode",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "isLanguageNeutral",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfNameNoScope_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"].content = [ 
	{
		"name": "self",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "parents",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"].content = [ 
	{
		"name": "self",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "parents",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculum_in"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "competenceAimSets",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_in"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculum_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculum_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculum_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "competenceAimSets",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculum_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculum_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_in"].content = [ 
	{
		"name": "sortingOrder",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "resources",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_in"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "sortingOrder",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	},
	{
		"name": "resources",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfBasicResource_out"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_in"].content = [ 
	{
		"name": "ndla",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_in"] },
		"deprecated": false
	},
	{
		"name": "deling",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_in"] },
		"deprecated": false
	},
	{
		"name": "fyr",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_in"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_out"].content = [ 
	{
		"name": "ndla",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_out"] },
		"deprecated": false
	},
	{
		"name": "deling",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_out"] },
		"deprecated": false
	},
	{
		"name": "fyr",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_in"].content = [ 
	{
		"name": "relationType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "competenceAimId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "humanId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "resourcePsi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "apprenticeRelevance",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "curriculumSetId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "level",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_out"].content = [ 
	{
		"name": "humanId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "relationType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "competenceAimId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "resourcePsi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "apprenticeRelevance",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "curriculumSetId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "level",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfRelationPayload_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_in"].content = [ 
	{
		"name": "displayLanguage",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_out"].content = [ 
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "displayLanguage",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psi",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLocale_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_in"].content = [ 
	{
		"name": "subject",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_in"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_out"].content = [ 
	{
		"name": "subject",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subject_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload$Subjects_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfIdAuthor_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "setType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "setType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "url",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "url",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUrlAuthor_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "setType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "courseStructures",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_in"] },
		"deprecated": false
	},
	{
		"name": "levels",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_in"] },
		"deprecated": false
	},
	{
		"name": "curriculumSets",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "levels",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_out"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "setType",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	},
	{
		"name": "courseStructures",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_out"] },
		"deprecated": false
	},
	{
		"name": "curriculumSets",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculumSet_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_in"].content = [ 
	{
		"name": "text",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_out"].content = [ 
	{
		"name": "language",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "text",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfTextEntry_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_in"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "externalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "versionNumber",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "courseStructures",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_in"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_out"].content = [ 
	{
		"name": "name",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "versionNumber",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "externalId",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "link",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "courseStructures",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_in"].content = [ 
	{
		"name": "languageCode",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "text",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_out"].content = [ 
	{
		"name": "text",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "languageCode",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload$RolfIngressShort_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_in"].content = [ 
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_in"] },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_in"] },
		"deprecated": false
	},
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_out"].content = [ 
	{
		"name": "id",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "names",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfName_out"] },
		"deprecated": false
	},
	{
		"name": "psis",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "links",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLink_out"] },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_out"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusAssociationPayload_in"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "associations",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusAssociationPayload_in"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusAssociationPayload_in"].comment = null;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusAssociationPayload_out"].content = [ 
	{
		"name": "processingTime",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	},
	{
		"name": "associations",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } },
		"deprecated": false
	},
	{
		"name": "status",
		"comment": null,
		"fullComment": null,
		"typeValue": { "type": "simple", "typeValue": "string" },
		"deprecated": false
	}
];
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusAssociationPayload_out"].ordered = false;
com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusAssociationPayload_out"].comment = null;
com.qmino.miredot.restApiSource.interfaces = [
	{
		"beschrijving": "GET account by id",
		"url": "/v1/users/{userName}/accounts/{id}",
		"http": "GET",
		"title": "Get account by id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfAccount_out"], "comment": "Account as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "226236241",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "account id", "jaxrs": "PATH"}
                ],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "PUT relations",
		"url": "/v1/users/{userName}/relations",
		"http": "PUT",
		"title": "Update relations",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_out"], "comment": "Update statistics as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "2039682380",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [{"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"}],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "POST GREP import by user id",
		"url": "/v1/users/{userName}/grepimport/start",
		"http": "POST",
		"title": "Start GREP import by user id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json, application/x-www-form-urlencoded"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_out"], "comment": "Imported element ids as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "290312136",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [],
                "BODY": [
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_in"], "comment": "the Http request", "jaxrs": "BODY"},
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CourseMappingPayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"}
                ],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "Import curriculum objects",
		"url": "/v1/grepimports",
		"http": "GET",
		"title": "Import curriculum objects",
		"tags": [],
		"authors": ["Jan-Espen Oversand"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_out"], "comment": "Imported elements as JSON"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "961623699",
		"inputs": {
                "PATH": [],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET competence aim sets by id",
		"url": "/v1/users/{userName}/competence-aim-sets/{id}",
		"http": "GET",
		"title": "Get competence aim set by id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceaimSet_out"], "comment": "Competence aim set as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-323932911",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "competence aim set id", "jaxrs": "PATH"}
                ],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "Import curriculum object",
		"url": "/v1/grepimport/{id}",
		"http": "GET",
		"title": "Import curriculum object",
		"tags": [],
		"authors": ["Jan-Espen Oversand"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_out"], "comment": "Status for the import as JSON"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "555884178",
		"inputs": {
                "PATH": [{"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the id of the element to import", "jaxrs": "PATH"}],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "POST Education Group",
		"url": "/v1/users/{userName}/education-groups",
		"http": "POST",
		"title": "Create an education group",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_out"], "comment": "Creation statistics as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "1383074069",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"},
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_EducationGroupPayload_in"], "comment": "the response object", "jaxrs": "BODY"}
                ],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "POST Create Course",
		"url": "/v1/users/{userName}/courses",
		"http": "POST",
		"title": "Create a course",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_out"], "comment": "Creation statistics as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "-246807171",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"},
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_CoursePayload_in"], "comment": "the response object", "jaxrs": "BODY"}
                ],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET competence aim by id",
		"url": "/v1/users/{userName}/competence-aims/{id}",
		"http": "GET",
		"title": "Get competence aim by id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCompetenceAimWithResources_out"], "comment": "Competence aim as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "95907249",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "competence aim id", "jaxrs": "PATH"}
                ],
                "QUERY": [
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"},
                    {"name": "resources", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to include resources [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "DELETE User",
		"url": "/v1/users/{userName}/users/{id}",
		"http": "DELETE",
		"title": "Delete User by user name and id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_out"], "comment": "Processing time as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-122013226",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the userName", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "the id", "jaxrs": "PATH"}
                ],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "POST Create User",
		"url": "/v1/users/{userName}/users",
		"http": "POST",
		"title": "Create User",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_out"], "comment": "Creation statistics as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "-1750716766",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user name", "jaxrs": "PATH"}],
                "QUERY": [],
                "BODY": [
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_in"], "comment": "payload the payload as JSON", "jaxrs": "BODY"},
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_in"], "comment": "the response", "jaxrs": "BODY"}
                ],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Users",
		"url": "/v1/users/{userName}/users",
		"http": "GET",
		"title": "Get Users",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUser_out"] }, "comment": "Users as JSONArray"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "1813226882",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user name", "jaxrs": "PATH"}],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET course structure by id",
		"url": "/v1/users/{userName}/course-structures/{id}",
		"http": "GET",
		"title": "Get course structure by id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourseStructure_out"], "comment": "Course structure as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "1548596951",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "course structure id", "jaxrs": "PATH"}
                ],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Education Groups",
		"url": "/v1/users/{userName}/education-groups",
		"http": "GET",
		"title": "Get education groups",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_out"] }, "comment": "Education groups as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "400268331",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"},
                    {"name": "tree", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to resolve data as tree structure [not required]", "jaxrs": "QUERY"},
                    {"name": "resources", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to resolve resources [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Education Group",
		"url": "/v1/users/{userName}/education-groups/{externalId}",
		"http": "GET",
		"title": "Get education group by name and id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_out"], "comment": "Education group as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-1112233033",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "externalId", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "group's externalId", "jaxrs": "PATH"}
                ],
                "QUERY": [
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"},
                    {"name": "tree", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to resolve data as tree structure [not required]", "jaxrs": "QUERY"},
                    {"name": "resources", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to resolve resources [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET process data",
		"url": "/v1/users/{userName}/processes/start-{id}",
		"http": "GET",
		"title": "Get information about ongoing process",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_out"], "comment": "Process data as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "1675544741",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the start id for the process", "jaxrs": "PATH"}
                ],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "POST Create resource",
		"url": "/v1/users/{userName}/resources",
		"http": "POST",
		"title": "Create a resource",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusPayload_out"], "comment": "Creation statistics as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "1639001117",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the userName", "jaxrs": "PATH"}],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"},
                    {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ResourcePayload_in"], "comment": "the response", "jaxrs": "BODY"}
                ],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Level",
		"url": "/v1/users/{userName}/levels/{id}",
		"http": "GET",
		"title": "Get Level by id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_out"], "comment": "Level as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "1103748529",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the Level id", "jaxrs": "PATH"}
                ],
                "QUERY": [
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"},
                    {"name": "course-structures", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> show course-structures or not [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "PUT User",
		"url": "/v1/users/{userName}/users/{id}",
		"http": "PUT",
		"title": "Update User by user name and id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_ProcessingPayload_out"], "comment": "Processing time as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "-1579519015",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the userName", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "the user id", "jaxrs": "PATH"}
                ],
                "QUERY": [],
                "BODY": [{"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_UserPayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"}],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Resources",
		"url": "/v1/users/{userName}/resources",
		"http": "GET",
		"title": "Get resources",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfResource_out"], "comment": "Resources as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-309382937",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [
                    {"name": "psi", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the resource PSI, e.g http://domain/{document id}", "jaxrs": "QUERY"},
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"},
                    {"name": "competence-aims", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to resolve competence aims [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "POST Start indexer",
		"url": "/v1/users/{userName}/searchindexer/start",
		"http": "POST",
		"title": "\"Start indexer",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json, application/x-www-form-urlencoded"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_out"], "comment": "Index data as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "1979148936",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the userName", "jaxrs": "PATH"}],
                "QUERY": [],
                "BODY": [
                    {"typeValue": { "type": "simple", "typeValue": "javax.servlet.http.HttpServletRequest" }, "comment": "the request", "jaxrs": "BODY"},
                    {"typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the payload as JSON", "jaxrs": "BODY"}
                ],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "POST relations",
		"url": "/v1/users/{userName}/relations",
		"http": "POST",
		"title": "Create relations",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusAssociationPayload_out"], "comment": "Creation statistics as JSONObject"},
		"statusCodes": [
                { "httpCode": 200, "comment": "The service call has completed successfully."},
                { "httpCode": 412, "comment": "Invalid JSON/XML input."}
            ],
		"hash": "2047553465",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [{"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RelationsPayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"}],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET all curricula",
		"url": "/v1/users/{userName}/curriculums",
		"http": "GET",
		"title": "Get all curricula. These are stubs with name and curriculum id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculums_out"] }, "comment": "Curricula as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "145735975",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Course",
		"url": "/v1/users/{userName}/courses/{externalId}",
		"http": "GET",
		"title": "GET course",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_out"], "comment": "Course as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "785711419",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "externalId", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "<em>String</em> external id of education group", "jaxrs": "PATH"}
                ],
                "QUERY": [
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"},
                    {"name": "tree", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to reender data as tree [not required]", "jaxrs": "QUERY"},
                    {"name": "resources", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to resolve resources [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Indexed imports",
		"url": "/v1/searchindexers",
		"http": "GET",
		"title": "\"Get indexed imports",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_LinkIdPayload_out"], "comment": "Indexer status as JSON"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-2027306457",
		"inputs": {
                "PATH": [],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET curriculum sets by id",
		"url": "/v1/users/{userName}/curriculum-sets/{id}",
		"http": "GET",
		"title": "Get curriculum sets by id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfCurriculumSetPayload_out"], "comment": "Curriculum as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "1044919223",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "Curriculum set id", "jaxrs": "PATH"}
                ],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Search indexer types",
		"url": "/v1/searchindexer/types",
		"http": "GET",
		"title": "\"GET indexer types",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": { "type": "collection", "typeValue":{ "type": "simple", "typeValue": "string" } }, "comment": "Indexer types as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-1998540383",
		"inputs": {
                "PATH": [],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "DELETE relations",
		"url": "/v1/users/{userName}/relations",
		"http": "DELETE",
		"title": "Delete relations",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": ["application/json"],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_StatusNoLinkPayload_out"], "comment": "DElete statistics as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-1896058326",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [{"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_RolfDeleteRelationPayload_in"], "comment": "the payload as JSON", "jaxrs": "BODY"}],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET curriculum by id",
		"url": "/v1/users/{userName}/curriculums/{id}",
		"http": "GET",
		"title": "Get curriculum by id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCurriculum_out"], "comment": "Curriculum as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "-335772485",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "Curriculum id", "jaxrs": "PATH"}
                ],
                "QUERY": [{"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"}],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Courses",
		"url": "/v1/users/{userName}/courses",
		"http": "GET",
		"title": "GET courses",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfCourse_out"], "comment": "Courses as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "1890902732",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long</em> version number [not required]", "jaxrs": "QUERY"},
                    {"name": "education-group", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "<em>String</em> external id of education group", "jaxrs": "QUERY"},
                    {"name": "tree", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to render data as tree [not required]", "jaxrs": "QUERY"},
                    {"name": "resources", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> whether to resolve resources [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET User",
		"url": "/v1/users/{userName}/users/{id}",
		"http": "GET",
		"title": "Get User by user name and id",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfUserById_out"], "comment": "User as JSONArray"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "650343179",
		"inputs": {
                "PATH": [
                    {"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user name", "jaxrs": "PATH"},
                    {"name": "id", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "the user id", "jaxrs": "PATH"}
                ],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Indexer status",
		"url": "/v1/searchindexer/{id}",
		"http": "GET",
		"title": "\"Get indexer status",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": com.qmino.miredot.restApiSource.tos["org_mycurriculum_business_api_IndexStatusPayload_out"], "comment": "Indexer status as String"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "68581502",
		"inputs": {
                "PATH": [{"name": "id", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "indexer id", "jaxrs": "PATH"}],
                "QUERY": [],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	},
	{
		"beschrijving": "GET Levels",
		"url": "/v1/users/{userName}/levels",
		"http": "GET",
		"title": "Get Levels",
		"tags": [],
		"authors": ["Jan-Espen Overland"],
		"compressed": false,
		"deprecated": false,
		"consumes": [],
		"produces": ["application/json"],
		"roles": [],
		"rolesAllowed": null,
		"permitAll": false,
		"output": {"typeValue": { "type": "collection", "typeValue":com.qmino.miredot.restApiSource.tos["org_mycurriculum_data_tms_domain_RolfLevel_out"] }, "comment": "Levels as JSONObject"},
		"statusCodes": [{ "httpCode": 200, "comment": "The service call has completed successfully."}],
		"hash": "561495805",
		"inputs": {
                "PATH": [{"name": "userName", "typeValue": { "type": "simple", "typeValue": "string" }, "comment": "the user Name", "jaxrs": "PATH"}],
                "QUERY": [
                    {"name": "version", "typeValue": { "type": "simple", "typeValue": "number" }, "comment": "<em>Long version</em> number [not required]", "jaxrs": "QUERY"},
                    {"name": "course-structures", "typeValue": { "type": "simple", "typeValue": "boolean" }, "comment": "<em>boolean</em> show course-structures or not [not required]", "jaxrs": "QUERY"}
                ],
                "BODY": [],
                "HEADER": [],
                "COOKIE": [],
                "FORM": [],
                "MATRIX": []
            }
	}];
com.qmino.miredot.projectWarnings = [
	{
		"category": "PARTIAL_RESOURCE_OVERLAP",
		"description": "This rest interface (partially) hides another rest interface",
		"failedBuild": false,
		"interface": "-1998540383",
		"implementationClass": "org.mycurriculum.service.rest.SearchIndexerController",
		"implementationMethod": "getIndexerTypes",
		"entity": "Overlaps with: /v1/searchindexer/{id}"
	}];
com.qmino.miredot.processErrors  = [
];

