package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.data.cache.CacheDaoEhcacheImpl;

public class EhcacheDirEscapeTest extends TestCase {
    public EhcacheDirEscapeTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( HarnessTest.class );
    }

    public void testApp()
    {
        assertTrue(CacheDaoEhcacheImpl.dirEscape("test/t\\bla/\\h").equals("test\\/t\\\\bla\\/\\\\h"));
        assertTrue(CacheDaoEhcacheImpl.dirEscape("/t\\bla/\\").equals("\\/t\\\\bla\\/\\\\"));
    }
}
