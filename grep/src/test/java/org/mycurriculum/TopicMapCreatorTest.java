package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.TopicMapCreator;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.connections.topicmapstore.TopicMapStoreCreator;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.sql.SQLException;

public class TopicMapCreatorTest extends TestCase {
    private AbstractApplicationContext context;
    private String topicMapConnectionString;
    private TopicMapLocator locator;

    public TopicMapCreatorTest()
    {
    }

    public static Test suite() {
        return new TestSuite( TopicMapCreatorTest.class );
    }

    public void setApplicationContext(AbstractApplicationContext context) {
        this.context = context;
    }
    public ApplicationContext getApplicationContext() {
        return context;
    }
    public TopicMapLocator getTopicMapLocator() {
        return locator;
    }

    public void testTopicMap(TopicMapRoot topicMapRoot) throws TestException {
    }
    public void testTopicMapConnectionString(String connectionString) throws TestException, IOException {
        TopicMapCreator topicMapCreator = (TopicMapCreator) context.getBean("topicMapCreator");
        locator = topicMapCreator.getTopicMapFromConnectionString(connectionString);
        if (locator == null)
            throw new TestException("Stub: Returned null (locator)");
        testTopicMap(locator.getTopicMapRoot());
    }
    public void testApp() throws TestException {
        try (AbstractApplicationContext context = new ClassPathXmlApplicationContext("classpath*:spring.xml")) {
            this.context = context;
            doTestApp();
        } finally {
            this.context.close();
            this.context = null;
        }
    }
    public void doTestApp() throws TestException
    {
        TopicMapStoreCreator creator = (TopicMapStoreCreator) context.getBean("topicMapStoreCreator");

        try {
            creator.testTemplateConnection();
        } catch (SQLException e) {
            System.out.println("The template database for creating topic maps does not work. Cannot test.");
            return;
        }
        try {
            TopicMapCreator topicMapCreator = (TopicMapCreator) context.getBean("topicMapCreator");
            try {
                topicMapConnectionString = topicMapCreator.createTopicMap("testcreatestore", "testtm").getConnectionString();
                testTopicMapConnectionString(topicMapConnectionString);
            } finally {
                if (creator.isOpenStore("testcreatestore")) {
                    creator.closeStore("testcreatestore");
                }
                creator.deleteStore("testcreatestore");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new TestException("Failed to create store");
        } catch (IOException e) {
            e.printStackTrace();
            throw new TestException("Failed to create store");
        }
    }

    public String getTopicMapConnectionString() {
        return topicMapConnectionString;
    }
}
