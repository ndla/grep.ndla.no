package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaLevel;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;

public class AddCourseStructureTest extends AddEducationalProgramTest {
    public static Test suite() {
        return new TestSuite( AddCourseStructureTest.class );
    }

    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        super.testTopicMap(topicMap);

        try {
            /* Have got http://psi.mycurriculum.org/test/eduprog/test */
            URL parentPsi = new URL("http://psi.mycurriculum.org/test/eduprog/test");
            OntopiaCourseStructure parent = topicMap.getCourseStructure(parentPsi);
            if (parent == null)
                throw new TestException("Parent course structure not found");
            System.out.println("Creating test level");
            OntopiaLevel level = topicMap.getLevelBuilder()
                    .name("Test level")
                    .psi("http://psi.mycurriculum.org/test/level/test")
                    .build();
            level.save();
            System.out.println("Creating course structure test object");
            topicMap.getCourseStructureBuilder()
                    .name("Course structure test", new Locale("eng"), true)
                    .name("Kurstrukturtest", new Locale("nob"))
                    .psi("http://psi.mycurriculum.org/test/coursestructure/test")
                    .addCourseStructureParent(parent)
                    .addLevel(level)
                    .build().save();
            System.out.println("Trying to load the course structure object");
            OntopiaCourseStructure courseStructure = topicMap.getCourseStructure(new URL("http://psi.mycurriculum.org/test/coursestructure/test"));
            System.out.println("Found: "+courseStructure.getName(new Locale("eng")));
            int parents = 0;
            int childs = 0;
            for (OntopiaCourseStructure foundParent : courseStructure.getCourseStructureParents()) {
                System.out.println("Found parent path: "+foundParent.getName());
                for (OntopiaCourseStructure foundChild : foundParent.getCourseStructures()) {
                    System.out.println("Found parent-child path: "+foundChild.getName());
                    childs++;
                }
                parents++;
            }
            if (childs != 1 && parents != 1)
                throw new TestException("Invalid/wrong container-containee structure found in check phase");
            System.out.println("Level test: ");
            for (OntopiaLevel assocLevel : courseStructure.getLevels()) {
                System.out.println(" * "+assocLevel.getName());
                for (OntopiaCourseStructure assocCourseStructure : assocLevel.getCourseStructures()) {
                    System.out.println("  * "+assocCourseStructure.getName(new Locale("eng")));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new TestException("IO-error during testing of creating topic: "+e.getMessage());
        }
    }
}
