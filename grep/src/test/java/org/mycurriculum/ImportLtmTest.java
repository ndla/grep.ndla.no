package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class ImportLtmTest extends CreatePostgresqlDatabaseTest {
    public static Test suite()
    {
        return new TestSuite( ImportLtmTest.class );
    }

    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        try {
            System.out.println("Creating reader");
            Reader reader = new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("my_curriculum_org_ontologi.ltm"), "UTF-8");
            System.out.println("Importing topicmap ltmdata");
            topicMap.importLtm(reader);
            System.out.println("LTM imported");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new TestException("UTF-8 is not supported!");
        } catch (IOException e) {
            e.printStackTrace();
            throw new TestException("Failed to import ltm due to IO-error: "+e.getMessage());
        }
    }
}
