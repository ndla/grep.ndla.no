package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.connections.TopicMapStore;
import org.mycurriculum.data.tms.connections.TopicMapStoreLocator;
import org.mycurriculum.data.tms.connections.topicmapstore.TopicMapStoreCreator;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by janespen on 5/9/14.
 */
public class CreatePostgresqlDatabaseTest extends TestCase {
    private AbstractApplicationContext context;

    public CreatePostgresqlDatabaseTest()
    {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( CreatePostgresqlDatabaseTest.class );
    }

    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
    }
    public void testApp() throws TestException {
        TopicMapStoreCreator creator = (TopicMapStoreCreator) context.getBean("topicMapStoreCreator");

        try {
            try {
                creator.testTemplateConnection();
            } catch (SQLException e) {
                System.out.println("The template database for creating topic maps does not work. Cannot test.");
                return;
            }
            try {
                creator.createStore("testcreatestore");
                try {
                    TopicMapStoreLocator locator = creator.getStoreLocator("testcreatestore");
                    System.out.println("Created " + locator.getDbConnectionSetup().getConnectionString());
                    TopicMapStore store = locator.getTopicMapStore();
                    testTopicMap(store.createTopicMap("testtm").getTopicMapRoot());
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new TestException("Exception in test code");
                } finally {
                    if (creator.isOpenStore("testcreatestore")) {
                        creator.closeStore("testcreatestore");
                    }
                    creator.deleteStore("testcreatestore");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new TestException("Failed to create store");
            } catch (IOException e) {
                e.printStackTrace();
                throw new TestException("Failed to create store");
            }
        } finally {
            context.close();
            context = null;
        }
    }
}
