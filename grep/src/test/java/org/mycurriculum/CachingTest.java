package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.data.cache.CacheDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.Serializable;

public class CachingTest extends TestCase {
    private AbstractApplicationContext context;
    private CacheDao cacheDao;

    public CachingTest() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
        cacheDao = (CacheDao) context.getBean("cacheDao");
    }

    public static Test suite()
    {
        return new TestSuite( CachingTest.class );
    }

    public void testApp() {
        DataObject d1 = new DataObject();
        DataObject d2 = new DataObject();
        DataObject r1;
        DataObject r2;

        d1.setName("d1");
        d1.setNumber(1);
        d2.setName("d2");
        d2.setNumber(2);

        CacheDao.TypedCacheNamespace<DataObject> cache = new CacheDao.TypedCacheNamespace<DataObject>(cacheDao.getNamespace("testns"));

        cache.writeObject("d1", d1);
        cache.writeObject("d2", d2);

        r1 = cache.readObject("d1");
        r2 = cache.readObject("d2");

        System.out.println(d1.toString()+"="+r1.toString());
        System.out.println(d2.toString()+"="+r2.toString());

        assertEquals(d1.toString(), r1.toString());
        assertEquals(d2.toString(), r2.toString());

        context.close();
        context = null;
    }

    private class DataObject implements Serializable {
        private String name;
        private int number;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String toString() {
            return "["+name+" "+number+"]";
        }
    }
}
