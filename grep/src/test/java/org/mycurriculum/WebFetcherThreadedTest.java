package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.WebFetcherFactory;
import org.mycurriculum.data.adapter.WebFetcherInstance;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by janespen on 4/10/14.
 */
public class WebFetcherThreadedTest extends TestCase {
    private AbstractApplicationContext context;
    private WebFetcher fetcher;

    public WebFetcherThreadedTest(String name)
    {
        super(name);

        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
        WebFetcherFactory fetcherFactory = (WebFetcherFactory) context.getBean("webFetcherFactory");
        fetcher = fetcherFactory.createWebFetcher();
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( WebFetcherThreadedTest.class );
    }

    public void testApp()
    {
        try {
            ThreadSet threads = new ThreadSet(10, new URL("http://seria.no/"));
            assertTrue(threads.equalResults());
            assertEquals(1, fetcher.getNumberOfWebRequests());
        } catch (InterruptedException e) {
            assertTrue(false);
        } catch (MalformedURLException e) {
            assertTrue(false);
        } finally {
            context.close();
            context = null;
        }
    }

    private class TestThread extends Thread
    {
        String result = null;
        URL url;

        public TestThread(URL url)
        {
            super();
            this.url = url;
        }
        public void run()
        {
            WebFetcherInstance fetcherInstance = fetcher.getFetcher(url);
            try {
                fetcherInstance.fetch();
                result = fetcherInstance.getContents();
            } catch (IOException e) {
                result = null;
            }
        }
        public String getResult() throws InterruptedException {
            this.join();
            if (result != null)
                System.out.println("Thread result: (string:" + result.length()+")");
            else
                System.out.println("Thread result: null");
            return result;
        }
    }
    private class ThreadSet
    {
        private TestThread[] threads;

        public ThreadSet(int num, URL url)
        {
            threads = new TestThread[num];
            for (int i = 0; i < num; i++)
                threads[i] = new TestThread(url);
            for (int i = 0; i < num; i++)
                threads[i].start();
        }
        public boolean equalResults() throws InterruptedException {
            String res;
            boolean equal = true;

            if (threads.length == 0)
                return true;
            res = threads[0].getResult();
            for (int i = 1; i < threads.length; i++) {
                String next = threads[i].getResult();

                if (res == next)
                    continue;
                if (next == null || res.compareTo(next) != 0)
                    equal = false;
            }
            return equal;
        }
    }
}
