package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.cache.CacheDaoNullImpl;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;

public class ResourceAssocBug20141103Test extends ResourceTest {
    public static final String curriculumSetId = "cset";

    public static Test suite() {
        return new TestSuite( ResourceAssocBug20141103Test.class );
    }

    private class Info {
        public String relationId;
        public String aimId;
        public String curriculumSetId;
    }
    private Info testRelationRead(TopicMapRoot topicMap, String resourcePsi, int assertAssocNum) throws TestException, IOException {
        OntopiaResource resource;
        try {
            resource = topicMap.getResource(new URL(resourcePsi));
        } catch (MalformedURLException e) {
            throw new TestException("Url-problem in the test", e);
        }
        Collection<OntopiaResourceAimAssociation> assocs = resource.getAimAssociations();
        System.out.println("Associations:");
        String relationId = null;
        String aimId = null;
        String curriculumSetId = null;
        assertEquals(assocs.size(), assertAssocNum);
        for (OntopiaResourceAimAssociation assoc : assocs) {
            OntopiaAim aim = assoc.getAim();
            System.out.println(" * "+aim.getName());
            relationId = assoc.getHumanId();
            aimId = aim.getHumanId();
            curriculumSetId = assoc.getCurriculumSet().getHumanId();
        }
        Info info = new Info();
        info.relationId = relationId;
        info.aimId = aimId;
        info.curriculumSetId = curriculumSetId;
        return info;
    }
    private void testUpdateRelation(TopicMapRoot topicMap, String relationId, String resourcePsi, String aimId, String curriculumSetId) throws TestException, IOException {
        OntopiaResource resource;
        try {
            resource = topicMap.getResource(new URL(resourcePsi));
        } catch (MalformedURLException e) {
            throw new TestException("Url-problem in the test", e);
        }
        OntopiaAim aim = topicMap.getAim(aimId);
        OntopiaCurriculumSet curriculumSet = topicMap.getCurriculumSet(curriculumSetId);
        assertNotNull(resource);
        assertNotNull(aim);
        assertNotNull(curriculumSet);
        OntopiaResourceAimAssociation assoc = topicMap.getResourceAimAssociation(relationId);
        assertNotNull(assoc);
        assertEquals(assoc.getAim().getHumanId(), aimId);
        assertEquals(assoc.getResource().getPsi().toString(), resourcePsi);
        assertNotNull(assoc.getCurriculumSet());
        assoc.setCurriculumSet(curriculumSet);
        assoc.save();
    }
    private void createCurriculumSet(TopicMapRoot topicMap) throws IOException {
        OntopiaCurriculumSet.Builder builder = topicMap.getCurriculumSetBuilder(curriculumSetId);
        builder
                .name("Curriculum reset testing");
        OntopiaCurriculumSet curriculumSet = builder.build();
        curriculumSet.save();
    }

    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        super.testTopicMap(topicMap);
        CacheDao cache = new CacheDaoNullImpl();
        topicMap.setCache(cache.getNamespace("null"));
        try {
            Info info = testRelationRead(topicMap, ResourceTest.testResource, 1);
            assertNotNull(info.aimId);
            System.out.println("Relation "+info.relationId);
            testUpdateRelation(topicMap, info.relationId, ResourceTest.testResource, info.aimId, info.curriculumSetId);
            testRelationRead(topicMap, ResourceTest.testResource, 1);
            createCurriculumSet(topicMap);
            testUpdateRelation(topicMap, info.relationId, ResourceTest.testResource, info.aimId, curriculumSetId);
            testRelationRead(topicMap, ResourceTest.testResource, 1);
        } catch (IOException e) {
            throw new TestException("I/O error during the test", e);
        }
    }
}
