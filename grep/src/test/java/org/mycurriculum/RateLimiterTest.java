package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.data.adapter.PerSecondRateLimiterImpl;
import org.mycurriculum.data.adapter.RateLimiter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

/**
 * Created by janespen on 4/15/14.
 */
public class RateLimiterTest extends TestCase {
    private AbstractApplicationContext context;
    private RateLimiter rateLimiter;

    public RateLimiterTest()
    {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
        PerSecondRateLimiterImpl rateLimiter = new PerSecondRateLimiterImpl();
        rateLimiter.setAveragingSeconds(3.0f);
        rateLimiter.setMaxPerSecond(3.3f);
        this.rateLimiter = rateLimiter;
    }
    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( RateLimiterTest.class );
    }

    public void testApp()
    {
        long minimumElapsed = 3900;
        RatelimitedMessage[] messages = new RatelimitedMessage[10];
        Date before, after;
        long elapsed;

        before = new Date();
        System.out.println(before.toString());
        for (int i = 0; i < messages.length; i++)
            messages[i] = new RatelimitedMessage("Message "+i, i);
        for (int i = 0; i < messages.length; i++) {
            try {
                messages[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        after = new Date();
        System.out.println(after.toString());
        elapsed = after.getTime() - before.getTime();
        System.out.println("Elapsed time: "+elapsed+" seconds (minimum "+minimumElapsed+" seconds)");
        assertTrue(minimumElapsed <= elapsed);

        context.close();
        context = null;
    }

    private class RatelimitedMessage extends Thread
    {
        private String message;
        private int recur;

        public RatelimitedMessage(String message, int recur)
        {
            this.message = message;
            this.recur = recur;
            this.start();
        }

        public void run()
        {
            rateLimiter.rateLimitWait();
            System.out.println(message);
            if (recur > 0) {
                RatelimitedMessage sub = new RatelimitedMessage(message + ": recur", recur - 1);
                try {
                    sub.join();
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
