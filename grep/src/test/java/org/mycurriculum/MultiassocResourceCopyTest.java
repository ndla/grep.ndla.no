package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.TopicMapCreator;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.connections.topicmapstore.TopicMapStoreCreator;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.data.tms.domain.OntopiaResourceAimAssociation;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.parser.ProgressBit;
import org.mycurriculum.data.util.parser.ProgressTrackerImpl;
import org.mycurriculum.data.util.parser.ResourceAssociationsCopier;
import org.mycurriculum.data.util.parser.ResourceCopier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class MultiassocResourceCopyTest extends MultiassocResourceTest {
    public class DestinationTree extends DestinationTreeEnvironment {
        public DestinationTree(AbstractApplicationContext context, Runnable whenReady) {
            super(context, whenReady);
        }

        public void copyFrom(TopicMapLocator src) throws IOException {
            ProgressBit p1 = new ProgressTrackerImpl("Test tracker 1");
            ProgressBit p2 = new ProgressTrackerImpl("Test tracker 2");
            ResourceAssociationsCopier assocCopier = new ResourceAssociationsCopier(null, p2, src, getTopicMapLocator());
            /* Copy nodes */
            System.out.println("Copying nodes...");
            ResourceCopier resourceCopier = new ResourceCopier(null, p1, src, getTopicMapLocator(), assocCopier);
            resourceCopier.run();
            System.out.println("Copying relations...");
            assocCopier.run();
        }
    }
    public class DestinationTreeEnvironment extends CurriculumTreeTest {
        private String topicMapConnectionString;
        private Runnable whenReady;
        private TopicMapRoot topicMapRoot;

        public DestinationTreeEnvironment(AbstractApplicationContext context, Runnable whenReady) {
            super();
            setApplicationContext(context);
            this.whenReady = whenReady;
        }

        @Override
        public void testTopicMap(TopicMapRoot topicMap) throws TestException {
            this.topicMapRoot = topicMap;
            super.testTopicMap(topicMap);
            whenReady.run();
        }
        public void testApp() throws TestException {
            doTestApp();
        }
        public void doTestApp() throws TestException
        {
            ApplicationContext context = getApplicationContext();
            TopicMapStoreCreator creator = (TopicMapStoreCreator) context.getBean("topicMapStoreCreator");

            try {
                creator.testTemplateConnection();
            } catch (SQLException e) {
                System.out.println("The template database for creating topic maps does not work. Cannot test.");
                return;
            }
            try {
                TopicMapCreator topicMapCreator = (TopicMapCreator) context.getBean("topicMapCreator");
                try {
                    topicMapConnectionString = topicMapCreator.createTopicMap("testcreatestore2", "testtm").getConnectionString();
                    testTopicMapConnectionString(topicMapConnectionString);
                } finally {
                    if (creator.isOpenStore("testcreatestore2")) {
                        creator.closeStore("testcreatestore2");
                    }
                    creator.deleteStore("testcreatestore2");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new TestException("Failed to create store");
            } catch (IOException e) {
                e.printStackTrace();
                throw new TestException("Failed to create store");
            }
        }

        public TopicMapRoot getTopicMapRoot() {
            return topicMapRoot;
        }
    }

    public static Test suite() {
        return new TestSuite( MultiassocResourceCopyTest.class );
    }

    public void testRelationsCorrect(TopicMapRoot topicMap) throws TestException, IOException {
        /* Test data */
        String[] assocIds = {"aplha", "charlie"};
        String[] authors = {"En Author", "Tre Author"};

        URL resourcePsi;
        try {
            resourcePsi = new URL("http://psi.mycurriculum.org/test/resource");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        OntopiaResource resource = topicMap.getResource(resourcePsi);
        assertNotNull(resource);
        Collection<OntopiaResourceAimAssociation> assocs = resource.getAimAssociations();
        Collection<String> seen = new ArrayList<String>();
        System.out.println("Multiple associations:");
        for (OntopiaResourceAimAssociation assoc : assocs) {
            System.out.println(" * "+assoc.getHumanId()+" "+assoc.getAuthors()[0]);
            assertNotNull(assoc.getHumanId());
            int i = 0;
            while (i < assocIds.length) {
                if (assocIds[i].equals(assoc.getHumanId())) {
                    break;
                }
                i++;
            }
            assertTrue(i < assocIds.length);
            assertFalse(seen.contains(assoc.getHumanId()));
            seen.add(assoc.getHumanId());
            assertNotNull(assoc.getAuthors());
            assertTrue(assoc.getAuthors().length == 1);
            assertEquals(assoc.getAuthors()[0], authors[i]);
        }
        assertTrue(assocs.size() == 2);
    }
    private DestinationTree dst;
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        super.testTopicMap(topicMap);

        final TopicMapLocator src = getTopicMapLocator();

        /*
         * Create a destination topicmap.
         */
        final boolean[] success = new boolean[1];
        success[0] = false;
        System.out.println("<<===== second tree =====>>");
        dst = new DestinationTree((AbstractApplicationContext) getApplicationContext(), new Runnable() {
            @Override
            public void run() {
                try {
                    dst.copyFrom(src);
                    testRelationsCorrect(dst.getTopicMapRoot());
                    success[0] = true;
                } catch (IOException e) {
                    e.printStackTrace();
                    /* not successful */
                } catch (TestException e) {
                    e.printStackTrace();
                    /* not successful */
                }
            }
        });
        dst.testApp();
        assertTrue(success[0]);
        System.out.println(">>===== second tree =====<<");
    }
}
