package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.data.adapter.ConcurrencyLimiter;
import org.mycurriculum.data.adapter.ConcurrencyLimiterImpl;
import org.springframework.context.ApplicationContext;

import java.util.Date;

public class ConcurrencyLimiterTest extends TestCase {
    private ApplicationContext context;
    private ConcurrencyLimiter limiter;

    public ConcurrencyLimiterTest()
    {

    }
    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ConcurrencyLimiterTest.class );
    }

    public void testApp()
    {
        TestThread[] threads = new TestThread[10];
        long minimumElapsed = 6000;
        long maximumElapsed = 10000;
        long elapsed;
        Date before, after;

        before = new Date();
        System.out.println(before.toString());
        limiter = new ConcurrencyLimiterImpl();
        limiter.setConcurrency(2);
        limiter.setIdlePercentage(50);
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new TestThread("Concurrency testing thread "+i);
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
            }
        }
        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        after = new Date();
        System.out.println(after.toString());
        elapsed = after.getTime() - before.getTime();
        System.out.println("Elapsed time: "+elapsed+" ms (minimum "+minimumElapsed+" ms, maximum "+maximumElapsed+" ms)");
        assertTrue(minimumElapsed <= elapsed);
        assertTrue(maximumElapsed >= elapsed);
    }

    private class TestThread extends Thread
    {
        private String message;

        public TestThread(String message)
        {
            this.message = message;
            setDaemon(true);
            start();
        }

        public void run()
        {
            ConcurrencyLimiter.Handle handle = limiter.acquire();
            System.out.println(message);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(message+" releasing");
            handle.release();
        }
    }
}
