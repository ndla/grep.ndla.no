package org.mycurriculum;

import com.sun.jmx.remote.internal.ArrayQueue;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.topicmapservice.data.TopicMapService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Queue;

public class ResourceStressTest extends TestCase {
    private AbstractApplicationContext context;
    private TopicMapLocator locator;

    public void setApplicationContext(AbstractApplicationContext context) {
        this.context = context;
    }
    public ApplicationContext getApplicationContext() {
        return context;
    }
    public TopicMapLocator getTopicMapLocator() {
        return locator;
    }
    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public ResourceStressTest(String testName)
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ResourceStressTest.class );
    }

    private int total = 0;
    private int inserted = 0;
    private boolean killLoader = false;
    public void testApp() throws TestException {
        context();
        try {
            final String poison = "";
            final Thread[] threads = new Thread[70];
            String connectionString = "db=org.mycurriculum.data.tms.connections.config.PostgresqlConnectionSetupImpl%3Ajdbc%3Apostgresql%3A%2F%2Flocalhost%2Ftestudirimport&tm=postgresql-1";
            TopicMapService topicMapService = (TopicMapService) context.getBean("topicMapLinkService");
            locator = topicMapService.getTopicMapLocator(connectionString);
            TopicMapRoot root = locator.getTopicMapRoot();
            final Collection<OntopiaResource> resources = root.getResources();
            final ArrayQueue<String> psis = new ArrayQueue<String>(500000);
            Thread loader = new Thread() {
                @Override
                public void run() {
                    for (OntopiaResource resource : resources) {
                        psis.add(resource.getPsi().toString());
                        inserted++;
                        if (killLoader)
                            break;
                    }
                    total = inserted;
                    for (int i = 0; i < threads.length; i++) {
                        psis.add(poison);
                    }
                }
            };
            total = resources.size();
            loader.start();
            System.out.println("Waiting 40 seconds...");
            try {
                Thread.sleep(40000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Done waiting.");
            killLoader = true;
            synchronized (psis) {

            }
            System.out.println("Stress loading "+psis.size());
            for (int i = 0; i < threads.length; i++) {
                threads[i] = new Thread() {
                    @Override
                    public void run() {
                        while (true) {
                            String psi;
                            synchronized (psis) {
                                while (psis.size() == 0) {
                                    try {
                                        Thread.sleep(100);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                psi = psis.get(0);
                                psis.remove(0);
                            }
                            if (psi == poison)
                                break;
                            try {
                                TopicMapRoot root = locator.getTopicMapRoot();
                                OntopiaResource resource = root.getResource(new URL(psi));
                            } catch (MalformedURLException e) {
                                throw new RuntimeException(e.getMessage(), e);
                            } catch (IOException e) {
                                throw new RuntimeException(e.getMessage(), e);
                            }
                        }
                    }
                };
                threads[i].start();
            }
            Thread watcher = new Thread() {
                @Override
                public void run() {
                    while (inserted < total || psis.size() > 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.print("\rinserted"+inserted+" of "+total+", remaining " + psis.size());
                    }
                    System.out.println();
                }
            };
            watcher.start();
            for (int i = 0; i < threads.length; i++) {
                try {
                    threads[i].join();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            assertTrue(psis.size() == 0);
        } catch (IOException e) {
            throw new TestException(e.getMessage(), e);
        } finally {
            context.close();
            context = null;
        }
    }
}
