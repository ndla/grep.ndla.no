package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BadRequestBusinessException;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.ConflictBusinessException;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.business.domain.Version;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.domain.OntopiaAim;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.data.tms.domain.OntopiaResourceAimAssociation;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class MultiassocResourceTest extends ResourceTest {
    public static Test suite() {
        return new TestSuite( MultiassocResourceTest.class );
    }

    public JSONObject deleteRelationJson(String assocId, String resourcePsi, String aimId) {
        JSONObject json = new JSONObject();
        JSONArray relations = new JSONArray();
        JSONObject relation = new JSONObject();
        relation.put("humanId", assocId);
        relation.put("resourcePsi", resourcePsi);
        relation.put("competenceAimId", aimId);
        relations.put(relation);
        json.put("relations", relations);
        return json;
    }
    public JSONObject getCreateRelationsJson(String authorName, String authorUrl, String aimId, String resourcePsi, String assocId) {
        JSONObject createRelationsJson = new JSONObject();
        JSONObject author = new JSONObject();
        author.put("name", authorName);
        author.put("url", authorUrl);
        createRelationsJson.put("author", author);
        createRelationsJson.put("subjectMatterId", "not-used");
        JSONArray relations = new JSONArray();
        JSONObject relation = new JSONObject();
        if (assocId != null) {
            relation.put("humanId", assocId);
        }
        relation.put("competenceAimId", aimId);
        relation.put("relationType", "related");
        relation.put("resourcePsi", resourcePsi);
        relation.put("apprenticeRelevance", true);
        relation.put("curriculumSetId", JSONObject.NULL);
        relation.put("level", JSONObject.NULL);
        relation.put("courseId", JSONObject.NULL);
        relation.put("courseType", JSONObject.NULL);
        relation.put("curriculumId", JSONObject.NULL);
        relation.put("competenceAimSetId", JSONObject.NULL);
        relations.put(relation);
        createRelationsJson.put("relations", relations);
        return createRelationsJson;
    }
    public JSONObject getCreateRelationsJson() {
        return getCreateRelationsJson("Test Name", "http://seria.no/", "aim1", "http://psi.mycurriculum.org/test/resource", null);
    }
    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        super.testTopicMap(topicMap);
        BusinessFacade businessFacade = (BusinessFacade) getApplicationContext().getBean("businessFacade");
        final Version version = new Version();
        version.setTopicMapConnectionInfo(getTopicMapConnectionString());
        URL resourcePsi;
        try {
            resourcePsi = new URL("http://psi.mycurriculum.org/test/resource");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        try {
            OntopiaResource resource = topicMap.getResource(resourcePsi);
            assertNotNull(resource);
            OntopiaAim aim = topicMap.getAim("aim1");
            assertNotNull(aim);
            Collection<OntopiaResourceAimAssociation> assocs = resource.getAimAssociations();
            assertTrue(assocs.size() == 1);
            OntopiaResourceAimAssociation oldassoc = resource.buildAimAssociation(aim)
                    .author("Old Test Author")
                    .build();
            oldassoc.getAssociationObject().getTestInterface().createWithOldOntopiaMissingHumanId();
            oldassoc.save();
            businessFacade.createRelations(version, getCreateRelationsJson().toString());
            assocs = resource.getAimAssociations();
            System.out.println("Multiple associations:");
            for (OntopiaResourceAimAssociation assoc : assocs) {
                System.out.println(" * "+assoc.getHumanId()+" "+assoc.getAuthors()[0]);
                assertNotNull(assoc.getHumanId());
            }
            assertTrue(assocs.size() == 3);
            String[] assocIds = new String[3];
            int i = 0;
            for (OntopiaResourceAimAssociation assoc : assocs) {
                assocIds[i] = assoc.getHumanId();
                i++;
            }
            i = 0;
            String[] assocIdsRepl = {"aplha", "bravo", "charlie"};
            for (OntopiaResourceAimAssociation assoc : assocs) {
                assoc.setHumanId(assocIdsRepl[i]);
                assoc.save();
                i++;
            }
            OntopiaResourceAimAssociation[] assocArr = new OntopiaResourceAimAssociation[3];
            for (i = 0; i < assocIds.length; i++) {
                assocArr[i] = topicMap.getResourceAimAssociation(assocIds[i]);
                assertNull(assocArr[i]);
            }
            assocIds = assocIdsRepl;
            for (i = 0; i < assocIds.length; i++) {
                assocArr[i] = topicMap.getResourceAimAssociation(assocIds[i]);
                assertNotNull(assocArr[i]);
                System.out.println(" --- "+assocArr[i].getHumanId());
            }
            String[] authors = {"En Author", "To Author", "Tre Author"};
            JSONObject update1 = getCreateRelationsJson(authors[0], "http://seria.no/", "aim1", "http://psi.mycurriculum.org/test/resource", assocIds[0]);
            JSONObject update2 = getCreateRelationsJson(authors[1], "http://seria.no/2", "aim1", "http://psi.mycurriculum.org/test/resource", assocIds[1]);
            JSONObject update3 = getCreateRelationsJson(authors[2], "http://seria.no/3", "aim1", "http://psi.mycurriculum.org/test/resource", assocIds[2]);
            businessFacade.createRelations(version, update1.toString());
            businessFacade.createRelations(version, update2.toString());
            businessFacade.createRelations(version, update3.toString());
            assocs = resource.getAimAssociations();
            Collection<String> seen = new ArrayList<String>();
            System.out.println("Multiple associations:");
            for (OntopiaResourceAimAssociation assoc : assocs) {
                System.out.println(" * "+assoc.getHumanId()+" "+assoc.getAuthors()[0]);
                assertNotNull(assoc.getHumanId());
                i = 0;
                while (i < assocIds.length) {
                    if (assocIds[i].equals(assoc.getHumanId())) {
                        break;
                    }
                    i++;
                }
                assertTrue(i < assocIds.length);
                assertFalse(seen.contains(assoc.getHumanId()));
                seen.add(assoc.getHumanId());
                assertNotNull(assoc.getAuthors());
                assertTrue(assoc.getAuthors().length == 1);
                assertEquals(assoc.getAuthors()[0], authors[i]);
            }
            assertTrue(assocs.size() == 3);
            System.out.println("Deleting "+assocIds[1]);
            JSONObject delete = deleteRelationJson(assocIds[1], "http://psi.mycurriculum.org/test/resource", "aim1");
            String[] cp = {assocIds[0], assocIds[2]};
            assocIds = cp;
            cp = new String[2];
            cp[0] = authors[0];
            cp[1] = authors[2];
            authors = cp;
            businessFacade.deleteRelations(version, delete.toString());
            assocs = resource.getAimAssociations();
            seen = new ArrayList<String>();
            System.out.println("Multiple associations:");
            for (OntopiaResourceAimAssociation assoc : assocs) {
                System.out.println(" * "+assoc.getHumanId()+" "+assoc.getAuthors()[0]);
                assertNotNull(assoc.getHumanId());
                i = 0;
                while (i < assocIds.length) {
                    if (assocIds[i].equals(assoc.getHumanId())) {
                        break;
                    }
                    i++;
                }
                assertTrue(i < assocIds.length);
                assertFalse(seen.contains(assoc.getHumanId()));
                seen.add(assoc.getHumanId());
                assertNotNull(assoc.getAuthors());
                assertTrue(assoc.getAuthors().length == 1);
                assertEquals(assoc.getAuthors()[0], authors[i]);
            }
            assertTrue(assocs.size() == 2);
        } catch (IOException e) {
            throw new TestException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new TestException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new TestException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new TestException(e.getMessage(), e);
        }
    }
}
