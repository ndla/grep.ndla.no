package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class ResourceTest extends CurriculumTreeTest {
    public final static String testResource = "http://psi.mycurriculum.org/test/resource";

    public static Test suite() {
        return new TestSuite( ResourceTest.class );
    }

    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        super.testTopicMap(topicMap);
        try {
            OntopiaResource resource = topicMap.getResourceBuilder()
                    .psi(testResource)
                    .name("Test resource")
                    .build();
            resource.save();
            System.out.println("Saved resource: "+resource.getName());
            OntopiaAim aim = topicMap.getAim("aim1");
            assertNotNull(aim);
            Iterator<OntopiaCurriculum> curricula = aim.getCurricula().iterator();
            if (!curricula.hasNext())
                throw new TestException("The aim does not belong to a curriculum");
            Iterator<OntopiaCurriculumSet> curriculaSets = curricula.next().getCurriculumSets().iterator();
            if (!curriculaSets.hasNext())
                throw new TestException("The curriculum does not belong to a curricula set");
            OntopiaCurriculumSet curriculaSet = curriculaSets.next();
            System.out.println("Found a parent curricula set: "+curriculaSet.getName());
            /*
             * The intention is to get a level object from an aim:
             */
            Iterator<OntopiaAimSet> aimSets = aim.getAimSets().iterator();
            Iterator<OntopiaLevel> levels;
            while (true) {
                if (!aimSets.hasNext())
                    throw new TestException("No parent aim set");
                OntopiaAimSet aimSet = aimSets.next();
                levels = aimSet.getLevels().iterator();
                if (levels.hasNext())
                    break;
                aimSets = aimSet.getParentAimSets().iterator();
            }
            OntopiaLevel level = levels.next();
            System.out.println("Found a level: "+level.getName());
            /* */
            OntopiaResourceAimAssociation.Builder assocBuilder = resource.buildAimAssociation(aim);
            assocBuilder
                    .author("Test")
                    .timestampDate(new Date())
                    .curriculumSet(curriculaSet)
                    .level(level)
                    .courseId("courseId")
                    .courseType("courseType")
                    .build()
                    .save();
            System.out.println("Saved connection between resource and aim. Testing the existence of the reverse association..");
            Collection<OntopiaResourceAimAssociation> assocs = aim.getResourceAssociations();
            for (OntopiaResourceAimAssociation assocObj : assocs) {
                OntopiaResource assocResource = assocObj.getResource();
                System.out.println(" * Resource: " + assocResource.getName());
                String[] authors = assocObj.getAuthors();
                String author = "(null)";
                if (authors.length == 1)
                    author = authors[0];
                else
                    throw new TestException("Expected one author, got "+authors.length);
                if (!author.equals("Test"))
                    throw new TestException("Expected author to be Test, got "+author);
                System.out.println("Association reifier: "+author);
                OntopiaCurriculumSet assocCurriculumSet = assocObj.getCurriculumSet();
                if (assocCurriculumSet == null)
                    throw new TestException("Expected a curriculum set for the relation");
                System.out.println("  - Relation curriculum set: "+assocCurriculumSet.getName());
                OntopiaLevel assocLevel = assocObj.getLevel();
                if (assocLevel == null)
                    throw new TestException("Expected a level for the relation");
                System.out.println("  - Relation level: "+assocLevel.getName());
                assertEquals(assocObj.getCourseId(), "courseId");
                assertEquals(assocObj.getCourseType(), "courseType");
                System.out.println("  - Relation course id: "+assocObj.getCourseId());
                System.out.println("  - Relation course type: "+assocObj.getCourseType());
            }
            System.out.println("Testing quick methods to walk the associations:");
            for (OntopiaResource assocResource : aim.getResources()) {
                System.out.println(" * "+assocResource.getName());
                for (OntopiaResourceAimAssociation association : assocResource.getAimAssociations()) {
                    OntopiaAim assocAim = association.getAim();
                    System.out.println("  * "+assocAim.getName());
                    Date ts = association.getTimestampDate();
                    System.out.println("  - association metadata object timestamp: "+(ts != null ? ts.toString() : "N/A"));
                }
            }
        } catch (IOException e) {
            throw new TestException("Failed resource test", e);
        }
    }
}
