package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;

public class CurriculumTreeTest extends AddCourseStructureTest {
    public static Test suite() {
        return new TestSuite( CurriculumTreeTest.class );
    }

    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        super.testTopicMap(topicMap);
        try {
            OntopiaCourseStructure courseStructure = topicMap.getCourseStructure(new URL("http://psi.mycurriculum.org/test/coursestructure/test"));
            if (courseStructure == null)
                throw new TestException("Did not find the parent object (courseStructure)");
            OntopiaLevel level = topicMap.getLevelBuilder("level1")
                    .name("Level 1")
                    .build();
            level.save();
            OntopiaAim aim1 = topicMap.getAimBuilder("aim1")
                    .name("Aim 1")
                    .build();
            aim1.save();
            OntopiaAimSet level2AimSet = topicMap.getAimSetBuilder("level2-aim-set")
                    .name("Level 2 aim set")
                    .aim(aim1)
                    .build();
            level2AimSet.save();
            OntopiaAimSet level1AimSet = topicMap.getAimSetBuilder("level1-aim-set")
                    .name("Level 1 aim set")
                    .aimSet(level2AimSet)
                    .build();
            level1AimSet.addLevel(level);
            level1AimSet.save();
            OntopiaCurriculum curriculum = topicMap.getCurriculumBuilder("curriculumTest")
                    .name("Curriculum test")
                    .aimSet(level1AimSet)
                    .level(level)
                    .build();
            curriculum.save();
            OntopiaCurriculumSet curriculumSet = topicMap.getCurriculumSetBuilder("curriculumSetTest")
                    .name("Curriculum set test")
                    .curriculum(curriculum)
                    .build();
            curriculumSet.save();
            courseStructure.addCurriculumSet(curriculumSet);
            courseStructure.save();
        } catch (IOException e) {
            throw new TestException("Tree build failure", e);
        }
        try {
            OntopiaCourseStructure courseStructure = topicMap.getCourseStructure(new URL("http://psi.mycurriculum.org/test/coursestructure/test"));
            if (courseStructure == null)
                throw new TestException("Did not find the parent object (courseStructure)");
            for (OntopiaCurriculumSet curriculumSet : courseStructure.getCurriculumSets()) {
                System.out.println(" - Curriculum set: "+curriculumSet.getName()+" ("+curriculumSet.getObjectId()+")");
                for (OntopiaCurriculum curriculum : curriculumSet.getCurricula()) {
                    System.out.println("  - Curriculum: "+curriculum.getName()+" ("+curriculum.getObjectId()+")");
                    for (OntopiaAimSet aimSet1 : curriculum.getAimSets()) {
                        System.out.println("   - Aim set: "+aimSet1.getName()+" ("+aimSet1.getObjectId()+")");
                        for (OntopiaAimSet aimSet2 : aimSet1.getAimSets()) {
                            System.out.println("    - Aim set: "+aimSet2.getName()+" ("+aimSet2.getObjectId()+")");
                            for (OntopiaAim aim : aimSet2.getAims()) {
                                System.out.println("     - Aim: "+aim.getName()+" ("+aim.getObjectId()+")");
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new TestException("Tree build failure", e);
        }
    }
}
