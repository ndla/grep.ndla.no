package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mycurriculum.data.tms.abstracts.CopyOnWriteSet;

import java.util.*;

public class COWSetTest extends TestCase {
    public COWSetTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        return new TestSuite( COWSetTest.class );
    }

    public void testApp() {
        Set<String> origSet = new HashSet<String>();
        origSet.add("1");
        origSet.add("2");
        origSet.add("3");
        Set<String> cowSet = new CopyOnWriteSet<String>(origSet);
        Iterator<String> cowSetIter = cowSet.iterator();
        boolean deleteItem = false;
        Collection<String> kept = new ArrayList<String>();
        Collection<String> deleted = new ArrayList<String>();
        while (cowSetIter.hasNext()) {
            String item = cowSetIter.next();
            if (deleteItem) {
                cowSetIter.remove();
                System.out.println("Removed "+item);
                deleted.add(item);
            } else {
                System.out.println("Kept "+item);
                kept.add(item);
            }
            deleteItem = !deleteItem;
        }
        System.out.println("Cow set:");
        for (String item : cowSet) {
            System.out.println(" * "+item);
        }
        for (String item : kept) {
            assertTrue(cowSet.contains(item));
        }
        for (String item : deleted) {
            assertFalse(cowSet.contains(item));
        }
    }
}
