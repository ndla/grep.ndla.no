package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.MutableTranslatedText;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Map;

public class ResourceBug20140911Test extends CurriculumTreeTest {
    public static Test suite() {
        return new TestSuite( ResourceBug20140911Test.class );
    }

    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        super.testTopicMap(topicMap);
        try {
            String psi = "http://psi.mycurriculum.org/test/resource";
            OntopiaResource resource = topicMap.getResourceBuilder()
                    .psi(psi)
                    .nodeType("ndla-node")
                    .build();
            resource.setName(new Locale("und"), "H\u00f8na eller egget - hvem har det verst?");
            MutableTranslatedText ingress = resource.getIngress();
            Locale und = new Locale("und");
            ingress.addText(und, "");
            ingress.setDefaultLocale(und);
            resource.setIngress(ingress);
            try {
                resource.save();
            } catch (NullPointerException nullPtrEx) {
                System.out.println("NullPointerException bug encountered: " + nullPtrEx.getMessage());
                if (nullPtrEx.getMessage().equals("null is not a valid argument.")) {
                    System.out.println("Identified as the 20140911 save bug");
                    try {
                        resource = topicMap.getResource(new URL(psi));
                        if (resource == null)
                            System.out.println("The resource was not found as expected after a failed save");
                        else
                            throw new TestException("The resource was found after a failed save", nullPtrEx);
                    } catch (RuntimeException runtimeException) {
                        System.out.println("RuntimeException bug encountered: " + runtimeException.getMessage());
                        if (runtimeException.getMessage().equals("An expected-to-exist topic was not found when needed")) {
                            System.out.println("Identified as the 20140911 inconsitent cache bug");
                            throw new TestException("RuntimeException following a NullPointerException bug", runtimeException);
                        }
                        throw new TestException("Unknown RuntimeException following a NullPointerException bug", runtimeException);
                    }
                    throw new TestException("NullPointerException without the inconsitent cache", nullPtrEx);
                }
                throw new TestException("NullPointerException not identified", nullPtrEx);
            }
            System.out.println("Saved resource: "+resource.getName());
            resource = topicMap.getResource(new URL(psi));
            System.out.println("Got the resource: " + resource.getName());
            ingress = resource.getIngress();
            for (Map.Entry<Locale,String> entry : ingress.getTextMap().entrySet()) {
                System.out.println("Ingress: "+entry.getKey().getISO3Language()+": "+entry.getValue());
            }
        } catch (IOException e) {
            throw new TestException("Failed resource test", e);
        }
    }
}
