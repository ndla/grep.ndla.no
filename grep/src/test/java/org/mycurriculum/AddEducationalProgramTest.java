package org.mycurriculum;

import junit.framework.Test;
import junit.framework.TestSuite;
import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;

public class AddEducationalProgramTest extends TopicMapCreatorTest {
    public static Test suite() {
        return new TestSuite( AddEducationalProgramTest.class );
    }

    @Override
    public void testTopicMap(TopicMapRoot topicMap) throws TestException {
        try {
            URL psi = new URL("http://psi.mycurriculum.org/test/eduprog/test");
            OntopiaCourseStructure.Builder builder = topicMap.getCourseStructureBuilder();
            builder.name("Test edu")
                   .psi(psi.toString())
                   .name("Test edu", new Locale("nob"));
            builder.build().save();
            OntopiaCourseStructure eduProg = topicMap.getCourseStructure(psi);
            System.out.println("Found created object: "+eduProg.getName());
        } catch (IOException e) {
            e.printStackTrace();
            throw new TestException("IO-error during testing of creating topic: "+e.getMessage());
        }
    }
}
