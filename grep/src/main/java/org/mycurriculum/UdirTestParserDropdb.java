package org.mycurriculum;

import org.mycurriculum.data.topicmapservice.ParserService;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.sql.SQLException;

public class UdirTestParserDropdb {
    AbstractApplicationContext context;

    public static void main(String[] args) {
        UdirTestParserDropdb test = new UdirTestParserDropdb();
        test.context();
        try {
            test.execute();
        } finally {
            test.destroyContext();
        }
    }

    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public void destroyContext() {
        context.close();
    }
    public void execute() {
        ParserService parserService = (ParserService) context.getBean("parserService");
        String connectionString = "db=org.mycurriculum.data.tms.connections.config.PostgresqlConnectionSetupImpl%3Ajdbc%3Apostgresql%3A%2F%2Flocalhost%2Ftestudirimport&tm=postgresql-1";
        try {
            parserService.deleteTopicmapDatabase(connectionString);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }
}
