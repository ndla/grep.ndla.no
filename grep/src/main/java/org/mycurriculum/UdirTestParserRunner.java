package org.mycurriculum;

import org.mycurriculum.data.topicmapservice.ParserService;
import org.mycurriculum.data.util.TranslatedText;
import org.mycurriculum.data.util.parser.MultithreadedTextProgressOutputter;
import org.mycurriculum.data.util.parser.Parser;
import org.mycurriculum.data.util.parser.ProgressTracker;
import org.mycurriculum.data.util.parser.Report;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public class UdirTestParserRunner {
    AbstractApplicationContext context;

    public static void main(String[] args) {
        UdirTestParserRunner test = new UdirTestParserRunner();
        test.context();
        try {
            test.execute();
        } finally {
            test.destroyContext();
        }
    }

    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public void destroyContext() {
        context.close();
    }
    public void execute() {
        ParserService parserService = (ParserService) context.getBean("parserService");
        String resourcesConnectionString = "db=org.mycurriculum.data.tms.connections.config.PostgresqlConnectionSetupImpl%3Ajdbc%3Apostgresql%3A%2F%2Flocalhost%2Fresourcestest&tm=postgresql-1";
        String connectionString = "db=org.mycurriculum.data.tms.connections.config.PostgresqlConnectionSetupImpl%3Ajdbc%3Apostgresql%3A%2F%2Flocalhost%2Ftestudirimport&tm=postgresql-1";
        try {
            Parser.Context parserContext;
            if (parserService.topicmapExists(resourcesConnectionString)) {
                parserContext = parserService.importTopicmap(resourcesConnectionString, connectionString);
            } else {
                parserContext = parserService.importTopicmap(connectionString);
            }
            final ProgressTracker progressTracker = parserContext.getProgressTracker();
            progressTracker.addListener(new MultithreadedTextProgressOutputter(progressTracker));
            parserContext.waitForCompletion();
            System.out.println();
            System.out.println("Parsing completed");
            Report report = parserContext.getReport();
            if (report.getAimsAssociationsReport() != null) {
                Map<String,Collection<String>> missingAims = report.getAimsAssociationsReport().getAimsNotFound();
                if (missingAims.size() > 0) {
                    System.out.println("Missing aims in the new import:");
                    for (Map.Entry<String, Collection<String>> entry : missingAims.entrySet()) {
                        String aimId = entry.getKey();
                        Collection<String> resources = entry.getValue();
                        TranslatedText translatedName = report.getAimsAssociationsReport().getAimName(aimId);
                        String name = translatedName.getDefaultText();
                        if (name == null)
                            name = "No name (null)";
                        System.out.println(" * Aim "+aimId+" ("+name+") is missing:");
                        for (String resource : resources) {
                            System.out.println("   - Connected to resource: "+resource);
                        }
                    }
                }
                Map<String,Collection<String>> missingCurriculumSets = report.getAimsAssociationsReport().getCurriculumSetsNotFound();
                if (missingCurriculumSets.size() > 0) {
                    System.out.println("Missing curriculum sets in the new import:");
                    for (Map.Entry<String, Collection<String>> entry : missingCurriculumSets.entrySet()) {
                        String curriculumSetId = entry.getKey();
                        Collection<String> resources = entry.getValue();
                        TranslatedText translatedName = report.getAimsAssociationsReport().getCurriculumSetName(curriculumSetId);
                        String name = translatedName.getDefaultText();
                        if (name == null)
                            name = "No name (null)";
                        System.out.println(" * Curriculum set "+curriculumSetId+" ("+name+") is missing:");
                        for (String resource : resources) {
                            System.out.println("   - Connected to resource: "+resource);
                        }
                    }
                }
                Map<String,Collection<String>> missingLevels = report.getAimsAssociationsReport().getLevelsNotFound();
                if (missingLevels.size() > 0) {
                    System.out.println("Missing levels in the new import:");
                    for (Map.Entry<String, Collection<String>> entry : missingLevels.entrySet()) {
                        String levelId = entry.getKey();
                        Collection<String> resources = entry.getValue();
                        TranslatedText translatedName = report.getAimsAssociationsReport().getLevelName(levelId);
                        String name = translatedName.getDefaultText();
                        if (name == null)
                            name = "No name (null)";
                        System.out.println(" * Level "+levelId+" ("+name+") is missing:");
                        for (String resource : resources) {
                            System.out.println("   - Connected to resource: "+resource);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO error: "+e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Interrupted");
        }
    }

    private abstract class ProgressListener implements ProgressTracker.Listener {
    }
}
