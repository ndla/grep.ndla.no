package org.mycurriculum.data.topicmapservice.data;

import org.mycurriculum.business.domain.Course;
import org.mycurriculum.business.domain.CourseGroup;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.entityservice.CourseGroupService;
import org.mycurriculum.data.entityservice.CourseService;
import org.mycurriculum.data.tms.TopicMapCreator;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.topicmapservice.TopicMapLinkService;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.data.util.parser.Parser;
import org.mycurriculum.data.util.parser.ParserFactory;
import org.mycurriculum.data.util.searchindexer.SearchIndexerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

public class TopicMapServiceImpl implements TopicMapService {
    private ParserFactory parserFactory;
    private TopicMapCreator topicMapCreator;

    @Autowired
    private SearchIndexerFactory searchIndexerFactory;

    @Autowired
    private CourseGroupService courseGroupService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CacheDao cacheDao;

    public TopicMapServiceImpl(ParserFactory parserFactory, TopicMapCreator topicMapCreator) {
        this.parserFactory = parserFactory;
        this.topicMapCreator = topicMapCreator;
    }

    @Override
    public TopicMapLocator getTopicMapLocator(String connectionString) throws IOException {
        return topicMapCreator.getTopicMapFromConnectionString(connectionString);
    }

    @Override
    public String createTopicmap(String dbName, String tmName) throws IOException, SQLException {
        TopicMapLocator locator = topicMapCreator.createTopicMap(dbName, tmName);
        String connectionString = null;
        try {
            connectionString = locator.getConnectionString();
        } finally {
            if (connectionString == null) {
                try {
                    topicMapCreator.deleteStore(dbName);
                } catch (Exception e) {
                }
            }
        }
        return connectionString;
    }

    @Override
    public boolean topicmapExists(String connectionString) {
        try {
            return (getTopicMapLocator(connectionString).getTopicMapRoot().getTopicMap() != null);
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public Parser.Context importTopicmap(String connectionString) throws IOException {
        TopicMapLocator locator;
        locator = getTopicMapLocator(connectionString);
        Parser parser = parserFactory.createParser(locator);
        return parser.parse();
    }

    @Override
    public Parser.Context importTopicmap(String resourcesConnectionString, String connectionString) throws IOException {
        TopicMapLocator resourcesLocator, locator;
        resourcesLocator = getTopicMapLocator(resourcesConnectionString);
        locator = getTopicMapLocator(connectionString);
        Parser parser = parserFactory.createParser(resourcesLocator, locator);
        return parser.parse();
    }

    @Override
    public void mapCourseGroups(long version, String connectionString, Map<String, Collection<String>> courseToCourseStructure) {
        Map<String,Course> courseMap;

        for (Map.Entry<String,Collection<String>> entr : courseToCourseStructure.entrySet()) {
            String courseExternalId = entr.getKey();
            Collection<String> courseStructurePsisMaster = entr.getValue();
            List<String> courseStructurePsis = new ArrayList<String>();
            for (String psi : courseStructurePsisMaster) {
                if (!courseStructurePsis.contains(psi))
                    courseStructurePsis.add(psi);
            }
            Course course = courseService.findByExternalId(courseExternalId, version);
            for (CourseGroup courseGroup : course.getCourseGroups()) {
                String psi = courseGroup.getPsi();
                if (courseStructurePsis.contains(psi))
                    courseStructurePsis.remove(psi);
            }
            for (String courseStructurePsi : courseStructurePsis) {
                CourseGroup courseGroup = new CourseGroup();
                courseGroup.setCourse(course);
                courseGroup.setPsi(courseStructurePsi);
                courseGroupService.save(courseGroup);
            }
        }
    }

    @Override
    public void deleteTopicmapDatabase(String connectionString) throws IOException, SQLException {
        topicMapCreator.deleteStoreByConnectionString(connectionString);
    }

    public CourseGroupService getCourseGroupService() {
        return courseGroupService;
    }

    public void setCourseGroupService(CourseGroupService courseGroupService) {
        this.courseGroupService = courseGroupService;
    }

    public CourseService getCourseService() {
        return courseService;
    }

    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }

    public CacheDao getCacheDao() {
        return cacheDao;
    }

    public void setCacheDao(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }

    private static class TopicMapHandle implements TopicMapService.TopicMapHandle {
        public TopicMapLocator locator;

        @Override
        public String getConnectionString() throws IOException {
            return locator.getConnectionString();
        }
    }

    @Override
    public TopicMapHandle getTopicMapHandle(String connectionString) throws IOException {
        TopicMapHandle handle = new TopicMapHandle();
        handle.locator = getTopicMapLocator(connectionString);
        return handle;
    }

    @Override
    public Collection<OntopiaCourseStructure> getCourseStructures(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCourseStructures();
    }

    @Override
    public Collection<OntopiaCurriculumSet> getCurriculumSets(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurriculumSets();
    }

    @Override
    public Collection<OntopiaCurriculum> getCurricula(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurricula();
    }

    @Override
    public Collection<OntopiaAimSet> getAimSets(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAimSets();
    }

    @Override
    public Collection<OntopiaAim> getAims(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAims();
    }

    @Override
    public Collection<OntopiaResource> getResources(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getResources();
    }

    @Override
    public Collection<OntopiaLevel> getLevels(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getLevels();
    }

    @Override
    public OntopiaCourseStructure getCourseStructure(TopicMapLinkService.TopicMapHandle handleIn, String psi) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCourseStructure(new URL(psi));
    }

    @Override
    public OntopiaCurriculumSet getCurriculumSet(TopicMapLinkService.TopicMapHandle handleIn, String psi) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurriculumSet(new URL(psi));
    }

    @Override
    public OntopiaCurriculum getCurriculum(TopicMapLinkService.TopicMapHandle handleIn, String psi) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurriculum(new URL(psi));
    }

    @Override
    public OntopiaAimSet getAimSet(TopicMapLinkService.TopicMapHandle handleIn, String psi) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAimSet(new URL(psi));
    }

    @Override
    public OntopiaAim getAim(TopicMapLinkService.TopicMapHandle handleIn, String psi) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAim(new URL(psi));
    }

    @Override
    public OntopiaResource getResource(TopicMapLinkService.TopicMapHandle handleIn, String psi) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getResource(new URL(psi));
    }

    @Override
    public OntopiaLevel getLevel(TopicMapLinkService.TopicMapHandle handleIn, String psi) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getLevel(new URL(psi));
    }

    @Override
    public OntopiaCourseStructure getCourseStructureByHumanId(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCourseStructure(humanId);
    }

    @Override
    public OntopiaCurriculumSet getCurriculumSetByHumanId(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurriculumSet(humanId);
    }

    @Override
    public OntopiaCurriculum getCurriculumByHumanId(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurriculum(humanId);
    }

    @Override
    public OntopiaAimSet getAimSetByHumanId(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAimSet(humanId);
    }

    @Override
    public OntopiaAim getAimByHumanId(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAim(humanId);
    }

    @Override
    public OntopiaResource getResourceByHumanId(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getResource(humanId);
    }

    @Override
    public OntopiaLevel getLevelByHumanId(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getLevel(humanId);
    }

    @Override
    public OntopiaCourseStructure.Builder courseStructureBuilder(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCourseStructureBuilder();
    }

    @Override
    public OntopiaCurriculumSet.Builder curriculumSetBuilder(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurriculumSetBuilder();
    }

    @Override
    public OntopiaCurriculum.Builder curriculumBuilder(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getCurriculumBuilder();
    }

    @Override
    public OntopiaAimSet.Builder aimSetBuilder(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAimSetBuilder();
    }

    @Override
    public OntopiaAim.Builder aimBuilder(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getAimBuilder();
    }

    @Override
    public OntopiaResource.Builder resourceBuilder(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getResourceBuilder();
    }

    @Override
    public OntopiaLevel.Builder levelBuilder(TopicMapLinkService.TopicMapHandle handleIn) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getLevelBuilder();
    }

    @Override
    public OntopiaResourceAimAssociation createResourceAimsAssociation(OntopiaResource resource, OntopiaAim aim) throws IOException {
        return new OntopiaResourceAimAssociationImpl(resource, aim);
    }

    @Override
    public OntopiaResourceAimAssociation getResourceAimsAssociation(TopicMapLinkService.TopicMapHandle handleIn, String humanId) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return handle.locator.getTopicMapRoot().getResourceAimAssociation(humanId);
    }

    @Override
    public Set<String> getSearchIndexerTypes() {
        return searchIndexerFactory.getSearchIndexerTypes();
    }

    @Override
    public AsyncTask createSearchIndexer(TopicMapLinkService.TopicMapHandle handleIn, String type) throws IOException {
        TopicMapHandle handle = (TopicMapHandle) handleIn;
        return searchIndexerFactory.createSearchIndexer(handle.locator, type);
    }

    @Override
    public Parser.Context createGrepImporter(TopicMapLinkService.TopicMapHandle resourcesSourceIn, TopicMapLinkService.TopicMapHandle destinationIn) throws IOException {
        TopicMapHandle resourcesSource = (TopicMapHandle) resourcesSourceIn;
        TopicMapHandle destination = (TopicMapHandle) destinationIn;
        String resourcesConnectionString = resourcesSource.locator.getConnectionString();
        String destinationConnectionString = destination.locator.getConnectionString();
        return importTopicmap(resourcesConnectionString, destinationConnectionString);
    }

    @Override
    public Parser.Context createGrepImporter(TopicMapLinkService.TopicMapHandle destinationIn) throws IOException {
        TopicMapHandle destination = (TopicMapHandle) destinationIn;
        String destinationConnectionString = destination.locator.getConnectionString();
        return importTopicmap(destinationConnectionString);
    }

    private Collection<OntopiaAim> getAimsByHumanIds(TopicMapRoot topicMapRoot, Collection<String> humanIds) throws IOException {
        Collection<OntopiaAim> aims = new ArrayList<OntopiaAim>(humanIds.size());
        for (String humanId : humanIds) {
            OntopiaAim aim = topicMapRoot.getAim(humanId);
            if (aim == null)
                throw new IOException("Competence aim with human-id "+humanId+" was not found.");
            aims.add(aim);
        }
        return aims;
    }
}
