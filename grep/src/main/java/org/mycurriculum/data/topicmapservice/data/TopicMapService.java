package org.mycurriculum.data.topicmapservice.data;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.topicmapservice.ParserService;
import org.mycurriculum.data.topicmapservice.TopicMapLinkService;

import java.io.IOException;

public interface TopicMapService extends ParserService, TopicMapLinkService {
    public TopicMapLocator getTopicMapLocator(String connectionString) throws IOException;
}
