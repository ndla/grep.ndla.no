package org.mycurriculum.data.topicmapservice;

import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.data.util.parser.Parser;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

public interface TopicMapLinkService {
    public TopicMapHandle getTopicMapHandle(String connectionString) throws IOException;
    public Collection<OntopiaCourseStructure> getCourseStructures(TopicMapHandle handle) throws IOException;
    public Collection<OntopiaCurriculumSet> getCurriculumSets(TopicMapHandle handle) throws IOException;
    public Collection<OntopiaCurriculum> getCurricula(TopicMapHandle handle) throws IOException;
    public Collection<OntopiaAimSet> getAimSets(TopicMapHandle handle) throws IOException;
    public Collection<OntopiaAim> getAims(TopicMapHandle handle) throws IOException;
    public Collection<OntopiaResource> getResources(TopicMapHandle handle) throws IOException;
    public Collection<OntopiaLevel> getLevels(TopicMapHandle handle) throws IOException;

    /* By psi */
    public OntopiaCourseStructure getCourseStructure(TopicMapHandle handle, String psi) throws IOException;
    public OntopiaCurriculumSet getCurriculumSet(TopicMapHandle handle, String psi) throws IOException;
    public OntopiaCurriculum getCurriculum(TopicMapHandle handle, String psi) throws IOException;
    public OntopiaAimSet getAimSet(TopicMapHandle handle, String psi) throws IOException;
    public OntopiaAim getAim(TopicMapHandle handle, String psi) throws IOException;
    public OntopiaResource getResource(TopicMapHandle handle, String psi) throws IOException;
    public OntopiaLevel getLevel(TopicMapHandle handle, String psi) throws IOException;

    /* By humanId */
    public OntopiaCourseStructure getCourseStructureByHumanId(TopicMapHandle handle, String humanId) throws IOException;
    public OntopiaCurriculumSet getCurriculumSetByHumanId(TopicMapHandle handle, String humanId) throws IOException;
    public OntopiaCurriculum getCurriculumByHumanId(TopicMapHandle handle, String humanId) throws IOException;
    public OntopiaAimSet getAimSetByHumanId(TopicMapHandle handle, String humanId) throws IOException;
    public OntopiaAim getAimByHumanId(TopicMapHandle handle, String humanId) throws IOException;
    public OntopiaResource getResourceByHumanId(TopicMapHandle handle, String humanId) throws IOException;
    public OntopiaLevel getLevelByHumanId(TopicMapHandle handle, String humanId) throws IOException;

    /* builders */
    public OntopiaCourseStructure.Builder courseStructureBuilder(TopicMapHandle handle) throws IOException;
    public OntopiaCurriculumSet.Builder curriculumSetBuilder(TopicMapHandle handle) throws IOException;
    public OntopiaCurriculum.Builder curriculumBuilder(TopicMapHandle handle) throws IOException;
    public OntopiaAimSet.Builder aimSetBuilder(TopicMapHandle handle) throws IOException;
    public OntopiaAim.Builder aimBuilder(TopicMapHandle handle) throws IOException;
    public OntopiaResource.Builder resourceBuilder(TopicMapHandle handle) throws IOException;
    public OntopiaLevel.Builder levelBuilder(TopicMapHandle handle) throws IOException;

    /* resource - aim - associations */
    public OntopiaResourceAimAssociation createResourceAimsAssociation(OntopiaResource resource, OntopiaAim aim) throws IOException;
    public OntopiaResourceAimAssociation getResourceAimsAssociation(TopicMapHandle handle, String humanId) throws IOException;

    public Set<String> getSearchIndexerTypes();
    public AsyncTask createSearchIndexer(TopicMapHandle handle, String type) throws IOException;

    public Parser.Context createGrepImporter(TopicMapHandle resourcesSource, TopicMapHandle destination) throws IOException;
    public Parser.Context createGrepImporter(TopicMapHandle destination) throws IOException;

    public interface TopicMapHandle {
        public String getConnectionString() throws IOException;
    }
}
