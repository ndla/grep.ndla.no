package org.mycurriculum.data.topicmapservice;

import org.mycurriculum.data.util.parser.Parser;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

/**
 * This is implemented by an data.TopicMapService
 */
public interface ParserService {
    /**
     *
     * @param dbName
     * @param tmName
     * @return Topicmap connection string
     */
    public String createTopicmap(String dbName, String tmName) throws IOException, SQLException;

    public boolean topicmapExists(String connectionString);
    public Parser.Context importTopicmap(String connectionString) throws IOException;
    public Parser.Context importTopicmap(String resourcesConnectionString, String connectionString) throws IOException;

    /**
     * Add mapping course to course-structure (topicmap)
     * @param connectionString
     * @param courseToCourseStructure Course external id (string) to course structure psi (string) map.
     */
    public void mapCourseGroups(long version, String connectionString, Map<String,Collection<String>> courseToCourseStructure);

    public void deleteTopicmapDatabase(String connectionString) throws IOException, SQLException;
}
