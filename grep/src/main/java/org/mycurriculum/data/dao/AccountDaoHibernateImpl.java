package org.mycurriculum.data.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.mycurriculum.business.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("accountDaoHibernateImpl")
public class AccountDaoHibernateImpl implements AccountDao {

    @Autowired
    @Qualifier("securitySessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public void save(Account account) {
        currentSession().save(account);
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Account find(Long id) {
        Account result = (Account) currentSession().get(Account.class, id);
        return result;
    }

    @Override
    public Account findByName(String name) {
        Query query = currentSession().getNamedQuery("findAccountByName");
        query.setParameter("name", name);
        return (Account) query.uniqueResult();
    }

    @Override
    public List<Account> list() {
        List result = currentSession().createQuery("from Account").list();
        return result;
    }

    @Override
    public void update(Account account) {
        currentSession().update(account);
    }

    @Override
    public void delete(Account account) {
        currentSession().delete(account);
    }
}
