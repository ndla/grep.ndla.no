package org.mycurriculum.data.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDetailsDaoJdbcImpl implements UserDetailsDao {

    private static final String FIND_PASSWORD_SQL = "select password from \"user\" where username = ?";
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public String findPasswordByUsername(String username) {
        return jdbcTemplate.queryForObject(FIND_PASSWORD_SQL, new Object[]{username}, String.class);
    }
}
