package org.mycurriculum.data.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.mycurriculum.business.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDaoHibernateImpl")
public class UserDaoHibernateImpl implements UserDao {

    @Autowired
    @Qualifier("securitySessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public void save(User user) {
        currentSession().save(user);
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public User find(Long id) {
        User result = (User) currentSession().get(User.class, id);
        return result;
    }

    @Override
    public User findByUsername(String username) {
        Query query = currentSession().getNamedQuery("findUserByUsername");
        query.setParameter("username", username);
        return (User) query.uniqueResult();
    }

    @Override
    public List<User> list() {
        List result = currentSession().createQuery("from User").list();
        return result;
    }

    @Override
    public void update(User user) {
        currentSession().update(user);
    }

    @Override
    public void delete(User user) {
        currentSession().delete(user);
    }
}
