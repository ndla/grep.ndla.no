package org.mycurriculum.data.dao;

import org.mycurriculum.business.domain.EducationGroup;

import java.util.List;

public interface EducationGroupDao {
    public void save(EducationGroup educationGroup);

    public EducationGroup find(Long id);

    public EducationGroup findByExternalId(String externalId);

    public List<EducationGroup> list();

    public void update(EducationGroup educationGroup);

    public void delete(EducationGroup educationGroup);
}
