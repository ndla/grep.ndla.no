package org.mycurriculum.data.dao;

import org.mycurriculum.business.domain.Version;

import java.util.List;

public interface VersionDao {
    public void save(Version version);

    public Version find(Long id);

    public Version findVersionByVersionNumber(Long versionNumber);

    public List list();

    public void update(Version version);

    public void delete(Version version);

    public Version findActive();

    public void setActive(Version version);
}
