package org.mycurriculum.data.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.mycurriculum.business.domain.CourseGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("courseGroupDaoHibernateImpl")
public class CourseGroupDaoHibernateImpl implements CourseGroupDao {

    @Autowired
    @Qualifier("businessSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public void save(CourseGroup courseGroup) {
        currentSession().save(courseGroup);
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public CourseGroup find(Long id) {
        CourseGroup result = (CourseGroup) currentSession().get(CourseGroup.class, id);
        return result;
    }

    @Override
    public CourseGroup findCourseGroupByPsi(String psi, Long versionNumber) {
        Query query = currentSession().getNamedQuery("findCourseGroupByPsi");

        query.setParameter("psi", psi);
        query.setParameter("versionNumber", versionNumber);
        Object[] result = (Object[]) query.uniqueResult();
        CourseGroup courseGroup = null;
        if (result.length > 0 && result[0] instanceof CourseGroup) {
            courseGroup = (CourseGroup) result[0];
        }
        return courseGroup;
    }

    @Override
    public List<CourseGroup> list() {
        List result = currentSession().createQuery("from CourseGroup").list();
        return result;
    }

    @Override
    public void update(CourseGroup courseGroup) {
        currentSession().update(courseGroup);
    }

    @Override
    public void delete(CourseGroup courseGroup) {
        currentSession().delete(courseGroup);
    }
}
