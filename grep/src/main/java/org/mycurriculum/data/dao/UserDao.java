package org.mycurriculum.data.dao;

import org.mycurriculum.business.domain.User;

import java.util.List;

public interface UserDao {
    public void save(User user);

    public User find(Long id);

    public User findByUsername(String username);

    public List<User> list();

    public void update(User user);

    public void delete(User user);
}
