package org.mycurriculum.data.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.mycurriculum.business.domain.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("versionDaoHibernateImpl")
public class VersionDaoHibernateImpl implements VersionDao {

    //private Logger logger = LoggerFactory.getLogger(VersionDaoHibernateImpl.class);

    @Autowired
    @Qualifier("businessSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public void save(Version version) {
        currentSession().save(version);
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Version find(Long id) {
        Version result = (Version) currentSession().get(Version.class, id);
        return result;
    }

    @Override
    public Version findVersionByVersionNumber(Long versionNumber) {
        Query query = currentSession().getNamedQuery("findVersionByVersionNumber");
        query.setParameter("versionNumber", versionNumber);
        return (Version) query.uniqueResult();
    }

    @Override
    public List<Version> list() {
        List<Version> result = currentSession().createQuery("from Version").list();
        return result;
    }

    @Override
    public void update(Version version) {
        currentSession().update(version);
    }

    @Override
    public void delete(Version version) {
        currentSession().delete(version);
    }

    @Override
    public Version findActive() {
        Query query = currentSession().getNamedQuery("findActiveVersion");
        return (Version) query.uniqueResult();
    }

    @Override
    public void setActive(Version version) {
        /*
        Query query = currentSession().getNamedQuery("findActiveVersion");
        Version sourceVersion = (Version) query.uniqueResult();
        if (sourceVersion != null) {
            sourceVersion.setActive(false);
            currentSession().update(sourceVersion);
        }
        */
        Query query = currentSession().getNamedQuery("deactivateAllVersions");
        query.executeUpdate();
        version.setActive(true); // Destination version.
        currentSession().update(version);
    }
}
