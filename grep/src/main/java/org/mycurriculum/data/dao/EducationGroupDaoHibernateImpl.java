package org.mycurriculum.data.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.mycurriculum.business.domain.EducationGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("educationGroupDaoHibernateImpl")
public class EducationGroupDaoHibernateImpl implements EducationGroupDao {

    @Autowired
    @Qualifier("businessSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public void save(EducationGroup educationGroup) {
        currentSession().save(educationGroup);
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public EducationGroup find(Long id) {
        EducationGroup result = (EducationGroup) currentSession().get(EducationGroup.class, id);
        return result;
    }

    @Override
    public EducationGroup findByExternalId(String externalId) {
        Query query = currentSession().getNamedQuery("findEducationGroupByExternalId");

        query.setParameter("externalId", externalId);
        return (EducationGroup) query.uniqueResult();
    }

    @Override
    public List<EducationGroup> list() {
        List result = currentSession().createQuery("from EducationGroup").list();
        return result;
    }

    @Override
    public void update(EducationGroup educationGroup) {
        currentSession().update(educationGroup);
    }

    @Override
    public void delete(EducationGroup educationGroup) {
        currentSession().delete(educationGroup);
    }
}
