package org.mycurriculum.data.dao;

import org.mycurriculum.business.domain.Account;

import java.util.List;

public interface AccountDao {
    public void save(Account account);

    public Account find(Long id);

    public Account findByName(String name);

    public List<Account> list();

    public void update(Account account);

    public void delete(Account account);
}
