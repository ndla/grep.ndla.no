package org.mycurriculum.data.dao;

import org.mycurriculum.business.domain.CourseGroup;

import java.util.List;

public interface CourseGroupDao {
    public void save(CourseGroup courseGroup);

    public CourseGroup find(Long id);

    public CourseGroup findCourseGroupByPsi(String psi, Long versionNumber);

    public List<CourseGroup> list();

    public void update(CourseGroup courseGroup);

    public void delete(CourseGroup courseGroup);
}
