/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 13, 2014
 */

package org.mycurriculum.data.dao;

import org.hibernate.Filter;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.mycurriculum.business.domain.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("courseDaoHibernateImpl")
public class CourseDaoHibernateImpl implements CourseDao {

    @Autowired
    @Qualifier("businessSessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public void save(Course course) {
        currentSession().save(course);
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    /*
    @Override
    public void create(Course course) {
        try {
            currentSession().beginTransaction();
            currentSession().save(course);
            currentSession().getTransaction().commit();
        } catch (RuntimeException e) {
            currentSession().getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            currentSession().close();
        }
    }
    */

    @Override
    public Course find(Long id) {
        Course result = (Course) currentSession().get(Course.class, id);
        return result;
    }

    @Override
    public Course findByExternalId(String externalId, Long version) {
        Query query = currentSession().getNamedQuery("findCourseByExternalId");

        Filter filter = currentSession().enableFilter("courseVersionNumberFilter");
        filter.setParameter("courseVersionNumberFilterCode", version);

        query.setParameter("externalId", externalId);
        return (Course) query.uniqueResult();
    }

    @Override
    public List<Course> list(Long version) {
        Filter filter = currentSession().enableFilter("courseVersionNumberFilter");
        filter.setParameter("courseVersionNumberFilterCode", version);
        List result = currentSession().createQuery("from Course").list();
        return result;
    }

    @Override
    public void update(Course course) {
        currentSession().update(course);
    }

    @Override
    public void delete(Course course) {
        currentSession().delete(course);
    }
}
