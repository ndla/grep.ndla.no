package org.mycurriculum.data.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.mycurriculum.business.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("roleDaoHibernateImpl")
public class RoleDaoHibernateImpl implements RoleDao {

    @Autowired
    @Qualifier("securitySessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public void save(Role role) {
        currentSession().save(role);
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Role find(Long id) {
        Role result = (Role) currentSession().get(Role.class, id);
        return result;
    }

    @Override
    public Role findByName(String name) {
        Query query = currentSession().getNamedQuery("findRoleByName");
        query.setParameter("name", name);
        return (Role) query.uniqueResult();
    }

    @Override
    public List<Role> list() {
        List result = currentSession().createQuery("from Role").list();
        return result;
    }

    @Override
    public void update(Role role) {
        currentSession().update(role);
    }

    @Override
    public void delete(Role role) {
        currentSession().delete(role);
    }
}
