package org.mycurriculum.data.dao;

public interface UserDetailsDao {
    String findPasswordByUsername(String username);
}
