package org.mycurriculum.data.dao;

import org.mycurriculum.business.domain.Role;

import java.util.List;

public interface RoleDao {
    public void save(Role role);

    Role find(Long id);

    Role findByName(String name);

    public List<Role> list();

    public void update(Role role);

    public void delete(Role role);
}
