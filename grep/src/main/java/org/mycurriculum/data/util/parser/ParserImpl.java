package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.*;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.util.multicore.TaskUnitRunner;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ParserImpl implements Parser {
    private UdirFactory srcFactory;
    private UdirRoot src;
    private TopicMapLocator resourcesSrc;
    private TopicMapLocator dst;

    public ParserImpl(UdirRoot src, TopicMapLocator dst)
    {
        this.srcFactory = null;
        this.src = src;
        this.resourcesSrc = null;
        this.dst = dst;
    }
    public ParserImpl(UdirFactory udirFactory, TopicMapLocator tmLocator) {
        this.srcFactory = udirFactory;
        this.src = null;
        this.resourcesSrc = null;
        this.dst = tmLocator;
    }
    public ParserImpl(UdirFactory udirFactory, TopicMapLocator resourcesLocator, TopicMapLocator tmLocator) {
        this.srcFactory = udirFactory;
        this.src = null;
        this.resourcesSrc = resourcesLocator;
        this.dst = tmLocator;
    }

    private class UdirRootCloser implements Context.FinishListener {
        private UdirRoot udirRoot;

        public UdirRootCloser(UdirRoot udirRoot) {
            this.udirRoot = udirRoot;
        }

        @Override
        public void ioException(IOException e) {
        }

        @Override
        public void finished() {
            udirRoot.close();
        }
    }
    @Override
    public Parser.Context parse() throws IOException {
        UdirRoot src = this.src;
        Context.FinishListener finishListener;

        if (src == null)
            src = this.srcFactory.createUdirRoot();
        Context context = new Context(this, src, resourcesSrc, dst);
        if (src != this.src)
            context.addFinishListener(new UdirRootCloser(src));
        context.start();
        return context;
    }

    @Override
    public URL getPsiRoot()
    {
        try {
            return new URL("http://psi.mycurriculum.org/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("This url should be valid");
        }
    }

    public static class Context extends Thread implements Parser.Context {
        private static class ParserReport implements Report {
            private AimsAssociations aimsAssociationsReport;

            @Override
            public AimsAssociations getAimsAssociationsReport() {
                return aimsAssociationsReport;
            }

            public void setAimsAssociationsReport(AimsAssociations aimsAssociationsReport) {
                this.aimsAssociationsReport = aimsAssociationsReport;
            }
        }
        private Parser parser;
        private UdirRoot src;
        private TopicMapLocator resourcesSrc;
        private TopicMapLocator dst;
        private ProgressTracker progress;
        private IOException ioException;
        private Set<FinishListener> finishListeners = new HashSet<FinishListener>();
        private ParserReport report = new ParserReport();

        public Context(Parser parser, UdirRoot src, TopicMapLocator resourcesSrc, TopicMapLocator dst)
        {
            this.parser = parser;
            this.src = src;
            this.resourcesSrc = resourcesSrc;
            this.dst = dst;
            progress = new ProgressTrackerImpl("Udir import");
        }

        public void run()
        {
            try {
                /* setup */
                /* setup levels */
                List<UdirLevel> levels = src.getLevelsRoot().getUdirLevels();
                ProgressBit levelsProgress = progress.createBit("Levels import");
                levelsProgress.setExpected(levels.size());
                progress.addSubBit(levelsProgress);
                LevelsImport levelsImport = new LevelsImport(parser, levels, dst, levelsProgress);
                /* setup educational programs */
                UdirEducationalProgramsRoot educationalProgramsRoot = src.getEducationalProgramsRoot();
                List<UdirEducationalProgramRef> educationalPrograms = educationalProgramsRoot.getEducationalProgramReferences();
                ProgressBit educationalProgramsProgress = progress.createBit("Educational programs import");
                educationalProgramsProgress.setExpected(educationalPrograms.size());
                progress.addSubBit(educationalProgramsProgress);
                EducationalProgramsImport educationalProgramsImport = new EducationalProgramsImport(parser, educationalPrograms, dst, educationalProgramsProgress);
                /* setup program areas */
                UdirProgramAreasRoot programAreasRoot = src.getProgramAreasRoot();
                List<UdirProgramAreaRef> programAreas = programAreasRoot.getProgramAreaReferences();
                ProgressBit programAreasProgress = progress.createBit("Program areas import");
                programAreasProgress.setExpected(programAreas.size());
                progress.addSubBit(programAreasProgress);
                ProgramAreasImport programAreasImport = new ProgramAreasImport(parser, programAreas, dst, programAreasProgress);
                /* setup curriculums */
                UdirCurriculumRoot curriculumRoot = src.getCurriculumRoot();
                List<UdirCurriculumRef> curriculums = curriculumRoot.getCurriculumReferences();
                ProgressBit curriculumProgress = progress.createBit("Curriculum import");
                curriculumProgress.setExpected(curriculums.size());
                progress.addSubBit(curriculumProgress);
                CurriculumsImport curriculumsImport = new CurriculumsImport(parser, curriculums, dst, curriculumProgress);
                CurriculumsImport.AimSetImporter aimSetImport = curriculumsImport.getAimSetImporter();
                progress.addSubBit(aimSetImport.getProgress());
                /* setup program areas curriculum sets */
                ProgressBit programAreasCurriculumsetsProgress = progress.createBit("Program areas curriculums");
                progress.setWeight(2.0f);
                programAreasCurriculumsetsProgress.setExpected(programAreas.size());
                progress.addSubBit(programAreasCurriculumsetsProgress);
                CurriculumSetImporter programAreasCurriculumSetsImporter = new CurriculumSetImporter(parser, programAreas, dst, programAreasCurriculumsetsProgress);
                /* setup curriculum levels */
                ProgressBit curriculumLevelsProgress = progress.createBit("Curriculum levels");
                curriculumLevelsProgress.setWeight(5.0f);
                curriculumLevelsProgress.setExpected(curriculums.size());
                progress.addSubBit(curriculumLevelsProgress);
                CurriculumLevelImporter curriculumLevelsImport = new CurriculumLevelImporter(parser, curriculums, dst, curriculumLevelsProgress);
                /* remove total progress inhibitor */
                progress.setExpected(0);
                /* set dependencies */
                programAreasImport.addDependency(levelsImport);
                programAreasImport.addDependency(educationalProgramsImport);
                programAreasCurriculumSetsImporter.addDependency(programAreasImport);
                programAreasCurriculumSetsImporter.addDependency(curriculumsImport);
                curriculumsImport.addDependency(levelsImport);
                curriculumLevelsImport.addDependency(curriculumsImport);
                /* create set of imports */
                TaskUnitRunner importers = new TaskUnitRunner(4);
                importers.add(levelsImport);
                importers.add(educationalProgramsImport);
                importers.add(programAreasImport);
                importers.add(curriculumsImport);
                importers.add(aimSetImport);
                importers.add(curriculumLevelsImport);
                importers.add(programAreasCurriculumSetsImporter);
                /* resource copy */
                ResourceAssociationsCopier resourceAssociationsCopier = null;
                if (resourcesSrc != null) {
                    ProgressBit resourceAssociationsCopierProgress = new ProgressBitImpl("Resource associations");
                    resourceAssociationsCopier = new ResourceAssociationsCopier(parser, resourceAssociationsCopierProgress, resourcesSrc, dst);
                    /* Copy resources */
                    ProgressBit resourceCopierProgress = new ProgressBitImpl("Resource copy");
                    ResourceCopier resourceCopier = new ResourceCopier(parser, resourceCopierProgress, resourcesSrc, dst, resourceAssociationsCopier);
                    progress.addSubBit(resourceCopierProgress);
                    importers.add(resourceCopier);
                    /* Copy resource associations */
                    progress.addSubBit(resourceAssociationsCopierProgress);
                    resourceAssociationsCopier.addDependency(aimSetImport);
                    resourceAssociationsCopier.addDependency(programAreasCurriculumSetsImporter);
                    importers.add(resourceAssociationsCopier);
                }
                /* run all iterations */
                importers.run();
                /* get the reports */
                if (resourceAssociationsCopier != null)
                    report.setAimsAssociationsReport(resourceAssociationsCopier.getReport());
            } catch (IOException e) {
                ioException = e;
                for (FinishListener listener : finishListeners) {
                    listener.ioException(e);
                }
            } finally {
                for (FinishListener listener : finishListeners) {
                    listener.finished();
                }
            }
        }

        @Override
        public ProgressTracker getProgressTracker() {
            return progress;
        }

        @Override
        public void waitForCompletion() throws IOException, InterruptedException {
            this.join();
            if (ioException != null)
                throw new IOException("Parser failed", ioException);
        }

        @Override
        public Report getReport() {
            return report;
        }

        public void addFinishListener(FinishListener listener) {
            finishListeners.add(listener);
        }
        public interface FinishListener {
            public void ioException(IOException e);
            public void finished();
        }
    }
}
