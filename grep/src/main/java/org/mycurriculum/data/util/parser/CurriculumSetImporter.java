package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.UdirCurriculum;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumRef;
import org.mycurriculum.data.adapter.udir.domain.UdirProgramArea;
import org.mycurriculum.data.adapter.udir.domain.UdirProgramAreaRef;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaCurriculum;
import org.mycurriculum.data.tms.domain.OntopiaCurriculumSet;
import org.mycurriculum.data.tms.domain.OntopiaCurriculumSetImpl;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CurriculumSetImporter extends UdirObjectImporter<UdirProgramAreaRef> {
    public CurriculumSetImporter(Parser parser, List<UdirProgramAreaRef> srcList, TopicMapLocator dst, ProgressBit progress) {
        super(parser, srcList, dst, progress);
    }

    public static URL generatePsi(String id) {
        return OntopiaCurriculumSetImpl.generatePsi(id);
    }

    public URL generatePsi(UdirProgramAreaRef obj) throws IOException {
        return generatePsi(obj.getProgramArea().getKode());
    }
    public String generateCommonHumanId(UdirProgramAreaRef obj) throws IOException {
        return "common-"+obj.getProgramArea().getKode();
    }
    public String generateProgramHumanId(UdirProgramAreaRef obj) throws IOException {
        return "program-"+obj.getProgramArea().getKode();
    }
    public String generateProgramOptionalHumanId(UdirProgramAreaRef obj) throws IOException {
        return "optional-program-"+obj.getProgramArea().getKode();
    }

    private void parseCurriculumSet(OntopiaCurriculumSet curriculumSet, Collection<UdirCurriculum> curricula, TopicMapRoot dst) throws IOException {
        Collection<OntopiaCurriculum> removeCurricula = curriculumSet.getCurricula();
        Collection<OntopiaCurriculum> addCurricula = new ArrayList<OntopiaCurriculum>();

        for (UdirCurriculum udirCurriculum : curricula) {
            OntopiaCurriculum curriculum = dst.getCurriculum(new URL(udirCurriculum.getUrlPsi()));
            if (curriculum == null)
                throw new IOException("Data integrity error: Curriculum not found: "+udirCurriculum.getUrlPsi());
            if (!removeCurricula.contains(curriculum)) {
                addCurricula.add(curriculum);
            } else {
                removeCurricula.remove(curriculum);
            }
        }
        for (OntopiaCurriculum remove : removeCurricula) {
            curriculumSet.removeCurriculum(remove);
        }
        for (OntopiaCurriculum add : addCurricula) {
            curriculumSet.addCurriculum(add);
        }
    }
    private void parseCurriculumSet(OntopiaCourseStructure courseStructure, Collection<OntopiaCurriculumSet> curriculumSetCollection, String humanId, String setType, Collection<UdirCurriculum> curricula, TopicMapRoot dst) throws IOException {
        URL setPsi = OntopiaCurriculumSetImpl.generatePsi(humanId);
        OntopiaCurriculumSet curriculumSet = dst.getCurriculumSet(setPsi);
        if (curriculumSet == null) {
            curriculumSet = dst.getCurriculumSetBuilder()
                    .psi(setPsi.toString())
                    .name("Curriculum set for: "+courseStructure.getName())
                    .build();
        }
        String st = curriculumSet.getSetType();
        if (st == null || !st.equals(setType))
            curriculumSet.setSetType(setType);
        curriculumSet.setHumanId(humanId);
        parseCurriculumSet(curriculumSet, curricula, dst);
        curriculumSet.save();
        boolean matching = false;
        for (OntopiaCurriculumSet matchCheck : curriculumSetCollection) {
            if (matchCheck.hasPsi(setPsi.toString())) {
                matching = true;
                break;
            }
        }
        if (!matching) {
            courseStructure.addCurriculumSet(curriculumSet);
            courseStructure.save();
        }
    }
    @Override
    public void parse(UdirProgramAreaRef src, TopicMapRoot dst) throws IOException {
        UdirProgramArea udirProgramArea = src.getProgramArea();
        if (!ProgramAreasImport.importFilter(udirProgramArea))
            return;
        List<UdirCurriculumRef> udirCurriculumRefs = udirProgramArea.getCurricula();
        OntopiaCourseStructure courseStructure = dst.getCourseStructure(new URL(udirProgramArea.getUrlPsi()));
        if (courseStructure == null)
            throw new IOException("Data integrity error: Course structure not found: "+udirProgramArea.getUrlPsi());
        URL setPsi = generatePsi(src);
        Collection<OntopiaCurriculumSet> curriculumSetCollection = courseStructure.getCurriculumSets();
        Collection<UdirCurriculum> curriculaCommon = new ArrayList<UdirCurriculum>();
        Collection<UdirCurriculum> curriculaProgram = new ArrayList<UdirCurriculum>();
        Collection<UdirCurriculum> curriculaOptionalProgram = new ArrayList<UdirCurriculum>();
        for (UdirCurriculumRef curriculumRef : udirCurriculumRefs) {
            UdirCurriculum udirCurriculum = curriculumRef.getCurriculum();
            if (!udirCurriculum.isPublished()) {
                continue;
            }
            String type = udirCurriculum.getCurriculumType();
            /* Sort into the various categories */
            if (type.equals("http://psi.udir.no/ontologi/fellesfag")) {
                curriculaCommon.add(udirCurriculum);
                continue;
            }
            if (type.equals("http://psi.udir.no/ontologi/felles_programfag")) {
                curriculaProgram.add(udirCurriculum);
                continue;
            }
            if (type.equals("http://psi.udir.no/ontologi/valgfritt_programfag")) {
                curriculaOptionalProgram.add(udirCurriculum);
                continue;
            }
            /* Unknown type */
            throw new IOException("Unknown curriculum type: "+type);
        }
        if (curriculaCommon.size() > 0) {
            parseCurriculumSet(courseStructure, curriculumSetCollection, generateCommonHumanId(src), "COMMON", curriculaCommon, dst);
        }
        if (curriculaProgram.size() > 0) {
            parseCurriculumSet(courseStructure, curriculumSetCollection, generateProgramHumanId(src), "PROGRAM", curriculaProgram, dst);
        }
        if (curriculaOptionalProgram.size() > 0) {
            parseCurriculumSet(courseStructure, curriculumSetCollection, generateProgramOptionalHumanId(src), "PROGRAMOPTIONAL", curriculaOptionalProgram, dst);
        }
    }
}
