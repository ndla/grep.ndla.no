package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.UdirAim;
import org.mycurriculum.data.adapter.udir.domain.UdirAimRef;
import org.mycurriculum.data.adapter.udir.domain.UdirAimsByArea;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculum;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaAimSet;
import org.mycurriculum.data.tms.domain.OntopiaAimSetImpl;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.multicore.TaskUnit;
import org.mycurriculum.data.util.multicore.TaskUnitRunner;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AreaAimSetImport extends UdirObjectImporter<Map.Entry<OntopiaAimSet,UdirAimsByArea>> {
    private Parser parser;
    private UdirCurriculum curriculum;
    private TaskUnitRunner aimsImporters = new TaskUnitRunner(1);

    public AreaAimSetImport(Parser parser, UdirCurriculum curriculum, List<Map.Entry<OntopiaAimSet,UdirAimsByArea>> srcList, TopicMapLocator dst, ProgressBit progress) {
        super(parser, srcList, dst, progress);
        this.parser = parser;
        this.curriculum = curriculum;
        aimsImporters.addDependency(this);
    }

    public URL generatePsi(Map.Entry<OntopiaAimSet,UdirAimsByArea> obj) throws IOException {
        return generatePsi(obj.getKey(), obj.getValue());
    }
    public URL generatePsi(OntopiaAimSet parentAimSet, UdirAimsByArea obj) {
        return OntopiaAimSetImpl.generatePsi(parentAimSet.getHumanId() + "_" + obj.getKode());
    }
    public URL generatePsi(UdirAim aim) {
        return OntopiaAimSetImpl.generatePsi(aim.getKode());
    }

    @Override
    public void parse(Map.Entry<OntopiaAimSet,UdirAimsByArea> mapping, TopicMapRoot dst) throws IOException {
        OntopiaAimSet parentAimSet = mapping.getKey();
        UdirAimsByArea src = mapping.getValue();
        /* Set aims import progress expectations */
        ProgressBit aimSetProgress = new ProgressBitImpl("Aim set "+src.getKode());
        UdirAimRef[] aimRefs = src.getAimRefs();
        aimSetProgress.setExpected(aimRefs.length);
        getProgress().addSubBit(aimSetProgress);

        /* Import aim set record */
        OntopiaAimSet.Builder builder = dst.getAimSetBuilder()
                .psi(generatePsi(parentAimSet, src).toString())
                .upstreamId(src.getId());
        OntopiaAimSet aimSet = dst.getAimSet(generatePsi(parentAimSet, src));
        if (aimSet == null) {
            aimSet = builder.build();
        }
        aimSet.setSetType("MAINAREA");
        String uniqueId = parentAimSet.getHumanId()+"_"+src.getKode();
        aimSet.setHumanId(uniqueId);
        importTitle(aimSet, src.getTranslatedTitle());
        aimSet.setSortingOrder(src.getSortingOrder());
        aimSet.save();
        /* AimSet (1) to AimSet (n) */
        boolean foundAimSet = false;
        synchronized (parentAimSet) {
            parentAimSet.resetTopicMap(dst);
            Iterator<OntopiaAimSet> iterator = parentAimSet.getAimSets().iterator();
            while (iterator.hasNext()) {
                OntopiaAimSet cur = iterator.next();
                if (cur.equals(aimSet)) {
                    foundAimSet = true;
                    break;
                }
            }
            if (!foundAimSet) {
                parentAimSet.addAimSet(aimSet);
                parentAimSet.save();
            }
        }

        /* Import aims */
        long aimProgressCounter = 0;
        if (aimRefs.length > 0) {
            List<UdirAimRef> aimList = new ArrayList<UdirAimRef>(aimRefs.length);
            for (UdirAimRef aim: aimRefs) {
                aimList.add(aim);
            }
            AimsImporter aimsImporter = new AimsImporter(parser, aimList, getTopicMapLocator(), aimSetProgress, aimSet);
            aimsImporters.add(aimsImporter);
        }
    }

    public TaskUnit getAimsImporter() {
        return aimsImporters;
    }
}
