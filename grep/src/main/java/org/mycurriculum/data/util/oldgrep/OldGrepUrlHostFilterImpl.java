package org.mycurriculum.data.util.oldgrep;

import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class OldGrepUrlHostFilterImpl extends OldGrepUrlFilter {
    private Collection<String> hostnames;
    private Map<String,Counter> seenHostnames = new HashMap<String,Counter>();
    private boolean reverse = false;

    @Override
    public void printSummary() {
        System.out.println("Hostname summary:");
        for (Map.Entry<String,Counter> entry : seenHostnames.entrySet()) {
            int count = entry.getValue().getCount();
            String text = ""+count;
            if (count != 1)
                text += " instances";
            else
                text += " instance";
            System.out.println(" * "+entry.getKey()+": "+text);
        }
    }

    private static class Counter {
        int count = 0;

        public Counter() {
        }

        public void up() {
            count++;
        }
        public int getCount() {
            return count;
        }
    }

    public OldGrepUrlHostFilterImpl(Collection<String> hostnames) {
        this.hostnames = hostnames;
    }
    public OldGrepUrlHostFilterImpl(Collection<String> hostnames, boolean reverse) {
        this.hostnames = hostnames;
        this.reverse = reverse;
    }
    private boolean actualUrlFilter(URL url) {
        String hostname = url.getHost();
        Counter counter = seenHostnames.get(hostname);
        if (counter == null) {
            counter = new Counter();
            seenHostnames.put(hostname, counter);
        }
        counter.up();
        return hostnames.contains(hostname);
    }
    @Override
    public boolean urlFilter(URL url) {
        return reverse ^ actualUrlFilter(url);
    }
}
