package org.mycurriculum.data.util.searchindexer.ndla;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.data.util.searchindexer.SearchIndexerFactoryImpl;
import org.mycurriculum.data.util.searchindexer.SearchIndexerType;

import java.net.URI;

public class NdlaSearchIndexerTypeImpl implements SearchIndexerType, NdlaSearchIndexerConfig {
    private URI serviceUri;
    private String username;
    private String password;
    private String httpUsername = null;
    private String httpPassword = null;

    public NdlaSearchIndexerTypeImpl(String name, URI serviceUri, String username, String password, SearchIndexerFactoryImpl factory) {
        this.serviceUri = serviceUri;
        this.username = username;
        this.password = password;
        factory.addSearchIndexer(name, this);
    }

    @Override
    public AsyncTask createSearchIndexer(TopicMapLocator topicMapLocator) {
        return new NdlaSearchIndexerImpl(this, topicMapLocator);
    }

    @Override
    public URI getServiceUri() {
        return serviceUri;
    }

    @Override
    public String getServiceUsername() {
        return username;
    }

    @Override
    public String getServicePassword() {
        return password;
    }

    @Override
    public String getHttpUsername() {
        return httpUsername;
    }

    public void setHttpUsername(String httpUsername) {
        this.httpUsername = httpUsername;
    }

    @Override
    public String getHttpPassword() {
        return httpPassword;
    }

    public void setHttpPassword(String httpPassword) {
        this.httpPassword = httpPassword;
    }
}
