package org.mycurriculum.data.util;

import java.util.Locale;
import java.util.Map;

public interface TranslatedText {
    public String getDefaultText();

    public Locale getDefaultLocale();
    public String getDefaultLocalePsi();

    public String getText(Locale locale);

    public String getText(String iso639lang);

    public Map<Locale,String> getTextMap();
    public Map<String,String> getPsiTextMap();

    public Locale[] getLanguages();
}
