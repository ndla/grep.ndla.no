package org.mycurriculum.data.util.multicore;

import org.json.JSONObject;
import org.mycurriculum.data.util.parser.ProgressTracker;

import java.io.IOException;

public interface AsyncTask extends Runnable {
    public void updateStatus(JSONObject status);
    public ProgressTracker getProgressTracker();
    public void completionCheck() throws IOException;
}
