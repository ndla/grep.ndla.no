package org.mycurriculum.data.util.parser;

import java.util.Collection;
import java.util.Date;

public interface ProgressBit {
    public String getName();
    public long getTotalExpected();
    public long getTotalDone();
    public void setExpected(long expected);
    public void setDone(long done);
    public void setWeight(float weight);
    public float getWeight();
    public void addSubBit(ProgressBit bit);
    public Collection<ProgressBit> getSubBits();
    public void removeSubBit(ProgressBit bit);
    public ProgressBit createBit(String name);

    public void addListener(Listener listener);
    public void removeListener(Listener listener);

    public void setStartTime();
    public Date getStartedTime();
    public Date getEstimatedFinish();
    public Date getMetaEstimatedFinish();

    public interface Listener {
        public void progressUpdate();
    }
}
