package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.TranslatedText;
import org.mycurriculum.data.util.multicore.TaskUnit;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ResourceAssociationsCopier implements TaskUnit {
    public final static int RECYCLE_ONTOPIA_CONNECTION = 2000;

    private static class ReportObject implements Report.AimsAssociations {
        private Map<String,Collection<String>> aimsNotFound = new HashMap<String,Collection<String>>();
        private Map<String,TranslatedText> aimNames = new HashMap<String,TranslatedText>();
        private Map<String,Collection<String>> curriculumSetsNotFound = new HashMap<String,Collection<String>>();
        private Map<String,TranslatedText> curriculumSetNames = new HashMap<String,TranslatedText>();
        private Map<String,Collection<String>> levelsNotFound = new HashMap<String,Collection<String>>();
        private Map<String,TranslatedText> levelNames = new HashMap<String,TranslatedText>();
        private Map<String,Collection<String>> curriculaNotFound = new HashMap<String,Collection<String>>();
        private Map<String,TranslatedText> curriculaNames = new HashMap<String,TranslatedText>();
        private Map<String,Collection<String>> aimSetsNotFound = new HashMap<String,Collection<String>>();
        private Map<String,TranslatedText> aimSetsNames = new HashMap<String,TranslatedText>();

        public void addMissingAimRelation(OntopiaAim aim, String resourcePsi) {
            String aimId = aim.getHumanId();
            TranslatedText name = aim.getTranslatedName();
            synchronized (this) {
                Collection<String> resources = aimsNotFound.get(aimId);
                if (resources == null)
                    resources = new HashSet<String>();
                resources.add(resourcePsi);
                aimsNotFound.put(aimId, resources);
                if (aimNames.get(aimId) == null) {
                    if (name != null)
                        aimNames.put(aimId, name);
                }
            }
        }
        public void addMissingCurriculumSet(OntopiaCurriculumSet curriculumSet, String resourcePsi) {
            String curriculumSetId = curriculumSet.getHumanId();
            TranslatedText name = curriculumSet.getTranslatedName();
            synchronized (this) {
                Collection<String> resources = curriculumSetsNotFound.get(curriculumSetId);
                if (resources == null)
                    resources = new HashSet<String>();
                resources.add(resourcePsi);
                curriculumSetsNotFound.put(curriculumSetId, resources);
                if (curriculumSetNames.get(curriculumSetId) == null) {
                    if (name != null)
                        curriculumSetNames.put(curriculumSetId, name);
                }
            }
        }
        public void addMissingLevel(OntopiaLevel level, String resourcePsi) {
            String levelId = level.getHumanId();
            TranslatedText name = level.getTranslatedName();
            synchronized (this) {
                Collection<String> resources = levelsNotFound.get(levelId);
                if (resources == null)
                    resources = new HashSet<String>();
                resources.add(resourcePsi);
                curriculumSetsNotFound.put(levelId, resources);
                if (curriculumSetNames.get(levelId) == null) {
                    if (name != null)
                        curriculumSetNames.put(levelId, name);
                }
            }
        }
        public void addMissingCurriculum(OntopiaCurriculum curriculum, String resourcePsi) {
            String curriculumId = curriculum.getHumanId();
            TranslatedText name = curriculum.getTranslatedName();
            synchronized (this) {
                Collection<String> resources = curriculaNotFound.get(curriculumId);
                if (resources == null)
                    resources = new HashSet<String>();
                resources.add(resourcePsi);
                curriculaNotFound.put(curriculumId, resources);
                if (curriculaNames.get(curriculumId) == null) {
                    if (name != null)
                        curriculaNames.put(curriculumId, name);
                }
            }
        }
        public void addMissingAimSet(OntopiaAimSet aimSet, String resourcePsi) {
            String aimSetId = aimSet.getHumanId();
            TranslatedText name = aimSet.getTranslatedName();
            synchronized (this) {
                Collection<String> resources = aimSetsNotFound.get(aimSetId);
                if (resources == null)
                    resources = new HashSet<String>();
                resources.add(resourcePsi);
                aimSetsNotFound.put(aimSetId, resources);
                if (aimSetsNames.get(aimSetId) == null) {
                    if (name != null)
                        aimSetsNames.put(aimSetId, name);
                }
            }
        }

        @Override
        public Map<String,Collection<String>> getAimsNotFound() {
            return aimsNotFound;
        }

        @Override
        public TranslatedText getAimName(String humanId) {
            return aimNames.get(humanId);
        }

        @Override
        public Map<String, Collection<String>> getCurriculumSetsNotFound() {
            return curriculumSetsNotFound;
        }

        @Override
        public TranslatedText getCurriculumSetName(String humanId) {
            return curriculumSetNames.get(humanId);
        }

        @Override
        public Map<String, Collection<String>> getLevelsNotFound() {
            return levelsNotFound;
        }

        @Override
        public TranslatedText getLevelName(String humanId) {
            return levelNames.get(humanId);
        }

        @Override
        public Map<String, Collection<String>> getCurriculaNotFound() {
            return curriculaNotFound;
        }

        @Override
        public TranslatedText getCurriculumName(String humanId) {
            return curriculaNames.get(humanId);
        }

        @Override
        public Map<String, Collection<String>> getAimSetsNotFound() {
            return aimSetsNotFound;
        }

        @Override
        public TranslatedText getAimSetName(String humanId) {
            return aimSetsNames.get(humanId);
        }
    }
    private Parser parser;
    private ProgressBit progress;
    private Set<TaskUnit> dependencies = new HashSet<TaskUnit>();
    private TopicMapLocator src;
    private TopicMapLocator dst;
    private BlockingQueue<String> copyQueue = new LinkedBlockingQueue<String>();
    private boolean copyCompleted = false;
    private String poison = "blockingQueueEnd";
    private ReportObject report = new ReportObject();

    public ResourceAssociationsCopier(Parser parser, ProgressBit progress, TopicMapLocator src, TopicMapLocator dst) {
        this.parser = parser;
        this.progress = progress;
        this.src = src;
        this.dst = dst;
        progress.setExpected(150000);
    }

    public void queueResource(String resourcePsi) throws InterruptedException {
        copyQueue.put(resourcePsi);
    }
    public void endQueue() throws InterruptedException {
        copyQueue.put(poison);
    }

    public ProgressBit getProgress() {
        return progress;
    }

    @Override
    public void addDependency(TaskUnit dependency) {
        dependencies.add(dependency);
    }

    @Override
    public Set<TaskUnit> getDependencies() {
        return dependencies;
    }

    public boolean copy(TopicMapRoot src, TopicMapRoot dst) throws IOException {
        try {
            String psi;
            synchronized (this) {
                if (copyCompleted)
                    return false;
                psi = copyQueue.take();
                if (psi == poison) {
                    copyCompleted = true;
                    return false;
                }
            }
            OntopiaResource resource = src.getResource(new URL(psi));
            if (resource == null)
                throw new IOException("Resource not found in associations stage: "+psi);
            OntopiaResource dstResource = dst.getResource(new URL(psi));
            if (dstResource == null)
                throw new IOException("Destination resource not found in associations stage: "+psi);
            Collection<String> srcAims = new ArrayList<String>();
            for (OntopiaResourceAimAssociation srcAssoc : resource.getAimAssociations()) {
                OntopiaAim aim = dst.getAim(srcAssoc.getAim().getHumanId());
                if (aim != null) {
                    if (dst.getResourceAimAssociation(srcAssoc.getHumanId()) == null) {
                        OntopiaResourceAimAssociation.Builder dstAssoc = dstResource.buildAimAssociation(aim);
                        Long timestamp = srcAssoc.getTimestamp();
                        OntopiaCurriculumSet srcCurriculumSet = srcAssoc.getCurriculumSet();
                        OntopiaLevel srcLevel = srcAssoc.getLevel();
                        OntopiaCurriculum srcCurriculum = srcAssoc.getCurriculum();
                        OntopiaAimSet srcAimSet = srcAssoc.getAimSet();
                        OntopiaCurriculumSet curriculumSet = null;
                        OntopiaLevel level = null;
                        OntopiaCurriculum curriculum = null;
                        OntopiaAimSet aimSet = null;
                        if (srcCurriculumSet != null) {
                            curriculumSet = dst.getCurriculumSet(srcCurriculumSet.getHumanId());
                            if (curriculumSet == null) {
                                report.addMissingCurriculumSet(srcCurriculumSet, psi);
                            }
                        }
                        if (srcLevel != null) {
                            level = dst.getLevel(srcLevel.getHumanId());
                            if (level == null) {
                                report.addMissingLevel(srcLevel, psi);
                            }
                        }
                        if (srcCurriculum != null) {
                            curriculum = dst.getCurriculum(srcCurriculum.getHumanId());
                            if (curriculum == null) {
                                report.addMissingCurriculum(srcCurriculum, psi);
                            }
                        }
                        if (srcAimSet != null) {
                            aimSet = dst.getAimSet(srcAimSet.getHumanId());
                            String srcAimSetUpstreamId = srcAimSet.getUpstreamId();
                            if (srcAimSetUpstreamId != null) {
                                if (aimSet == null || !srcAimSetUpstreamId.equals(aimSet.getUpstreamId())) {
                                    boolean found = false;
                                    Collection<OntopiaAimSet> searchForUuid = aim.getAimSets();
                                    while (searchForUuid.size() > 0) {
                                        Collection<OntopiaAimSet> collectedAimSets = new ArrayList<OntopiaAimSet>();
                                        for (OntopiaAimSet search : searchForUuid) {
                                            if (srcAimSetUpstreamId.equals(search.getUpstreamId())) {
                                                aimSet = search;
                                                found = true;
                                                break;
                                            }
                                            Collection<OntopiaAimSet> parents = search.getParentAimSets();
                                            collectedAimSets.addAll(parents);
                                        }
                                        if (found) {
                                            break;
                                        }
                                        searchForUuid = collectedAimSets;
                                    }
                                    if (aimSet != null && !found) {
                                        System.err.println("WARNING: Competence aim set exactly matching " + srcAimSet.getHumanId() + "/" + srcAimSetUpstreamId + " was not found!");
                                    }
                                }
                            }
                            if (aimSet == null) {
                                report.addMissingAimSet(srcAimSet, psi);
                            }
                        }
                        if (curriculumSet != null)
                            dstAssoc.curriculumSet(curriculumSet);
                        if (level != null)
                            dstAssoc.level(level);
                        if (curriculum != null)
                            dstAssoc.curriculum(curriculum);
                        if (aimSet != null)
                            dstAssoc.aimSet(aimSet);
                        if (timestamp != null)
                            dstAssoc.timestamp(srcAssoc.getTimestamp());
                        dstAssoc
                                .humanId(srcAssoc.getHumanId())
                                .apprenticeRelevant(srcAssoc.getApprenticeRelevant())
                                .authors(srcAssoc.getAuthors())
                                .authorUrls(srcAssoc.getAuthorUrls())
                                .courseId(srcAssoc.getCourseId())
                                .courseType(srcAssoc.getCourseType())
                                .build()
                                .save();
                        srcAims.add(srcAssoc.getAim().getHumanId());
                    }
                } else {
                    report.addMissingAimRelation(srcAssoc.getAim(), psi);
                }
            }
            for (OntopiaResourceAimAssociation dstAssoc : dstResource.getAimAssociations()) {
                if (!srcAims.contains(dstAssoc.getAim().getHumanId())) {
                    dstAssoc.remove();
                }
            }
        } catch (InterruptedException e) {
            throw new IOException("Interrupted", e);
        }
        return true;
    }

    private long progressCount;
    public void resourceAssociationsCopier() throws IOException {
        int recyclingCount = RECYCLE_ONTOPIA_CONNECTION;
        boolean recycling = true;
        while (recycling) {
            recycling = false;
            try (ConnectedTopicMapRoot srcConn = new ConnectedTopicMapRoot(this.src.getTopicMapRoot())) {
                TopicMapRoot src = srcConn.getTopicMapRoot();
                try (ConnectedTopicMapRoot dstConn = new ConnectedTopicMapRoot(this.dst.getTopicMapRoot())) {
                    TopicMapRoot dst = dstConn.getTopicMapRoot();
                    while (copy(src, dst)) {
                        synchronized (this) {
                            progressCount++;
                            progress.setDone(progressCount);
                        }
                        recyclingCount--;
                        if (recyclingCount == 0) {
                            recyclingCount = RECYCLE_ONTOPIA_CONNECTION;
                            recycling = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    private class ResourceAssociationsCopierThread extends Thread {
        private IOException ioException = null;
        public void run() {
            try {
                resourceAssociationsCopier();
            } catch (IOException e) {
                e.printStackTrace();
                ioException = e;
            }
        }

        public IOException getIoException() {
            return ioException;
        }
    }
    @Override
    public void run() throws IOException {
        ResourceAssociationsCopierThread[] threads = new ResourceAssociationsCopierThread[5];
        IOException ioException = null;
        progress.setStartTime();
        progressCount = 0;
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new ResourceAssociationsCopierThread();
            threads[i].start();
        }
        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
                if (threads[i].getIoException() != null) {
                    ioException = threads[i].getIoException();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (ioException != null) {
            throw new IOException("Thread threw io-exception", ioException);
        }
    }

    public Report.AimsAssociations getReport() {
        return report;
    }
}
