package org.mycurriculum.data.util.searchindexer;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.util.multicore.AsyncTask;

import java.util.Set;

public interface SearchIndexerFactory {
    public Set<String> getSearchIndexerTypes();
    public AsyncTask createSearchIndexer(TopicMapLocator topicMapLocator, String type);
}
