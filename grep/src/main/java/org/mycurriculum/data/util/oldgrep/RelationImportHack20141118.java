package org.mycurriculum.data.util.oldgrep;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaAim;
import org.mycurriculum.data.tms.domain.OntopiaAimSet;

import javax.json.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RelationImportHack20141118 implements OldGrepResourceImporter.RelationImportHack, Closeable, AutoCloseable {
    private interface Mapping {
        public String getAimId();
        public String getAimSetUuid();
    }
    private interface CurriculumMapping {
        public String getNewId();
        public Map<String,Mapping> getWhitelist();
    }
    private class CurriculumMappingImpl implements CurriculumMapping {
        private String newId;
        private Map<String, Mapping> whitelist = new HashMap<String, Mapping>();

        public void setNewId(String newId){
            this.newId = newId;
        }
        @Override
        public String getNewId() {
            return newId;
        }

        @Override
        public Map<String, Mapping> getWhitelist() {
            return whitelist;
        }
    }

    final Map<String,CurriculumMapping> removeWithCurriculum;
    public Map<String,CurriculumMapping> getRemoveWithCurriculumMapping(String file) throws FileNotFoundException {
        Map<String,CurriculumMapping> generated = new HashMap<String,CurriculumMapping>();
        File fileObj;
        if (file.startsWith("classpath:")) {
            file = file.substring(10);
            if (file.startsWith("/")) {
                file = file.substring(1);
            }
            ClassLoader classLoader = getClass().getClassLoader();
            fileObj = new File(classLoader.getResource(file).getFile());
        } else {
            fileObj = new File(file);
        }
        InputStream inputStream = new FileInputStream(fileObj);
        JsonReader reader = Json.createReader(inputStream);
        JsonObject mappingObject = reader.readObject();
        for (Map.Entry<String,JsonValue> entry : mappingObject.entrySet()) {
            final String oldCurriculumId = entry.getKey();
            JsonArray newCurriculums = (JsonArray) entry.getValue();
            for (JsonValue newCurriculumValue : newCurriculums) {
                JsonObject newCurriculumObject = (JsonObject) newCurriculumValue;
                final String newCurriculumId = newCurriculumObject.getString("ny_id");
                System.out.println(" * "+oldCurriculumId+" -> "+newCurriculumId);
                JsonArray exceptionsEncapArray = newCurriculumObject.getJsonArray("unntak");
                for (JsonValue exceptionsObjectValue : exceptionsEncapArray) {
                    JsonObject exceptionsObject = (JsonObject) exceptionsObjectValue;
                    for (Map.Entry<String,JsonValue> exceptionsEntry : exceptionsObject.entrySet()) {
                        String fullAimInputId = exceptionsEntry.getKey();
                        final String fullAimId;
                        if (fullAimInputId.startsWith("kompetansemaal_")) {
                            fullAimId = fullAimInputId.substring(15);
                        } else {
                            fullAimId = fullAimInputId;
                        }
                        System.out.println("  * "+fullAimId);
                        JsonArray aimSetEncapArray = (JsonArray) exceptionsEntry.getValue();
                        for (JsonValue aimSetEncap : aimSetEncapArray) {
                            JsonObject aimSetEncapObject = (JsonObject) aimSetEncap;
                            for (Map.Entry<String,JsonValue> aimSetEntry : aimSetEncapObject.entrySet()) {
                                String uuidInputId = aimSetEntry.getKey();
                                final String aimSetUuid;
                                aimSetUuid = uuidInputId;
                                System.out.println("   * "+aimSetUuid);
                                JsonArray aimsArray = (JsonArray) aimSetEntry.getValue();
                                for (JsonValue newAimValue : aimsArray) {
                                    JsonString fullNewAimIdString = (JsonString) newAimValue;
                                    String fullNewAimId = fullNewAimIdString.getString();
                                    int indexOfSep = fullNewAimId.indexOf('_');
                                    if (indexOfSep < 0)
                                        throw new RuntimeException("Separator _ in full aim id "+fullNewAimId+" not found");
                                    final String newAimCurId = fullNewAimId.substring(0, indexOfSep);
                                    fullNewAimId = fullNewAimId.substring(indexOfSep + 1);
                                    indexOfSep = fullNewAimId.indexOf('_');
                                    if (indexOfSep < 0)
                                        throw new RuntimeException("Second separator _ in full aim id "+newAimCurId+"_"+fullNewAimId+"(part="+fullNewAimId+") not found");
                                    final String newMainAreaId = fullNewAimId.substring(0, indexOfSep);
                                    final String newAimId = fullNewAimId.substring(indexOfSep + 1);
                                    System.out.println("    * "+newAimId);
                                    CurriculumMapping curriculumMapping;
                                    if (generated.containsKey(oldCurriculumId)) {
                                        curriculumMapping = generated.get(oldCurriculumId);
                                        if (!curriculumMapping.getNewId().equals(newCurriculumId))
                                            throw new RuntimeException("Duplicate entry for "+oldCurriculumId);
                                    } else {
                                        CurriculumMappingImpl obj = new CurriculumMappingImpl();
                                        obj.setNewId(newCurriculumId);
                                        curriculumMapping = obj;
                                        generated.put(oldCurriculumId, curriculumMapping);
                                    }
                                    Mapping mapping = new Mapping() {
                                        @Override
                                        public String getAimId() {
                                            return newAimId;
                                        }
                                        @Override
                                        public String getAimSetUuid() {
                                            return aimSetUuid;
                                        }
                                    };
                                    curriculumMapping.getWhitelist().put(fullAimId, mapping);
                                }
                            }
                        }
                        /* */
                    }
                }
            }
        }
        return generated;
    }

    final public static Map<String,String> aimIdTranslation = new HashMap<String,String>() {{
        put("K661", "K14126");
    }};

    private class Logger implements Closeable, AutoCloseable {
        FileWriter logfile;

        public Logger(String logger) throws IOException {
            logfile = new FileWriter(logger);
        }
        public void log(String logline) throws IOException {
            logfile.write(logline+"\n");
            logfile.flush();
        }
        @Override
        public void close() throws IOException {
            logfile.close();
        }
    }
    private Logger logger;

    public RelationImportHack20141118(String mappingFile, String logfile) throws IOException {
        this.logger = new Logger(logfile);
        this.removeWithCurriculum = getRemoveWithCurriculumMapping(mappingFile);
    }

    private String translateAimId(String aimId) {
        if (aimIdTranslation.containsKey(aimId)) {
            return aimIdTranslation.get(aimId);
        }
        return aimId;
    }

    @Override
    public Info filter(final TopicMapLocator destinationLocator, String resourcePsi, String origId, final String curriculumId, final String aimSetId, final String aimId) throws IOException {
        if (curriculumId != null) {
            for (Map.Entry<String,CurriculumMapping> mappingEntry : removeWithCurriculum.entrySet()) {
                String checkCurriculumId = mappingEntry.getKey();
                if (curriculumId.equalsIgnoreCase(checkCurriculumId)) {
                    if (origId != null) {
                        CurriculumMapping mapping = mappingEntry.getValue();
                        final String newCurriculumId = mapping.getNewId();
                        Map<String, Mapping> whitelist = mapping.getWhitelist();
                        if (whitelist.containsKey(origId)) {
                            final Mapping whitelistMapping = whitelist.get(origId);
                            final String newAimId = whitelistMapping.getAimId();
                            final String aimSetUuid = whitelistMapping.getAimSetUuid();
                            if (aimSetUuid != null) {
                                OntopiaAim aim = destinationLocator.getTopicMapRoot().getAim(newAimId);
                                Collection<OntopiaAimSet> searchParents = aim.getAimSets();
                                Collection<String> found = new ArrayList<String>();
                                while (searchParents.size() != 0) {
                                    Collection<OntopiaAimSet> collectedParents = new ArrayList<OntopiaAimSet>();
                                    for (OntopiaAimSet parentAimSet : searchParents) {
                                        String upstreamId = parentAimSet.getUpstreamId();
                                        if (upstreamId == null)
                                            throw new RuntimeException("The destination topicmap is supposed to have upstream ids");
                                        found.add(upstreamId);
                                        if (aimSetUuid.equals(upstreamId)) {
                                            /* FOUND!!!!!! (happy!!!!) */
                                            final String exactAimSetId = parentAimSet.getHumanId();
                                            return new Info() {
                                                @Override
                                                public String getCurriculum() {
                                                    return newCurriculumId;
                                                }
                                                @Override
                                                public String getAimSet() {
                                                    return exactAimSetId;
                                                }
                                                @Override
                                                public String getAimId() {
                                                    return newAimId;
                                                }
                                            };
                                        }
                                        Collection<OntopiaAimSet> superparents = parentAimSet.getParentAimSets();
                                        for (OntopiaAimSet superparent : superparents) {
                                            collectedParents.add(superparent);
                                        }
                                    }
                                    searchParents = collectedParents;
                                }
                                /* Not found! (sad feelings....) */
                                System.err.println("Did not find the competence aim set "+aimSetUuid+". Did find:");
                                for (String uuid : found) {
                                    System.err.println(" * "+uuid);
                                }
                                throw new IOException("The competence aim set with upstream id "+aimSetUuid+" does not exist or is not connected to this aim! The mapping cannot be done");
                            }
                            /*
                             * Whitelisted: curriculumId and aimId overridden.
                             */
                            return new Info() {
                                @Override
                                public String getCurriculum() {
                                    return newCurriculumId;
                                }
                                @Override
                                public String getAimSet() {
                                    return aimSetId;
                                }
                                @Override
                                public String getAimId() {
                                    return newAimId;
                                }
                            };
                        }
                    }
                    /*
                     * Not whitelisted
                     */
                    try {
                        logger.log("kompetansemaal_"+origId+","+resourcePsi);
                    } catch (IOException e) {
                        throw new RuntimeException(e.getMessage(), e);
                    }
                    return null; /* remove */
                }
            }
        }
        /*
         * Leaving unchanged
         */
        return new Info() {
            @Override
            public String getCurriculum() {
                return curriculumId;
            }

            @Override
            public String getAimSet() {
                return aimSetId;
            }

            @Override
            public String getAimId() {
                return translateAimId(aimId);
            }
        };
    }

    @Override
    public void close() throws IOException {
        logger.close();
    }
}
