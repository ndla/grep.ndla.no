package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.*;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructureImpl;
import org.mycurriculum.data.tms.domain.OntopiaLevel;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.TranslatedText;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ProgramAreasImport extends UdirObjectImporter<UdirProgramAreaRef> {
    private Parser parser;

    public ProgramAreasImport(Parser parser, List<UdirProgramAreaRef> programAreas, TopicMapLocator dst, ProgressBit progress) {
        super(parser, programAreas, dst, progress);
        this.parser = parser;
    }
    public static URL generatePsi(UdirProgramAreaRef obj) throws IOException {
        return OntopiaCourseStructureImpl.generatePsi(obj.getProgramArea().getKode());
    }

    public static boolean importFilter(UdirProgramArea src) {
        List<? extends UdirStickerRef> stickerRefs = src.getStickers();
        if (stickerRefs != null) {
            for (UdirStickerRef stickerRef : stickerRefs) {
                String kode = stickerRef.getKode();
                if (kode != null && kode.equals("for_privatskoler")) {
                    /*
                     * TODO - ignoring private school content.
                     */
                    return false;
                }
            }
        }
        return true;
    }
    @Override
    public void parse(UdirProgramAreaRef ref, TopicMapRoot dst) throws IOException {
        UdirProgramArea src = ref.getProgramArea();
        if (!importFilter(src))
            return;
        OntopiaCourseStructure.Builder builder = dst.getCourseStructureBuilder()
                .psi(generatePsi(ref).toString())
                .psi(src.getUrlPsi());
        TranslatedText title = src.getTranslatedTitle();
        String defaultName = title.getDefaultText();
        OntopiaCourseStructure programArea;
        programArea = dst.getCourseStructure(new URL(src.getUrlPsi()));
        if (programArea == null)
            programArea = builder.build();
        else {
            if (programArea.hasPsi(generatePsi(ref).toString()))
                programArea.addPsi(generatePsi(ref).toString());
        }
        programArea.setSetType("PROGRAM_AREA");
        programArea.setHumanId(src.getKode());
        for (Locale lang : title.getLanguages()) {
            String text = title.getText(lang);
            if (defaultName == null || !defaultName.equals(text)) {
                programArea.setName(lang, text);
            } else {
                programArea.setName(lang, text).setLanguageNeutral(true);
                defaultName = null;
            }
        }
        if (defaultName != null) {
            programArea.setName(defaultName);
        }

        UdirLevelRef levelRef = src.getLevelRef();
        if (levelRef != null) {
            UdirLevel level = levelRef.getLevel();
            OntopiaLevel tmLevel = dst.getLevel(new URL(level.getUrlPsi()));
            if (tmLevel != null) {
                if (!programArea.getLevels().contains(tmLevel))
                    programArea.addLevel(tmLevel);
            }
        }

        for (UdirEducationalProgramRef eduProgRef : src.getEducationalProgramRefs()) {
            UdirEducationalProgram eduProg = eduProgRef.getEducationalProgram();
            OntopiaCourseStructure parent = dst.getCourseStructure(new URL(eduProg.getUrlPsi()));
            if (parent != null) {
                boolean found = false;
                for (OntopiaCourseStructure p : programArea.getCourseStructureParents()) {
                    if (p.equals(parent)) {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    programArea.addCourseStructureParent(parent);
            }
        }
        programArea.save();
    }
}
