package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.UdirAim;
import org.mycurriculum.data.adapter.udir.domain.UdirAimRef;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicFinderById;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicRef;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaAim;
import org.mycurriculum.data.tms.domain.OntopiaAimImpl;
import org.mycurriculum.data.tms.domain.OntopiaAimSet;
import org.mycurriculum.data.tms.domain.OntopiaAimSetImpl;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class AimsImporter extends UdirObjectImporter<UdirAimRef> {
    private String aimSetId;
    private OntopiaAimSet aimSet = null;
    private Parser parser;

    public AimsImporter(Parser parser, List<UdirAimRef> srcList, TopicMapLocator dst, ProgressBit progress, OntopiaAimSet aimSet) throws IOException {
        super(parser, srcList, dst, progress);
        this.aimSetId = aimSet.getTopic().getObjectId();
        this.parser = parser;
    }

    public static URL generatePsi(UdirAimRef ref) throws IOException {
        UdirAim aim = ref.getAim();
        return OntopiaAimImpl.generatePsi(aim.getKode());
    }

    @Override
    public void parse(UdirAimRef ref, TopicMapRoot dst) throws IOException {
        if (aimSet == null) {
            OntopiaTopicRef aimSetRef = new OntopiaTopicFinderById(dst, aimSetId);
            aimSet = new OntopiaAimSetImpl(aimSetRef);
        }
        UdirAim aim = ref.getAim();
        OntopiaAim.Builder aimBuilder = dst.getAimBuilder()
                .psi(aim.getUrlPsi());
        OntopiaAim outAim = dst.getAim(new URL(aim.getUrlPsi()));
        if (outAim == null)
            outAim = aimBuilder.build();
        URL aimPsi = generatePsi(ref);
        if (!outAim.hasPsi(aimPsi.toString()))
            outAim.addPsi(aimPsi);
        outAim.setHumanId(aim.getKode());
        outAim.setSortingOrder(new Integer(ref.getSortingOrder()));
        importTitle(outAim, aim.getTranslatedTitle());
        if (!outAim.getAimSets().contains(aimSet))
            outAim.addToAimSet(aimSet);
        outAim.save();
    }
}
