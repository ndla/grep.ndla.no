package org.mycurriculum.data.util;

import java.util.Locale;

public interface MutableTranslatedText extends TranslatedText {
    public void addText(Locale locale, String text);
    public void removeText(Locale locale);
    public void setDefaultLocale(Locale defaultLocale);
    public void setDefaultUntaggedText(String untaggedText);
}
