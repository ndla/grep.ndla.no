package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.util.TranslatedText;

import java.util.Collection;
import java.util.Map;

public interface Report {
    public interface AimsAssociations {
        /*
         * aim-id -> list<resource-psi>
         */
        public Map<String,Collection<String>> getAimsNotFound();
        public TranslatedText getAimName(String humanId);
        public Map<String,Collection<String>> getCurriculumSetsNotFound();
        public TranslatedText getCurriculumSetName(String humanId);
        public Map<String,Collection<String>> getLevelsNotFound();
        public TranslatedText getLevelName(String humanId);
        public Map<String,Collection<String>> getCurriculaNotFound();
        public TranslatedText getCurriculumName(String humanId);
        public Map<String,Collection<String>> getAimSetsNotFound();
        public TranslatedText getAimSetName(String humanId);
    }
    public AimsAssociations getAimsAssociationsReport();
}
