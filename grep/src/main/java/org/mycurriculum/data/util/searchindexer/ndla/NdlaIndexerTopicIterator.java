package org.mycurriculum.data.util.searchindexer.ndla;

import org.mycurriculum.data.util.parser.ProgressBit;
import org.mycurriculum.data.util.parser.ProgressBitImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public abstract class NdlaIndexerTopicIterator<T> implements Runnable {
    private Object lock = new Object();
    private Collection<T> inputCollection;
    private IOException ioException = null;
    private int objectCount = 0;
    private ProgressBit progressBit;
    private int itemsPerIteration;
    private Collection<T> chunk = null;

    public NdlaIndexerTopicIterator(Collection<T> collection, int itemsPerIteration, String name) {
        this.inputCollection = collection;
        progressBit = new ProgressBitImpl(name);
        progressBit.setExpected(1);
        this.itemsPerIteration = itemsPerIteration;
    }

    public abstract void objectChunk(Collection<T> chunk) throws IOException;

    public void iterateObjects(T object) throws IOException {
        if (object != null) {
            /* got one object */
            if (chunk == null) {
                chunk = new ArrayList<T>(itemsPerIteration);
            }
            chunk.add(object);
            if (chunk.size() >= itemsPerIteration) {
                objectChunk(chunk);
                chunk = null;
            }
            progressBit.setDone(objectCount);
            objectCount++;
        } else {
            /*
             * finished with all the objects (EOL).
             * Send remaining data to search service.
             */
            if (chunk != null) {
                objectChunk(chunk);
                chunk = null;
            }
            progressBit.setDone(objectCount);
        }
    }
    public void runWrapped() throws IOException {
        final Object poison = new Object();
        final BlockingQueue<Object> objectQueue = new ArrayBlockingQueue<Object>(32);
        Thread iteratorThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        Object object = objectQueue.take();
                        if (object == poison) {
                            iterateObjects(null);
                            break;
                        }
                        T typedObject = (T) object;
                        boolean ok = false;
                        try {
                            iterateObjects(typedObject);
                            ok = true;
                        } catch (IOException e) {
                            synchronized (lock) {
                                System.out.println("Indexer write loop IO error: " + e.getMessage());
                                ioException = e;
                            }
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                            synchronized (lock) {
                                ioException = new IOException(e.getMessage(), e);
                            }
                        } finally {
                            if (!ok) {
                                while (objectQueue.take() != poison) {
                                }
                            }
                        }
                        if (!ok)
                            break;
                    }
                } catch (InterruptedException e) {
                    synchronized (lock) {
                        ioException = new IOException("Interrupted thread", e);
                    }
                } catch (IOException e) {
                    synchronized (lock) {
                        System.out.println("Indexer write loop IO error: "+e.getMessage());
                        ioException = e;
                    }
                }
            }
        };
        iteratorThread.start();
        try {
            progressBit.setExpected(inputCollection.size());
            for (T object : inputCollection) {
                synchronized (lock) {
                    if (ioException != null) {
                        System.out.println("Aborting indexer read loop");
                        /* Exception: Abort the read loop asap */
                        break;
                    }
                }
                objectQueue.put(object);
            }
        } catch (InterruptedException | RuntimeException e) {
            throw new IOException("Failed to read inputCollection", e);
        } finally {
            try {
                objectQueue.put(poison);
                iteratorThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        try {
            runWrapped();
        } catch (IOException e) {
            synchronized (lock) {
                if (ioException == null) {
                    ioException = e;
                }
            }
        }
    }

    public void completionCheck() throws IOException {
        if (ioException != null)
            throw new IOException("Delayed IO exception", ioException);
    }

    public ProgressBit getProgressBit() {
        return progressBit;
    }

    public org.mycurriculum.data.util.multicore.TaskUnit getTaskUnit() {
        return new TaskUnit(this);
    }

    public static class TaskUnit implements org.mycurriculum.data.util.multicore.TaskUnit {
        private NdlaIndexerTopicIterator indexer;
        private HashSet<org.mycurriculum.data.util.multicore.TaskUnit> dependencies = new HashSet<org.mycurriculum.data.util.multicore.TaskUnit>();

        public TaskUnit(NdlaIndexerTopicIterator indexer) {
            this.indexer = indexer;
        }

        @Override
        public void addDependency(org.mycurriculum.data.util.multicore.TaskUnit dependency) {
            dependencies.add(dependency);
        }

        @Override
        public Set<org.mycurriculum.data.util.multicore.TaskUnit> getDependencies() {
            return (Set<org.mycurriculum.data.util.multicore.TaskUnit>) dependencies.clone();
        }

        @Override
        public void run() throws IOException {
            indexer.run();
            indexer.completionCheck();
        }
    }
}
