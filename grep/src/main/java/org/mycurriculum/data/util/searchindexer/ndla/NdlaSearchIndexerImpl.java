package org.mycurriculum.data.util.searchindexer.ndla;

import org.json.JSONObject;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.data.util.multicore.TaskUnitRunner;
import org.mycurriculum.data.util.parser.ProgressTracker;
import org.mycurriculum.data.util.parser.ProgressTrackerImpl;

import java.io.IOException;

public class NdlaSearchIndexerImpl implements AsyncTask {
    private ProgressTracker progressTracker;
    private TopicMapLocator topicMapLocator;
    private IOException ioException = null;
    private RuntimeException runtimeException = null;
    private NdlaSearchIndexerConfig config;

    public NdlaSearchIndexerImpl(NdlaSearchIndexerConfig indexerConfig, TopicMapLocator topicMapLocator) {
        this.topicMapLocator = topicMapLocator;
        progressTracker = new ProgressTrackerImpl("NDLA search indexer");
        progressTracker.setExpected(1);
        this.config = indexerConfig;
    }

    @Override
    public void updateStatus(JSONObject status) {
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    @Override
    public void completionCheck() throws IOException {
        if (ioException != null) {
            throw new IOException("IO exception: "+ioException.getMessage(), ioException);
        }
        if (runtimeException != null) {
            throw new RuntimeException("Runtime exception: "+runtimeException.getMessage(), runtimeException);
        }
    }

    private class NdlaSearchIndexerSessionError {
        public IOException ioException = null;
    }
    @Override
    public void run() {
        try {
            final NdlaSearchIndexerSession session = new NdlaSearchIndexerSession(config.getServiceUri(), config.getServiceUsername(), config.getServicePassword());
            session.setHttpUsername(config.getHttpUsername());
            session.setHttpPassword(config.getHttpPassword());
            final NdlaSearchIndexerSessionError sessionError = new NdlaSearchIndexerSessionError();
            Thread sessionRequestThread = new Thread() {
                @Override
                public void run() {
                    try {
                        session.login();
                        session.deleteAll();
                    } catch (/*ClientProtocolException |*/ IOException e) {
                        sessionError.ioException = e;
                    }
                }
            };
            sessionRequestThread.start();
            TaskUnitRunner runner = new TaskUnitRunner(2);
            NdlaCurriculaIndexer curriculaIndexer = new NdlaCurriculaIndexer(session, topicMapLocator.getTopicMapRoot());
            progressTracker.addSubBit(curriculaIndexer.getProgressBit());
            runner.add(curriculaIndexer.getTaskUnit());
            /* need to ensure that the search session is ready here before starting to do something */
            try {
                sessionRequestThread.join();
            } catch (InterruptedException e) {
                throw new IOException("The session login was interrupted", e);
            }
            if (sessionError.ioException != null) {
                throw new IOException("Failed to log in with search service", sessionError.ioException);
            }
            TaskUnitRunner.Runner runnerThread = runner.start();
            NdlaAimsIndexer aimsIndexer = new NdlaAimsIndexer(session, topicMapLocator.getTopicMapRoot());
            progressTracker.addSubBit(aimsIndexer.getProgressBit());
            progressTracker.setDone(1);
            runner.add(aimsIndexer.getTaskUnit());
            runnerThread.setFinishedAdding();
            runnerThread.waitForTasks();
            curriculaIndexer.completionCheck();
            aimsIndexer.completionCheck();
        } catch (IOException e) {
            ioException = e;
        } catch (RuntimeException e) {
            e.printStackTrace();
            runtimeException = e;
        } catch (Exception e) {
            e.printStackTrace();
            runtimeException = new RuntimeException("Unhandled exception: "+e.getMessage(), e);
        }
    }
}
