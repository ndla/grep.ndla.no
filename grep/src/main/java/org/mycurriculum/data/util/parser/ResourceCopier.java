package org.mycurriculum.data.util.parser;

import net.ontopia.topicmaps.core.UniquenessViolationException;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicRef;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicSaveQueue;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.data.tms.exceptions.InvalidDataException;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.TranslatedText;
import org.mycurriculum.data.util.multicore.TaskUnit;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ResourceCopier implements TaskUnit {
    public final static int READ_RESOURCE_RECYCLE_CONNECTION = 2000;

    private Parser parser;
    private ProgressBit progress;
    private Set<TaskUnit> dependencies = new HashSet<TaskUnit>();
    private TopicMapLocator src;
    private TopicMapLocator dst;
    private ResourceAssociationsCopier assocCopier;

    public ResourceCopier(Parser parser, ProgressBit progress, TopicMapLocator src, TopicMapLocator dst, ResourceAssociationsCopier assocCopier) {
        this.parser = parser;
        this.progress = progress;
        this.src = src;
        this.dst = dst;
        this.assocCopier = assocCopier;
        progress.setExpected(150000);
    }

    public ProgressBit getProgress() {
        return progress;
    }

    @Override
    public void addDependency(TaskUnit dependency) {
        dependencies.add(dependency);
    }

    @Override
    public Set<TaskUnit> getDependencies() {
        return dependencies;
    }

    public void copy(OntopiaResource src, TopicMapRoot dst, OntopiaTopicSaveQueue saveQueue, OntopiaTopicSaveQueue.SavedEvent savedEvent, OntopiaTopicSaveQueue.ConflictEvent conflictEvent) throws IOException, InvalidDataException {
        OntopiaResource.Builder builder = dst.getResourceBuilder()
                .psi(src.getPsi().toString());
        OntopiaResource resource = builder.build();
        for (OntopiaTopic.Name name : src.getNameObjects()) {
            resource.addNameObject(name);
        }
        String[] authors = src.getAuthors();
        if (authors != null && authors.length > 0) {
            resource.setAuthors(authors);
        }
        String[] authorUrls = src.getAuthorUrls();
        if (authorUrls != null && authorUrls.length > 0) {
            resource.setAuthorUrls(authorUrls);
        }
        String nodeType = src.getNodeType();
        if (nodeType != null) {
            resource.setNodeType(nodeType);
        }
        TranslatedText ingress = src.getIngress();
        if (ingress != null) {
            resource.setIngress(ingress);
        }
        TranslatedText license = src.getLicense();
        if (license != null) {
            resource.setLicense(license);
        }
        String status = src.getStatus();
        if (status != null) {
            resource.setStatus(status);
        }
        saveQueue.saveTopic(resource, savedEvent, conflictEvent);
    }

    private static class OntopiaResourceRef {
        public OntopiaResource resource;
    }
    private long progressCount;
    private Set<URL> resourceDuplicateCheck = new HashSet<URL>();
    private BlockingQueue<OntopiaResourceRef> resourceQueue = new LinkedBlockingQueue<OntopiaResourceRef>();
    private Object resourceQueueReadLock = new Object();
    private boolean resourceQueueFinsihed = false;
    private void savedResource(OntopiaResource resource) throws IOException {
        try {
            synchronized (this) {
                assocCopier.queueResource(resource.getPsi().toString());
                progressCount++;
                progress.setDone(progressCount);
            }
        } catch (InterruptedException e) {
            throw new IOException("Interrupted", e);
        }
    }
    private void writeQueueRunner() throws IOException {
        TopicMapRoot dst = this.dst.getTopicMapRoot();
        OntopiaTopicSaveQueue saveQueue = new OntopiaTopicSaveQueue(this.dst, 1);
        dst.openPersistentConnection();
        try {
            while (true) {
                final OntopiaResource resource;
                synchronized (resourceQueueReadLock) {
                    if (resourceQueueFinsihed)
                        break;
                    OntopiaResourceRef resourceRef = resourceQueue.take();
                    if (resourceRef.resource == null) {
                        resourceQueueFinsihed = true;
                        break;
                    }
                    resource = resourceRef.resource;
                }
                boolean isDuplicate = false;
                Set<URL> psis = new HashSet<URL>();
                psis.add(resource.getPsi());
                Set<URL> duplicatePsis = new HashSet<URL>();
                synchronized (resourceDuplicateCheck) {
                    for (URL psi : psis) {
                        if (resourceDuplicateCheck.contains(psi)) {
                            duplicatePsis.add(psi);
                            isDuplicate = true;
                        }
                        resourceDuplicateCheck.add(psi);
                    }
                }
                if (!isDuplicate) {
                    final ResourceCopier copier = this;
                    copy(resource, dst, saveQueue, new OntopiaTopicSaveQueue.SavedEvent() {
                        @Override
                        public void saved(OntopiaTopic topic) {
                            try {
                                savedResource((OntopiaResource) topic);
                            } catch (IOException e) {
                                throw new RuntimeException("IO-error in asynchronous code", e);
                            }
                        }
                    }, new OntopiaTopicSaveQueue.ConflictEvent() {
                        @Override
                        public void conflict(OntopiaTopic topic, UniquenessViolationException e) {
                            System.err.println("ERROR: CONFLICT IN SAVE: "+resource.getPsi());
                            e.printStackTrace();
                            synchronized (copier) {
                                progressCount++;
                                progress.setDone(progressCount);
                            }
                        }
                    });
                } else {
                    System.err.println("ERROR: DUPLICATE RESOURCE "+resource.getId()+": resource duplicate randomly deleted!");
                    for (URL psi : duplicatePsis) {
                        System.err.println(" - PSI conflict: "+psi.toString());
                    }
                    synchronized (this) {
                        progressCount++;
                        progress.setDone(progressCount);
                    }
                }
            }
        } catch (InterruptedException e) {
            throw new IOException("Interrupted", e);
        } catch (InvalidDataException e) {
            throw new IOException("Invalid data", e);
        } finally {
            dst.closePersistentConnection();
        }
        saveQueue.spawnNewThreads(1);
        saveQueue.endSaveQueue();
    }
    private class WriteQueueThread extends Thread {
        private IOException ioException = null;

        public void run() {
            try {
                writeQueueRunner();
            } catch (IOException e) {
                e.printStackTrace();
                ioException = e;
            }
        }

        public IOException getIoException() {
            return ioException;
        }
    }

    @Override
    public void run() throws IOException {
        progress.setStartTime();
        WriteQueueThread[] threads = new WriteQueueThread[1];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new WriteQueueThread();
            threads[i].start();
        }
        TopicMapRoot src = this.src.getTopicMapRoot();
        int recycle = READ_RESOURCE_RECYCLE_CONNECTION;
        src.openPersistentConnection();
        try {
            progressCount = 1;
            progress.setDone(progressCount);
            Collection<OntopiaTopicRef> resources = src.getResourceRefs();
            progress.setExpected(resources.size() + 1);
            assocCopier.getProgress().setExpected(resources.size());
            try {
                for (OntopiaTopicRef ref : resources) {
                    recycle--;
                    if (recycle == 0) {
                        recycle = READ_RESOURCE_RECYCLE_CONNECTION;
                        src.closePersistentConnection();
                        src.openPersistentConnection();
                    }
                    OntopiaResourceRef resourceRef = new OntopiaResourceRef();
                    resourceRef.resource = src.getResource(ref);
                    resourceQueue.put(resourceRef);
                }
            } catch (InterruptedException e) {
                throw new IOException("Interrupted", e);
            } finally {
                OntopiaResourceRef resourceRef = new OntopiaResourceRef();
                resourceRef.resource = null;
                resourceQueue.put(resourceRef);
            }
        } catch (InterruptedException e) {
            throw new IOException("Interrupted", e);
        } finally {
            src.closePersistentConnection();
            try {
                for (int i = 0; i < threads.length; i++) {
                    threads[i].join();
                }
                assocCopier.endQueue();
            } catch (InterruptedException e) {
                throw new IOException("Interrupted", e);
            }
        }
        for (WriteQueueThread thread : threads) {
            thread.getIoException();
        }
    }
}
