package org.mycurriculum.data.util.searchindexer.ndla;

import org.json.JSONObject;
import org.mycurriculum.data.tms.domain.OntopiaCurriculum;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;

public class NdlaCurriculaIndexer extends NdlaTopicIndexer<OntopiaCurriculum> {
    public NdlaCurriculaIndexer(NdlaSearchIndexerSession session, TopicMapRoot topicMapRoot) throws IOException {
        super(session, topicMapRoot.getCurricula(), "NDLA Curricula Indexer");
    }

    @Override
    public JSONObject createPayload(OntopiaCurriculum obj) {
        JSONObject json = createIdNamePayload(obj);
        json.put("type", "curriculum");
        return json;
    }
}
