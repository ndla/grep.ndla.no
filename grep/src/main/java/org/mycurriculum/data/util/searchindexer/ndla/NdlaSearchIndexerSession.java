package org.mycurriculum.data.util.searchindexer.ndla;

import org.apache.http.*;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class NdlaSearchIndexerSession {
    public final static String loginEndpoint = "/api/topics/user/login";
    public final static String tokenEndpoint = "/services/session/token";
    public final static String deleteEndpoint = "/api/topics/grep";

    private URI serverUrl;
    private String username;
    private String password;
    private String httpUsername = null;
    private String httpPassword = null;

    private URI loginEndpointUri;
    private URI tokenEndpointUri;
    private URI deleteEndpointUri;

    private CloseableHttpClient httpClient;
    private JsonObject userObject;

    public NdlaSearchIndexerSession(URI serverUrl, String username, String password) throws URISyntaxException {
        this.serverUrl = serverUrl;
        this.username = username;
        this.password = password;

        this.loginEndpointUri = getUri(loginEndpoint);
        this.tokenEndpointUri = getUri(tokenEndpoint);
        this.deleteEndpointUri = getUri(deleteEndpoint);
    }

    public URI getUri(String path) throws URISyntaxException {
        String base = serverUrl.toString();
        if (path.startsWith("/")) {
            if (base.endsWith("/")) {
                path = path.substring(1);
            }
        } else {
            if (!base.endsWith("/")) {
                if (path.length() > 0) {
                    path = "/" + path;
                }
            }
        }
        return new URI(base + path);
    }

    private String getCsrfHeader() throws ClientProtocolException, IOException {
        HttpGet get = new HttpGet(tokenEndpointUri);
        String csrf = executeStringRequest(get);
        return csrf;
    }

    public void login() throws ClientProtocolException, IOException {
        HttpPost post = new HttpPost(loginEndpointUri);
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        formparams.add(new BasicNameValuePair("username", username));
        formparams.add(new BasicNameValuePair("password", password));
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
        post.setEntity(entity);
        JsonObject response = executeFirstJsonRequest(post);
        userObject = response;
    }
    public void delete(String id) throws ClientProtocolException, IOException {
        String uri = deleteEndpointUri.toString();
        if (!uri.endsWith("/")) {
            uri += "/";
        }
        uri += id;
        HttpDelete delete;
        try {
            delete = new HttpDelete(new URI(uri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("URI generating error", e);
        }
        JsonObject response = executeJsonRequest(delete);
    }
    public void deleteAll() throws ClientProtocolException, IOException {
        delete("all");
    }

    private ResponseHandler<String> getStringResponseHandler() {
        return new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                StatusLine statusLine = response.getStatusLine();
                HttpEntity entity = response.getEntity();
                if (statusLine.getStatusCode() >= 300) {
                    throw new HttpResponseException(
                            statusLine.getStatusCode(),
                            statusLine.getReasonPhrase());
                }
                if (entity == null) {
                    throw new ClientProtocolException("Response contains no content");
                }
                ContentType contentType = ContentType.getOrDefault(entity);
                Charset charset = contentType.getCharset();
                InputStream input = entity.getContent();
                int total = 0;
                Collection<byte[]> chunks = new ArrayList<byte[]>();
                while (true) {
                    byte[] tmp = new byte[4096];
                    int len = input.read(tmp);
                    if (len < 0)
                        break;
                    if (len == 0)
                        continue;
                    byte[] chunk;
                    if (len == tmp.length) {
                        chunk = tmp;
                    } else {
                        chunk = new byte[len];
                        System.arraycopy(tmp, 0, chunk, 0, len);
                    }
                    chunks.add(chunk);
                    total += len;
                }
                byte[] complete = new byte[total];
                int off = 0;
                for (byte[] chunk : chunks) {
                    System.arraycopy(chunk, 0, complete, off, chunk.length);
                    off += chunk.length;
                }
                if (charset == null) {
                    charset = Charset.defaultCharset();
                }
                String str = new String(complete, charset);
                return str;
            }
        };
    }
    private ResponseHandler<JsonObject> getJsonResponseHandler() {
        return new ResponseHandler<JsonObject>() {
            @Override
            public JsonObject handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                StatusLine statusLine = response.getStatusLine();
                HttpEntity entity = response.getEntity();
                if (statusLine.getStatusCode() >= 300) {
                    throw new HttpResponseException(
                            statusLine.getStatusCode(),
                            statusLine.getReasonPhrase());
                }
                if (entity == null) {
                    throw new ClientProtocolException("Response contains no content");
                }
                ContentType contentType = ContentType.getOrDefault(entity);
                Charset charset = contentType.getCharset();
                JsonReader jsonReader = Json.createReader(entity.getContent());
                return jsonReader.readObject();
            }
        };
    }
    private String executeStringRequest(HttpUriRequest request) throws ClientProtocolException, IOException {
        CloseableHttpClient client = this.httpClient;
        ResponseHandler<String> responseHandler = getStringResponseHandler();
        synchronized (this) {
            return client.execute(request, responseHandler);
        }
    }
    public JsonObject executeJsonRequest(HttpUriRequest request) throws ClientProtocolException, IOException {
        request.addHeader("X-CSRF-Token", getCsrfHeader());
        CloseableHttpClient client = this.httpClient;
        ResponseHandler<JsonObject> responseHandler = getJsonResponseHandler();
        synchronized (client) {
            return client.execute(request, responseHandler);
        }
    }
    private JsonObject executeFirstJsonRequest(HttpUriRequest request) throws ClientProtocolException, IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        ResponseHandler<JsonObject> responseHandler = getJsonResponseHandler();
        boolean hasCredentials = false;
        while (true) {
            try {
                JsonObject jsonObject = client.execute(request, responseHandler);
                this.httpClient = client;
                return jsonObject;
            } catch (HttpResponseException e) {
                if (e.getStatusCode() == 401) {
                    if (hasCredentials)
                        throw e;
                    hasCredentials = true;
                    /* retry with username and password */
                    CredentialsProvider credsProvider = new BasicCredentialsProvider();
                    credsProvider.setCredentials(new AuthScope(request.getURI().getHost(), request.getURI().getPort()), new UsernamePasswordCredentials(getHttpUsername(), getHttpPassword()));
                    client = HttpClients.custom()
                            .setDefaultCredentialsProvider(credsProvider)
                            .build();
                } else {
                    throw e;
                }
            }
        }
    }

    public String getHttpUsername() {
        return httpUsername;
    }

    public void setHttpUsername(String httpUsername) {
        this.httpUsername = httpUsername;
    }

    public String getHttpPassword() {
        return httpPassword;
    }

    public void setHttpPassword(String httpPassword) {
        this.httpPassword = httpPassword;
    }
}
