package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.UdirLevel;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaLevel;
import org.mycurriculum.data.tms.domain.OntopiaLevelImpl;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class LevelsImport extends UdirObjectImporter<UdirLevel> {
    private Parser parser;
    public LevelsImport(Parser parser, List<UdirLevel> levels, TopicMapLocator dst, ProgressBit progress)
    {
        super(parser, levels, dst, progress);
        this.parser = parser;
    }
    public static String getUdirId(UdirLevel level) {
        String psi = level.getId();
        String prefix = "http://psi.udir.no/laereplan/aarstrinn/";
        if (psi.length() <= prefix.length())
            return psi;
        if (psi.substring(0, prefix.length()).equals(prefix))
            return psi.substring(prefix.length());
        return psi;
    }
    public static URL generatePsi(UdirLevel level) {
        return OntopiaLevelImpl.generatePsi(getUdirId(level));
    }

    public void parse(UdirLevel src, TopicMapRoot dst) throws IOException {
        URL levelPsi = generatePsi(src);
        OntopiaLevel level = dst.getLevel(levelPsi);
        if (level == null) {
            level = dst.getLevelBuilder()
                    .psi(levelPsi.toString())
                    .psi(src.getUrlPsi())
                    .build();
        }
        level.setHumanId(getUdirId(src));
        String defaultName = src.getTranslatedTitle().getDefaultText();
        for (Locale lang : src.getTranslatedTitle().getLanguages()) {
            String text = src.getTranslatedTitle().getText(lang);
            OntopiaTopic.Name name = level.setName(lang, text);
            if (defaultName != null && text.equals(defaultName)) {
                name.setLanguageNeutral(true);
                defaultName = null;
            }
        }
        if (defaultName != null)
            level.setName(defaultName);
        level.save();
    }
}
