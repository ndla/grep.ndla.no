package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.*;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicType;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.multicore.TaskUnit;
import org.mycurriculum.data.util.multicore.TaskUnitRunner;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class CurriculumsImport extends UdirObjectImporter<UdirCurriculumRef> {
    private Parser parser;
    private List<ParseContext> parseContexts = new ArrayList<ParseContext>();
    private TaskUnitRunner areaAimSetImports = new TaskUnitRunner(1);
    private TaskUnitRunner.Runner areaAimSetImportsRunner;
    private List<AreaAimSetImport> aimSetSubImporters = new ArrayList<AreaAimSetImport>();
    private AimSetImporter aimSetImporter;

    public CurriculumsImport(Parser parser, List<UdirCurriculumRef> srcList, TopicMapLocator dst, ProgressBit progress) {
        super(parser, srcList, dst, progress);
        this.parser = parser;
        ProgressBit progressBit = new ProgressBitImpl("Aim sets");
        this.aimSetImporter = new AimSetImporter(progressBit);
        this.aimSetImporter.addDependency(this);
    }

    public AimSetImporter getAimSetImporter() {
        return aimSetImporter;
    }

    public URL generatePsi(UdirCurriculumRef obj) throws IOException {
        UdirCurriculum curriculum = obj.getCurriculum();
        return OntopiaCurriculumImpl.generatePsi(curriculum.getKode());
    }
    public URL generatePsi(UdirCurriculum curriculum, int index) {
        return OntopiaAimSetImpl.generatePsi(curriculum.getKode() + "_" + index);
    }

    private final static int CURRICULUM_LOAD_INIT_WEIGHT = 1000;
    public void preParse(UdirCurriculumRef ref) throws IOException {
        UdirCurriculum curriculum = ref.getCurriculum();
        if (!curriculum.isPublished()) {
            return;
        }
        ParseContext parseContext = new ParseContext();
        parseContext.ref = ref;
        parseContexts.add(parseContext);
        parseContext.curriculumProgress = new ProgressBitImpl("Loading curriculum "+ref.getUrlPsi());
        parseContext.curriculumProgress.setExpected(CURRICULUM_LOAD_INIT_WEIGHT);
        getProgress().addSubBit(parseContext.curriculumProgress);
    }
    public ParseContext getContext(UdirCurriculumRef ref) {
        Iterator<ParseContext> iterator = parseContexts.iterator();
        if (!iterator.hasNext())
            throw new RuntimeException("No parser contexts available");
        ParseContext parseContext = iterator.next();
        if (parseContext.ref != ref)
            throw new RuntimeException("Parse context out of order");
        iterator.remove();
        return parseContext;
    }
    class Mapping<K,V> implements Map.Entry<K,V> {
        private K key;
        private V value;

        public Mapping(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V old = this.value;
            this.value = value;
            return old;
        }
    }
    private void deployAimLoad(ParseContext parseContext, UdirCurriculum udirCurriculum, OntopiaCurriculum curriculum, TopicMapRoot dst) throws IOException {
        int index = 0;
        List<UdirAimsByArea> importList = new ArrayList<UdirAimsByArea>();
        for (UdirAimSet aimSet : udirCurriculum.getAimSets()) {
            List<UdirAimsByArea> aimSets = new ArrayList<UdirAimsByArea>();
            for (UdirAimsByArea aimsByArea : aimSet.getAimsByArea()) {
                /* This is the aim set (type = by main area) */
                aimSets.add(aimsByArea);
            }
            ProgressBit progress = new ProgressBitImpl("Importing competence aim set");
            progress.setExpected(100);
            /* Create aim set */
            URL aimSetPsi = generatePsi(udirCurriculum, index);
            String id = udirCurriculum.getKode()+"_"+index;
            index++;
            OntopiaAimSet tmAimSet = dst.getAimSet(aimSetPsi);
            if (tmAimSet == null) {
                tmAimSet = dst.getAimSetBuilder()
                    .psi(aimSetPsi.toString())
                    .upstreamId(aimSet.getId())
                    .build();
            }
            tmAimSet.setSetType("COMEPTENCEAIMSET");
            UdirLevelRef[] levelRefs = aimSet.getLevelRefs();
            UdirLevel level;
            for (UdirLevelRef levelRef : levelRefs) {
                try {
                    level = levelRef.getLevel();
                } catch (IOException e) {
                    level = null;
                    System.out.println("ERROR: Level error on curriculum "+udirCurriculum.getKode()+" "+udirCurriculum.getTranslatedTitle().getDefaultText()+": "+e.getMessage());
                }
                if (level != null) {
                    OntopiaLevel tmLevel = dst.getLevel(new URL(level.getUrlPsi()));
                    if (tmLevel != null && !tmAimSet.getLevels().contains(tmLevel)) {
                        tmAimSet.addLevel(tmLevel);
                    }
                }
            }
            tmAimSet.setHumanId(id);
            if (!tmAimSet.getCurricula().contains(curriculum)) {
                tmAimSet.addCurriculum(curriculum);
            }
            tmAimSet.setSortingOrder(new Integer(aimSet.getSortingOrder()));
            importTitle(tmAimSet, aimSet.getTranslatedTitle());
            tmAimSet.save();
            List<Map.Entry<OntopiaAimSet,UdirAimsByArea>> mappings = new ArrayList<Map.Entry<OntopiaAimSet,UdirAimsByArea>>();
            for (UdirAimsByArea aimsByArea : aimSets) {
                mappings.add(new Mapping<OntopiaAimSet,UdirAimsByArea>(tmAimSet, aimsByArea));
            }
            AreaAimSetImport importer = new AreaAimSetImport(getParser(), udirCurriculum, mappings, getTopicMapLocator(), progress);
            areaAimSetImports.add(importer);
            aimSetSubImporters.add(importer);
        }
    }
    @Override
    public void parse(UdirCurriculumRef ref, TopicMapRoot dst) throws IOException {
        new OntopiaTopicType("http://psi.udir.no/ontologi/fellesfag").createTopicIfNotExists(dst);
        new OntopiaTopicType("http://psi.udir.no/ontologi/felles_programfag").createTopicIfNotExists(dst);
        new OntopiaTopicType("http://psi.udir.no/ontologi/valgfritt_programfag").createTopicIfNotExists(dst);
        UdirCurriculum src = ref.getCurriculum();
        if (!src.isPublished()) {
            return;
        }
        ParseContext parseContext = getContext(ref);
        String curriculumType = src.getCurriculumType();
        String curriculumSetType;
        if (curriculumType.equals("http://psi.udir.no/ontologi/fellesfag")) {
            curriculumSetType = "COMMON";
        } else if (curriculumType.equals("http://psi.udir.no/ontologi/felles_programfag")) {
            curriculumSetType = "PROGRAM";
        } else if (curriculumType.equals("http://psi.udir.no/ontologi/valgfritt_programfag")) {
            curriculumSetType = "PROGRAMOPTIONAL";
        } else {
            throw new IOException("Unknown curriculum type: "+curriculumType);
        }
        OntopiaCurriculum.Builder builder = dst.getCurriculumBuilder()
            .psi(generatePsi(ref).toString())
            .psi(src.getUrlPsi());
        OntopiaCurriculum curriculum;
        curriculum = dst.getCurriculum(new URL(src.getUrlPsi()));
        if (curriculum == null)
            curriculum = builder.build();
        else {
            if (!curriculum.hasPsi(generatePsi(ref).toString()))
                curriculum.addPsi(generatePsi(ref));
        }
        curriculum.setType(curriculumType);
        curriculum.setHumanId(src.getKode());
        importTitle(curriculum, src.getTranslatedTitle());
        curriculum.save();
        deployAimLoad(parseContext, src, curriculum, dst);
        parseContext.curriculumProgress.setExpected(0);
        /* 1-1 curriculum set */
        String curriculumSetPsi = OntopiaCurriculumSetImpl.generatePsi(src.getKode()).toString();
        OntopiaCurriculumSet curriculumSet = dst.getCurriculumSet(src.getKode());
        if (curriculumSet == null) {
            curriculumSet = dst.getCurriculumSetBuilder()
                    .psi(curriculumSetPsi)
                    .name("Curriculum set for curriculum " + src.getKode())
                    .humanId(src.getKode())
                    .curriculum(curriculum)
                    .build();
            curriculumSet.setSetType(curriculumSetType);
            curriculumSet.save();
        }
        /* course structure */
        OntopiaCourseStructure courseStructure = dst.getCourseStructure(src.getKode());
        if (courseStructure == null) {
            courseStructure = dst.getCourseStructureBuilder()
                    .psi(OntopiaCourseStructureImpl.generatePsi(src.getKode()).toString())
                    .humanId(src.getKode())
                    .build();
            courseStructure.addCurriculumSet(curriculumSet);
            courseStructure.setSetType("CURRICULUM");
            importTitle(courseStructure, src.getTranslatedTitle());
            courseStructure.save();
        }
    }

    private static class ParseContext {
        public UdirCurriculumRef ref;
        /* */
        ProgressBit curriculumProgress;
    }

    @Override
    public void run() throws IOException {
        getProgress().setStartTime();
        for (UdirCurriculumRef ref : getSrcList())
            preParse(ref);
        super.run();
    }

    public class AimSetImporter implements TaskUnit {
        private ProgressBit progress;
        private Set<TaskUnit> dependencies = new HashSet<TaskUnit>();

        public AimSetImporter(ProgressBit progress) {
            this.progress = progress;
            this.progress.setExpected(2);
        }

        public ProgressBit getProgress() {
            return progress;
        }

        @Override
        public void addDependency(TaskUnit dependency) {
            dependencies.add(dependency);
        }

        @Override
        public Set<TaskUnit> getDependencies() {
            return dependencies;
        }

        @Override
        public void run() throws IOException {
            this.progress.setStartTime();
            this.progress.setDone(1);
            areaAimSetImportsRunner = areaAimSetImports.start();
            try {
                for (AreaAimSetImport importer : aimSetSubImporters) {
                    this.progress.addSubBit(importer.getProgress());
                    areaAimSetImports.add(importer.getAimsImporter());
                }
            } finally {
                areaAimSetImportsRunner.setFinishedAdding();
                areaAimSetImportsRunner.waitForTasks();
            }
            this.progress.setDone(2);
        }
    }
}
