package org.mycurriculum.data.util.parser;

import java.io.PrintStream;
import java.util.*;

/**
 * Created by janespen on 5/16/14.
 */
public class MultithreadedTextProgressOutputter implements ProgressTracker.Listener {
    public interface Listener {
        public void newProgressText(String text);
        public void newLogLine(String textLine);
    }
    public static class StreamOutputter implements Listener {
        private PrintStream stream;
        private String lastText = "";

        public StreamOutputter(PrintStream stream) {
            this.stream = stream;
        }

        @Override
        public void newProgressText(String inputText) {
            String text = "\r" + inputText;
            String output = text;
            if (text.length() < lastText.length()) {
                char[] padddingCh = new char[lastText.length() - text.length()];
                for (int i = 0; i < padddingCh.length; i++)
                    padddingCh[i] = ' ';
                output += new String(padddingCh);
            }
            stream.print(output);
            lastText = text;
        }

        @Override
        public void newLogLine(String textLine) {
            String text = "\r" + textLine;
            if (text.length() < lastText.length()) {
                char[] padddingCh = new char[lastText.length() - text.length()];
                for (int i = 0; i < padddingCh.length; i++)
                    padddingCh[i] = ' ';
                text += new String(padddingCh);
            }
            lastText = "";
            stream.println(text);
        }
    }
    private Listener outputListener;
    private ProgressTracker progressTracker;
    private Set<ProgressBit> completed = new HashSet<ProgressBit>();

    public MultithreadedTextProgressOutputter(ProgressTracker progressTracker, Listener outputListener) {
        this.outputListener = outputListener;
        this.progressTracker = progressTracker;
    }
    public MultithreadedTextProgressOutputter(ProgressTracker progressTracker) {
        this(progressTracker, new StreamOutputter(System.out));
    }

    private Collection<ProgressBit> getRunning(ProgressBit container) {
        Collection<ProgressBit> bits = new ArrayList<ProgressBit>();
        for (ProgressBit sub : container.getSubBits()) {
            long done = sub.getTotalDone();
            long expected = sub.getTotalExpected();
            if (done > 0 && done < expected)
                bits.add(sub);
            else if (expected == done && expected > 0) {
                if (!completed.contains(sub)) {
                    completed.add(sub);
                    Date now = new Date();
                    Date started = sub.getStartedTime();
                    long elapsed = now.getTime() - started.getTime();

                    String text = "Completed: " + sub.getName() + " in " + formatTimeCounterHMS(elapsed / 1000) + " between "+ started + " and " + now;
                    outputListener.newLogLine(text);
                }
            }
        }
        return bits;
    }
    private static String getThreadSummary(Collection<ProgressBit> bits) {
        String text = "";
        for (ProgressBit bit : bits) {
            long done100 = bit.getTotalDone() * 100;
            long expected = bit.getTotalExpected();
            if (expected > 0) {
                long percent = done100 / expected;
                text += " ["+percent + "%" + " " + bit.getName()+"]";
            }
        }
        return text;
    }
    private static String formatTimeCounterHMS(long tmSec) {
        long hour, min, sec;
        hour = tmSec / 3600;
        min = (tmSec % 3600) / 60;
        sec = tmSec % 60;
        String h = Integer.toString((int) hour);
        String m = Integer.toString((int) min);
        String s = Integer.toString((int) sec);
        if (h.length() == 1)
            h = "0"+h;
        if (m.length() == 1)
            m = "0"+m;
        if (s.length() == 1)
            s = "0"+s;
        return h+":"+m+":"+s;
    }
    private static String getEstimatedTime(ProgressBit bit) {
        Date etc = bit.getMetaEstimatedFinish();
        if (etc == null)
            return "";
        Date now = new Date();
        long remaining = etc.getTime() - now.getTime();
        remaining /= 1000;
        return formatTimeCounterHMS(remaining);
    }
    private static String getElapsedTime(ProgressBit bit) {
        Date started = bit.getStartedTime();
        if (started == null)
            return "";
        Date now = new Date();
        long elapsed = now.getTime() - started.getTime();
        return formatTimeCounterHMS(elapsed / 1000);
    }

    @Override
    public void progressUpdate() {
        long done100 = progressTracker.getTotalDone();
        long expected = progressTracker.getTotalExpected();
        if (expected > 0) {
            done100 *= 100;
            long percent = done100 / expected;
            String estimated = getEstimatedTime(progressTracker);
            if (estimated != null && estimated.length() > 0)
                estimated = " est " + estimated;
            else
                estimated = "";
            String text = "" + percent + "%" + getThreadSummary(getRunning(progressTracker)) + " " + getElapsedTime(progressTracker) + estimated;
            outputListener.newProgressText(text);
        } else if (done100 == expected)
            getRunning(progressTracker); /* Make sure that we output all completion lines */
    }
}
