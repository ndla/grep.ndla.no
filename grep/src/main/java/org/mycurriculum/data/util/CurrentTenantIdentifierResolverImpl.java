package org.mycurriculum.data.util;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.mycurriculum.business.domain.UserDetailsAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantId = null;

        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetailsAdapter) {
                UserDetailsAdapter userDetails = (UserDetailsAdapter) authentication.getPrincipal();
                tenantId = userDetails.getUser().getUsername(); // TODO: Review.
            } else {
                tenantId = (authentication.getPrincipal().toString());
            }
        } else {
            return null;
        }

        return tenantId;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
