package org.mycurriculum.data.util.searchindexer.ndla;

import java.net.URI;

public interface NdlaSearchIndexerConfig {
    public URI getServiceUri();
    public String getServiceUsername();
    public String getServicePassword();
    public String getHttpUsername();
    public String getHttpPassword();
}
