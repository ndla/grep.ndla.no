package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.UdirCurriculum;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumRef;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaAimSet;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaCurriculum;
import org.mycurriculum.data.tms.domain.OntopiaLevel;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CurriculumLevelImporter extends UdirObjectImporter<UdirCurriculumRef> {
    public CurriculumLevelImporter(Parser parser, List<UdirCurriculumRef> srcList, TopicMapLocator dst, ProgressBit progress) {
        super(parser, srcList, dst, progress);
    }

    public void insertAimSetLevels(OntopiaAimSet aimSet, Set<OntopiaLevel> levels) throws IOException {
        Collection<OntopiaLevel> hasLevels = aimSet.getLevels();
        boolean changed = false;
        for (OntopiaLevel level : levels) {
            if (!hasLevels.contains(level)) {
                aimSet.addLevel(level);
                changed = true;
            }
        }
        if (changed)
            aimSet.save();
    }
    public void mergeLevels(Set<OntopiaLevel> levelSet, Collection<OntopiaLevel> levels) {
        for (OntopiaLevel level : levels) {
            if (!levelSet.contains(level))
                levelSet.add(level);
        }
    }
    public Set<OntopiaLevel> gatherLevels(OntopiaAimSet aimSet) throws IOException {
        Set<OntopiaLevel> levels = new HashSet<OntopiaLevel>();
        mergeLevels(levels, gatherLevels(aimSet.getAimSets()));
        insertAimSetLevels(aimSet, levels);
        mergeLevels(levels, aimSet.getLevels());
        return levels;
    }
    public Set<OntopiaLevel> gatherLevels(Collection<OntopiaAimSet> aimSets) throws IOException {
        Set<OntopiaLevel> levels = new HashSet<OntopiaLevel>();
        for (OntopiaAimSet aimSet : aimSets) {
            mergeLevels(levels, gatherLevels(aimSet));
        }
        return levels;
    }
    @Override
    public void parse(UdirCurriculumRef ref, TopicMapRoot dst) throws IOException {
        UdirCurriculum src = ref.getCurriculum();
        if (!src.isPublished()) {
            return;
        }
        OntopiaCurriculum curriculum;
        curriculum = dst.getCurriculum(new URL(src.getUrlPsi()));
        if (curriculum == null)
            throw new IOException("Missing curriculum during import: "+src.getUrlPsi());
        Set<OntopiaLevel> levels = gatherLevels(curriculum.getAimSets());
        Collection<OntopiaLevel> hasLevels = curriculum.getLevels();
        boolean changed = false;
        for (OntopiaLevel level : levels) {
            if (!hasLevels.contains(level)) {
                curriculum.addLevel(level);
                changed = true;
            }
        }
        if (changed) {
            curriculum.save();
        }
        /* Add levels to the course structure as well */
        String humanId = curriculum.getHumanId();
        OntopiaCourseStructure courseStructure = dst.getCourseStructure(humanId);
        hasLevels = courseStructure.getLevels();
        changed = false;
        for (OntopiaLevel level : levels) {
            if (!hasLevels.contains(level)) {
                courseStructure.addLevel(level);
                changed = true;
            }
        }
        if (changed) {
            courseStructure.save();
        }
    }
}
