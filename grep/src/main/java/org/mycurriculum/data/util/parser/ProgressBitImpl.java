package org.mycurriculum.data.util.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class ProgressBitImpl implements ProgressBit, ProgressBit.Listener {
    private String name;
    private volatile long expected = 100;
    private volatile long done = 0;
    private volatile float weight = 1.0f;
    private Collection<ProgressBit> subBits = new ArrayList<ProgressBit>();
    private Collection<ProgressBit.Listener> listeners = new ArrayList<ProgressBit.Listener>();
    private Date start = new Date();
    private boolean startTimeSet = false;

    public ProgressBitImpl(String name)
    {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public long getTotalExpected() {
        long cumulative = 0;
        Collection<ProgressBit> subBits = getSubBits();
        for (ProgressBit subBit : subBits) {
            cumulative += subBit.getTotalExpected();
        }
        float w = (float) (expected + cumulative);
        w *= weight;
        return (long) w;
    }

    @Override
    public long getTotalDone() {
        long cumulative = 0;
        Collection<ProgressBit> subBits = getSubBits();
        for (ProgressBit subBit : subBits) {
            cumulative += subBit.getTotalDone();
        }
        float w = (float) (done + cumulative);
        w *= weight;
        return (long) w;
    }

    @Override
    public void setExpected(long expected) {
        this.expected = expected;
    }

    private Date ratelimiterDate = null;
    @Override
    public void setDone(long done) {
        Date now = new Date();
        boolean fireUpdate;
        synchronized (this) {
            this.done = done;
            if (this.done == this.expected || ratelimiterDate == null)
                fireUpdate = true;
            else {
                long diff = now.getTime() - ratelimiterDate.getTime();
                if (diff < -1000 || diff > 1000)
                    fireUpdate = true;
                else
                    fireUpdate = false;
            }
            if (fireUpdate)
                ratelimiterDate = now;
        }
        if (fireUpdate)
            progressUpdate();
    }

    @Override
    public void addSubBit(ProgressBit bit) {
        synchronized (this) {
            subBits.add(bit);
        }
        bit.addListener(new Listener(this, bit) {
            @Override
            public void progressUpdate() {
                if (getParent() == null)
                    return;
                if (subBits.contains(getBit()))
                    getParent().progressUpdate();
                else {
                    getBit().removeListener(this);
                    setParent(null); /* unref */
                    setBit(null);
                }
            }
        });
    }

    @Override
    public Collection<ProgressBit> getSubBits() {
        synchronized (this) {
            ArrayList<ProgressBit> cp = new ArrayList<ProgressBit>(subBits.size());
            cp.addAll(subBits);
            return cp;
        }
    }

    @Override
    public void removeSubBit(ProgressBit bit) {
        synchronized (this) {
            subBits.remove(bit);
        }
    }

    @Override
    public ProgressBit createBit(String name) {
        return new ProgressBitImpl(name);
    }

    @Override
    public void addListener(ProgressBit.Listener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(ProgressBit.Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void setStartTime() {
        if (!startTimeSet) {
            startTimeSet = true;
            start = new Date();
        }
    }

    @Override
    public Date getStartedTime() {
        return start;
    }

    @Override
    public Date getEstimatedFinish() {
        Date now = new Date();
        long elapsed = (now.getTime() / 1000) - (start.getTime() / 1000);
        if (elapsed > 30) {
            float steptime = ((float) elapsed) / ((float) getTotalDone());
            float remainingTime = ((float) (getTotalExpected() - getTotalDone())) * steptime;
            Date finishTime = new Date(now.getTime() + (((long) remainingTime) * 1000));
            return finishTime;
        }
        return null;
    }

    private final static int ESTIMATE_LOW_ADJUST = 80;
    private final static int ESTIMATE_LOW_THRESH = 90;
    private final static int ESTIMATE_HIGH_THRESH = 110;
    private final static int ESTIMATE_HIGH_ADJUST = 125;
    private final static int ESTIMATE_MARGIN = 15;
    private final static int REFERENCE_TIME_FRAME = 60000;
    private Date previousEstimate = null;
    private Date steppingStart = null;
    private int stepDirection;
    private Date applyEstimateMargin(Date input) {
        long time = input.getTime() - start.getTime();
        time *= (100 + ESTIMATE_MARGIN) / 100;
        return new Date(start.getTime() + time);
    }
    @Override
    public Date getMetaEstimatedFinish() {
        if (previousEstimate == null) {
            previousEstimate = getEstimatedFinish();
            return previousEstimate;
        }
        Date estimate = getEstimatedFinish();
        Date now = new Date();
        Date reference = start;
        if ((now.getTime() - reference.getTime()) > REFERENCE_TIME_FRAME) {
            reference = new Date(now.getTime() - REFERENCE_TIME_FRAME);
        }
        long timeDiff = estimate.getTime() - reference.getTime();
        long previousTotal = previousEstimate.getTime() - reference.getTime();
        Date usingPreviousEstimate = previousEstimate;
        if (steppingStart != null) {
            long step = now.getTime() - steppingStart.getTime();
            step /= 4;
            previousTotal += (step * stepDirection);
            usingPreviousEstimate = new Date(reference.getTime() + previousTotal);
        }
        long lowAdjust = (previousTotal * ESTIMATE_LOW_ADJUST) / 100;
        long lowStep = (previousTotal * ESTIMATE_LOW_THRESH) / 100;
        long highStep = (previousTotal * ESTIMATE_HIGH_THRESH) / 100;
        long highAdjust = (previousTotal * ESTIMATE_HIGH_ADJUST) / 100;
        if (timeDiff < lowAdjust || timeDiff > highAdjust) {
            steppingStart = null;
            stepDirection = 0;
            previousEstimate = estimate;
            return estimate;
        }
        if (stepDirection < 0) {
            if (previousTotal < timeDiff) {
                steppingStart = null;
                stepDirection = 0;
                previousEstimate = usingPreviousEstimate;
                return usingPreviousEstimate;
            }
            return usingPreviousEstimate;
        }
        if (stepDirection > 0) {
            if (previousTotal > timeDiff) {
                steppingStart = null;
                stepDirection = 0;
                previousEstimate = usingPreviousEstimate;
                return usingPreviousEstimate;
            }
            return usingPreviousEstimate;
        }
        if (timeDiff < lowStep) {
            stepDirection = -1;
            steppingStart = new Date();
        } else if (timeDiff > highStep) {
            stepDirection = 1;
            steppingStart = new Date();
        }
        return usingPreviousEstimate;
    }

    @Override
    public void progressUpdate() {
        for (ProgressBit.Listener listener : listeners) {
            listener.progressUpdate();
        }
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public void setWeight(float weight) {
        this.weight = weight;
    }

    public static abstract class Listener implements ProgressBit.Listener {
        private ProgressBitImpl parent;
        private ProgressBit bit;

        public Listener(ProgressBitImpl parent, ProgressBit bit) {
            this.parent = parent;
            this.bit = bit;
        }

        public ProgressBitImpl getParent() {
            return parent;
        }

        public void setParent(ProgressBitImpl parent) {
            this.parent = parent;
        }

        public ProgressBit getBit() {
            return bit;
        }

        public void setBit(ProgressBit bit) {
            this.bit = bit;
        }
    }
}
