package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.TranslatedText;
import org.mycurriculum.data.util.multicore.TaskUnit;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public abstract class UdirObjectImporter<T> implements TaskUnit {
    private Parser parser;
    private List<T> srcList;
    private TopicMapLocator dst;
    private ProgressBit progress;
    private Set<TaskUnit> dependencies = new HashSet<TaskUnit>();

    public UdirObjectImporter(Parser parser, List<T> srcList, TopicMapLocator dst, ProgressBit progress) {
        this.parser = parser;
        this.srcList = srcList;
        this.dst = dst;
        this.progress = progress;
    }
    public abstract void parse(T src, TopicMapRoot dst) throws IOException;

    public ProgressBit getProgress() {
        return progress;
    }

    public List<T> getSrcList() {
        return srcList;
    }

    public TopicMapLocator getTopicMapLocator() {
        return dst;
    }

    @Override
    public void addDependency(TaskUnit dependency) {
        dependencies.add(dependency);
    }

    @Override
    public Set<TaskUnit> getDependencies() {
        return dependencies;
    }

    public void run() throws IOException {
        TopicMapRoot dst = this.dst.getTopicMapRoot();

        int i = 0;
        progress.setStartTime();
        progress.setExpected(srcList.size());
        progress.setDone(i);
        dst.openPersistentConnection();
        for (T ref : srcList) {
            parse(ref, dst);
            i++;
            progress.setDone(i);
        }
        dst.closePersistentConnection();
    }

    public void importTitle(OntopiaTopic topic, TranslatedText title) {
        String defaultName = title.getDefaultText();
        for (Locale lang : title.getLanguages()) {
            String text = title.getText(lang);
            if (defaultName == null || !defaultName.equals(text)) {
                topic.setName(lang, text);
            } else {
                topic.setName(lang, text).setLanguageNeutral(true);
                defaultName = null;
            }
        }
        if (defaultName != null) {
            topic.setName(defaultName);
        }
    }

    public Parser getParser() {
        return parser;
    }
}
