package org.mycurriculum.data.util.oldgrep;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.*;
import net.ontopia.topicmaps.query.core.*;
import net.ontopia.topicmaps.query.utils.QueryUtils;
import org.mycurriculum.data.cache.CacheDaoNullImpl;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.connections.TopicMapStore;
import org.mycurriculum.data.tms.connections.TopicMapStoreLocator;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.exceptions.InvalidDataException;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.mycurriculum.data.util.MutableTranslatedText;
import org.mycurriculum.data.util.MutableTranslatedTextImpl;
import org.mycurriculum.data.util.parser.ProgressBit;
import org.mycurriculum.data.util.parser.ProgressTracker;
import org.mycurriculum.data.util.parser.ProgressTrackerImpl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class OldGrepResourceImporter {
    public final static int RECONNECT_INTERVAL = 2000;

    public void setRelationImportHack(RelationImportHack relationImportHack) {
        this.relationImportHack = relationImportHack;
    }

    public interface RelationImportHack {
        public Info filter(TopicMapLocator destinationLocator, String resourcePsi, String origId, String curriculumId, String aimSetId, String aimId) throws IOException;
        public interface Info {
            public String getCurriculum();
            public String getAimSet();
            public String getAimId();
        }
    }

    private RelationImportHack relationImportHack = null;
    private OldGrepTopicFilter filter;
    private TopicMapStoreLocator storeLocator = null;
    private ProgressTracker progress = new ProgressTrackerImpl("Old grep resources");
    private ProgressBit parseProgress = progress.createBit("Parser");
    private long progressCount;

    public ProgressTracker getProgress() {
        return progress;
    }

    private static class Reifier {
        public String aimId;
        /**/
        public String username = null; /* http://psi.topic.ndla.no/#bruker-navn */
        public URL userUrl = null; /* http://psi.topic.ndla.no/#bruker-url */
        public boolean relevantForApprentices = false; /* http://psi.topic.ndla.no/#apprentice */
        public Date timestamp; /* unix timestamp: http://psi.topic.ndla.no/#timestamp */
        public String curriculumId;
        public String aimSetId;
        /**/
        public String origIdString;
        /**/
        OntopiaAim aim;
    }
    private static class Resource {
        public URL psi;
        public String type = null;
        public boolean subjectPage = false;
        public String name;
        public String nameLang = null;
        public String authorUrl = null;
        public Set<String> extraAuthorUrls;
        public String author = null;
        public Set<String> extraAuthors;
        public Map<String,Collection<String>> usufructs;
        public String ingress = null;
        public boolean published = true;
        public Collection<Reifier> relatedAims;
        public Set<String> relatedCurriculums;

        public Resource() {
            usufructs = new HashMap<String,Collection<String>>();
            extraAuthors = new HashSet<String>();
            extraAuthorUrls = new HashSet<String>();
            relatedAims = new ArrayList<Reifier>();
            relatedCurriculums = new HashSet<String>();
        }
    }
    private static final Resource poison = new Resource() {
        @Override
        public String toString() {
            return "Botox 1mg";
        }
    };
    private class ResourceImporter implements Runnable {

        private BlockingQueue<Resource> inputQueue;
        private BlockingQueue<Resource> notFoundQueue;
        private TopicMapLocator dst;
        private ProgressBit progress;

        public ResourceImporter(BlockingQueue<Resource> inputQueue, BlockingQueue<Resource> notFoundQueue, TopicMapLocator dst, ProgressBit progress) {
            this.inputQueue = inputQueue;
            this.notFoundQueue = notFoundQueue;
            this.dst = dst;
            this.progress = progress;
        }

        @Override
        public void run() {
            int reconnect = RECONNECT_INTERVAL;
            TopicMapRoot dst;
            try {
                dst = this.dst.getTopicMapRoot();
                dst.openPersistentConnection();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            try {
                while (true) {
                    if (reconnect <= 0) {
                        dst.closePersistentConnection();
                        dst.openPersistentConnection();
                        reconnect = RECONNECT_INTERVAL;
                    }
                    reconnect--;
                    Resource resource = inputQueue.take();
                    if (resource == poison)
                        break;
                    boolean abortResource = false;
                    if (resource.relatedAims.size() > 0) {
                        for (Reifier reifier : resource.relatedAims) {
                            String aimCode = reifier.aimId;
                            OntopiaAim aim = dst.getAim(aimCode);
                            if (aim == null) {
                                if (notFoundQueue != null) {
                                    notFoundQueue.put(resource);
                                    abortResource = true;
                                    break;
                                } else {
                                    System.out.println("ERROR: Aim with code " + aimCode + " was not found for " + resource.psi + " : " + resource.name);
                                    aim = dst.getAimBuilder()
                                            .psi(OntopiaAimImpl.generatePsi(aimCode).toString())
                                            .id(aimCode)
                                            .humanId(aimCode)
                                            .name("Aim not found " + aimCode)
                                            .build();
                                    aim.save();
                                }
                            }
                            reifier.aim = aim;
                        }
                    }
                    if (abortResource)
                        continue;
                    OntopiaResource r;
                    r = dst.getResource(resource.psi);
                    if (r == null) {
                        r = dst.getResourceBuilder()
                                .psi(resource.psi.toString())
                                .build();
                    }
                    Locale locale = null;
                    if (resource.nameLang != null) {
                        locale = new Locale(resource.nameLang);
                        r.setName(locale, resource.name).setLanguageNeutral(true);
                    } else {
                        r.setName(resource.name).setLanguageNeutral(true);
                    }
                    if (resource.author != null) {
                        if (resource.extraAuthors.size() == 0) {
                            String[] authors = {resource.author};
                            r.setAuthors(authors);
                        } else {
                            List<String> authorList = new ArrayList<String>();
                            authorList.add(resource.author);
                            for (String author : resource.extraAuthors) {
                                authorList.add(author);
                            }
                            String[] authors = new String[authorList.size()];
                            int i = 0;
                            for (String author : authorList) {
                                authors[i++] = author;
                            }
                            r.setAuthors(authors);
                        }
                    }
                    if (resource.authorUrl != null) {
                        if (resource.extraAuthorUrls.size() == 0) {
                            String[] authorUrls = {resource.authorUrl};
                            r.setAuthorUrls(authorUrls);
                        } else {
                            List<String> authorUrlList = new ArrayList<String>();
                            authorUrlList.add(resource.authorUrl);
                            for (String authorUrl : resource.extraAuthorUrls) {
                                authorUrlList.add(authorUrl);
                            }
                            String[] authorUrls = new String[authorUrlList.size()];
                            int i = 0;
                            for (String authorUrl : authorUrlList) {
                                authorUrls[i++] = authorUrl;
                            }
                            r.setAuthorUrls(authorUrls);
                        }
                    }
                    if (resource.type != null) {
                        r.setNodeType(resource.type);
                    }
                    if (resource.ingress != null) {
                        if (locale != null)
                            r.setIngress(new MutableTranslatedTextImpl(locale, resource.ingress));
                        else
                            r.setIngress(new MutableTranslatedTextImpl(resource.ingress));
                    }
                    if (resource.usufructs != null && resource.usufructs.size() > 0) {
                        Locale defaultLocale = null;
                        String defaultText = null;
                        Map<Locale,String> localeMap = new HashMap<Locale,String>();
                        for (Map.Entry<String,Collection<String>> entry : resource.usufructs.entrySet()) {
                            String licenseUrl = entry.getKey();
                            Collection<String> psis = entry.getValue();
                            Collection<Locale> locales = new ArrayList<Locale>();
                            boolean langNeutral = false;
                            for (String psi : psis) {
                                if(psi.equals("http://psi.topic.ndla.no/#language-neutral"))
                                    langNeutral = true;
                                else {
                                    Locale localeObj = MutableTranslatedTextImpl.getLocaleFromPsi(psi);
                                    if (localeObj != null) {
                                        locales.add(localeObj);
                                    } else {
                                        System.out.println("Unrecognised locale psi: "+psi);
                                    }
                                }
                            }
                            if (langNeutral) {
                                if (locales.size() > 0)
                                    defaultLocale = locales.iterator().next();
                                else
                                    defaultText = licenseUrl;
                            }
                            for (Locale tLocale : locales) {
                                localeMap.put(tLocale, licenseUrl);
                            }
                        }
                        MutableTranslatedText textObj;
                        if (defaultLocale != null)
                            textObj = new MutableTranslatedTextImpl(localeMap, defaultLocale);
                        else if (defaultText != null)
                            textObj = new MutableTranslatedTextImpl(localeMap, defaultText);
                        else if (localeMap.size() > 0)
                            textObj = new MutableTranslatedTextImpl(localeMap);
                        else
                            textObj = null;
                        if (textObj != null) {
                            r.setLicense(textObj);
                        }
                    }
                    r.setStatus((resource.published ? "published" : "not-published"));
                    r.save();
                    if (resource.relatedAims.size() > 0) {
                        for (Reifier reifier : resource.relatedAims) {
                            OntopiaAim aim = reifier.aim;
                            OntopiaCurriculum curriculum = null;
                            if (reifier.curriculumId != null) {
                                curriculum = dst.getCurriculum(reifier.curriculumId);
                            }
                            OntopiaAimSet aimSet = null;
                            if (reifier.aimSetId != null) {
                                String prefix = reifier.curriculumId+"_";
                                String search = "_"+reifier.aimSetId;
                                Collection<OntopiaAimSet> aimSets = aim.getAimSets();
                                for (OntopiaAimSet candAimSet : aimSets) {
                                    String humanId = candAimSet.getHumanId();
                                    if (humanId.startsWith(prefix) && humanId.endsWith(search)) {
                                        aimSet = candAimSet;
                                        break;
                                    }
                                }
                            }
                            OntopiaResourceAimAssociation.Builder builder = r.buildAimAssociation(aim);
                            if (reifier.username != null) {
                                String[] authors = new String[1];
                                authors[0] = reifier.username;
                                builder.authors(authors);
                                if (reifier.userUrl != null) {
                                    String[] authorUrls = new String[1];
                                    authorUrls[0] = reifier.userUrl.toString();
                                    builder.authorUrls(authorUrls);
                                }
                            }
                            if (reifier.timestamp != null) {
                                builder.timestampDate(reifier.timestamp);
                            }
                            builder.apprenticeRelevant(reifier.relevantForApprentices);
                            if (curriculum != null) {
                                builder.curriculum(curriculum);
                            }
                            if (aimSet != null) {
                                builder.aimSet(aimSet);
                            }
                            builder.build().save();
                        }
                    }
                    synchronized (progress) {
                        progressCount++;
                        progress.setDone(progressCount);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InvalidDataException e) {
                e.printStackTrace();
            } finally {
                dst.closePersistentConnection();
            }
        }
    }

    public OldGrepResourceImporter(TopicMapStoreLocator storeLocator, OldGrepTopicFilter filter) {
        this.storeLocator = storeLocator;
        this.filter = filter;
    }

    private static Collection<URL> getSubjectIdentifiers(TopicIF topic) {
        Collection<URL> urls = new HashSet<URL>();
        Collection<LocatorIF> locators = topic.getSubjectIdentifiers();
        for (LocatorIF locator : locators) {
            try {
                urls.add(new URL(locator.getAddress()));
            } catch (MalformedURLException e) {
                throw new RuntimeException("The url is supposed to be valid", e);
            }
        }
        return urls;
    }
    public Thread parseResources(Collection<TopicIF> resources, final TopicMapLocator destinationLocator) throws IOException {
        TopicMapRoot destination = destinationLocator.getTopicMapRoot();
        destination.setCache(new CacheDaoNullImpl().getNamespace("null"));
        final Collection<Resource> parsedResources = new ArrayList<Resource>();
        final BlockingQueue<Resource> resourceQueue = new LinkedBlockingQueue<Resource>();
        final BlockingQueue<Resource> resourceWithAimsNotFound = new LinkedBlockingQueue<Resource>();
        final Thread[] resourceImporterThreads = new Thread[4];
        progressCount = 0;
        for (int i = 0; i < resourceImporterThreads.length; i++) {
            Thread resourceImporterThread = new Thread(new ResourceImporter(resourceQueue, resourceWithAimsNotFound, destinationLocator, parseProgress));
            resourceImporterThread.start();
            resourceImporterThreads[i] = resourceImporterThread;
        }
        String langPrefix = "http://psi.oasis-open.org/iso/639/#";
        try {
            for (TopicIF resource : resources) {
                Resource r = new Resource();
                Collection<TopicIF> types = resource.getTypes();
                String typePsiPrefix = "http://psi.topic.ndla.no/#";
                for (TopicIF type : types) {
                    Collection<URL> typePsis = getSubjectIdentifiers(type);
                    if (typePsis.size() != 1)
                        throw new IOException("Expected 1 type psi, found " + typePsis.size());
                    String typePsi = typePsis.iterator().next().toString();
                    if (typePsi.length() > typePsiPrefix.length() &&
                            typePsi.substring(0, typePsiPrefix.length()).equals(typePsiPrefix)) {
                        String typeName = typePsi.substring(typePsiPrefix.length());
                        if (typeName.equals("ndla-node") ||
                                typeName.equals("deling-node") ||
                                typeName.equals("nygiv-node")) {
                            r.type = typeName;
                        } else if (typeName.equals("fag")) {
                            r.subjectPage = true;
                        } else {
                            throw new IOException("Type name of resource not recognised: " + typeName);
                        }
                    } else {
                        throw new IOException("Type psi of resource not recognised: " + typePsi);
                    }
                }
                if (r.type == null)
                    throw new IOException("The resource has no type");
                Collection<URL> psis = getSubjectIdentifiers(resource);
                if (psis.size() != 1)
                    throw new IOException("Expected the number of psis to be 1, found " + psis.size());
                r.psi = psis.iterator().next();
                Iterator<TopicNameIF> names = resource.getTopicNames().iterator();
                if (!names.hasNext())
                    throw new IOException("Expected the number of names to be 1, found 0");
                TopicNameIF name = names.next();
                while (names.hasNext()) {
                    TopicNameIF extraName = names.next();
                    if (extraName.getValue().equals(name.getValue()))
                        continue;
                    System.out.println("IGNORED: " + name.getValue() + " has extra name " + extraName.getValue());
                }
                r.name = name.getValue();
                for (TopicIF nameScope : name.getScope()) {
                    Collection<LocatorIF> namePsis = nameScope.getSubjectIdentifiers();
                    if (namePsis.size() == 0)
                        continue;
                    if (namePsis.size() != 1)
                        throw new IOException("Expected the number of name scope psis to be 1, found " + namePsis.size());
                    String namePsi = namePsis.iterator().next().getAddress();
                    if (namePsi.substring(0, langPrefix.length()).equals(langPrefix)) {
                        r.nameLang = namePsi.substring(langPrefix.length());
                    } else {
                    }
                }
                Collection<OccurrenceIF> occurrences = resource.getOccurrences();
                for (OccurrenceIF occurrence : occurrences) {
                    Collection<LocatorIF> occPsis = occurrence.getType().getSubjectIdentifiers();
                    if (occPsis.size() != 1)
                        throw new IOException("Expected the number of psis to be 1, found " + occPsis.size());
                    String type = occPsis.iterator().next().getAddress();
                    if (type.equals("http://psi.topic.ndla.no/#node-author-url")) {
                        if (r.authorUrl == null) {
                            r.authorUrl = occurrence.getValue();
                        } else {
                            r.extraAuthorUrls.add(occurrence.getValue());
                        }
                    } else if (type.equals("http://psi.topic.ndla.no/#node-author")) {
                        if (r.author == null) {
                            r.author = occurrence.getValue();
                        } else {
                            r.extraAuthors.add(occurrence.getValue());
                        }
                    } else if (type.equals("http://psi.topic.ndla.no/#node-ingress")) {
                        if (r.ingress == null) {
                            r.ingress = occurrence.getValue();
                        } else if (!r.ingress.equals(occurrence.getValue())) {
                            System.out.println("Using: " + r.ingress);
                            System.out.println("Duplicate: " + occurrence.getValue());
                            System.out.println("IGNORED: Duplicate ingress");
                        }
                    } else if (type.equals("http://psi.topic.ndla.no/#node-usufruct")) {
                        Collection<TopicIF> topicScopes = occurrence.getScope();
                        Collection<String> scopePsis = new ArrayList<String>();
                        for (TopicIF scopeTopic : topicScopes) {
                            Collection<LocatorIF> scopeLocators = scopeTopic.getSubjectIdentifiers();
                            for (LocatorIF scopeLocator : scopeLocators) {
                                scopePsis.add(scopeLocator.getAddress());
                            }
                        }
                        r.usufructs.put(occurrence.getValue(), scopePsis);
                    } else if (type.equals("http://psi.topic.ndla.no/#publisert")) {
                        if (occurrence.getValue().equals("true"))
                            r.published = true;
                        else if (occurrence.getValue().equals("false"))
                            r.published = false;
                        else
                            throw new IOException("Published must be either true or false");
                    } else {
                        throw new IOException("Unknown occurrence " + occPsis.iterator().next().getAddress() + " with value " + occurrence.getValue());
                    }
                }
                Collection<AssociationIF> associations = resource.getAssociations();
                for (AssociationIF association : associations) {
                    Reifier reifierDesc = new Reifier();
                    TopicIF reifier = association.getReifier();
                    if (reifier != null) {
                        Collection<OccurrenceIF> reifierOccurrences = reifier.getOccurrences();
                        for (OccurrenceIF occurrence : reifierOccurrences) {
                            Collection<LocatorIF> typePsis = occurrence.getType().getSubjectIdentifiers();
                            if (typePsis.size() == 0)
                                continue;
                            if (typePsis.size() != 1)
                                throw new IOException("Expected 1 type psi for the occurrence, found " + typePsis.size());
                            String occTPsi = typePsis.iterator().next().getAddress();
                            if (occTPsi.equals("http://psi.topic.ndla.no/#bruker-navn")) {
                                if (reifierDesc.username == null) {
                                    reifierDesc.username = occurrence.getValue();
                                } else if (!occurrence.getValue().equals(reifierDesc.username))
                                    System.out.println("IGNORED: Duplicate username " + occurrence.getValue() + ", but using " + reifierDesc.username);
                            } else if (occTPsi.equals("http://psi.topic.ndla.no/#bruker-url")) {
                                if (reifierDesc.userUrl == null) {
                                    reifierDesc.userUrl = new URL(occurrence.getValue());
                                } else if (!occurrence.getValue().equals(reifierDesc.userUrl.toString()))
                                    System.out.println("IGNORED: Duplicate user-url " + occurrence.getValue() + ", but using " + reifierDesc.userUrl.toString());
                            } else if (occTPsi.equals("http://psi.topic.ndla.no/#apprentice")) {
                                if (occurrence.getValue().equalsIgnoreCase("true"))
                                    reifierDesc.relevantForApprentices = true;
                            } else if (occTPsi.equals("http://psi.topic.ndla.no/#timestamp")) {
                                long timestamp = Long.decode(occurrence.getValue());
                                timestamp *= 1000; /* sec->ms */
                                Date newTs = new Date(timestamp);
                                if (reifierDesc.timestamp == null) {
                                    reifierDesc.timestamp = new Date(timestamp);
                                } else if (!reifierDesc.timestamp.equals(newTs)) {
                                    System.out.println("IGNORED: duplicate reifier timestamp " + newTs.toString() + ", but using " + reifierDesc.timestamp.toString());
                                }
                            } else if (occTPsi.equals("http://psi.topic.ndla.no/#fag")) {
                                /* Don't know why we have this - ignored (uuid) */
                            } else if (occTPsi.equals("http://psi.topic.ndla.no/#level")) {
                                /* Garbage data - level on the assoc-reifier?? */
                            } else {
                                throw new IOException("Unknown occurrence type on reifier: " + occTPsi + " with value " + occurrence.getValue());
                            }
                        }
                    }
                    Collection<LocatorIF> assocPsis = association.getType().getSubjectIdentifiers();
                    if (assocPsis.size() != 1)
                        throw new IOException("Assuming one type psi for the association, found " + assocPsis.size());
                    String assocPsi = assocPsis.iterator().next().getAddress();
                    if (assocPsi.equals("http://psi.topic.ndla.no/#dekker-delvis") ||
                            assocPsi.equals("http://psi.topic.ndla.no/#yrkesrelevant") ||
                            assocPsi.equals("http://psi.topic.ndla.no/#dekker-helt") ||
                            assocPsi.equals("http://psi.topic.ndla.no/#oppgave-til") ||
                            assocPsi.equals("http://psi.topic.ndla.no/#dekker-mer-enn")) {
                        Collection<AssociationRoleIF> roles = association.getRoles();
                        for (AssociationRoleIF role : roles) {
                            Collection<LocatorIF> rolePsis = role.getType().getSubjectIdentifiers();
                            if (rolePsis.size() == 0)
                                continue;
                            if (rolePsis.size() != 1)
                                throw new IOException("Expected number of role type psis to be 1, found " + rolePsis.size());
                            String rolePsi = rolePsis.iterator().next().getAddress();
                            if (rolePsi.equals("http://psi.udir.no/ontologi/lkt/#kompetansemaal")) {
                                TopicIF aim = role.getPlayer();
                                Collection<LocatorIF> aimIIds = aim.getItemIdentifiers();
                                String searchFor = "#kompetansemaal_";
                                if (aimIIds != null && aimIIds.size() > 0) {
                                    String aimIId = aimIIds.iterator().next().getAddress();
                                    int index = aimIId.indexOf(searchFor);
                                    if (index >= 0) {
                                        index += searchFor.length();
                                        String idPart = aimIId.substring(index);
                                        reifierDesc.origIdString = idPart;
                                        index = idPart.indexOf("_");
                                        if (index > 0) {
                                            String curriculumId = idPart.substring(0, index);
                                            idPart = idPart.substring(index + 1);
                                            index = idPart.indexOf("_");
                                            if (index > 0) {
                                                String aimSetId = idPart.substring(0, index);
                                                String aimId = idPart.substring(index + 1);
                                                reifierDesc.curriculumId = curriculumId;
                                                reifierDesc.aimSetId = aimSetId;
                                                reifierDesc.aimId = aimId;
                                            }
                                        }
                                    }
                                }
                                if (reifierDesc != null && reifierDesc.aimId == null) {
                                    TopicIF aimCodeType = aim.getTopicMap().getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#competence_aim_code"));
                                    Iterator<OccurrenceIF> aimCodeOccurrences = aim.getOccurrencesByType(aimCodeType).iterator();
                                    if (aimCodeOccurrences.hasNext()) {
                                        String aimCode = aimCodeOccurrences.next().getValue();
                                        while (aimCodeOccurrences.hasNext()) {
                                            String dupeCode = aimCodeOccurrences.next().getValue();
                                            if (!aimCode.equals(dupeCode))
                                                throw new IOException("The competence aim is supposed to have 1 aim code, found " + dupeCode + " as well.");
                                        }
                                        reifierDesc.aimId = aimCode;
                                    } else {
                                        /*String itemIds = "";
                                        for (LocatorIF itemId : aim.getItemIdentifiers()) {
                                            if (itemIds.length() > 0)
                                                itemIds += ", ";
                                            itemIds += itemId.getAddress();
                                        }
                                        System.out.println("IGNORED: The competence aim has no code node=" + r.psi + " aim-ids=" + itemIds);*/
                                    }
                                }
                                if (relationImportHack != null) {
                                    RelationImportHack.Info info = relationImportHack.filter(destinationLocator, r.psi.toString(), reifierDesc.origIdString, reifierDesc.curriculumId, reifierDesc.aimSetId, reifierDesc.aimId);
                                    if (info != null) {
                                        reifierDesc.curriculumId = info.getCurriculum();
                                        reifierDesc.aimSetId = info.getAimSet();
                                        reifierDesc.aimId = info.getAimId();
                                    } else {
                                        reifierDesc = null;
                                    }
                                }
                                if (reifierDesc != null && reifierDesc.aimId != null) {
                                    r.relatedAims.add(reifierDesc);
                                }
                            } else if (rolePsi.equals("http://psi.topic.ndla.no/#laeringsressurs") ||
                                    rolePsi.equals("http://psi.topic.ndla.no/#ndla-node") ||
                                    rolePsi.equals("http://psi.topic.ndla.no/#deling-node")) {
                            } else
                                throw new IOException("Unknown role psi: " + rolePsi);
                        }
                    } else if (assocPsi.equals("http://psi.topic.ndla.no/#subject-curriculum")) {
                        Collection<AssociationRoleIF> roles = association.getRoles();
                        for (AssociationRoleIF role : roles) {
                            Collection<LocatorIF> rolePsis = role.getType().getSubjectIdentifiers();
                            if (rolePsis.size() == 0)
                                continue;
                            if (rolePsis.size() != 1)
                                throw new IOException("Expected number of role type psis to be 1, found " + rolePsis.size());
                            String rolePsi = rolePsis.iterator().next().getAddress();
                            if (rolePsi.equals("http://psi.udir.no/ontologi/lkt/#laereplan")) {
                                TopicIF curriculum = role.getPlayer();
                                TopicIF curriculumCodeType = curriculum.getTopicMap().getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#curricula_code"));
                                Iterator<OccurrenceIF> curriculumCodeOccurrences = curriculum.getOccurrencesByType(curriculumCodeType).iterator();
                                if (curriculumCodeOccurrences.hasNext()) {
                                    String curriculumCode = curriculumCodeOccurrences.next().getValue();
                                    while (curriculumCodeOccurrences.hasNext()) {
                                        String dupeCode = curriculumCodeOccurrences.next().getValue();
                                        if (!curriculumCode.equals(dupeCode))
                                            throw new IOException("The curriculum is supposed to have 1 aim code, found " + dupeCode + " as well.");
                                    }
                                    r.relatedCurriculums.add(curriculumCode);
                                } else
                                    System.out.println("IGNORED: The curriculum has no code");
                            } else if (rolePsi.equals("http://psi.topic.ndla.no/#laeringsressurs") ||
                                    rolePsi.equals("http://psi.topic.ndla.no/#ndla-node") ||
                                    rolePsi.equals("http://psi.topic.ndla.no/#deling-node")) {
                            } else
                                throw new IOException("Unknown role psi: " + rolePsi);
                        }
                    } else {
                        throw new IOException("Found association " + assocPsi);
                    }
                }
                parsedResources.add(r);
                resourceQueue.put(r);
            }
            for (int i = 0; i < resourceImporterThreads.length; i++)
                resourceQueue.put(poison);
            Thread watcher = new Thread() {
                public void run() {
                    try {
                        System.out.println("Waiting for the importers to complete write to the new topicmap...");
                        for (int i = 0; i < resourceImporterThreads.length; i++)
                            resourceImporterThreads[i].join();
                        resourceWithAimsNotFound.put(poison);
                        System.out.println("Waiting for the aims not found importer to write to the new topicmap...");
                        Thread resourceImporterThread = new Thread(new ResourceImporter(resourceWithAimsNotFound, null, destinationLocator, parseProgress));
                        resourceImporterThread.start();
                        resourceImporterThread.join();
                        System.out.println("Have got " + parsedResources.size() + " resources from the old grep.");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            watcher.start();
            return watcher;
        } catch (InterruptedException e) {
            throw new IOException("Interrupted", e);
        }
    }
    public static final int estimatedNumberOfResources = 103000;
    public void parse(TopicMapLocator destination) throws IOException {
        parseProgress.setExpected(estimatedNumberOfResources);
        progress.addSubBit(parseProgress);
        TopicMapStore store = storeLocator.getTopicMapStore();
        String[] tmIds = store.getTopicMaps();
        Thread parser;
        if (tmIds.length != 1)
            throw new IOException("Need to have just one topicmap in the store for importing");
        TopicMap topicMap = store.getTopicMap(tmIds[0]);
        try {
            Collection<TopicIF> topics = new HashSet<TopicIF>();
            String[] queries = {
                    "instance-of($topic,i\"http://psi.topic.ndla.no/#deling-node\")?",
                    "instance-of($topic,i\"http://psi.topic.ndla.no/#ndla-node\")?",
                    "instance-of($topic,i\"http://psi.topic.ndla.no/#nygiv-node\")?",
                    "instance-of($topic,i\"http://psi.topic.ndla.no/#exercise\")?",
            };
            for (String query : queries) {
                TopicMapIF topicMapIF = topicMap.getTopicMap();
                ParsedStatementIF statement;
                QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
                QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMapIF, null, null);
                QueryResultIF tologResult;
                try {
                    statement = queryProcessor.parse(query);
                    if (statement != null) {
                        tologResult = ((ParsedQueryIF) statement).execute();
                        while (tologResult.next()) {
                            TopicIF topic = (TopicIF) tologResult.getValue("topic");
                            if (filter == null || filter.filter(topic)) {
                                topics.add(topic);
                            }
                        }
                    } else
                        throw new IOException("Invalid tolog query");
                } catch (InvalidQueryException e) {
                    throw new IOException("Invalid tolog query", e);
                }
            }
            parseProgress.setExpected(topics.size());
            parseProgress.setDone(0);
            progress.setExpected(100);
            progress.setDone(99);
            parser = parseResources(topics, destination);
        } finally {
            topicMap.abort();
            topicMap.close();
        }
        try {
            parser.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        progress.setDone(100);
    }

    public OldGrepTopicFilter getFilter() {
        return filter;
    }
}
