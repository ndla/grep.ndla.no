package org.mycurriculum.data.util;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.engine.jdbc.connections.internal.DriverManagerConnectionProviderImpl;
import org.hibernate.engine.jdbc.connections.spi.AbstractMultiTenantConnectionProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.spi.Configurable;
import org.hibernate.service.spi.Stoppable;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.*;
import java.util.logging.Logger;

public class MultiTenantConnectionProviderImpl extends AbstractMultiTenantConnectionProvider implements DisposableBean {

    private static final String FIND_TENANT_CONNECTION_URL = "SELECT a.conn_database FROM account a, \"user\" u WHERE u.username = ? AND a.id = u.account_id";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Collection<Stoppable> connectionProviders = new ArrayList<Stoppable>();
    private Map<String, BasicDataSourceConnectionProviderWrapper> connectionMap = new HashMap<String, BasicDataSourceConnectionProviderWrapper>();

    private String hibernateDialect;
    private String hibernateDriver;
    private String hibernateUrl;
    private String hibernateUsername;
    private String hibernatePassword;

    private String hibernatePoolInitialSize;
    private String hibernatePoolMaxActive;

    @Override
    protected ConnectionProvider getAnyConnectionProvider() {
        Properties properties = getConnectionProperties();

        DriverManagerConnectionProviderImpl defaultProvider = new DriverManagerConnectionProviderImpl();
        defaultProvider.configure(properties);
        connectionProviders.add(defaultProvider);
        return defaultProvider;
    }

    @Override
    protected ConnectionProvider selectConnectionProvider(String tenantId) {
        Properties properties = getConnectionProperties();
        String tenantConnectionUrl = jdbcTemplate.queryForObject(FIND_TENANT_CONNECTION_URL, new Object[]{tenantId}, String.class);

        synchronized (this) {
            if (connectionMap.containsKey(tenantConnectionUrl)) {
                return connectionMap.get(tenantConnectionUrl);
            }
            BasicDataSourceConnectionProviderWrapper defaultProvider = new BasicDataSourceConnectionProviderWrapper();
            properties.put(AvailableSettings.URL, tenantConnectionUrl);
            defaultProvider.configure(properties);
            connectionProviders.add(defaultProvider);
            connectionMap.put(tenantConnectionUrl, defaultProvider);
            return defaultProvider;
        }
    }

    private Properties getConnectionProperties() {
        Properties properties = new Properties();

        properties.put(AvailableSettings.DIALECT, getHibernateDialect());
        properties.put(AvailableSettings.DRIVER, getHibernateDriver());
        properties.put(AvailableSettings.URL, getHibernateUrl());
        properties.put(AvailableSettings.USER, getHibernateUsername());
        properties.put(AvailableSettings.PASS, getHibernatePassword());

        properties.put("intialSize", getHibernatePoolInitialSize()); // 5
        properties.put("maxActive", getHibernatePoolMaxActive()); // 20

        return properties;
    }

    public String getHibernateDialect() {
        return hibernateDialect;
    }

    public void setHibernateDialect(String hibernateDialect) {
        this.hibernateDialect = hibernateDialect;
    }

    public String getHibernateDriver() {
        return hibernateDriver;
    }

    public void setHibernateDriver(String hibernateDriver) {
        this.hibernateDriver = hibernateDriver;
    }

    public String getHibernateUrl() {
        return hibernateUrl;
    }

    public void setHibernateUrl(String hibernateUrl) {
        this.hibernateUrl = hibernateUrl;
    }

    public String getHibernateUsername() {
        return hibernateUsername;
    }

    public void setHibernateUsername(String hibernateUsername) {
        this.hibernateUsername = hibernateUsername;
    }

    public String getHibernatePassword() {
        return hibernatePassword;
    }

    public void setHibernatePassword(String hibernatePassword) {
        this.hibernatePassword = hibernatePassword;
    }

    public String getHibernatePoolInitialSize() {
        return hibernatePoolInitialSize;
    }

    public void setHibernatePoolInitialSize(String hibernatePoolInitialSize) {
        this.hibernatePoolInitialSize = hibernatePoolInitialSize;
    }

    public String getHibernatePoolMaxActive() {
        return hibernatePoolMaxActive;
    }

    public void setHibernatePoolMaxActive(String hibernatePoolMaxActive) {
        this.hibernatePoolMaxActive = hibernatePoolMaxActive;
    }

    @Override
    public void destroy() {
        synchronized (this) {
            for (Stoppable connectionProvider : connectionProviders) {
                connectionProvider.stop();
            }
            /* Empty the list and map of connections */
            connectionProviders = new ArrayList<Stoppable>();
            connectionMap = new HashMap<String, BasicDataSourceConnectionProviderWrapper>();
        }
    }


    private static class BasicDataSourceConnectionProviderWrapper extends BasicDataSource implements ConnectionProvider, Stoppable, Configurable {

        @Override
        public Logger getParentLogger() throws SQLFeatureNotSupportedException {
            return null;
        }

        @Override
        public void closeConnection(Connection conn) throws SQLException {
            conn.close();
        }

        @Override
        public boolean supportsAggressiveRelease() {
            return false;
        }

        @Override
        public boolean isUnwrappableAs(Class unwrapType) {
            return false;
        }

        @Override
        public <T> T unwrap(Class<T> iface) {
            return null;
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return false;
        }

        @Override
        public void stop() {
            try {
                close();
            } catch (SQLException e) {
                throw new RuntimeException("Failed to stop connection pool", e);
            }
        }

        @Override
        public void configure(Map configurationValues) {
            Map<String, String> typedMap = (Map<String, String>) configurationValues;
            for (Map.Entry<String, String> entry : typedMap.entrySet()) {
                String name = entry.getKey();
                String value = entry.getValue();
                if (name.equals(AvailableSettings.DRIVER)) {
                    setDriverClassName(value);
                } else if (name.equals(AvailableSettings.URL)) {
                    setUrl(value);
                } else if (name.equals(AvailableSettings.USER)) {
                    setUsername(value);
                } else if (name.equals(AvailableSettings.PASS)) {
                    setPassword(value);
                } else if (name.equals(AvailableSettings.DIALECT)) {
                    /* ... */
                } else if (name.equals("intialSize")) {
                    setInitialSize(Integer.decode(value));
                } else if (name.equals("maxActive")) {
                    setMaxActive(Integer.decode(value));
                }
            }
        }
    }
}
