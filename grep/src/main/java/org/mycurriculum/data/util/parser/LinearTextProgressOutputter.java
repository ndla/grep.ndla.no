package org.mycurriculum.data.util.parser;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by janespen on 5/16/14.
 */
public class LinearTextProgressOutputter implements ProgressTracker.Listener {
    private ProgressTracker progressTracker;
    private Collection<ProgressBit> outputted = new ArrayList<ProgressBit>();
    private Collection<ProgressBit> started = new ArrayList<ProgressBit>();

    public LinearTextProgressOutputter(ProgressTracker progressTracker) {
        this.progressTracker = progressTracker;
    }

    private boolean inList(Collection<ProgressBit> list, ProgressBit bit) {
        for (ProgressBit b : list) {
            if (bit == b)
                return true;
        }
        return false;
    }
    private boolean hasBeenOutputted(ProgressBit bit) {
        return inList(outputted, bit);
    }
    private boolean hasBeenStarted(ProgressBit bit) {
        return inList(started, bit);
    }

    private boolean isCompleted(ProgressBit bit) {
        return (bit.getTotalDone() >= bit.getTotalExpected());
    }

    private boolean outputProgress(ProgressBit bit) {
        if (!hasBeenStarted(bit)) {
            if (bit.getSubBits().size() > 0)
                System.out.println(bit.getName());
            started.add(bit);
        }

        for (ProgressBit sub : bit.getSubBits()) {
            if (!outputProgress(sub))
                return false;
        }
        if (bit.getTotalDone() < bit.getTotalExpected()) {
            System.out.print("\r" + bit.getName() + ": " + getPercent(bit) + "%, total " + getPercent(progressTracker) + "%");
            return false;
        }
        if (!hasBeenOutputted(bit)) {
            System.out.print("\r"+bit.getName()+": "+getPercent(bit)+"%, total "+getPercent(progressTracker)+"%\n");
            outputted.add(bit);
        }
        return true;
    }

    private int getPercent(ProgressBit bit)
    {
        if (bit.getTotalExpected() <= 0)
            return 0;
        if (bit.getTotalDone() >= bit.getTotalExpected())
            return 100;
        return (int) ((bit.getTotalDone() * 100) / bit.getTotalExpected());
    }

    @Override
    public void progressUpdate() {
        for (ProgressBit bit : progressTracker.getSubBits()) {
            if (!outputProgress(bit)) {
                System.out.flush();
                return;
            }
        }
        System.out.print("\r"+progressTracker.getName()+": "+getPercent(progressTracker)+"%");
        System.out.flush();
    }
}
