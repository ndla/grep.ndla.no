package org.mycurriculum.data.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MutableTranslatedTextImpl implements MutableTranslatedText, Serializable {
    public static final long serialVersionUID = 57456667L;

    private Map<Locale,String> translations;
    private Locale defaultLocale;
    private String untaggedUniversal;

    private Map<Locale,String> copyMap(Map<Locale,String> map) {
        if (map == null)
            return null;
        Map<Locale,String> output = new HashMap<Locale,String>();
        for (Map.Entry<Locale,String> entry : map.entrySet()) {
            output.put(entry.getKey(), entry.getValue());
        }
        return output;
    }
    public MutableTranslatedTextImpl(Map<Locale,String> translations) {
        this.translations = copyMap(translations);
        Locale und = new Locale("und");
        this.defaultLocale = null;
        if (this.translations != null && this.translations.containsKey(und)) {
            this.untaggedUniversal = this.translations.get(und);
            this.translations.remove(und);
        } else {
            this.untaggedUniversal = null;
        }
    }
    public MutableTranslatedTextImpl(Map<Locale,String> translations, Locale defaultLocale) {
        this(translations);
        if (!defaultLocale.getISO3Language().equals("und")) {
            this.defaultLocale = defaultLocale;
        }
    }
    public MutableTranslatedTextImpl(Map<Locale,String> translations, String untaggedUniversal) {
        this(translations);
        this.untaggedUniversal = untaggedUniversal;
    }
    private static Map<Locale,String> singleLanguageMap(Locale locale, String text) {
        Map<Locale,String> map = new HashMap<Locale,String>();
        map.put(locale, text);
        return map;
    }
    public MutableTranslatedTextImpl(Locale locale, String text) {
        this(singleLanguageMap(locale, text), locale);
    }
    public MutableTranslatedTextImpl(String untaggedUniversal) {
        this((Map<Locale,String>) null);
        this.untaggedUniversal = untaggedUniversal;
    }
    public MutableTranslatedTextImpl(TranslatedText copyFrom) {
        this.translations = copyFrom.getTextMap();
        Locale und = new Locale("und");
        if (this.translations != null && this.translations.containsKey(und)) {
            this.translations.remove(und);
        }
        this.defaultLocale = copyFrom.getDefaultLocale();
        this.untaggedUniversal = copyFrom.getDefaultText();
    }

    public static Locale getLocaleFromPsi(String psi) {
        String psiLangUrl = "http://psi.oasis-open.org/iso/639/#";
        if (psi.length() <= psiLangUrl.length())
            return null;
        if (psi.substring(0, psiLangUrl.length()).compareToIgnoreCase(psiLangUrl) != 0)
            return null;
        String lang = psi.substring(psiLangUrl.length());
        return new Locale(lang);
    }
    public static String getPsiFromLocale(Locale locale) {
        String code = locale.getISO3Language();
        return "http://psi.oasis-open.org/iso/639/#" + code;
    }

    @Override
    public String getDefaultText() {
        synchronized (this) {
            if (defaultLocale != null && translations != null && translations.containsKey(defaultLocale)) {
                return translations.get(defaultLocale);
            }
            return untaggedUniversal;
        }
    }

    @Override
    public Locale getDefaultLocale() {
        synchronized (this) {
            if (defaultLocale != null)
                return defaultLocale;
            else if (untaggedUniversal != null)
                return new Locale("und");
            else
                return null;
        }
    }

    @Override
    public String getDefaultLocalePsi() {
        Locale defaultLocale = getDefaultLocale();
        if (defaultLocale != null)
            return getPsiFromLocale(defaultLocale);
        return null;
    }

    @Override
    public String getText(Locale locale) {
        synchronized (this) {
            if (translations.containsKey(locale)) {
                return translations.get(locale);
            }
        }
        return null;
    }

    @Override
    public String getText(String iso639lang) {
        return getText(new Locale(iso639lang));
    }

    @Override
    public Map<Locale, String> getTextMap() {
        Map<Locale,String> map = new HashMap<Locale,String>();
        synchronized (this) {
            if (translations == null) {
                if (untaggedUniversal != null) {
                    map.put(new Locale("und"), untaggedUniversal);
                    return map;
                }
                return null;
            }
            for (Map.Entry<Locale,String> entry : translations.entrySet())
                map.put(entry.getKey(), entry.getValue());
            if (untaggedUniversal != null && !map.containsKey(new Locale("und"))) {
                map.put(new Locale("und"), untaggedUniversal);
            }
        }
        return map;
    }

    @Override
    public Map<String, String> getPsiTextMap() {
        Map<Locale,String> localeMap = getTextMap();
        if (localeMap == null)
            return null;
        Map<String,String> map = new HashMap<String,String>();
        for (Map.Entry<Locale,String> entry : localeMap.entrySet()) {
            map.put(getPsiFromLocale(entry.getKey()), entry.getValue());
        }
        return map;
    }

    @Override
    public Locale[] getLanguages() {
        Locale[] locales;
        synchronized (this) {
            Collection<Locale> localeCollection = translations.keySet();
            locales = new Locale[localeCollection.size()];
            int i = 0;
            boolean hasUnd = false;
            for (Locale locale : localeCollection) {
                locales[i++] = locale;
                if (locale.getISO3Language().equals("und"))
                    hasUnd = true;
            }
            if (untaggedUniversal != null && !hasUnd) {
                Locale[] cp = new Locale[locales.length + 1];
                System.arraycopy(locales, 0, cp, 0, locales.length);
                cp[i++] = new Locale("und");
                locales = cp;
            }
        }
        return locales;
    }

    @Override
    public void addText(Locale locale, String text) {
        if (locale.getISO3Language().equals("und")) {
            setDefaultUntaggedText(text);
            return;
        }
        synchronized (this) {
            translations.put(locale, text);
        }
    }

    @Override
    public void removeText(Locale locale) {
        synchronized (this) {
            translations.remove(locale);
        }
    }

    @Override
    public void setDefaultLocale(Locale defaultLocale) {
        if (!defaultLocale.getISO3Language().equals("und"))
            this.defaultLocale = defaultLocale;
        else
            this.defaultLocale = null;
    }

    @Override
    public void setDefaultUntaggedText(String untaggedText) {
        this.untaggedUniversal = untaggedText;
    }
}
