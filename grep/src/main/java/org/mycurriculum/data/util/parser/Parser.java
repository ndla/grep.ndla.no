package org.mycurriculum.data.util.parser;

import java.io.IOException;
import java.net.URL;

public interface Parser {
    public Context parse() throws IOException;

    public URL getPsiRoot();

    public interface Context {
        public ProgressTracker getProgressTracker();

        void join() throws InterruptedException;
        public void waitForCompletion() throws IOException, InterruptedException;

        public Report getReport();
    }
}
