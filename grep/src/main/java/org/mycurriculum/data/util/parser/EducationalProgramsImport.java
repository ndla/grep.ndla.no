package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgram;
import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgramRef;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructureImpl;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class EducationalProgramsImport extends UdirObjectImporter<UdirEducationalProgramRef> {
    private Parser parser;

    public EducationalProgramsImport(Parser parser, List<UdirEducationalProgramRef> educationalPrograms, TopicMapLocator dst, ProgressBit progress)
    {
        super(parser, educationalPrograms, dst, progress);
        this.parser = parser;
    }
    public URL generatePsi(UdirEducationalProgramRef ref) throws IOException {
        UdirEducationalProgram obj = ref.getEducationalProgram();
        return OntopiaCourseStructureImpl.generatePsi(obj.getKode());
    }

    @Override
    public void parse(UdirEducationalProgramRef ref, TopicMapRoot dst) throws IOException {
        UdirEducationalProgram src = ref.getEducationalProgram();
        OntopiaCourseStructure.Builder builder = dst.getCourseStructureBuilder()
                .psi(generatePsi(ref).toString())
                .psi(src.getUrlPsi());
        OntopiaCourseStructure educationalProgram;
        educationalProgram = dst.getCourseStructure(new URL(src.getUrlPsi()));
        if (educationalProgram == null)
            educationalProgram = builder.build();
        else {
            if (!educationalProgram.hasPsi(generatePsi(ref).toString()))
                educationalProgram.addPsi(generatePsi(ref).toString());
        }
        educationalProgram.setHumanId(src.getKode());
        importTitle(educationalProgram, src.getTranslatedTitle());
        educationalProgram.setSetType("EDUCATIONAL_PROGRAM");
        educationalProgram.save();
    }
}
