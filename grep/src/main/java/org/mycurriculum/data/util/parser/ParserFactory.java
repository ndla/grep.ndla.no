package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.tms.connections.TopicMapLocator;

public interface ParserFactory {
    public Parser createParser(TopicMapLocator topicMapLocator);
    public Parser createParser(TopicMapLocator resourcesTopicMapLocator, TopicMapLocator topicMapLocator);
}
