package org.mycurriculum.data.util.multicore;

import java.io.IOException;
import java.util.Set;

public interface TaskUnit {
    public void addDependency(TaskUnit dependency);
    public Set<TaskUnit> getDependencies();
    public void run() throws IOException;
}
