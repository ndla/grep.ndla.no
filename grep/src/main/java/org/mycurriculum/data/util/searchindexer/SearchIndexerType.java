package org.mycurriculum.data.util.searchindexer;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.util.multicore.AsyncTask;

public interface SearchIndexerType {
    public AsyncTask createSearchIndexer(TopicMapLocator topicMapLocator);
}
