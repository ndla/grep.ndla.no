package org.mycurriculum.data.util.searchindexer.ndla;

import org.json.JSONObject;
import org.mycurriculum.data.tms.domain.OntopiaAim;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;

public class NdlaAimsIndexer extends NdlaTopicIndexer<OntopiaAim> {
    public NdlaAimsIndexer(NdlaSearchIndexerSession session, TopicMapRoot topicMapRoot) throws IOException {
        super(session, topicMapRoot.getAims(), "NDLA aims indexer");
    }

    @Override
    public JSONObject createPayload(OntopiaAim obj) {
        JSONObject json = createIdNamePayload(obj);
        json.put("type", "competenceAim");
        return json;
    }
}
