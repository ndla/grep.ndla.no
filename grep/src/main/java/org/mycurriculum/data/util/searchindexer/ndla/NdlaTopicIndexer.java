package org.mycurriculum.data.util.searchindexer.ndla;

import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.domain.OntopiaTopicCommon;
import org.mycurriculum.data.util.TranslatedText;

import javax.json.JsonObject;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public abstract class NdlaTopicIndexer<T> extends NdlaIndexerTopicIterator<T> {
    public final static String indexingEndpointPath = "/api/topics/grep";

    private NdlaSearchIndexerSession session;
    private URI indexingEndpointUri;

    private Collection<JsonObject> responses = new ArrayList<JsonObject>();

    public NdlaTopicIndexer(NdlaSearchIndexerSession session, Collection<T> collection, String name) {
        super(collection, 200, name);
        this.session = session;
        try {
            this.indexingEndpointUri = session.getUri(indexingEndpointPath);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Unable to construct endpoint uri", e);
        }
    }

    public JSONArray getNamesPayload(OntopiaTopic obj) {
        JSONArray json = new JSONArray();
        TranslatedText names = obj.getTranslatedName();
        Locale defaultLocale = names.getDefaultLocale();
        Map<Locale,String> textMap = names.getTextMap();
        if (textMap == null) {
            textMap = new HashMap<Locale,String>();
            String defaultText = names.getDefaultText();
            if (defaultText != null) {
                textMap.put(new Locale("und"), defaultText);
            }
        }
        for (Map.Entry<Locale,String> name : textMap.entrySet()) {
            JSONObject lang = new JSONObject();
            String lang3 = name.getKey().getISO3Language();
            String text = name.getValue();
            lang.put("name", text);
            lang.put("language", lang3);
            if (defaultLocale != null && defaultLocale.getISO3Language().equals(lang3)) {
                lang.put("lang_neutral", "1");
            } else {
                lang.put("lang_neutral", "0");
            }
            json.put(lang);
        }
        return json;
    }
    public JSONObject createIdNamePayload(OntopiaTopicCommon obj) {
        JSONObject json = new JSONObject();
        json.put("id", obj.getHumanId());
        json.put("names", getNamesPayload(obj));
        return json;
    }

    public abstract JSONObject createPayload(T obj);

    public JSONObject createChunkPayload(Collection<T> chunk) {
        JSONArray items = new JSONArray();
        for (T item : chunk) {
            items.put(createPayload(item));
        }
        JSONObject reqObj = new JSONObject();
        reqObj.put("grep", items);
        return reqObj;
    }

    @Override
    public void objectChunk(Collection<T> chunk) throws IOException {
        JSONObject payload = createChunkPayload(chunk);
        HttpPost post = new HttpPost(indexingEndpointUri);
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        formparams.add(new BasicNameValuePair("grep", payload.toString()));
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
        post.setEntity(entity);
        JsonObject response = session.executeJsonRequest(post);
        synchronized (this) {
            responses.add(response);
        }
    }

    public Collection<JsonObject> getResponses() {
        synchronized (this) {
            Collection<JsonObject> clone = new ArrayList<JsonObject>(responses.size());
            clone.addAll(responses);
            return clone;
        }
    }
}
