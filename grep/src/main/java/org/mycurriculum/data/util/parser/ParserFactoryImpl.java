package org.mycurriculum.data.util.parser;

import org.mycurriculum.data.adapter.udir.domain.UdirFactory;
import org.mycurriculum.data.tms.connections.TopicMapLocator;

public class ParserFactoryImpl implements ParserFactory {
    private UdirFactory udirFactory;

    public ParserFactoryImpl(UdirFactory udirFactory) {
        this.udirFactory = udirFactory;
    }

    @Override
    public Parser createParser(TopicMapLocator topicMapLocator) {
        return new ParserImpl(udirFactory, topicMapLocator);
    }

    @Override
    public Parser createParser(TopicMapLocator resourcesTopicMapLocator, TopicMapLocator topicMapLocator) {
        return new ParserImpl(udirFactory, resourcesTopicMapLocator, topicMapLocator);
    }
}
