package org.mycurriculum.data.util.oldgrep;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;

public abstract class OldGrepUrlFilter implements OldGrepTopicFilter {
    public abstract boolean urlFilter(URL url);

    private static Collection<URL> getSubjectIdentifiers(TopicIF topic) {
        Collection<URL> urls = new HashSet<URL>();
        Collection<LocatorIF> locators = topic.getSubjectIdentifiers();
        for (LocatorIF locator : locators) {
            try {
                urls.add(new URL(locator.getAddress()));
            } catch (MalformedURLException e) {
                throw new RuntimeException("The url is supposed to be valid", e);
            }
        }
        return urls;
    }

    @Override
    public boolean filter(TopicIF topic) throws IOException {
        Collection<URL> psis = getSubjectIdentifiers(topic);
        if (psis.size() != 1)
            throw new IOException("Expected the number of psis to be 1, found " + psis.size());
        return urlFilter(psis.iterator().next());
    }
}
