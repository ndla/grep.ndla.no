package org.mycurriculum.data.util.searchindexer;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.util.multicore.AsyncTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SearchIndexerFactoryImpl implements SearchIndexerFactory {
    private Map<String,SearchIndexerType> searchIndexerTypes = new HashMap<String,SearchIndexerType>();

    @Override
    public Set<String> getSearchIndexerTypes() {
        return searchIndexerTypes.keySet();
    }

    @Override
    public AsyncTask createSearchIndexer(TopicMapLocator topicMapLocator, String type) {
        SearchIndexerType typeObj = searchIndexerTypes.get(type);
        if (typeObj == null)
            return null;
        return typeObj.createSearchIndexer(topicMapLocator);
    }

    public void addSearchIndexer(String name, SearchIndexerType type) {
        searchIndexerTypes.put(name, type);
    }
}
