package org.mycurriculum.data.util.oldgrep;

import net.ontopia.topicmaps.core.TopicIF;

import java.io.IOException;

public interface OldGrepTopicFilter {
    public boolean filter(TopicIF topic) throws IOException;
    public void printSummary();
}
