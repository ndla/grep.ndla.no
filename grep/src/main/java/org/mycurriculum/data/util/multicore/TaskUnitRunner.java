package org.mycurriculum.data.util.multicore;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by janespen on 5/16/14.
 */
public class TaskUnitRunner implements TaskUnit {
    private Object lock = new Object();
    private Set<TaskUnit> dependencies = new HashSet<TaskUnit>();
    private Set<TaskUnit> taskUnits = new HashSet<TaskUnit>();
    private Set<AddListener> addListeners = new HashSet<AddListener>();
    private int nThreads;

    public TaskUnitRunner(int nThreads) {
        this.nThreads = nThreads;
    }

    public void addAddListener(AddListener addListener) {
        synchronized (lock) {
            addListeners.add(addListener);
        }
    }
    public void removeAddListener(AddListener addListener) {
        synchronized (lock) {
            addListeners.remove(addListener);
        }
    }
    public void add(TaskUnit taskUnit) {
        synchronized (lock) {
            taskUnits.add(taskUnit);
            for (AddListener listener : addListeners) {
                listener.added(taskUnit);
            }
        }
    }

    @Override
    public void addDependency(TaskUnit dependency) {
        synchronized (lock) {
            dependencies.add(dependency);
        }
    }

    @Override
    public Set<TaskUnit> getDependencies() {
        synchronized (lock) {
            Set<TaskUnit> dependencies = new HashSet<TaskUnit>();
            dependencies.addAll(this.dependencies);
            return dependencies;
        }
    }

    public Runner start() {
        synchronized (lock) {
            AsyncRunnerImpl runner = new AsyncRunnerImpl(nThreads);
            runner.start();
            return runner;
        }
    }

    @Override
    public void run() throws IOException {
        (new RunnerImpl(nThreads)).run();
    }

    public interface Runner {
        public void setFinishedAdding();
        public void waitForTasks() throws IOException;
    }
    private class AsyncRunnerImpl extends RunnerImpl implements Runner {
        public AsyncRunnerImpl(int nThreads) {
            super(nThreads);
        }

        @Override
        public void waitForTasks() throws IOException {
            waitForTasks(false);
        }
    }
    private interface AddListener {
        public void added(TaskUnit added);
    }
    private class RunnerImpl implements AddListener {
        private Set<TaskUnit> satisfied;
        private Set<TaskUnit> running;
        private ExecutorService executorService;
        private int nThreads;
        private IOException ioException;
        private boolean finished = false;

        public RunnerImpl(int nThreads) {
            this.nThreads = nThreads;
        }

        private boolean isSatisfied(TaskUnit taskUnit) {
            synchronized (lock) {
                for (TaskUnit dep : taskUnit.getDependencies()) {
                    if (!satisfied.contains(dep))
                        return false;
                }
                return true;
            }
        }
        private boolean isFinished() {
            synchronized (lock) {
                for (TaskUnit taskUnit : taskUnits) {
                    if (!satisfied.contains(taskUnit))
                        return false;
                }
                return finished;
            }
        }
        private boolean isWorking() {
            synchronized (lock) {
                return (running.size() > 0);
            }
        }
        private TaskUnit getNext() {
            synchronized (lock) {
                for (TaskUnit taskUnit : taskUnits) {
                    if (!running.contains(taskUnit) && !satisfied.contains(taskUnit) && isSatisfied(taskUnit))
                        return taskUnit;
                }
            }
            return null;
        }

        private void satisfied(TaskUnit taskUnit) throws IOException {
            synchronized (lock) {
                if (ioException != null)
                    return;
                satisfied.add(taskUnit);
                boolean mustStartNext = finished && !isFinished() && (running.size() == 1);
                boolean started = false;
                TaskUnit start;
                while ((start = getNext()) != null) {
                    running.add(start);
                    executorService.submit(new TaskUnitRunWrapper(start));
                    started = true;
                }
                if (!started && mustStartNext)
                    throw new IOException("Dependency loop prevents running task units");
                if (isFinished())
                    executorService.shutdown();
            }
        }
        private void ioException(IOException e) {
            synchronized (lock) {
                System.out.println("IO err: "+e.getMessage());
                e.printStackTrace();
                ioException = e;
                executorService.shutdown();
            }
        }

        private int startFreeTasks() {
            int started = 0;
            synchronized (lock) {
                if (ioException != null)
                    return 0;
                for (TaskUnit taskUnit : taskUnits) {
                    if (!running.contains(taskUnit) && !satisfied.contains(taskUnit) && isSatisfied(taskUnit)) {
                        running.add(taskUnit);
                        executorService.submit(new TaskUnitRunWrapper(taskUnit));
                        started++;
                    }
                }
            }
            return started;
        }

        /**
         * Start the thread-pool. Returns the number of tasks that are initially ready to run.
         * @return
         */
        public int start() {
            synchronized (lock) {
                ioException = null;
                running = new HashSet<TaskUnit>();
                satisfied = new HashSet<TaskUnit>();
                executorService = Executors.newFixedThreadPool(nThreads);
            }
            addAddListener(this);
            return startFreeTasks();
        }
        public void setFinishedAdding() {
            synchronized (lock) {
                finished = true;
                if (isFinished())
                    executorService.shutdown();
            }
        }
        public void waitForTasks(boolean finished) throws IOException {
            synchronized (lock) {
                if (finished)
                    setFinishedAdding();
            }
            while (!executorService.isTerminated()) {
                try {
                    executorService.awaitTermination(1, TimeUnit.DAYS);
                } catch (InterruptedException e) {
                }
            }
            if (ioException != null)
                throw new IOException("Task unit aborted", ioException);
        }
        public void run() throws IOException {
            boolean started = (start() > 0);
            if (!started) {
                executorService.shutdown();
                throw new IOException("Unable to start any task units due to dependency loop");
            }
            waitForTasks(true);
        }

        @Override
        public void added(TaskUnit added) {
            startFreeTasks();
        }

        private class TaskUnitRunWrapper implements Runnable {
            private TaskUnit taskUnit;

            public TaskUnitRunWrapper(TaskUnit taskUnit) {
                this.taskUnit = taskUnit;
            }
            @Override
            public void run() {
                try {
                    taskUnit.run();
                    satisfied(taskUnit);
                } catch (IOException e) {
                    ioException(e);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    synchronized (lock) {
                        running.remove(taskUnit);
                    }
                }
            }
        }
    }
}
