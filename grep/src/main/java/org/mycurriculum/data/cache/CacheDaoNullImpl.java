package org.mycurriculum.data.cache;

public class CacheDaoNullImpl implements CacheDao {
    @Override
    public CacheNamespace getNamespace(String key) {
        return new CacheNamespace();
    }
    private static class CacheNamespace implements CacheDao.CacheNamespace {
        @Override
        public void clearCache() {
        }
        @Override
        public Object readObject(String key) {
            return null;
        }
        @Override
        public void writeObject(String key, Object object) {
        }
        @Override
        public void invalidateObject(String key) {
        }
    }
}
