package org.mycurriculum.data.cache;

public class CacheDaoDebuggerImpl implements CacheDao {
    private CacheDao cacheDao;

    public CacheDaoDebuggerImpl(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }

    @Override
    public CacheDao.CacheNamespace getNamespace(String key) {
        return new CacheNamespace(cacheDao.getNamespace(key), key);
    }

    private class CacheNamespace implements CacheDao.CacheNamespace {
        private String namespace;
        private CacheDao.CacheNamespace cache;

        public CacheNamespace(CacheDao.CacheNamespace cache, String namespace) {
            this.namespace = namespace;
            this.cache = cache;
        }

        @Override
        public void clearCache() {
            System.out.println("Clearing namespace cache: "+namespace);
        }

        @Override
        public Object readObject(String key) {
            Object obj = cache.readObject(key);
            System.out.println("Read object: "+namespace+"."+key+(obj != null ? ": hit" : ": miss"));
            return obj;
        }

        @Override
        public void writeObject(String key, Object object) {
            System.out.println("Write object: "+namespace+"."+key);
            cache.writeObject(key, object);
        }

        @Override
        public void invalidateObject(String key) {
            System.out.println("Invalidate object: "+namespace+"."+key);
            cache.invalidateObject(key);
        }
    }
}
