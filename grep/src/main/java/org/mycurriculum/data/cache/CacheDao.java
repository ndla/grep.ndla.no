package org.mycurriculum.data.cache;

public interface CacheDao {
    public CacheNamespace getNamespace(String key);

    public interface CacheNamespace {
        public void clearCache();
        public Object readObject(String key);
        public void writeObject(String key, Object object);
        public void invalidateObject(String key);
    }
    public class TypedCacheNamespace<T extends Object> {
        private CacheNamespace cacheNamespace;

        public TypedCacheNamespace(CacheNamespace cacheNamespace) {
            this.cacheNamespace = cacheNamespace;
        }

        public T readObject(String key) {
            return (T) cacheNamespace.readObject(key);
        }

        public void writeObject(String key, T object) {
            cacheNamespace.writeObject(key, object);
        }

        public void invalidateObject(String key) {
            cacheNamespace.invalidateObject(key);
        }
    }
}
