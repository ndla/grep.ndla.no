package org.mycurriculum.data.cache;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.cache.ehcache.EhCacheCacheManager;

public class CacheDaoEhcacheImpl implements CacheDao, DisposableBean {
    private final String cacheName;
    private final CacheManager cacheManager;

    public CacheDaoEhcacheImpl(EhCacheCacheManager cacheManager, String cacheName) {
        System.out.println("Initializing cache dao");
        this.cacheManager = cacheManager.getCacheManager();
        this.cacheName = cacheName;
    }

    @Override
    public CacheNamespace getNamespace(String key) {
        return new CacheNamespace(key);
    }

    private Ehcache cacheObj = null;
    private Ehcache getCache() {
        synchronized (this) {
            if (cacheObj == null)
                cacheObj = cacheManager.getEhcache(cacheName);
            return cacheObj;
        }
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("Flushing cache to disk");
        getCache().flush();
    }

    public static String dirEscape(String key) {
        char[] str = key.toCharArray();
        int[] esc = new int[str.length];
        int escNum = 0;
        for (int i = 0; i < str.length; i++) {
            if (str[i] == '\\' || str[i] == '/') {
                esc[escNum] = i;
                escNum++;
            }
        }
        if (escNum > 0) {
            char[] newStr = new char[str.length + escNum];
            int pos = 0;
            for (int i = 0; i < escNum; i++) {
                if (pos < esc[i]) {
                    System.arraycopy(str, pos, newStr, pos + i, esc[i] - pos);
                    newStr[esc[i] + i] = '\\';
                    pos = esc[i];
                }
            }
            if (pos < str.length) {
                System.arraycopy(str, pos, newStr, pos + escNum, str.length - pos);
            }
            return new String(newStr);
        }
        return key;
    }
    private class CacheNamespace implements CacheDao.CacheNamespace {
        private final Ehcache cache;
        private final String namespace;

        public CacheNamespace(String namespace) {
            cache = getCache();
            if (cache == null)
                throw new RuntimeException("Caching is not available (ehcache)");
            this.namespace = namespace;
        }

        private Object cacheGet(String key) {
            Element element = cache.get(key);
            if (element == null)
                return null;
            return element.getObjectValue();
        }
        private void cachePut(String key, Object object) {
            cache.put(new Element(key, object));
        }
        private void cacheInvalidate(String key) {
            cache.remove(key);
        }

        private String generateKeyString(String key) {
            return dirEscape(namespace)+"/"+dirEscape(key);
        }

        @Override
        public void clearCache() {
            cache.removeAll();
        }

        @Override
        public Object readObject(String key) {
            return cacheGet(generateKeyString(key));
        }

        @Override
        public void writeObject(String key, Object object) {
            cachePut(generateKeyString(key), object);
        }

        @Override
        public void invalidateObject(String key) {
            cacheInvalidate(generateKeyString(key));
        }
    }
}
