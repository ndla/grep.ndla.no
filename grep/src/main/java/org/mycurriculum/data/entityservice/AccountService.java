package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.Account;

import java.util.List;

public interface AccountService {
    public void save(Account account);

    public Account find(Long id);

    public Account findByName(String name);

    public List<Account> list();

    public void update(Account account);

    public void delete(Account account);
}
