package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.Role;

import java.util.List;

public interface RoleService {
    public void save(Role role);

    public Role find(Long id);

    Role findByName(String name);

    public List<Role> list();

    public void update(Role role);

    public void delete(Role role);
}
