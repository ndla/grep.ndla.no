package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.EducationGroup;
import org.mycurriculum.data.dao.EducationGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("educationGroupService")
@Transactional(value = "businessTransactionManager")
public class EducationGroupServiceImpl implements EducationGroupService {

    @Autowired
    private EducationGroupDao educationGroupDao;

    public void setEducationGroupDao(EducationGroupDao educationGroupDao) { // Property injection.
        this.educationGroupDao = educationGroupDao;
    }

    @Override
    public void save(EducationGroup educationGroup) {
        educationGroupDao.save(educationGroup);
    }

    @Override
    public EducationGroup find(Long id) {
        return educationGroupDao.find(id);
    }

    @Override
    public EducationGroup findByExternalId(String externalId) {
        return educationGroupDao.findByExternalId(externalId);
    }

    @Override
    public List<EducationGroup> list() {
        return educationGroupDao.list();
    }

    @Override
    public void update(EducationGroup educationGroup) {
        educationGroupDao.update(educationGroup);
    }

    @Override
    public void delete(EducationGroup educationGroup) {
        educationGroupDao.delete(educationGroup);
    }
}
