package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.CourseGroup;
import org.mycurriculum.data.dao.CourseGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("courseGroupService")
@Transactional(value = "businessTransactionManager")
public class CourseGroupServiceImpl implements CourseGroupService {

    @Autowired
    private CourseGroupDao courseGroupDao;

    public void setCourseGroupDao(CourseGroupDao courseGroupDao) { // Property injection.
        this.courseGroupDao = courseGroupDao;
    }

    @Override
    public void save(CourseGroup courseGroup) {
        courseGroupDao.save(courseGroup);
    }

    @Override
    public CourseGroup find(Long id) {
        CourseGroup courseGroup = courseGroupDao.find(id);
        return courseGroup;
    }

    @Override
    public CourseGroup findCourseGroupByPsi(String psi, Long versionNumber) {
        CourseGroup courseGroup = courseGroupDao.findCourseGroupByPsi(psi, versionNumber);
        return courseGroup;
    }

    @Override
    public List<CourseGroup> list() {
        return courseGroupDao.list();
    }

    @Override
    public void update(CourseGroup courseGroup) {
        courseGroupDao.update(courseGroup);
    }

    @Override
    public void delete(CourseGroup courseGroup) {
        courseGroup.setCourse(null);
        courseGroupDao.update(courseGroup);
        courseGroupDao.delete(courseGroup);
    }
}
