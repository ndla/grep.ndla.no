package org.mycurriculum.data.entityservice;

import org.hibernate.Hibernate;
import org.mycurriculum.business.domain.User;
import org.mycurriculum.data.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "securityTransactionManager")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public void setUserDao(UserDao userDao) { // Property injection.
        this.userDao = userDao;
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public User find(Long id) {
        User user = userDao.find(id);
        if (user != null) {
            Hibernate.initialize(user.getRoles());
            Hibernate.initialize(user.getAccount());
        }
        return user;
    }

    @Override
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);
        if (user != null) {
            Hibernate.initialize(user.getRoles());
            Hibernate.initialize(user.getAccount());
        }
        return user;
    }

    @Override
    public List<User> list() {
        return userDao.list();
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void delete(User user) {
        userDao.delete(user);
    }
}
