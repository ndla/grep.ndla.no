package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.Version;
import org.mycurriculum.data.dao.VersionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("versionService")
@Transactional(value = "businessTransactionManager")
public class VersionServiceImpl implements VersionService {

    @Autowired
    private VersionDao versionDao;

    public void setVersionDao(VersionDao versionDao) { // Property injection.
        this.versionDao = versionDao;
    }

    @Override
    public void save(Version version) {
        versionDao.save(version);
    }

    @Override
    public Version find(Long id) {
        Version version = versionDao.find(id);
        return version;
    }

    @Override
    public Version findVersionByVersionNumber(Long versionNumber) {
        Version version = versionDao.findVersionByVersionNumber(versionNumber);
        return version;
    }

    @Override
    public List<Version> list() {
        return versionDao.list();
    }

    @Override
    public void update(Version version) {
        versionDao.update(version);
    }

    @Override
    public void delete(Version version) {
        versionDao.delete(version);
    }

    @Override
    public Version findActive() {
        Version version = versionDao.findActive();
        return version;
    }

    @Override
    public void setActive(Version version) {
        versionDao.setActive(version);
    }
}
