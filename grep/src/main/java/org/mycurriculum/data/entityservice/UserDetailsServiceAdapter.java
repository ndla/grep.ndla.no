package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.User;
import org.mycurriculum.business.domain.UserDetailsAdapter;
import org.mycurriculum.data.dao.UserDetailsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
@Transactional(readOnly = true, value = "securityTransactionManager")
public class UserDetailsServiceAdapter implements UserDetailsService {

    @Autowired
    UserService userService;

    @Autowired
    UserDetailsDao userDetailsDao;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException, DataAccessException {
        User serviceUser = userService.findByUsername(username);
        if (serviceUser == null) {
            throw new UsernameNotFoundException("No such user: " + username);
        } else if (serviceUser.getRoles().isEmpty()) {
            throw new UsernameNotFoundException("User " + username + " has no authorities");
        }
        UserDetailsAdapter user = new UserDetailsAdapter(serviceUser);
        user.setPassword(userDetailsDao.findPasswordByUsername(username));
        return user;
    }
}