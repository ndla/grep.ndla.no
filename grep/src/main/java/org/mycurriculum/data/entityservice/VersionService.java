package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.Version;

import java.util.List;

public interface VersionService {
    public void save(Version version);

    public Version find(Long id);

    public Version findVersionByVersionNumber(Long versionNumber);

    public List<Version> list();

    public void update(Version version);

    public void delete(Version version);

    public Version findActive();

    public void setActive(Version version);
}
