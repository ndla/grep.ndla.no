package org.mycurriculum.data.entityservice;

import org.hibernate.Hibernate;
import org.mycurriculum.business.domain.Account;
import org.mycurriculum.data.dao.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "securityTransactionManager")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) { // Property injection.
        this.accountDao = accountDao;
    }

    @Override
    public void save(Account account) {
        accountDao.save(account);
    }

    @Override
    public Account find(Long id) {
        Account account = accountDao.find(id);
        if (account != null) {
            Hibernate.initialize(account.getUsers());
        }
        return account;
    }

    @Override
    public Account findByName(String name) {
        Account account = accountDao.findByName(name);
        if (account != null) {
            Hibernate.initialize(account.getUsers());
        }
        return account;
    }

    @Override
    public List<Account> list() {
        return accountDao.list();
    }

    @Override
    public void update(Account account) {
        accountDao.update(account);
    }

    @Override
    public void delete(Account account) {
        accountDao.delete(account);
    }
}
