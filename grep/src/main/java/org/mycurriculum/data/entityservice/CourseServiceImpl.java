/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 13, 2014
 */

package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.Course;
import org.mycurriculum.data.dao.CourseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("courseService")
@Transactional(value = "businessTransactionManager")
public class CourseServiceImpl implements CourseService {

    /*
    If there is only only one implementation of the CourseDao interface
    then this property could be autowired.
    */
    @Autowired
    private CourseDao courseDao;

    public void setCourseDao(CourseDao courseDao) { // Property injection.
        this.courseDao = courseDao;
    }

    @Override
    public void save(Course course) {
        courseDao.save(course);
    }

    @Override
    public Course find(Long id) {
        return courseDao.find(id);
    }

    @Override
    public Course findByExternalId(String externalId, Long version) {
        return courseDao.findByExternalId(externalId, version);
    }

    @Override
    public List<Course> list(Long version) {
        return courseDao.list(version);
    }

    @Override
    public void update(Course course) {
        courseDao.update(course);
    }

    @Override
    public void delete(Course course) {
        courseDao.delete(course);
    }
}
