package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.CourseGroup;

import java.util.List;

public interface CourseGroupService {

    public void save(CourseGroup courseGroup);

    public CourseGroup find(Long id);

    public CourseGroup findCourseGroupByPsi(String psi, Long versionNumber);

    public List<CourseGroup> list();

    public void update(CourseGroup courseGroup);

    public void delete(CourseGroup courseGroup);
}
