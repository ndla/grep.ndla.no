/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 13, 2014
 */

package org.mycurriculum.data.entityservice;

import org.mycurriculum.business.domain.Course;

import java.util.List;

public interface CourseService {
    public void save(Course course);

    public Course find(Long id);

    public Course findByExternalId(String externalId, Long version);

    public List<Course> list(Long version);

    public void update(Course course);

    public void delete(Course course);
}
