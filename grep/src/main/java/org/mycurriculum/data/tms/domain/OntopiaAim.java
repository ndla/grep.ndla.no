package org.mycurriculum.data.tms.domain;

import java.io.IOException;
import java.util.Collection;

public interface OntopiaAim extends OntopiaTopicCommon {
    public void addToAimSet(OntopiaAimSet aimSet);
    public void removeFromAimSet(OntopiaAimSet aimSet);
    public Collection<OntopiaAimSet> getAimSets();
    public Collection<OntopiaCurriculum> getCurricula();
    public Collection<OntopiaResourceAimAssociation> getResourceAssociations() throws IOException;
    public Collection<OntopiaResource> getResources() throws IOException;
    public void setSortingOrder(Integer sortingOrder);
    public Integer getSortingOrder();
    public interface Builder extends OntopiaTopicCommon.Builder<OntopiaAim,Builder> {
        public Builder aimSet(OntopiaAimSet aimSet);
        public Builder sortingOrder(Integer sortingOrder);
    }
}
