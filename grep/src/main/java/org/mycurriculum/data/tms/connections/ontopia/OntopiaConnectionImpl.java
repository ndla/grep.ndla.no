package org.mycurriculum.data.tms.connections.ontopia;

import net.ontopia.topicmaps.entry.TopicMapReferenceIF;
import net.ontopia.topicmaps.entry.TopicMapRepositoryIF;
import net.ontopia.topicmaps.entry.TopicMapSourceIF;
import net.ontopia.topicmaps.entry.XMLConfigSource;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.connections.TopicMapStore;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;
import org.mycurriculum.data.tms.references.TopicMapImpl;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by janespen on 4/29/14.
 */
public class OntopiaConnectionImpl implements OntopiaConnection, TopicMapStore {
    private File tmSources;
    private OntopiaConnectionFactory connectionFactory;
    private TmSourcesSetup setup;
    private DBConnectionSetup dbConnectionSetup;
    private TopicMapRepositoryIF repository = null;
    private Collection<String> referenceKeys = null;
    private String databaseConnectionString = null;
    private CacheDao cacheDao;

    public OntopiaConnectionImpl(OntopiaConnectionFactory connectionFactory, CacheDao cacheDao, TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) {
        this.connectionFactory = connectionFactory;
        this.cacheDao = cacheDao;
        this.setup = setup;
        this.dbConnectionSetup = dbConnectionSetup;
        this.databaseConnectionString = dbConnectionSetup.getFullConnectionString();
    }
    private void open() throws IOException {
        if (this.repository == null) {
            this.repository = this.connectionFactory.getOntopiaRepository(this.setup, this.dbConnectionSetup);
        }
    }
    public void close()
    {
    }
    public Collection<String> getTopicMapKeys() throws IOException {
        synchronized (this) {
            open();
            referenceKeys = repository.getReferenceKeys();
            return referenceKeys;
        }
    }
    private String getCacheNamespace(String topicmapKey) {
        return getDatabaseConnectionString()+":"+topicmapKey;
    }
    public TopicMapRoot getTopicMapByKey(String key) {
        TopicMapImpl tm = new TopicMapImpl(this, key);
        tm.setCache(cacheDao.getNamespace(getCacheNamespace(key)));
        return tm;
    }

    @Override
    public TopicMapRepositoryIF getTopicMapRepository() throws IOException {
        synchronized (this) {
            TopicMapRepositoryIF repository;
            open();
            repository = this.repository;
            return repository;
        }
    }

    @Override
    public String getDatabaseConnectionString() {
        if (databaseConnectionString != null)
            return databaseConnectionString;
        else
            throw new RuntimeException("The database connection string is not available in this context (fatal)");
    }

    @Override
    public String[] getTopicMaps() throws IOException {
        Collection<String> topicMaps = getTopicMapKeys();
        String[] topicMapArray = new String[topicMaps.size()];
        int i = 0;
        for (String tm : topicMaps) {
            topicMapArray[i] = tm;
            i++;
        }
        return topicMapArray;
    }

    @Override
    public TopicMapLocator getTopicMapLocator(String topicMap) {
        return new TopicMapReferenceLocator(this, topicMap);
    }

    @Override
    public TopicMapRoot getTopicMap(String topicMap) throws IOException {
        return getTopicMapLocator(topicMap).getTopicMapRoot();
    }

    @Override
    public TopicMapLocator getTopicMapByName(String topicMap) throws IOException {
        synchronized (this) {
            open();
            try {
                for (TopicMapReferenceIF ref : repository.getReferences()) {
                    if (topicMap.equals(ref.getTitle()))
                        return getTopicMapLocator(ref.getId());
                }
            } finally {
                close();
            }
        }
        return null;
    }

    @Override
    public TopicMapLocator createTopicMap(String topicMap) throws IOException {
        synchronized (this) {
            open();
            try {
                Collection<TopicMapSourceIF> sources = repository.getSources();
                if (sources.size() == 0)
                    throw new IOException("No topic map sources available");
                TopicMapSourceIF source = sources.iterator().next();
                TopicMapReferenceIF ref = source.createTopicMap(topicMap, "http://mycurriculum.org/ontopia/");
                ref.setTitle(topicMap);
                String id = ref.getId();
                ref.close();
                return getTopicMapLocator(id);
            } finally {
                close();
            }
        }
    }

    @Override
    public CacheDao getCacheDao() {
        return cacheDao;
    }

    public void setCacheDao(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }
}
