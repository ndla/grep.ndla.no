package org.mycurriculum.data.tms.abstracts;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by janespen on 5/2/14.
 */
public class OntopiaTopicFinder extends OntopiaTopicRef {
    private TopicMap topicMap;
    private LocatorIF locator;

    public OntopiaTopicFinder(TopicMap topicMap, LocatorIF locator)
    {
        this.topicMap = topicMap;
        this.locator = locator;
    }

    private static class LookupPsiToIdCache implements Serializable {
        private String id;

        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
    }
    public static String cacheGetIdFromPsi(CacheDao.CacheNamespace cacheNs, String psi) {
        CacheDao.TypedCacheNamespace<LookupPsiToIdCache> cache = new CacheDao.TypedCacheNamespace<LookupPsiToIdCache>(cacheNs);
        LookupPsiToIdCache lookup = cache.readObject("LookupPsiToId:"+psi);
        if (lookup != null)
            return lookup.getId();
        else
            return null;
    }
    public static void cacheSetIdFromPsi(CacheDao.CacheNamespace cacheNs, String psi, String id) {
        CacheDao.TypedCacheNamespace<LookupPsiToIdCache> cache = new CacheDao.TypedCacheNamespace<LookupPsiToIdCache>(cacheNs);
        String key = "LookupPsiToId:"+psi;
        if (id == null) {
            cache.invalidateObject(key);
            return;
        }
        LookupPsiToIdCache lookup = new LookupPsiToIdCache();
        lookup.setId(id);
        cache.writeObject(key, lookup);
    }
    public OntopiaTopicFinderById getFinderById() throws IOException {
        TopicIF topic = getTopic();
        if (topic == null)
            return null;
        String id = topic.getObjectId();
        return new OntopiaTopicFinderById(topicMap, id);
    }
    public OntopiaTopicFinderById getFinderById(CacheDao.CacheNamespace cache) throws IOException {
        String psi = locator.getAddress();
        String id = cacheGetIdFromPsi(cache, psi);
        if (id == null) {
            OntopiaTopicFinderById finder = getFinderById();
            if (finder == null)
                return null;
            id = finder.getId();
            cacheSetIdFromPsi(cache, psi, id);
            return finder;
        }
        return new OntopiaTopicFinderById(topicMap, id);
    }

    @Override
    public void resetTopicMap(TopicMap topicMap) {
        this.topicMap = topicMap;
    }

    @Override
    public TopicMap getTopicMapReference() {
        return topicMap;
    }

    @Override
    public TopicMapIF getTopicMap() throws IOException {
        return topicMap.getTopicMap();
    }

    @Override
    public TopicIF getTopic() throws IOException {
        return topicMap.getTopicMap().getTopicBySubjectIdentifier(locator);
    }

    @Override
    public void commit() {
        topicMap.commit();
    }

    @Override
    public void abort() {
        topicMap.abort();
    }

    @Override
    public void close() {
        topicMap.close();
    }
}
