package org.mycurriculum.data.tms.abstracts;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;

public class OntopiaTopicFinderById extends OntopiaTopicRef {
    private TopicMap topicMap;
    private String id;

    public OntopiaTopicFinderById(TopicMap topicMap, String id) {
        this.topicMap = topicMap;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public void resetTopicMap(TopicMap topicMap) {
        this.topicMap = topicMap;
    }

    @Override
    public TopicMap getTopicMapReference() {
        return topicMap;
    }

    @Override
    public TopicMapIF getTopicMap() throws IOException {
        return topicMap.getTopicMap();
    }

    @Override
    public TopicIF getTopic() throws IOException {
        return (TopicIF) topicMap.getTopicMap().getObjectById(id);
    }

    @Override
    public void commit() {
        topicMap.commit();
    }

    @Override
    public void abort() {
        topicMap.abort();
    }

    @Override
    public void close() {
        topicMap.close();
    }
}
