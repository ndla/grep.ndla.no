package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class OntopiaAimSetImpl extends OntopiaTopicCommonImpl implements OntopiaAimSet {
    private static OntopiaTopicType type = null;

    private boolean curriculaChanged = false;
    private LazyLoaderCollection<OntopiaCurriculum> curricula;
    private boolean aimsChanged = false;
    private LazyLoaderCollection<OntopiaAim> aims;
    private boolean aimSetsChanged = false;
    private LazyLoaderCollection<OntopiaAimSet> aimSets;
    private boolean levelsChanged = false;
    private LazyLoaderCollection<OntopiaLevel> levels;
    private LazyLoaderCollection<OntopiaAimSet> aimSetParents; /* Read only! */

    @OccurrenceProperty(type = OntopiaPsis.upstreamIdPsi, addedFieldTypeCompatibility = true)
    private String upstreamId;
    @OccurrenceProperty(type = OntopiaPsis.setTypePsi)
    private String setType;
    @OccurrenceProperty(type = OntopiaPsis.sortingOrderPsi)
    private Integer sortingOrder;

    public OntopiaAimSetImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }

    public OntopiaAimSetImpl(TopicMap topicMap, OntopiaTopicType type) throws IOException {
        super(topicMap, type);
    }

    public OntopiaAimSetImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    public static OntopiaTopicType getOntopiaType() {
        synchronized (OntopiaCourseStructureImpl.class) {
            if (type == null)
                type = createOntopiaType(OntopiaPsis.aimSetTypePsi);
            return type;
        }
    }

    public static URL generatePsi(String humanId) {
        try {
            return new URL(OntopiaPsis.getRootPsi()+"competence-aim-set#"+humanId);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unexpetected url exception", e);
        }
    }

    private void readCurricula() throws IOException {
        Collection<String> parents = getAssociatedParents(OntopiaPsis.curriculumHasCompetenceAimSetsPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(parents.size());
        for (String parent : parents) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), parent);
            refs.add(ref);
        }
        curricula = new LazyLoaderCollectionBuilder<OntopiaCurriculum,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaCurriculum load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCurriculumImpl(ref);
            }
        }.build();
        curriculaChanged = false;
    }
    private void writeCurricula() throws IOException {
        if (curriculaChanged) {
            curriculaChanged = false;
            Collection<TopicIF> parents = new ArrayList<TopicIF>(curricula.size());
            for (OntopiaCurriculum curriculum : curricula) {
                TopicIF topic = curriculum.getTopic();
                parents.add(topic);
            }
            saveAssociatedParents(OntopiaPsis.curriculumHasCompetenceAimSetsPsi, parents);
        }
    }
    private void readAims() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaAimImpl.aimSetHasAim);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String child : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), child);
            refs.add(ref);
        }
        aims = new LazyLoaderCollectionBuilder<OntopiaAim,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaAim load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimImpl(ref);
            }
        }.build();
        aimsChanged = false;
    }
    private void writeAims() throws IOException {
        if (aimsChanged) {
            aimsChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(aims.size());
            for (OntopiaAim aim : aims) {
                TopicIF topic = aim.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaAimImpl.aimSetHasAim, children);
        }
    }
    private void readInAimSets() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaPsis.aimSetHasAimSetPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String child : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), child);
            refs.add(ref);
        }
        aimSets = new LazyLoaderCollectionBuilder<OntopiaAimSet,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaAimSet load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimSetImpl(ref);
            }
        }.build();
        aimSetsChanged = false;
        Collection<String> parents = getAssociatedParents(OntopiaPsis.aimSetHasAimSetPsi);
        refs = new ArrayList<OntopiaTopicRef>(parents.size());
        for (String parent : parents) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), parent);
            refs.add(ref);
        }
        aimSetParents = new LazyLoaderCollectionBuilder<OntopiaAimSet,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaAimSet load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimSetImpl(ref);
            }
        }.build();
    }
    private void saveAimSets() throws IOException {
        if (aimSetsChanged) {
            aimSetsChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(aimSets.size());
            for (OntopiaAimSet aimSet : aimSets) {
                TopicIF topic = aimSet.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaPsis.aimSetHasAimSetPsi, children);
        }
    }
    public void readInLevels() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaPsis.aimSetHasLevelPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String child : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), child);
            refs.add(ref);
        }
        levels = new LazyLoaderCollectionBuilder<OntopiaLevel,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaLevel load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaLevelImpl(ref);
            }
        }.build();
        levelsChanged = false;
    }
    public void saveLevels() throws IOException {
        if (levelsChanged) {
            levelsChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(levels.size());
            for (OntopiaLevel level : levels) {
                TopicIF topic = level.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaPsis.aimSetHasLevelPsi, children);
        }
    }

    @Override
    public void readInValues() throws IOException {
        readCurricula();
        readAims();
        readInAimSets();
        readInLevels();
    }

    @Override
    public void saveValues() throws IOException {
        writeCurricula();
        writeAims();
        saveAimSets();
        saveLevels();
    }

    @Override
    public Set<OntopiaCurriculum> getAllContainingCurricula() {
        HashSet<OntopiaCurriculum> curricula = new HashSet<OntopiaCurriculum>();
        for (OntopiaAimSet aimSet : getParentAimSets()) {
            for (OntopiaCurriculum curriculum : aimSet.getAllContainingCurricula()) {
                if (!curricula.contains(curriculum))
                    curricula.add(curriculum);
            }
        }
        for (OntopiaCurriculum curriculum : getCurricula()) {
            if (!curricula.contains(curriculum))
                curricula.add(curriculum);
        }
        return curricula;
    }

    @Override
    public Collection<OntopiaCurriculum> getCurricula() {
        synchronized (this) {
            return (Collection<OntopiaCurriculum>) curricula.clone();
        }
    }

    @Override
    public void addCurriculum(OntopiaCurriculum curriculum) {
        synchronized (this) {
            curricula.add(curriculum);
            curriculaChanged = true;
        }
    }

    @Override
    public void removeCurriculum(OntopiaCurriculum curriculum) {
        synchronized (this) {
            curricula.remove(curriculum);
            curriculaChanged = true;
        }
    }

    @Override
    public Collection<OntopiaAim> getAims() {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaAim>) aims.clone();
        }
    }

    @Override
    public void addAim(OntopiaAim aim) {
        synchronized (this) {
            aims.add(aim);
            aimsChanged = true;
        }
    }

    @Override
    public void removeAim(OntopiaAim aim) {
        synchronized (this) {
            aims.remove(aim);
            aimsChanged = true;
        }
    }

    @Override
    public Collection<OntopiaAimSet> getAimSets() {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaAimSet>) aimSets.clone();
        }
    }

    @Override
    public void addAimSet(OntopiaAimSet aimSet) {
        synchronized (this) {
            aimSets.add(aimSet);
            aimSetsChanged = true;
        }
    }

    @Override
    public void removeAimSet(OntopiaAimSet aimSet) {
        synchronized (this) {
            aimSets.remove(aimSet);
            aimSetsChanged = true;
        }
    }

    @Override
    public Collection<OntopiaAimSet> getParentAimSets() {
        synchronized (this) {
            return (Collection<OntopiaAimSet>) aimSetParents.clone();
        }
    }

    @Override
    public Collection<OntopiaLevel> getLevels() {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaLevel>) levels.clone();
        }
    }

    @Override
    public void addLevel(OntopiaLevel level) {
        synchronized (this) {
            levels.add(level);
            levelsChanged = true;
        }
    }

    @Override
    public void removeLevel(OntopiaLevel level) {
        synchronized (this) {
            levels.remove(level);
            levelsChanged = true;
        }
    }

    @Override
    public String getSetType() {
        return setType;
    }

    @Override
    public void setSetType(String setType) {
        this.setType = setType;
    }

    @Override
    public Integer getSortingOrder() {
        return sortingOrder;
    }

    @Override
    public void setSortingOrder(Integer sortingOrder) {
        this.sortingOrder = sortingOrder;
    }

    @Override
    public String getUpstreamId() {
        return upstreamId;
    }

    @Override
    public void setUpstreamId(String upstreamId) {
        this.upstreamId = upstreamId;
    }

    public static class Builder extends OntopiaTopicCommonImpl.Builder<OntopiaAimSetImpl,Builder> implements OntopiaAimSet.Builder {
        private Set<OntopiaCurriculum> curriculums = new HashSet<OntopiaCurriculum>();
        private Set<OntopiaAimSet> aimSets = new HashSet<OntopiaAimSet>();
        private Set<OntopiaAim> aims = new HashSet<OntopiaAim>();
        private Integer sortingOrder = null;
        private String upstreamId = null;

        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaType());
        }

        @Override
        public OntopiaAimSet.Builder aimSet(OntopiaAimSet aimSet) {
            aimSets.add(aimSet);
            return this;
        }

        @Override
        public Builder addCurriculum(OntopiaCurriculum curriculum) {
            curriculums.add(curriculum);
            return this;
        }

        @Override
        public OntopiaAimSet.Builder sortingOrder(Integer sortingOrder) {
            this.sortingOrder = sortingOrder;
            return this;
        }

        @Override
        public OntopiaAimSet.Builder aim(OntopiaAim aim) {
            aims.add(aim);
            return this;
        }

        @Override
        public OntopiaAimSet.Builder upstreamId(String upstreamId) {
            this.upstreamId = upstreamId;
            return this;
        }

        @Override
        public OntopiaAimSetImpl createInstance() throws IOException {
            return new OntopiaAimSetImpl(getRef());
        }

        @Override
        public void builder(OntopiaAimSetImpl object) {
            super.builder(object);
            for (OntopiaCurriculum curriculum: curriculums) {
                object.addCurriculum(curriculum);
            }
            for (OntopiaAimSet aimSet : aimSets) {
                object.addAimSet(aimSet);
            }
            for (OntopiaAim aim : aims) {
                object.addAim(aim);
            }
            if (sortingOrder != null)
                object.setSortingOrder(sortingOrder);
            if (upstreamId != null)
                object.setUpstreamId(upstreamId);
        }
    }
}
