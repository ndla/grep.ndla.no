package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import org.mycurriculum.data.tms.abstracts.OccurrenceProperty;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicAbstract;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicRef;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicType;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;

public class OntopiaTopicCommonImpl extends OntopiaTopicAbstract implements OntopiaTopicCommon {
    @OccurrenceProperty(type = OntopiaPsis.humanIdPsi)
    private String humanId;

    public OntopiaTopicCommonImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }

    public OntopiaTopicCommonImpl(TopicMap topicMap, OntopiaTopicType type) throws IOException {
        super(topicMap, type);
    }

    public OntopiaTopicCommonImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    @Override
    public void readInValues() throws IOException {
    }

    @Override
    public void saveValues() throws IOException {
    }

    @Override
    public void setHumanId(String humanId) {
        this.humanId = humanId;
    }

    @Override
    public String getHumanId() {
        return humanId;
    }

    /* Validator class for the abstract Builder */
    public static class AnonymousTopicBuilder extends Builder<OntopiaTopicCommon,AnonymousTopicBuilder> implements AnonymousBuilder {
        public AnonymousTopicBuilder(TopicMap topicMap, OntopiaTopicType type) {
            super(topicMap, type);
        }
        @Override
        public OntopiaTopicCommon createInstance() throws IOException {
            return new OntopiaTopicCommonImpl(getRef());
        }
    }
    public static abstract class Builder<T extends OntopiaTopicCommon, K extends OntopiaTopicCommon.Builder> extends OntopiaTopicAbstract.Builder<T, K> {
        private String humanId = null;

        public Builder(TopicMap topicMap, OntopiaTopicType type) {
            super(topicMap, type);
        }

        public K humanId(String humanId) {
            this.humanId = humanId;
            return (K) this;
        }

        @Override
        public void builder(T object) {
            if (humanId != null)
                object.setHumanId(humanId);
        }
    }
}
