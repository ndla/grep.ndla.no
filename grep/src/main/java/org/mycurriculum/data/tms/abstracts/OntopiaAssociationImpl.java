package org.mycurriculum.data.tms.abstracts;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.AssociationIF;
import net.ontopia.topicmaps.core.AssociationRoleIF;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapBuilderIF;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.domain.OntopiaPsis;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class OntopiaAssociationImpl implements OntopiaAssociation {
    private Type type;
    private TopicMap topicMap;
    private String assocPsi;
    private boolean assocPsiChanged = false;
    private String assocId;
    private String humanId;
    private Map<String,Set<String>> roles = new HashMap<String,Set<String>>();
    private boolean rolesChanged = false;
    private String reifierId;
    private boolean reifierIdChanged = false;

    private static class AssociationCache implements Serializable {
        public Map<String,Set<String>> roles;
        public String humanId;
        public String assocPsi;
        public String reifierId;
    }
    private static class ObjectAssociationCache implements Serializable {
        public Collection<String> associations;
    }

    private void toCache(AssociationCache cache) {
        cache.roles = this.roles;
        cache.humanId = this.humanId;
        cache.assocPsi = this.assocPsi;
        cache.reifierId = this.reifierId;
    }
    private void fromCache(AssociationCache cache) {
        this.humanId = cache.humanId;
        this.assocPsi = cache.assocPsi;
        this.roles = cache.roles;
        this.rolesChanged = false;
        this.reifierId = cache.reifierId;
        this.reifierIdChanged = false;
    }
    private void removeFromCache() {
        String key = this.assocId;
        CacheDao.TypedCacheNamespace<AssociationCache> cache = new CacheDao.TypedCacheNamespace<AssociationCache>(topicMap.getCache());
        cache.invalidateObject(key);
    }
    private void saveToCache()
    {
        String key = this.assocId;
        CacheDao.TypedCacheNamespace<AssociationCache> cache = new CacheDao.TypedCacheNamespace<AssociationCache>(topicMap.getCache());
        CacheDao.TypedCacheNamespace<ObjectAssociationCache> lookupCache = new CacheDao.TypedCacheNamespace<ObjectAssociationCache>(topicMap.getCache());
        AssociationCache cached = new AssociationCache();
        toCache(cached);
        cache.writeObject(key, cached);
        for (Map.Entry<String,Set<String>> roleEntry : roles.entrySet()) {
            String rolePsi = roleEntry.getKey();
            Set<String> players = roleEntry.getValue();
            for (String playerId : players) {
                String ckey = rolePsi+"/"+playerId;
                ObjectAssociationCache objectCache = lookupCache.readObject(ckey);
                if (objectCache == null) {
                    objectCache = new ObjectAssociationCache();
                }
                if (objectCache.associations == null) {
                    objectCache.associations = new HashSet<String>();
                }
                if (!objectCache.associations.contains(this.assocId)) {
                    objectCache.associations.add(this.assocId);
                    lookupCache.writeObject(ckey, objectCache);
                }
            }
        }
    }
    private boolean loadFromCache()
    {
        if (this.assocId == null)
            return false;
        String key = this.assocId;
        CacheDao.TypedCacheNamespace<AssociationCache> cache = new CacheDao.TypedCacheNamespace<AssociationCache>(topicMap.getCache());
        AssociationCache cached = cache.readObject(key);
        if (cached != null) {
            fromCache(cached);
            return true;
        }
        return false;
    }
    private boolean checkAssocValid(String assocId, String topicTypePsi, String topicId) {
        String key = assocId;
        CacheDao.TypedCacheNamespace<AssociationCache> cache = new CacheDao.TypedCacheNamespace<AssociationCache>(topicMap.getCache());
        AssociationCache cached = cache.readObject(key);
        if (cached != null) {
            Set<String> players = cached.roles.get(topicTypePsi);
            if (players != null && players.contains(topicId))
                return true;
        }
        return false;
    }
    private boolean loadFromCache(String topicTypePsi, String topicId) {
        CacheDao.TypedCacheNamespace<ObjectAssociationCache> lookupCache = new CacheDao.TypedCacheNamespace<ObjectAssociationCache>(topicMap.getCache());
        String ckey = topicTypePsi+"/"+topicId;
        ObjectAssociationCache cached = lookupCache.readObject(ckey);
        if (cached == null || cached.associations == null)
            return false;
        for (String assocId : cached.associations) {
            if (checkAssocValid(assocId, topicTypePsi, topicId)) {
                this.assocId = assocId;
                return loadFromCache();
            }
        }
        return false;
    }
    private void checkId(boolean inTmSession) throws IOException {
        if (this.humanId == null) {
            boolean success = false;
            try {
                if (this.assocId == null) {
                    throw new RuntimeException("Expected to have an assocId to identify the object");
                }
                AssociationIF assoc = (AssociationIF) topicMap.getTopicMap().getObjectById(assocId);
                readHumanIdFromAssocIF(assoc);
                if (this.humanId == null) {
                    String uuid = UUID.randomUUID().toString();
                    this.assocPsi = topicMap.getTopicMap().getStore().getBaseAddress().getAddress() + "#" + uuid;
                    this.assocPsiChanged = false; /* Written to storage below */
                    this.humanId = uuid;
                    assoc.addItemIdentifier(new URILocator(this.assocPsi));
                }
                if (!inTmSession) {
                    topicMap.commit();
                }
                saveToCache();
                success = true;
            } finally {
                if (!inTmSession) {
                    if (!success) {
                        topicMap.abort();
                    }
                    topicMap.close();
                }
            }
        }
    }

    private OntopiaAssociationImpl(Type type, TopicMap topicMap, AssociationIF assoc) throws IOException {
        this.type = type;
        this.topicMap = topicMap;
        this.assocId = assoc.getObjectId();
        readFromAssocIF(assoc);
        checkId(true);
    }
    private OntopiaAssociationImpl(Type type, TopicMap topicMap, String assocId) throws IOException {
        this.type = type;
        this.topicMap = topicMap;
        this.assocId = assocId;
        if (!loadFromCache()) {
            boolean success = false;
            try {
                AssociationIF assoc = (AssociationIF) topicMap.getTopicMap().getObjectById(assocId);
                readFromAssocIF(assoc);
                topicMap.commit();
                success = true;
            } finally {
                if (!success)
                    topicMap.abort();
                topicMap.close();
            }
        }
        checkId(false);
    }
    public OntopiaAssociationImpl(Type type, TopicMap topicMap, String topicTypePsi, String topicObjectId) throws IOException {
        this.type = type;
        this.topicMap = topicMap;
        boolean success = false;
        /* check cache */
        if (loadFromCache(topicTypePsi, topicObjectId)) {
            success = true;
            checkId(false);
            return;
        }
        /* read topicmap */
        try {
            TopicIF topic = (TopicIF) topicMap.getTopicMap().getObjectById(topicObjectId);
            TopicIF assocType = topicMap.getTopicMap().getTopicBySubjectIdentifier(new URILocator(type.typePsi));
            Collection<AssociationIF> assocs = topic.getAssociationsByType(assocType);
            AssociationIF assoc;
            if (assocs.size() == 1) {
                assoc = assocs.iterator().next();
                readFromAssocIF(assoc);
            } else if (assocs.size() == 0) {
                this.assocId = null;
                this.reifierId = null;
                Set<String> myOwnRolePlayer = new HashSet<String>();
                myOwnRolePlayer.add(topicObjectId);
                this.roles.put(topicTypePsi, myOwnRolePlayer);
                this.reifierIdChanged = true;
            } else {
                throw new IOException("Can't handle a collection of association object (owner of association confusion)");
            }
            topicMap.commit();
            success = true;
        } finally {
            if (!success)
                topicMap.abort();
            topicMap.close();
        }
        checkId(false);
    }
    public OntopiaAssociationImpl(Type type, TopicMap topicMap) throws IOException {
        this.type = type;
        this.topicMap = topicMap;
        this.assocId = null;
        String uuid = UUID.randomUUID().toString();
        this.assocPsi = topicMap.getTopicMap().getStore().getBaseAddress().getAddress()+"#"+uuid;
        this.assocPsiChanged = true;
        this.humanId = uuid;
        this.reifierId = null;
        this.reifierIdChanged = false;
    }
    private static AssociationIF constructorGetAssocById(TopicMap topicMap, String humanId) throws IOException {
        boolean success = false;
        try {
            String psi = topicMap.getTopicMap().getStore().getBaseAddress().getAddress()+"#"+humanId;
            AssociationIF assoc = (AssociationIF) topicMap.getTopicMap().getObjectByItemIdentifier(new URILocator(psi));
            success = true;
            return assoc;
        } finally {
            if (!success) {
                topicMap.abort();
                topicMap.close();
            }
        }
    }
    public OntopiaAssociationImpl(String humanId, Type type, TopicMap topicMap) throws IOException {
        boolean success = false;
        this.type = type;
        this.topicMap = topicMap;
        AssociationIF assoc = constructorGetAssocById(topicMap, humanId);
        try {
            this.assocId = assoc.getObjectId();
            readFromAssocIF(assoc);
            checkId(true);
            topicMap.commit();
            success = true;
        } finally {
            if (!success)
                topicMap.abort();
            topicMap.close();
        }
    }

    public static OntopiaAssociationImpl getAssociationById(Type type, TopicMap topicMap, String humanId) throws IOException {
        boolean success = false;
        AssociationIF assoc = constructorGetAssocById(topicMap, humanId);
        OntopiaAssociationImpl assocObj;
        try {
            if (assoc != null) {
                assocObj = new OntopiaAssociationImpl(type, topicMap, assoc);
            } else {
                assocObj = null;
            }
            topicMap.commit();
            success = true;
        } finally {
            if (!success)
                topicMap.abort();
            topicMap.close();
        }
        return assocObj;
    }

    public static Collection<OntopiaAssociationImpl> getAssociationObjects(Type type, TopicMap topicMap, String topicObjectId) throws IOException {
        /* check cache */
        /* load from ontopia */
        Collection<OntopiaAssociationImpl> associations = new ArrayList<OntopiaAssociationImpl>();
        boolean success = false;
        try {
            TopicIF topic = (TopicIF) topicMap.getTopicMap().getObjectById(topicObjectId);
            TopicIF assocType = topicMap.getTopicMap().getTopicBySubjectIdentifier(new URILocator(type.typePsi));
            Collection<AssociationIF> assocs = topic.getAssociationsByType(assocType);
            for (AssociationIF assoc : assocs) {
                associations.add(new OntopiaAssociationImpl(type, topicMap, assoc));
            }
            topicMap.commit();
            success = true;
        } finally {
            if (!success)
                topicMap.abort();
            topicMap.close();
        }
        return associations;
    }

    private void readHumanIdFromAssocIF(AssociationIF assoc) {
        Collection<LocatorIF> iis = assoc.getItemIdentifiers();
        List<String> candidates = new ArrayList<String>();
        for (LocatorIF ii : iis) {
            this.assocPsi = ii.getAddress();
            int fragmentIndex = this.assocPsi.indexOf('#');
            if (fragmentIndex > 0) {
                candidates.add(this.assocPsi.substring(fragmentIndex + 1));
            }
        }
        if (candidates.size() == 1) {
            this.humanId = candidates.iterator().next();
            return;
        }
        if (candidates.size() == 0) {
            this.humanId = null;
            return;
        }
        this.humanId = null;
        for (String cand : candidates) {
            if (this.humanId == null || this.humanId.compareTo(cand) < 0) {
                this.humanId = cand;
            }
        }
    }
    private void readFromAssocIF(AssociationIF assoc) throws IOException {
        if (loadFromCache()) {
            return;
        }
        this.assocId = assoc.getObjectId();
        readHumanIdFromAssocIF(assoc);
        Map<String,TopicIF> roleTypes = new HashMap<String,TopicIF>();
        for (String rolePsi : type.rolePsis) {
            TopicIF roleType = topicMap.getTopicMap().getTopicBySubjectIdentifier(new URILocator(rolePsi));
            if (roleType == null)
                throw new IOException("Role type not found for assoc: "+rolePsi);
            roleTypes.put(rolePsi, roleType);
        }
        for (Map.Entry<String,TopicIF> roleTypeEntry : roleTypes.entrySet()) {
            String rolePsi = roleTypeEntry.getKey();
            TopicIF roleType = roleTypeEntry.getValue();
            Set<String> players = new HashSet<String>();
            for (AssociationRoleIF role : assoc.getRolesByType(roleType)) {
                players.add(role.getPlayer().getObjectId());
            }
            this.roles.put(rolePsi, players);
        }
        this.rolesChanged = false;
        TopicIF reifier = assoc.getReifier();
        if (reifier != null) {
            reifierId = reifier.getObjectId();
        } else {
            reifierId = null;
        }
        reifierIdChanged = false;
        saveToCache();
    }


    @Override
    public TopicMap getTopicMap() {
        return topicMap;
    }

    @Override
    public String getHumanId() {
        return humanId;
    }

    @Override
    public void setHumanId(String humanId) throws IOException {
        this.humanId = humanId;
        this.assocPsi = topicMap.getTopicMap().getStore().getBaseAddress().getAddress()+"#"+humanId;
        this.assocPsiChanged = true;
    }

    @Override
    public String[] getRoles() {
        String[] roles = new String[type.rolePsis.length];
        System.arraycopy(type.rolePsis, 0, roles, 0, roles.length);
        return roles;
    }

    @Override
    public Collection<String> getPlayers(String role) {
        synchronized (this) {
            Set<String> rolePlayers = new HashSet<String>();
            if (roles.containsKey(role)) {
                rolePlayers.addAll(roles.get(role));
            }
            return rolePlayers;
        }
    }

    @Override
    public void addPlayer(String role, OntopiaTopic topic) {
        synchronized (this) {
            this.rolesChanged = true;
            Set<String> players = this.roles.get(role);
            if (players == null) {
                players = new HashSet<String>();
                players.add(topic.getObjectId());
                this.roles.put(role, players);
                return;
            }
            players.add(topic.getObjectId());
        }
    }

    @Override
    public void removePlayer(String role, OntopiaTopic topic) {
        synchronized (this) {
            Set<String> players = this.roles.get(role);
            if (players != null) {
                players.remove(topic.getObjectId());
                this.rolesChanged = true;
            }
        }
    }

    @Override
    public void setPlayers(String role, Collection<? extends OntopiaTopic> topics) {
        Set<String> players = new HashSet<String>();
        for (OntopiaTopic topic : topics) {
            players.add(topic.getObjectId());
        }
        synchronized (this) {
            Set<String> origPlayers = this.roles.get(role);
            if ((origPlayers != null) && origPlayers.size() == players.size()) {
                boolean match = true;
                for (String player : players) {
                    if (!origPlayers.contains(player)) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    return;
                }
            }
            this.roles.put(role, players);
            this.rolesChanged = true;
        }
    }

    @Override
    public String getReifierId() {
        synchronized (this) {
            return this.reifierId;
        }
    }

    @Override
    public OntopiaTopicRef getReifierRef() {
        String id = getReifierId();
        if (id != null)
            return new OntopiaTopicFinderById(topicMap, id);
        else
            return null;
    }

    @Override
    public void setReifierId(String id) {
        synchronized (this) {
            this.reifierId = id;
            this.reifierIdChanged = true;
        }
    }

    @Override
    public void setReifier(OntopiaTopic topic) {
        setReifierId(topic.getObjectId());
    }

    @Override
    public void remove() throws IOException {
        if (this.assocId != null) {
            boolean success = false;
            try {
                synchronized (this) {
                    AssociationIF assoc = (AssociationIF) this.topicMap.getTopicMap().getObjectById(this.assocId);
                    assoc.remove();
                }
                topicMap.commit();
                removeFromCache();
                success = true;
            } finally {
                if (!success)
                    topicMap.abort();
                topicMap.close();
            }
        }
    }

    @Override
    public void save() throws IOException {
        TopicMapBuilderIF builder;
        AssociationIF assoc;
        boolean newObject;
        boolean success = false;
        try {
            synchronized (this) {
                builder = this.topicMap.getTopicMap().getBuilder();
                if (this.assocId != null) {
                    assoc = (AssociationIF) this.topicMap.getTopicMap().getObjectById(this.assocId);
                    newObject = false;
                } else {
                    TopicIF assocType = this.topicMap.getTopicMap().getTopicBySubjectIdentifier(new URILocator(this.type.typePsi));
                    if (assocType == null)
                        throw new IOException("Missing association type: " + this.type.typePsi);
                    assoc = builder.makeAssociation(assocType);
                    this.assocId = assoc.getObjectId();
                    if (this.assocPsi != null) {
                        assoc.addItemIdentifier(new URILocator(this.assocPsi));
                    }
                    this.assocPsiChanged = false;
                    newObject = true;
                }
                if (!newObject && assocPsiChanged) {
                    Collection<LocatorIF> locators = assoc.getItemIdentifiers();
                    Collection<LocatorIF> removeLocators = new ArrayList<LocatorIF>();
                    boolean found = false;
                    for (LocatorIF locator : locators) {
                        if (locator.getAddress().equals(this.assocPsi)) {
                            found = true;
                        } else {
                            removeLocators.add(locator);
                        }
                    }
                    if (!found) {
                        assoc.addItemIdentifier(new URILocator(this.assocPsi));
                    }
                    for (LocatorIF locator : removeLocators) {
                        assoc.removeItemIdentifier(locator);
                    }
                    assocPsiChanged = false;
                }
                if (rolesChanged) {
                    rolesChanged = false;
                    for (String typePsi : type.rolePsis) {
                        TopicIF assocType = this.topicMap.getTopicMap().getTopicBySubjectIdentifier(new URILocator(typePsi));
                        Set<String> existingRoles = new HashSet<String>();
                        Map<String, AssociationRoleIF> roleObjMap = new HashMap<String, AssociationRoleIF>();
                        if (!newObject) {
                            for (AssociationRoleIF role : assoc.getRolesByType(assocType)) {
                                existingRoles.add(role.getPlayer().getObjectId());
                                roleObjMap.put(role.getPlayer().getObjectId(), role);
                            }
                        }
                        Set<String> rawAddingRoles = roles.get(typePsi);
                        Set<String> addingRoles = new HashSet<String>();
                        if (rawAddingRoles != null) {
                            addingRoles.addAll(rawAddingRoles);
                        }
                        Iterator<String> addingRolesIterator = addingRoles.iterator();
                        while (addingRolesIterator.hasNext()) {
                            String addingRole = addingRolesIterator.next();
                            if (existingRoles.contains(addingRole)) {
                                addingRolesIterator.remove();
                                existingRoles.remove(addingRole);
                            }
                        }
                        if (addingRoles.size() > 0) {
                            for (String addingRole : addingRoles) {
                                TopicIF topic = (TopicIF) topicMap.getTopicMap().getObjectById(addingRole);
                                AssociationRoleIF assocRole = builder.makeAssociationRole(assoc, assocType, topic);
                            }
                        }
                        if (existingRoles.size() > 0) {
                            for (String removingRole : existingRoles) {
                                AssociationRoleIF remAssoc = roleObjMap.get(removingRole);
                                remAssoc.remove();
                            }
                        }
                    }
                }
                if (reifierIdChanged) {
                    reifierIdChanged = false;
                    if (reifierId != null) {
                        TopicIF reifier = (TopicIF) topicMap.getTopicMap().getObjectById(reifierId);
                        assoc.setReifier(reifier);
                    } else {
                        assoc.setReifier(null);
                    }
                }
            }
            topicMap.commit();
            saveToCache();
            success = true;
        } finally {
            if (!success)
                topicMap.abort();
            topicMap.close();
        }
    }

    @Override
    public TestInterface getTestInterface() {
        return new TestInterface() {
            @Override
            public void createWithOldOntopiaMissingHumanId() {
                assocPsi = null;
                humanId = null;
            }
        };
    }
}
