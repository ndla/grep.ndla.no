package org.mycurriculum.data.tms.connections.config.helpers;

import java.io.File;
import java.io.IOException;

public interface ConfigFileGenerator {
    public File generateTempFile(String prefix, String suffix) throws IOException;
}
