package org.mycurriculum.data.tms.abstracts;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.domain.OntopiaPsis;
import org.mycurriculum.data.tms.exceptions.OccurrenceTypeNotFoundException;
import org.mycurriculum.data.tms.exceptions.RecoverableInvalidDataException;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.util.MutableTranslatedText;
import org.mycurriculum.data.util.MutableTranslatedTextImpl;
import org.mycurriculum.data.util.TranslatedText;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public abstract class OntopiaTopicAbstract implements OntopiaTopic {
    private OntopiaTopicRef topicIfRef;
    /* data */
    private boolean readInValues = false;
    private String id = null;
    private String objectId = null;
    private Collection<String> types = null;
    private Collection<URL> origPsis = null;
    private Collection<URL> psis = null;
    private Collection<OntopiaTopicAbstract.Name> names = null;

    public static class Occurrence implements Serializable {
        private CopyOnWriteSet<String> scopes;
        private String value;

        public Occurrence(Set<String> scopes, String value) {
            this.value = value;
            setScopes(scopes);
        }
        public Occurrence(String value) {
            this(new HashSet<String>(), value);
        }

        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }

        public Set<String> getScopes() {
            synchronized (this) {
                try {
                    return (Set<String>) scopes.clone();
                } catch (CloneNotSupportedException e) {
                    throw new RuntimeException("Clone not supported (Set)", e);
                }
            }
        }
        public void setScopes(Set<String> scopes) {
            Set<String> copyScopes = new HashSet<String>();
            copyScopes.addAll(scopes);
            synchronized (this) {
                this.scopes = new CopyOnWriteSet<String>(copyScopes);
            }
        }
        public void addScope(String scope) {
            synchronized (this) {
                this.scopes.add(scope);
            }
        }
        public void removeScope(String scope) {
            synchronized (this) {
                this.scopes.remove(scope);
            }
        }
    }

    private static class TopicCache implements Serializable {
        private String id;
        private Collection<String> types;
        private Collection<URL> psis;
        private Collection<OntopiaTopicAbstract.Name> names;

        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public Collection<URL> getPsis() {
            synchronized (this) {
                return new CopyOnWriteCollection(psis);
            }
        }

        public void setPsis(Collection<URL> psis) {
            synchronized (this) {
                if (psis != null) {
                    this.psis = new ArrayList<URL>(psis.size());
                    this.psis.addAll(psis);
                } else
                    this.psis = null;
            }
        }

        public Collection<OntopiaTopicAbstract.Name> getNames() {
            synchronized (this) {
                List<OntopiaTopicAbstract.Name> names = new ArrayList<OntopiaTopicAbstract.Name>();
                for (OntopiaTopicAbstract.Name name : this.names) {
                    try {
                        names.add((OntopiaTopicAbstract.Name) name.clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
                return names;
            }
        }

        public void setNames(Collection<OntopiaTopicAbstract.Name> names) {
            synchronized (this) {
                if (names != null) {
                    this.names = new ArrayList<OntopiaTopicAbstract.Name>(names.size());
                    for (OntopiaTopicAbstract.Name name : names) {
                        try {
                            this.names.add((OntopiaTopicAbstract.Name) name.clone());
                        } catch (CloneNotSupportedException e) {
                            throw new RuntimeException("Clone not supported", e);
                        }
                    }
                } else
                    this.names = names;
            }
        }

        public Collection<String> getTypes() {
            synchronized (this) {
                if (this.types == null)
                    return null;
                return new CopyOnWriteCollection<String>(this.types);
            }
        }

        public void setTypes(Collection<String> types) {
            synchronized (this) {
                if (types != null) {
                    this.types = new ArrayList<String>(types.size());
                    this.types.addAll(types);
                } else
                    this.types = types;
            }
        }
    }
    private static class TopicOccurenceCache implements Serializable {
        private Occurrence[] values;

        public Occurrence[] getValues() {
            synchronized (this) {
                Occurrence[] copy = new Occurrence[values.length];
                for (int i = 0; i < copy.length; i++) {
                    copy[i] = new Occurrence(values[i].getScopes(), values[i].getValue());
                }
                return copy;
            }
        }

        public void setValues(Occurrence[] values) {
            synchronized (this) {
                this.values = values;
            }
        }
    }
    private static class TopicNToNAssociationCache implements Serializable {
        private Map<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> associations;

        private  Collection<String> cloneL4(Collection<String> input) {
            Collection<String> copy = new ArrayList<String>(input.size());
            copy.addAll(input);
            return copy;
        }
        private Map<String/*role*/,Collection<String>> cloneL3(Map<String/*role*/,Collection<String>> input) {
            Map<String/*role*/,Collection<String>> copy = new HashMap<String/*role*/,Collection<String>>();
            for (Map.Entry<String/*role*/,Collection<String>> entry : input.entrySet()) {
                copy.put(entry.getKey(), cloneL4(entry.getValue()));
            }
            return copy;
        }
        private Map<String/*id*/,Map<String/*role*/,Collection<String>>> cloneL2(Map<String/*id*/,Map<String/*role*/,Collection<String>>> input) {
            Map<String/*id*/,Map<String/*role*/,Collection<String>>> copy = new HashMap<String/*id*/,Map<String/*role*/,Collection<String>>>();
            for (Map.Entry<String/*id*/,Map<String/*role*/,Collection<String>>> entry : input.entrySet()) {
                copy.put(entry.getKey(), cloneL3(entry.getValue()));
            }
            return copy;
        }
        public Map<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> getFullyClonedAssociations() {
            Map<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> associations;
            synchronized (this) {
                associations = this.associations;
            }
            Map<String/*role*/, Map<String/*id*/, Map<String/*role*/, Collection<String>>>> copy = new HashMap<String/*role*/, Map<String/*id*/, Map<String/*role*/, Collection<String>>>>();
            for (Map.Entry<String/*role*/, Map<String/*id*/, Map<String/*role*/, Collection<String>>>> roleEntry : associations.entrySet()) {
                copy.put(roleEntry.getKey(), cloneL2(roleEntry.getValue()));
            }
            return copy;
        }

        public Map<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> getCORAssociations() {
            Map<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> associations;
            synchronized (this) {
                associations = this.associations;
            }
            return new GenericCopyOnReadMap<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>>(associations) {
                @Override
                public Map<String, Map<String, Collection<String>>> copyValue(Map<String, Map<String, Collection<String>>> value) {
                    return new GenericCopyOnReadMap<String, Map<String, Collection<String>>>(value) {
                        @Override
                        public Map<String, Collection<String>> copyValue(Map<String, Collection<String>> value) {
                            return cloneL3(value);
                        }
                    };
                }
            };
        }

        public Map<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> getAssociations() {
            return getCORAssociations();
        }
        public void setAssociations(Map<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> associations) {
            if (associations instanceof GenericCopyOnReadMap) {
                GenericCopyOnReadMap<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> cor = (GenericCopyOnReadMap<String/*role*/,Map<String/*id*/,Map<String/*role*/,Collection<String>>>>) associations;
                associations = cor.getMap();
                setAssociations(associations);
                return;
            }
            boolean cloned = false;
            for (Map.Entry<String,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> entry : associations.entrySet()) {
                if ((entry.getValue() instanceof GenericCopyOnReadMap)) {
                    /* Modification required - clone */
                    Map<String,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> orig = associations;
                    associations = new HashMap<String,Map<String/*id*/,Map<String/*role*/,Collection<String>>>>();
                    associations.putAll(orig);
                    cloned = true;
                    break;
                }
            }
            boolean hadCORMaps = false;
            for (Map.Entry<String,Map<String/*id*/,Map<String/*role*/,Collection<String>>>> entry : associations.entrySet()) {
                if (!(entry.getValue() instanceof GenericCopyOnReadMap))
                    continue;
                if (!cloned)
                    throw new RuntimeException("The map must be cloned (read only)");
                GenericCopyOnReadMap<String/*id*/,Map<String/*role*/,Collection<String>>> value = (GenericCopyOnReadMap<String/*id*/,Map<String/*role*/,Collection<String>>>) entry.getValue();
                Map<String/*id*/,Map<String/*role*/,Collection<String>>> map = value.getMap();
                entry.setValue(map);
                hadCORMaps = true;
            }
            if (hadCORMaps) {
                setAssociations(associations);
                return;
            }
            synchronized (this) {
                this.associations = associations;
            }
        }
    }

    /**
     * Requires locking.
     *
     * @param cached
     */
    private void getFromCache(TopicCache cached) {
        this.id = cached.getId();
        this.types = cached.getTypes();
        this.psis = cached.getPsis();
        this.origPsis = new ArrayList<URL>(this.psis.size());
        this.origPsis.addAll(this.psis);
        this.names = cached.getNames();
    }

    /**
     * Requires locking.
     *
     * @return
     */
    private TopicCache setToCache() {
        TopicCache cached = new TopicCache();
        cached.setId(this.id);
        cached.setTypes(this.types);
        cached.setNames(this.names);
        cached.setPsis(this.psis);
        /* update psi cache */
        Collection<String> gc = new HashSet<String>();
        Collection<String> add = new HashSet<String>();
        for (URL url : this.origPsis)
            gc.add(url.toString());
        for (URL url : this.psis) {
            String psi = url.toString();
            if (gc.contains(psi)) {
                gc.remove(psi);
            } else {
                add.add(psi);
            }
        }
        for (String psi : gc) {
            OntopiaTopicFinder.cacheSetIdFromPsi(topicIfRef.getTopicMapReference().getCache(), psi, null);
        }
        for (String psi : add) {
            OntopiaTopicFinder.cacheSetIdFromPsi(topicIfRef.getTopicMapReference().getCache(), psi, this.objectId);
        }
        this.origPsis = new ArrayList<URL>();
        this.origPsis.addAll(this.psis);
        return cached;
    }

    @Override
    public void resetTopicMap(TopicMap topicMap) {
        topicIfRef.resetTopicMap(topicMap);
    }

    public static OntopiaTopicType createOntopiaType(String url)
    {
        try {
            return new OntopiaTopicType(new URL(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to create topic type");
        }
    }

    private void deleteThisTopic() throws IOException {
        if (this.topicIfRef instanceof OntopiaTopicCreator)
            return;
        TopicMapIF topicMap = this.topicIfRef.getTopicMap();
        TopicMapStoreIF store = topicMap.getStore();
        TopicIF topic = this.topicIfRef.getTopic();
        String objectId = topic.getObjectId();
        String key = "OntopiaTopicAbstract:"+objectId;
        topic.remove();
        if (topicIfRef.getTopicMapReference().getCache() == null)
            throw new RuntimeException("Topicmap cache is not available (null)");
        CacheDao.TypedCacheNamespace<TopicCache> cache = new CacheDao.TypedCacheNamespace<TopicCache>(topicIfRef.getTopicMapReference().getCache());
        cache.invalidateObject(key);
    }
    private void readOntopiaTopicAbstractValues() throws IOException, OntopiaTopic.NotFoundException
    {
        TopicMapIF topicMap = this.topicIfRef.getTopicMap();
        if (this.topicIfRef instanceof OntopiaTopicCreator) {
            OntopiaTopicCreator creator = (OntopiaTopicCreator) this.topicIfRef;
            this.id = null;
            this.types = new HashSet<String>();
            this.types.add(creator.getType().getTopic(topicMap).getObjectId());
            this.psis = new ArrayList<URL>();
            this.origPsis = new ArrayList<URL>();
            this.names = new ArrayList<Name>();
        } else {
            TopicMapStoreIF store = topicMap.getStore();
            TopicIF topic = this.topicIfRef.getTopic();
            if (topic == null)
                throw new OntopiaTopic.NotFoundException("Topic not found");
            objectId = topic.getObjectId();
            /* id */
            Collection<LocatorIF> identifiers = topic.getItemIdentifiers();
            String expected = store.getBaseAddress().getAddress() + "#";
            for (LocatorIF identifier : identifiers) {
                String addr = identifier.getAddress();
                if (addr.length() < expected.length())
                    continue;
                if (addr.substring(0, expected.length()).equals(expected)) {
                    id = addr.substring(expected.length() + 1);
                    break;
                }
            }
            /* types */
            Collection<TopicIF> typeTopics = topic.getTypes();
            Collection<String> typeSet = new HashSet<String>();
            for (TopicIF typeTopic : typeTopics) {
                typeSet.add(typeTopic.getObjectId());
            }
            if (typeSet.size() == 0)
                throw new IOException("The topic is expected to have at least one type");
            setTypes(typeSet);
            /* psis */
            Collection<LocatorIF> locators = topic.getSubjectIdentifiers();
            psis = new ArrayList<URL>(locators.size());
            origPsis = new ArrayList<URL>(locators.size());
            for (LocatorIF locator : locators) {
                try {
                    URL url = new URL(locator.getAddress());
                    psis.add(url);
                    origPsis.add(url);
                } catch (MalformedURLException e) {
                }
            }
            /* names */
            Collection<TopicNameIF> names = topic.getTopicNames();
            this.names = new ArrayList<Name>(names.size());
            for (TopicNameIF name : names) {
                Name obj = Name.fromOntopia(name);
                this.names.add(obj);
            }
        }
    }
    private static class AnnotatedField {
        public Field field;
        public PropertyDescriptor propertyDescriptor;
        public Annotation annotation;
        public String type;
    }
    /* Mapping: className -> Annotation-className -> List */
    private static Map<String,Map<String,Collection<AnnotatedField>>> annotations = new HashMap<String,Map<String,Collection<AnnotatedField>>>();
    private static Map<String,Collection<AnnotatedField>> getAnnotationMapForClass(Class classObject) {
        synchronized (annotations) {
            String className = classObject.getName();
            if (!annotations.containsKey(className)) {
                Map<String, Collection<AnnotatedField>> map = new HashMap<String, Collection<AnnotatedField>>();
                annotations.put(className, map);
                return map;
            }
            return annotations.get(className);
        }
    }
    private static Collection<AnnotatedField> getAnnotationsCollection(Class classObject, Class annotationClass) throws IOException {
        Map<String,Collection<AnnotatedField>> map = getAnnotationMapForClass(classObject);

        synchronized (map) {
            String annotationClassName = annotationClass.getName();
            if (!map.containsKey(annotationClassName)) {
                Map<String,PropertyDescriptor> propmap = null;
                Collection<AnnotatedField> fields = new ArrayList<AnnotatedField>();
                PropertyDescriptor[] properties;
                try {
                    BeanInfo bi = Introspector.getBeanInfo(classObject);
                    properties = bi.getPropertyDescriptors();
                } catch (IntrospectionException e) {
                    throw new IOException("Introspecition exception", e);
                }
                while (classObject != null) {
                    for (Field field : classObject.getDeclaredFields()) {
                        Annotation annotation = field.getAnnotation(annotationClass);
                        if (annotation == null)
                            continue;
                        Type type = field.getGenericType();
                        if (propmap == null) {
                            propmap = new HashMap<String, PropertyDescriptor>();
                            for (PropertyDescriptor property : properties) {
                                propmap.put(property.getName(), property);
                            }
                        }
                        AnnotatedField annotatedField = new AnnotatedField();
                        annotatedField.field = field;
                        annotatedField.annotation = annotation;
                        annotatedField.type = type.toString();
                        if (propmap.containsKey(field.getName())) {
                            annotatedField.propertyDescriptor = propmap.get(field.getName());
                        }
                        fields.add(annotatedField);
                    }
                    classObject = classObject.getSuperclass();
                }
                map.put(annotationClassName, fields);
                return fields;
            }
            return map.get(annotationClassName);
        }
    }
    private Collection<Class> getAllInterfaces(Class classObj) {
        Set<Class> interfaces = new HashSet<Class>();
        while (classObj != null) {
            for (Class interfaceObj : classObj.getInterfaces()) {
                if (!interfaces.contains(interfaceObj))
                    interfaces.add(interfaceObj);
            }
            classObj = classObj.getSuperclass();
        }
        return interfaces;
    }
    private void readAnnotatedOccurences() throws IOException {
        Collection<AnnotatedField> fields = getAnnotationsCollection(this.getClass(), OccurrenceProperty.class);
        for (AnnotatedField field : fields) {
            OccurrenceProperty prop = (OccurrenceProperty) field.annotation;
            try {
                Class fieldType = field.field.getType();
                if (field.type.equals("class java.lang.String")) {
                    if (field.propertyDescriptor != null) {
                        field.propertyDescriptor.getWriteMethod().invoke(this, loadOccurence(prop.type()));
                    } else {
                        field.field.set(this, loadOccurence(prop.type()));
                    }
                } else if (field.type.equals("class [Ljava.lang.String;")) { // String[]
                    Object[] args = new Object[1];
                    args[0] = loadOccurrenceValues(prop.type());
                    if (field.propertyDescriptor != null) {
                        field.propertyDescriptor.getWriteMethod().invoke(this, args);
                    } else {
                        field.field.set(this, args);
                    }
                } else if (field.type.equals("boolean")) {
                    String value = loadOccurence(prop.type());
                    boolean obj;
                    if (value != null) {
                        if (value.equals("true"))
                            obj = true;
                        else if (value.equals("false"))
                            obj = false;
                        else
                            throw new IOException("Invalid boolean value for occurrence "+field.field.getName()+" on "+field.field.getDeclaringClass().getName()+": "+value);
                    } else
                        obj = false;
                    if (field.propertyDescriptor != null) {
                        field.propertyDescriptor.getWriteMethod().invoke(this, obj);
                    } else {
                        field.field.set(this, obj);
                    }
                } else if (fieldType.equals(Integer.class)) {
                    String value = loadOccurence(prop.type());
                    Integer obj;
                    if (value != null) {
                        try {
                            obj = Integer.decode(value);
                        } catch (NumberFormatException e) {
                            throw new IOException("Unable to read Integer occurrence " + field.field.getName() + " on " + field.field.getDeclaringClass().getName() + " (invalid number format): " + value, e);
                        }
                    } else
                        obj = null;
                    Object[] args = new Object[1];
                    args[0] = obj;
                    if (field.propertyDescriptor != null) {
                        field.propertyDescriptor.getWriteMethod().invoke(this, args);
                    } else {
                        field.field.set(this, args);
                    }
                } else if (fieldType.equals(Long.class)) {
                    String value = loadOccurence(prop.type());
                    Long obj;
                    if (value != null) {
                        try {
                            obj = Long.decode(value);
                        } catch (NumberFormatException e) {
                            throw new IOException("Unable to read Long occurrence " + field.field.getName() + " on " + field.field.getDeclaringClass().getName() + " (invalid number format): " + value, e);
                        }
                    } else
                        obj = null;
                    Object[] args = new Object[1];
                    args[0] = obj;
                    if (field.propertyDescriptor != null) {
                        field.propertyDescriptor.getWriteMethod().invoke(this, args);
                    } else {
                        field.field.set(this, args);
                    }
                } else if (fieldType.equals(Boolean.class)) {
                    String value = loadOccurence(prop.type());
                    Boolean obj;
                    if (value != null) {
                        if (value.equals("true"))
                            obj = new Boolean(true);
                        else if (value.equals("false"))
                            obj = new Boolean(false);
                        else
                            throw new IOException("Invalid boolean value for occurrence "+field.field.getName()+" on "+field.field.getDeclaringClass().getName()+": "+value);
                    } else
                        obj = new Boolean(false);
                    Object[] args = new Object[1];
                    args[0] = obj;
                    if (field.propertyDescriptor != null) {
                        field.propertyDescriptor.getWriteMethod().invoke(this, args);
                    } else {
                        field.field.set(this, args);
                    }
                } else if (fieldType.equals(MutableTranslatedText.class) ||
                        fieldType.equals(TranslatedText.class)) {
                    Object[] args = new Object[1];
                    args[0] = loadTranslatedTextOccurrence(prop.type());
                    if (field.propertyDescriptor != null) {
                        Method writeMethod = field.propertyDescriptor.getWriteMethod();
                        if (writeMethod == null)
                            throw new IOException("Missing a set-method for occurrence "+field.field.getName()+" on "+field.field.getDeclaringClass().getName());
                        writeMethod.invoke(this, args);
                    } else {
                        field.field.set(this, args);
                    }
                } else
                    throw new IOException("Unrecognised occurrence field type: "+field.type);
            } catch (InvocationTargetException e) {
                Throwable cause = e.getCause();
                if (cause != null && cause instanceof RecoverableInvalidDataException) {
                    RecoverableInvalidDataException invalidDataException = (RecoverableInvalidDataException) cause;
                    System.err.println("WARNING: Invalid data for " + field.field.getName() + " (repaired)");
                } else {
                    throw new IOException("Invokation target exception for annotated property " + field.field.getName(), e);
                }
            } catch (IllegalAccessException e) {
                throw new IOException("Illegal access exception for annotated property " + field.field.getName(), e);
            } catch (OccurrenceTypeNotFoundException e) {
                if (!prop.addedFieldTypeCompatibility()) {
                    throw new IOException("Ontology error: Occurrence type not found: " + prop.type(), e);
                } else {
                    /* Occurrence type was not found, but we are running in compat mode
                     * for the addition of a new field in the ontology. Just ignoring.
                     */
                }
            }
        }
    }
    private void saveAnnotatedOccurences() throws IOException {
        Collection<AnnotatedField> fields = getAnnotationsCollection(this.getClass(), OccurrenceProperty.class);
        for (AnnotatedField field : fields) {
            OccurrenceProperty prop = (OccurrenceProperty) field.annotation;
            try {
                Class fieldType = field.field.getType();
                if (field.type.equals("class java.lang.String")) {
                    String value;
                    if (field.propertyDescriptor != null) {
                        value = (String) field.propertyDescriptor.getReadMethod().invoke(this);
                    } else {
                        value = (String) field.field.get(this);
                    }
                    if (value != null) {
                        saveOccurence(prop.type(), value);
                    } else {
                        saveOccurrenceValues(prop.type(), new String[0]);
                    }
                } else if (field.type.equals("class [Ljava.lang.String;")) { // String[]
                    String[] values;
                    if (field.propertyDescriptor != null) {
                        values = (String[]) field.propertyDescriptor.getReadMethod().invoke(this);
                    } else {
                        values = (String[]) field.field.get(this);
                    }
                    if (values != null) {
                        saveOccurrenceValues(prop.type(), values);
                    }
                } else if (field.type.equals("boolean")) {
                    boolean value;
                    if (field.propertyDescriptor != null) {
                        value = (boolean) field.propertyDescriptor.getReadMethod().invoke(this);
                    } else {
                        value = (boolean) field.field.get(this);
                    }
                    saveOccurence(prop.type(), (value ? "true" : "false"));
                } else if (fieldType.equals(Integer.class)) {
                    Integer value;
                    if (field.propertyDescriptor != null) {
                        value = (Integer) field.propertyDescriptor.getReadMethod().invoke(this);
                    } else {
                        value = (Integer) field.field.get(this);
                    }
                    if (value != null)
                        saveOccurence(prop.type(), value.toString());
                    else
                        saveOccurrenceValues(prop.type(), new String[0]);
                } else if (fieldType.equals(Long.class)) {
                    Long value;
                    if (field.propertyDescriptor != null) {
                        value = (Long) field.propertyDescriptor.getReadMethod().invoke(this);
                    } else {
                        value = (Long) field.field.get(this);
                    }
                    if (value != null)
                        saveOccurence(prop.type(), value.toString());
                    else
                        saveOccurrenceValues(prop.type(), new String[0]);
                } else if (fieldType.equals(Boolean.class)) {
                    Boolean value;
                    if (field.propertyDescriptor != null) {
                        value = (Boolean) field.propertyDescriptor.getReadMethod().invoke(this);
                    } else {
                        value = (Boolean) field.field.get(this);
                    }
                    if (value != null)
                        saveOccurence(prop.type(), (value.booleanValue() ? "true" : "false"));
                    else
                        saveOccurrenceValues(prop.type(), new String[0]);
                } else if (fieldType.equals(TranslatedText.class) ||
                        getAllInterfaces(fieldType).contains(TranslatedText.class)) {
                    TranslatedText value;
                    if (field.propertyDescriptor != null) {
                        value = (TranslatedText) field.propertyDescriptor.getReadMethod().invoke(this);
                    } else {
                        value = (TranslatedText) field.field.get(this);
                    }
                    if (value != null)
                        saveTranslatedTextOccurrence(prop.type(), value);
                    else
                        saveOccurrenceValues(prop.type(), new String[0]);
                } else
                    throw new IOException("Unrecognised occurrence field type: "+field.type);
            } catch (InvocationTargetException e) {
                throw new IOException("Invokation target exception for annotated property " + field.field.getName(), e);
            } catch (IllegalAccessException e) {
                throw new IOException("Illegal access exception for annotated property " + field.field.getName(), e);
            } catch (OccurrenceTypeNotFoundException e) {
                if (!prop.addedFieldTypeCompatibility()) {
                    throw new IOException("Ontology error: Missing occurrence type: "+prop.type(), e);
                } else {
                    /* Not possible to persist the field, but we are running in
                     * compat mode. So just ignore it.
                     */
                }
            }
        }
    }

    public abstract void readInValues() throws IOException;

    public void read() throws IOException
    {
        synchronized (this) {
            if (topicIfRef instanceof OntopiaTopicFinder) {
                OntopiaTopicFinder finder = (OntopiaTopicFinder) topicIfRef;
                try {
                    OntopiaTopicFinderById finderById = finder.getFinderById(topicIfRef.getTopicMapReference().getCache());
                    if (finderById != null)
                        topicIfRef = finderById;
                } finally {
                    topicIfRef.close();
                }
            }
            TopicCache cached = null;
            if (topicIfRef instanceof OntopiaTopicFinderById) {
                OntopiaTopicFinderById finder = (OntopiaTopicFinderById) topicIfRef;
                String key = "OntopiaTopicAbstract:"+finder.getId();
                if (topicIfRef.getTopicMapReference().getCache() == null)
                    throw new RuntimeException("Topicmap cache is not available (null)");
                CacheDao.TypedCacheNamespace<TopicCache> cache = new CacheDao.TypedCacheNamespace<TopicCache>(topicIfRef.getTopicMapReference().getCache());
                cached = cache.readObject(key);
                if (cached != null)
                    objectId = finder.getId();
            }
            if (cached != null) {
                getFromCache(cached);
            } else {
                try {
                    readOntopiaTopicAbstractValues();
                } finally {
                    topicIfRef.close();
                }
            }
            try {
                readInValues();
                readAnnotatedOccurences();
                readInValues = true;
                if (topicIfRef instanceof OntopiaTopicFinderById && cached == null) {
                    cached = setToCache();
                    OntopiaTopicFinderById finder = (OntopiaTopicFinderById) topicIfRef;
                    String key = "OntopiaTopicAbstract:"+finder.getId();
                    if (topicIfRef.getTopicMapReference().getCache() == null)
                        throw new RuntimeException("Topicmap cache is not available (null)");
                    CacheDao.TypedCacheNamespace<TopicCache> cache = new CacheDao.TypedCacheNamespace<TopicCache>(topicIfRef.getTopicMapReference().getCache());
                    cache.writeObject(key, cached);
                }
            } finally {
                topicIfRef.close();
            }
        }
    }

    private void saveOntopiaTopicAbstractValues() throws IOException
    {
        TopicMapIF topicMap = this.topicIfRef.getTopicMap();
        TopicIF topic = this.topicIfRef.getTopic();
        TopicMapBuilderIF builder = topicMap.getBuilder();

        /* types */
        if (types.size() == 0)
            throw new IOException("Topic must have at least one type");
        Collection<TopicIF> existingTypeTopics = topic.getTypes();
        Map<String,TopicIF> existingTypeSet = new HashMap<String,TopicIF>();
        for (TopicIF typeTopic : existingTypeTopics) {
            existingTypeSet.put(typeTopic.getObjectId(), typeTopic);
        }
        for (String incomingType : types) {
            if (!existingTypeSet.containsKey(incomingType)) {
                TopicIF typeTopic = (TopicIF) topicMap.getObjectById(incomingType);
                if (typeTopic == null) {
                    throw new IOException("Type topic not found: "+incomingType);
                }
                topic.addType(typeTopic);
            } else {
                existingTypeSet.remove(incomingType);
            }
        }
        for (Map.Entry<String,TopicIF> removingType : existingTypeSet.entrySet()) {
            topic.removeType(removingType.getValue());
        }
        /* psis */
        HashSet<String> newPsis = new HashSet<String>();
        if (psis != null) {
            for (URL psiUrl : psis)
                newPsis.add(psiUrl.toString());
        }
        Collection<LocatorIF> existingPsis = topic.getSubjectIdentifiers();
        for (LocatorIF locator : existingPsis) {
            if (newPsis.contains(locator.getAddress())) {
                newPsis.remove(locator.getAddress());
            } else {
                topic.removeSubjectIdentifier(locator);
            }
        }
        for (String newPsi : newPsis) {
            topic.addSubjectIdentifier(new URILocator(newPsi));
        }
        if (this.topicIfRef instanceof OntopiaTopicCreator) {
            if (id == null) {
                id = "mycurriculum"+ (new Date().getTime()) + RandomStringUtils.randomNumeric(4);
            }
            topic.addItemIdentifier(new URILocator(topicMap.getStore().getBaseAddress().getAddress()+"#"+id));
        }

        Collection<TopicNameIF> outNamesCollection = new ArrayList<TopicNameIF>();
        outNamesCollection.addAll(topic.getTopicNames());
        Iterator<TopicNameIF> outNames = outNamesCollection.iterator();
        Iterator<Name> inNames = this.names.iterator();
        while (inNames.hasNext()) {
            Name inName = inNames.next();
            TopicNameIF outName;
            Collection<TopicIF> scope = inName.getScope(topicMap);

            if (outNames.hasNext()) {
                boolean scopeMatching = false;
                outName = outNames.next();
                Collection<TopicIF> outScope = new LinkedList<TopicIF>();
                for (TopicIF outScopeTopic : outName.getScope()) {
                    outScope.add(outScopeTopic);
                }
                /*
                 * Check if the scope matches.
                 */
                if (scope != null) {
                    scopeMatching = true;
                    for (TopicIF scopeTopic : scope) {
                        boolean found = false;
                        for (TopicIF outScopeTopic : outScope) {
                            if (outScopeTopic.getObjectId().equals(scopeTopic.getObjectId())) {
                                outScope.remove(outScopeTopic);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            outName.addTheme(scopeTopic);
                        }
                    }
                }
                for (TopicIF outScopeTopic : outScope) {
                    outName.removeTheme(outScopeTopic);
                }
                if (scopeMatching && (outName.getValue() != null ? (inName.getName() == null || !outName.getValue().equals(inName.getName())) : inName.getName() != null)) {
                    outName.setValue(inName.getName());
                }
            } else {
                outName = builder.makeTopicName(topic, inName.getName());
                for (TopicIF scopeTopic : scope)
                    outName.addTheme(scopeTopic);
            }

            Iterator<VariantNameIF> outVariantSet = outName.getVariants().iterator();
            Map<String,VariantNameIF> outMap = new HashMap<String,VariantNameIF>();
            Iterator<Map.Entry<String,String>> inVariantSet = inName.getVariantNames().entrySet().iterator();

            while (outVariantSet.hasNext()) {
                VariantNameIF variant = outVariantSet.next();
                Collection<TopicIF> scopeCollection = variant.getScope();
                if (scopeCollection.size() == 1) {
                    TopicIF scopeTopic = scopeCollection.iterator().next();
                    String scopeId = scopeTopic.getObjectId();
                    outMap.put(scopeId, variant);
                } else
                    variant.remove();
            }

            while (inVariantSet.hasNext()) {
                Map.Entry<String,String> inVariant = inVariantSet.next();
                String key = inVariant.getKey();
                String value = inVariant.getValue();
                TopicIF scopeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(key));
                VariantNameIF outVariant = outMap.get(scopeTopic.getObjectId());
                if (outVariant != null) {
                    outVariant.setValue(value);
                    outMap.remove(scopeTopic.getObjectId());
                } else {
                    Collection<TopicIF> scopeCollection = new LinkedList<TopicIF>();
                    scopeCollection.add(scopeTopic);
                    builder.makeVariantName(outName, value, scopeCollection);
                }
            }

            for (VariantNameIF clean : outMap.values())
                clean.remove();
        }

        while (outNames.hasNext()) {
            TopicNameIF clean = outNames.next();
            clean.remove();
        }
    }

    public abstract void saveValues() throws IOException;

    public void save() throws IOException
    {
        boolean aborted = false;

        synchronized (this) {
            try {
                if (topicIfRef instanceof OntopiaTopicCreator) {
                    TopicIF topic = getTopic();
                    objectId = topic.getObjectId();
                    topicIfRef = new OntopiaTopicFinderById(topicIfRef.getTopicMapReference(), objectId);
                }
                saveValues();
                saveOntopiaTopicAbstractValues();
                saveAnnotatedOccurences();
                if (topicIfRef instanceof OntopiaTopicFinderById) {
                    TopicCache cached = setToCache();
                    OntopiaTopicFinderById finder = (OntopiaTopicFinderById) topicIfRef;
                    String key = "OntopiaTopicAbstract:"+finder.getId();
                    if (topicIfRef.getTopicMapReference().getCache() == null)
                        throw new RuntimeException("Topicmap cache is not available (null)");
                    CacheDao.TypedCacheNamespace<TopicCache> cache = new CacheDao.TypedCacheNamespace<TopicCache>(topicIfRef.getTopicMapReference().getCache());
                    cache.writeObject(key, cached);
                }
            } catch (Exception e) {
                topicIfRef.abort();
                aborted = true;
                throw e;
            } finally {
                if (!aborted)
                    topicIfRef.commit();
                topicIfRef.close();
            }
        }
    }

    private Collection<TopicIF> getAssociatedObjects(OntopiaTopicNToNRelation relation) throws IOException {
        Set<TopicIF> list = new HashSet<TopicIF>();
        TopicIF assocType = getTopicMap().getTopicBySubjectIdentifier(new URILocator(relation.getAssociation()));
        TopicIF containerRole = getTopicMap().getTopicBySubjectIdentifier(new URILocator(relation.getSourcePsi()));
        TopicIF containedRole = getTopicMap().getTopicBySubjectIdentifier(new URILocator(relation.getDestinationPsi()));
        String myId = getTopic().getObjectId();
        if (containerRole == null || containedRole == null)
            throw new IOException("Can't find the source and destination association roles");
        if (assocType == null)
            throw new IOException("The association type was not found");
        Collection<AssociationIF> assocs = getTopic().getAssociationsByType(assocType);
        for (AssociationIF assoc : assocs) {
            for (AssociationRoleIF containerRoleIF : assoc.getRolesByType(containerRole)) {
                TopicIF container = containerRoleIF.getPlayer();
                if (container.getObjectId().equals(myId)) {
                    for (AssociationRoleIF containedRoleIF : assoc.getRolesByType(containedRole)) {
                        TopicIF contained = containedRoleIF.getPlayer();
                        list.add(contained);
                    }
                    break;
                }
            }
        }
        return list;
    }
    private TopicNToNAssociationCache getAssociationCache(OntopiaTopicNToNRelation relation) {
        String key = "OntopiaTopicAbstractNToNAssociation:"+relation.getAssociation().toString();
        if (topicIfRef.getTopicMapReference().getCache() == null)
            throw new RuntimeException("Cache is not available (null)");
        CacheDao.TypedCacheNamespace<TopicNToNAssociationCache> cache = new CacheDao.TypedCacheNamespace<TopicNToNAssociationCache>(topicIfRef.getTopicMapReference().getCache());
        TopicNToNAssociationCache cached = cache.readObject(key);
        if (cached == null) {
            cached = new TopicNToNAssociationCache();
            cached.setAssociations(new HashMap<String,Map<String,Map<String,Collection<String>>>>());
        }
        return cached;
    }
    private void saveAssociationCache(OntopiaTopicNToNRelation relation, TopicNToNAssociationCache cached) {
        String key = "OntopiaTopicAbstractNToNAssociation:"+relation.getAssociation().toString();
        if (topicIfRef.getTopicMapReference().getCache() == null)
            throw new RuntimeException("Cache is not available (null)");
        CacheDao.TypedCacheNamespace<TopicNToNAssociationCache> cache = new CacheDao.TypedCacheNamespace<TopicNToNAssociationCache>(topicIfRef.getTopicMapReference().getCache());
        cache.writeObject(key, cached);
    }
    public Collection<String> getAssociatedIds(OntopiaTopicNToNRelation relation) throws IOException {
        if (topicIfRef instanceof OntopiaTopicCreator) {
            return new ArrayList<String>();
        }
        TopicNToNAssociationCache cached = getAssociationCache(relation);
        Map<String,Map<String,Map<String,Collection<String>>>> map = cached.getAssociations();
        Map<String,Map<String,Collection<String>>> roleMap;
        Map<String,Collection<String>> sourceMap;
        if (map.containsKey(relation.getSourcePsi().toString())) {
            roleMap = map.get(relation.getSourcePsi().toString());
            if (roleMap.containsKey(getObjectId())) {
                sourceMap = roleMap.get(getObjectId());
                if (sourceMap.containsKey(relation.getDestinationPsi().toString())) {
                    return sourceMap.get(relation.getDestinationPsi().toString());
                }
            } else {
                sourceMap = new HashMap<String,Collection<String>>();
                roleMap.put(getObjectId(), sourceMap);
            }
        } else {
            roleMap = new HashMap<String,Map<String,Collection<String>>>();
            map.put(relation.getSourcePsi().toString(), roleMap);
            sourceMap = new HashMap<String,Collection<String>>();
            roleMap.put(getObjectId(), sourceMap);
        }
        Collection<TopicIF> topics = getAssociatedObjects(relation);
        Set<String> ids = new HashSet<String>();
        for (TopicIF topic : topics) {
            ids.add(topic.getObjectId());
        }
        sourceMap.put(relation.getDestinationPsi().toString(), ids);
        cached.setAssociations(map);
        saveAssociationCache(relation, cached);
        return ids;
    }
    public OntopiaTopicNToNRelation getContainerRelation(String assocTypePsi) throws MalformedURLException {
        return new OntopiaTopicNToNRelation(new URL(assocTypePsi), new URL("http://psi.mycurriculum.org/#container"), new URL("http://psi.mycurriculum.org/#containee"));
    }
    public Collection<String> getAssociatedChildren(String assocTypePsi) throws IOException {
        return getAssociatedIds(getContainerRelation(assocTypePsi));
    }
    public Collection<String> getAssociatedParents(String assocTypePsi) throws IOException {
        return getAssociatedIds(getContainerRelation(assocTypePsi).getReverse());
    }
    public void saveAssociatedObjects(OntopiaTopicNToNRelation relation, Collection<TopicIF> children) throws IOException {
        TopicIF me = getTopic();
        String myId = me.getObjectId();
        TopicMapBuilderIF builder = getTopicMap().getBuilder();
        TopicIF assocType = getTopicMap().getTopicBySubjectIdentifier(new URILocator(relation.getAssociation()));
        TopicIF containerRole = getTopicMap().getTopicBySubjectIdentifier(new URILocator(relation.getSourcePsi()));
        TopicIF containedRole = getTopicMap().getTopicBySubjectIdentifier(new URILocator(relation.getDestinationPsi()));
        Collection<TopicIF> gcBase = getAssociatedObjects(relation);
        Map<String,TopicIF> gc = new HashMap<String,TopicIF>();
        Collection<String> idCollection = new ArrayList<String>(children.size());

        for (TopicIF child : gcBase) {
            String id = child.getObjectId();
            if (!gc.containsKey(id))
                gc.put(id, child);
        }

        for (TopicIF child : children) {
            String id = child.getObjectId();
            idCollection.add(id);
            if (gc.containsKey(id)) {
                gc.remove(id);
                continue;
            }
            if (child.getTopicMap() != getTopicMap()) {
                child = (TopicIF) getTopicMap().getObjectById(child.getObjectId());
            }
            AssociationIF assoc = builder.makeAssociation(assocType);
            builder.makeAssociationRole(assoc, containerRole, me).addItemIdentifier(new URILocator("http://psi.mycurriculum.org/association/#parent-"+me.getObjectId()+"-"+child.getObjectId()));
            builder.makeAssociationRole(assoc, containedRole, child).addItemIdentifier(new URILocator("http://psi.mycurriculum.org/association/#child-"+me.getObjectId()+"-"+child.getObjectId()));
            assoc.addItemIdentifier(new URILocator("http://psi.mycurriculum.org/association/#"+me.getObjectId()+"-"+child.getObjectId()));
        }

        for (AssociationIF assoc : me.getAssociationsByType(assocType)) {
            Collection<AssociationRoleIF> containerRoleIFs = assoc.getRolesByType(containerRole);
            for (AssociationRoleIF containerRoleIF : containerRoleIFs) {
                TopicIF container = containerRoleIF.getPlayer();
                if (container.getObjectId().equals(myId)) {
                    Collection<AssociationRoleIF> containedRoleIFs = assoc.getRolesByType(containedRole);
                    for (AssociationRoleIF containedRoleIF : containedRoleIFs) {
                        TopicIF contained = containedRoleIF.getPlayer();
                        String contId = contained.getObjectId();
                        if (gc.containsKey(contId)) {
                            if (containedRoleIFs.size() == 1) {
                                if (containerRoleIFs.size() == 1) {
                                    assoc.remove();
                                } else {
                                    containerRoleIF.remove();
                                }
                            } else {
                                containedRoleIF.remove();
                            }
                        }
                    }
                    break;
                }
            }
        }

        /* Invalidate other objects paths and store this objects new paths */
        TopicNToNAssociationCache cached = new TopicNToNAssociationCache();
        Map<String,Map<String,Map<String,Collection<String>>>> map = new HashMap<String,Map<String,Map<String,Collection<String>>>>();
        Map<String,Map<String,Collection<String>>> roleMap = new HashMap<String,Map<String,Collection<String>>>();
        Map<String,Collection<String>> sourceMap = new HashMap<String,Collection<String>>();
        map.put(relation.getSourcePsi().toString(), roleMap);
        roleMap.put(getObjectId(), sourceMap);
        sourceMap.put(relation.getDestinationPsi().toString(), idCollection);
        cached.setAssociations(map);
        saveAssociationCache(relation, cached);
    }
    public void saveAssociatedChildren(String assocTypePsi, Collection<TopicIF> children) throws IOException {
        saveAssociatedObjects(getContainerRelation(assocTypePsi), children);
    }
    public void saveAssociatedParents(String assocTypePsi, Collection<TopicIF> parents) throws IOException {
        saveAssociatedObjects(getContainerRelation(assocTypePsi).getReverse(), parents);
    }

    public Occurrence[] loadOccurrences(String type) throws OccurrenceTypeNotFoundException {
        if (topicIfRef instanceof OntopiaTopicCreator) {
            return new Occurrence[0];
        }
        if (topicIfRef instanceof OntopiaTopicFinderById) {
            OntopiaTopicFinderById finder = (OntopiaTopicFinderById) topicIfRef;
            String key = "OntopiaTopicAbstractOccurence:"+finder.getId()+":"+type;
            if (topicIfRef.getTopicMapReference().getCache() == null)
                throw new RuntimeException("Cache is not available (null)");
            CacheDao.TypedCacheNamespace<TopicOccurenceCache> cache = new CacheDao.TypedCacheNamespace<TopicOccurenceCache>(topicIfRef.getTopicMapReference().getCache());
            TopicOccurenceCache cached = cache.readObject(key);
            if (cached != null)
                return cached.getValues();
        }
        List<Occurrence> values = new ArrayList<Occurrence>();
        try {
            TopicIF typeTopic = getTopicMap().getTopicBySubjectIdentifier(new URILocator(type));
            if (typeTopic == null) {
                throw new OccurrenceTypeNotFoundException();
            }
            Collection<OccurrenceIF> occurences = getTopic().getOccurrencesByType(typeTopic);
            for (OccurrenceIF occurrence : occurences) {
                Occurrence occurrenceObject = new Occurrence(occurrence.getValue());
                Collection<TopicIF> scopes = occurrence.getScope();
                for (TopicIF scopeTopic : scopes) {
                    Collection<LocatorIF> locators = scopeTopic.getSubjectIdentifiers();
                    if (locators.size() >= 1) {
                        occurrenceObject.addScope(locators.iterator().next().getAddress());
                    }
                }
                values.add(occurrenceObject);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to retrieve occurence "+type, e);
        }
        Occurrence[] arrcpy = new Occurrence[values.size()];
        int i = 0;
        for (Occurrence arritem : values) {
            arrcpy[i++] = arritem;
        }
        if (topicIfRef instanceof OntopiaTopicFinderById) {
            OntopiaTopicFinderById finder = (OntopiaTopicFinderById) topicIfRef;
            String key = "OntopiaTopicAbstractOccurence:"+finder.getId()+":"+type;
            if (topicIfRef.getTopicMapReference().getCache() == null)
                throw new RuntimeException("Cache is not available (null)");
            CacheDao.TypedCacheNamespace<TopicOccurenceCache> cache = new CacheDao.TypedCacheNamespace<TopicOccurenceCache>(topicIfRef.getTopicMapReference().getCache());
            TopicOccurenceCache cached = new TopicOccurenceCache();
            cached.setValues(arrcpy);
            cache.writeObject(key, cached);
        }
        return arrcpy;
    }
    public MutableTranslatedText loadTranslatedTextOccurrence(String type) throws IOException, OccurrenceTypeNotFoundException {
        Occurrence[] occurrences = loadOccurrences(type);
        Map<Locale,String> translated = new HashMap<Locale,String>();
        Locale defaultLocale = null;
        String defaultText = null;
        for (Occurrence occurrence : occurrences) {
            Set<String> psis = occurrence.getScopes();
            boolean defaultLang = false;
            if (psis.contains(OntopiaPsis.universalTypePsi)) {
                psis.remove(OntopiaPsis.universalTypePsi);
                defaultLang = true;
            }
            if (psis.size() == 1) {
                String psi = psis.iterator().next();
                Locale locale = MutableTranslatedTextImpl.getLocaleFromPsi(psi);
                if (defaultLang)
                    defaultLocale = locale;
                String value = occurrence.getValue();
                if (value == null)
                    value = "";
                translated.put(locale, value);
            } else if (psis.size() == 0) {
                if (!defaultLang)
                    throw new IOException("Translated occurrence must be either of a language, or an universal or both");
                defaultText = occurrence.getValue();
            } else {
                throw new IOException("Translated occurrences has more than one scope which we can't handle properly");
            }
        }
        if (defaultLocale != null)
            return new MutableTranslatedTextImpl(translated, defaultLocale);
        else if (defaultText != null)
            return new MutableTranslatedTextImpl(translated, defaultText);
        else
            return new MutableTranslatedTextImpl(translated);
    }
    public String[] loadOccurrenceValues(String type) throws OccurrenceTypeNotFoundException {
        Occurrence[] occurrences = loadOccurrences(type);
        String[] values = new String[occurrences.length];
        for (int i = 0; i < values.length; i++) {
            values[i] = occurrences[i].getValue();
        }
        return values;
    }
    public String loadOccurence(String type) throws OccurrenceTypeNotFoundException {
        String[] occurrences = loadOccurrenceValues(type);
        if (occurrences.length > 0)
            return occurrences[0];
        else
            return null;
    }
    public void saveOccurrences(String type, Occurrence[] values) throws OccurrenceTypeNotFoundException {
        try {
            TopicMapIF topicMap = getTopicMap();
            TopicIF topic = getTopic();
            TopicIF typeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(type));
            if (typeTopic == null) {
                throw new OccurrenceTypeNotFoundException();
            }
            Collection<OccurrenceIF> occurrences = new ArrayList<OccurrenceIF>();
            occurrences.addAll(topic.getOccurrencesByType(typeTopic));
            Collection<Occurrence> inputList = new ArrayList<Occurrence>();
            for (Occurrence occurrence : values) {
                inputList.add(occurrence);
            }
            Iterator<OccurrenceIF> iter = occurrences.iterator();
            while (iter.hasNext()) {
                OccurrenceIF existing = iter.next();
                Collection<TopicIF> scopes = existing.getScope();
                Set<String> scopePsis = new HashSet<String>();
                for (TopicIF scopeTopic : scopes) {
                    Collection<LocatorIF> locators = scopeTopic.getSubjectIdentifiers();
                    if (locators.size() >= 1) {
                        scopePsis.add(locators.iterator().next().getAddress());
                    }
                }
                Iterator<Occurrence> inputIter = inputList.iterator();
                while (inputIter.hasNext()) {
                    Occurrence obj = inputIter.next();
                    Collection<String> matchScope = obj.getScopes();
                    boolean matching = false;
                    if (matchScope.size() == scopePsis.size()) {
                        matching = true;
                        for (String match : matchScope) {
                            if (!scopePsis.contains(match)) {
                                matching = false;
                                break;
                            }
                        }
                    }
                    if (matching) {
                        inputIter.remove();
                        iter.remove();
                        existing.setValue(obj.getValue());
                        break;
                    }
                }
            }
            for (OccurrenceIF clean : occurrences) {
                clean.remove();
            }
            TopicMapBuilderIF tmBuilder = topicMap.getBuilder();
            for (Occurrence occurrence : inputList) {
                OccurrenceIF newOcc = tmBuilder.makeOccurrence(topic, typeTopic, occurrence.getValue());
                for (String psi : occurrence.getScopes()) {
                    TopicIF scopeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
                    newOcc.addTheme(scopeTopic);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to save occurence "+type, e);
        }
        if (topicIfRef instanceof OntopiaTopicFinderById) {
            OntopiaTopicFinderById finder = (OntopiaTopicFinderById) topicIfRef;
            String key = "OntopiaTopicAbstractOccurence:"+finder.getId()+":"+type;
            if (topicIfRef.getTopicMapReference().getCache() == null)
                throw new RuntimeException("Cache is not available (null)");
            CacheDao.TypedCacheNamespace<TopicOccurenceCache> cache = new CacheDao.TypedCacheNamespace<TopicOccurenceCache>(topicIfRef.getTopicMapReference().getCache());
            TopicOccurenceCache cached = new TopicOccurenceCache();
            cached.setValues(values);
            cache.writeObject(key, cached);
        }
    }
    public void saveTranslatedTextOccurrence(String type, TranslatedText translatedText) throws OccurrenceTypeNotFoundException {
        List<Occurrence> occurrenceList = new ArrayList<Occurrence>();
        boolean haveDefault = false;
        String defaultLocalePsi = translatedText.getDefaultLocalePsi();
        Map<String,String> psiTextMap = translatedText.getPsiTextMap();
        if (psiTextMap != null) {
            String undPsi = MutableTranslatedTextImpl.getPsiFromLocale(new Locale("und"));
            if (psiTextMap.containsKey(undPsi)) {
                psiTextMap.remove(undPsi);
            }
            for (Map.Entry<String, String> entry : psiTextMap.entrySet()) {
                String psi = entry.getKey();
                String text = entry.getValue();
                Set<String> scopeSet = new HashSet<String>();
                scopeSet.add(psi);
                if (defaultLocalePsi != null && psi.equals(defaultLocalePsi)) {
                    scopeSet.add(OntopiaPsis.universalTypePsi);
                    haveDefault = true;
                }
                occurrenceList.add(new Occurrence(scopeSet, text));
            }
        }
        if (!haveDefault) {
            String defaultText = translatedText.getDefaultText();
            if (defaultText != null) {
                Set<String> scopeSet = new HashSet<String>();
                scopeSet.add(OntopiaPsis.universalTypePsi);
                occurrenceList.add(new Occurrence(scopeSet, defaultText));
            }
        }
        Occurrence[] occurrences = new Occurrence[occurrenceList.size()];
        int i = 0;
        for (Occurrence occurrence : occurrenceList) {
            occurrences[i++] = occurrence;
        }
        saveOccurrences(type, occurrences);
    }
    public void saveOccurrenceValues(String type, String[] values) throws OccurrenceTypeNotFoundException {
        Occurrence[] occurrences = new Occurrence[values.length];
        for (int i = 0; i < occurrences.length; i++) {
            occurrences[i] = new Occurrence(values[i]);
        }
        saveOccurrences(type, occurrences);
    }
    public void saveOccurence(String type, String value) throws OccurrenceTypeNotFoundException {
        String[] values = {value};
        saveOccurrenceValues(type, values);
    }

    public OntopiaTopicAbstract(OntopiaTopicRef topicIfRef) throws IOException {
        this.topicIfRef = topicIfRef;
        read();
    }
    public OntopiaTopicAbstract(TopicMap topicMap, OntopiaTopicType type) throws IOException {
        this(new OntopiaTopicCreator(topicMap, type));
    }
    public OntopiaTopicAbstract(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, OntopiaTopic.NotFoundException {
        this(new OntopiaTopicFinder(topicMap, subjectIdentifier));
    }

    /**
     * Be careful: this must not be used unless in the read or save context. May open a new connection.
     *
     * @return
     */
    public TopicMapIF getTopicMap() throws IOException {
        return topicIfRef.getTopicMap();
    }
    /**
     * Be careful: this must not be used unless in the read or save context. May open a new connection.
     *
     * @return
     */
    public TopicIF getTopic() throws IOException {
        return topicIfRef.assertGetTopic();
    }

    @Override
    public OntopiaTopicRef getRef() {
        return topicIfRef;
    }

    @Override
    public boolean equals(Object obj) {
        try {
            synchronized (this) {
                OntopiaTopicAbstract topic = (OntopiaTopicAbstract) obj;
                if (topic == this)
                    return true;
                if (objectId == null || topic == null || topic.getObjectId() == null)
                    return false;
                return objectId.equals(topic.getObjectId());
            }
        } catch (ClassCastException e) {
            return false;
        }
    }
    @Override
    public int hashCode()
    {
        synchronized (this) {
            if (objectId != null)
                return objectId.hashCode();
            else
                return super.hashCode();
        }
    }

    @Override
    public String getId() {
        synchronized (this) {
            return id;
        }
    }
    @Override
    public void setId(String id) {
        synchronized (this) {
            if (this.id != null)
                throw new RuntimeException("The id can only be set once");
            this.id = id;
        }
    }
    public String getObjectId() {
        synchronized (this) {
            return objectId;
        }
    }

    /**
     *
     * @return Collection of object-ids.
     */
    public Collection<String> getTypes() {
        Collection<String> types = new ArrayList<String>(this.types.size());
        synchronized (this) {
            types.addAll(this.types);
        }
        return types;
    }

    /**
     *
     * @param types Collection of object-ids.
     */
    public void setTypes(Collection<String> types) {
        synchronized (this) {
            this.types = types;
        }
    }

    /**
     *
     * @param type Object id.
     */
    public void addType(String type) {
        synchronized (this) {
            this.types.add(type);
        }
    }

    /**
     *
     * @param type Object id.
     */
    public void removeType(String type) {
        synchronized (this) {
            this.types.remove(type);
        }
    }

    public void addPsi(String url) throws MalformedURLException {
        synchronized (this) {
            if (psis == null)
                psis = new ArrayList<URL>(1);
            psis.add(new URL(url));
        }
    }
    public void addPsi(URL url) {
        synchronized (this) {
            if (psis == null)
                psis = new ArrayList<URL>(1);
            psis.add(url);
        }
    }
    public void removePsi(String url) {
        synchronized (this) {
            for (URL m : psis) {
                if (m.toString().equals(url)) {
                    psis.remove(m);
                }
            }
        }
    }
    public void removePsi(URL url) {
        synchronized (this) {
            removePsi(url.toString());
        }
    }
    public boolean hasPsi(String url)
    {
        synchronized (this) {
            for (URL psi : psis) {
                if (psi.toString().equals(url))
                    return true;
            }
        }
        return false;
    }

    private final static String PREFERRED_PSI = "psi.mycurriculum.org";
    @Override
    public URL getPsi() throws NotFoundException {
        if (psis != null && psis.size() > 0) {
            Iterator<URL> iterator = psis.iterator();
            URL first = iterator.next();
            if (!first.getHost().equals(PREFERRED_PSI)) {
                while (iterator.hasNext()) {
                    URL cand = iterator.next();
                    if (cand.getHost().equals(PREFERRED_PSI))
                        return cand;
                }
            }
            return first;
        } else
            throw new NotFoundException("PSI is not available, no PSIs on the object");
    }

    @Override
    public Collection<OntopiaTopic.Name> getNameObjects() {
        synchronized (this) {
            if (names == null)
                return new ArrayList<OntopiaTopic.Name>(1);
            Collection<OntopiaTopic.Name> nameCollection = new ArrayList<OntopiaTopic.Name>(names.size());
            nameCollection.addAll(names);
            return nameCollection;
        }
    }

    public Collection<URL> getPsis()
    {
        synchronized (this) {
            Collection<URL> psis = new ArrayList<URL>(this.psis.size());
            for (URL psi : this.psis)
                psis.add(psi);
            return psis;
        }
    }
    @Override
    public Name getNameObject()
    {
        synchronized (this) {
            if (names != null) {
                for (Name name : names) {
                    if (name.getLang() == null)
                        return name;
                }
            }
            return null;
        }
    }
    @Override
    public Name getNameObject(Locale locale)
    {
        synchronized (this) {
            if (names != null) {
                for (Name nameObj : names) {
                    Locale lang = nameObj.getLang();
                    if (lang != null && lang.getISO3Language().equals(locale.getISO3Language()))
                        return nameObj;
                }
            }
            return null;
        }
    }

    @Override
    public void addNameObject(OntopiaTopic.Name name) {
        synchronized (this) {
            names.add((Name) name);
        }
    }

    @Override
    public void removeNameObject(OntopiaTopic.Name name) {
        synchronized (this) {
            names.remove(name);
        }
    }

    @Override
    public OntopiaTopic.Name setName(String name)
    {
        synchronized (this) {
            if (this.names == null)
                this.names = new ArrayList<Name>(1);
            Name nameObj = getNameObject();
            if (nameObj != null) {
                nameObj.setName(name);
                return nameObj;
            }
            nameObj = new Name(name, null);
            this.names.add(nameObj);
            return nameObj;
        }
    }
    public String getName()
    {
        synchronized (this) {
            if (names != null) {
                Name name = getNameObject();
                if (name != null)
                    return name.getName();
                if (names.size() > 0) {
                    Name nameObj = names.iterator().next();
                    return nameObj.getName();
                }
            }
            return null;
        }
    }
    private static String getLanguageUrlPrefix()
    {
        return "http://psi.oasis-open.org/iso/639/#";
    }
    public static String getLanguageUrl(Locale locale)
    {
        return getLanguageUrlPrefix() + locale.getISO3Language();
    }
    public static Locale getLocaleFromUrl(String url)
    {
        String prefix = getLanguageUrlPrefix();
        if (prefix.length() > url.length())
            return null;
        if (url.substring(0, prefix.length()).compareTo(prefix) != 0)
            return null;
        String langCode = url.substring(prefix.length());
        return new Locale(langCode);
    }
    public OntopiaTopic.Name setName(Locale locale, String name)
    {
        synchronized (this) {
            if (names == null)
                names = new ArrayList<Name>(1);
            Name nameObj = getNameObject(locale);
            if (nameObj != null) {
                nameObj.setName(name);
                return nameObj;
            }
            nameObj = new Name(locale, name, null);
            names.add(nameObj);
            return nameObj;
        }
    }
    public String getName(Locale locale)
    {
        synchronized (this) {
            Name name = getNameObject(locale);
            if (name != null)
                return name.getName();
        }
        return null;
    }

    @Override
    public MutableTranslatedText getTranslatedName() {
        Collection<OntopiaTopic.Name> nameObjects = getNameObjects();
        Map<Locale,String> names = new HashMap<Locale,String>();
        String defaultText = null;
        Locale defaultLocale = null;
        for (OntopiaTopic.Name nameObject : nameObjects) {
            Collection<String> scopes = nameObject.getScopes();
            boolean langNeutral = false;
            Collection<Locale> locales = new ArrayList<Locale>();
            for (String scope : scopes) {
                if (scope.equals(OntopiaPsis.universalTypePsi))
                    langNeutral = true;
                else {
                    Locale locale = getLocaleFromUrl(scope);
                    if (locale != null)
                        locales.add(locale);
                }
            }
            if (langNeutral) {
                if (locales.size() > 0)
                    defaultLocale = locales.iterator().next();
                else
                    defaultText = nameObject.getName();
            }
            for (Locale locale : locales) {
                names.put(locale, nameObject.getName());
            }
        }
        if (names.size() > 0) {
            if (defaultLocale != null)
                return new MutableTranslatedTextImpl(names, defaultLocale);
            else if (defaultText != null)
                return new MutableTranslatedTextImpl(names, defaultText);
            else
                return new MutableTranslatedTextImpl(names);
        } else if (defaultText != null) {
            return new MutableTranslatedTextImpl(defaultText);
        } else
            return null;
    }

    public void remove() throws IOException {
        synchronized (this) {
            try {
                deleteThisTopic();
                topicIfRef.commit();
            } finally {
                topicIfRef.close();
            }
        }
    }

    private static class Name implements OntopiaTopic.Name, Cloneable, Serializable {
        private Map<String,String> variantNames;
        private String name;
        private Locale lang = null;
        private Collection<String> scopePsis = new ArrayList<String>();

        public Name(String name, Map<String, String> variantNames)
        {
            this.name = name;
            this.variantNames = variantNames;
        }
        public Name(Locale lang, String name, Map<String, String> variantNames)
        {
            this.lang = lang;
            this.name = name;
            this.variantNames = variantNames;
            scopePsis.add(getLanguageUrl(lang));
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            synchronized (this) {
                Name name = (Name) super.clone();
                if (this.variantNames != null) {
                    Map<String, String> variantNames = new HashMap<String, String>();
                    variantNames.putAll(this.variantNames);
                    name.setVariantNames(variantNames);
                }
                if (this.scopePsis != null) {
                    Collection<String> scopePsis = new ArrayList<String>(this.scopePsis.size());
                    scopePsis.addAll(this.scopePsis);
                    name.setScopePsis(scopePsis);
                }
                return name;
            }
        }

        public Map<String, String> getVariantNames() {
            synchronized (this) {
                if (variantNames == null)
                    variantNames = new HashMap<String, String>();
                Map<String, String> cp = new HashMap<String, String>();
                for (Map.Entry<String, String> entry : variantNames.entrySet()) {
                    cp.put(entry.getKey(), entry.getValue());
                }
                return cp;
            }
        }

        public void setScopePsis(Collection<String> scopePsis)
        {
            synchronized (this) {
                this.scopePsis = scopePsis;
            }
        }

        public void setVariantNames(Map<String, String> variantNames) {
            synchronized (this) {
                this.variantNames = variantNames;
            }
        }

        public void setVariantName(String variant, String name)
        {
            synchronized (this) {
                if (variantNames == null)
                    variantNames = new HashMap<String, String>();
                variantNames.put(variant, name);
            }
        }
        public String getVariantName(String variant)
        {
            synchronized (this) {
                if (variantNames != null)
                    return variantNames.get(variant);
                else
                    return null;
            }
        }
        public void removeVariantName(String variant)
        {
            synchronized (this) {
                if (variantNames != null)
                    variantNames.remove(variant);
            }
        }

        @Override
        public String getName() {
            synchronized (this) {
                return name;
            }
        }

        @Override
        public Collection<String> getScopes() {
            synchronized (this) {
                Collection<String> scope = new ArrayList<String>();
                scope.addAll(scopePsis);
                return scope;
            }
        }

        @Override
        public void addScope(String scope) {
            synchronized (this) {
                scopePsis.add(scope);
            }
        }

        @Override
        public void removeScope(String scope) {
            synchronized (this) {
                scopePsis.remove(scope);
            }
        }

        private static String languageNeutralScope = "http://psi.mycurriculum.org/#universal-type";
        @Override
        public void setLanguageNeutral(boolean languageNeutral) {
            if (languageNeutral)
                addScope(languageNeutralScope);
            else
                removeScope(languageNeutralScope);
        }

        @Override
        public boolean isLanguageNeutral() {
            for (String scope : getScopes()) {
                if (scope.equals(languageNeutralScope))
                    return true;
            }
            return false;
        }

        @Override
        public void setName(String name) {
            synchronized (this) {
                this.name = name;
            }
        }

        public Collection<TopicIF> getScope(TopicMapIF topicMap) {
            ArrayList<TopicIF> scope = new ArrayList<TopicIF>(1);
            synchronized (this) {
                for (String psi : scopePsis) {
                    try {
                        TopicIF topic = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
                        if (topic != null)
                            scope.add(topic);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        throw new RuntimeException("Generated faulty url string");
                    }
                }
            }
            return scope;
        }
        public static Name fromOntopia(TopicNameIF nameIF)
        {
            Collection<VariantNameIF> variantIFs = nameIF.getVariants();
            HashMap<String, String> variants = null;
            if (variantIFs != null) {
                variants = new HashMap<String, String>();
                for (VariantNameIF variant : variantIFs) {
                    Collection<TopicIF> scope = variant.getScope();
                    if (scope.size() >= 1) {
                        TopicIF scopeTopic = scope.iterator().next();
                        Collection<LocatorIF> subjectIdentifiers = scopeTopic.getSubjectIdentifiers();
                        if (subjectIdentifiers.size() >= 1) {
                            String address = subjectIdentifiers.iterator().next().getAddress();
                            String value = variant.getValue();
                            variants.put(address, value);
                        }
                    }
                }
            }
            Collection<TopicIF> scope = nameIF.getScope();
            Collection<String> scopePsis = new ArrayList<String>();
            Locale lang = null;
            if (scope != null && scope.size() > 0) {
                for (TopicIF scopeTopic : scope) {
                    String address = scopeTopic.getSubjectIdentifiers().iterator().next().getAddress();
                    Locale l = getLocaleFromUrl(address);
                    if (l != null) {
                        try {
                            l.getISO3Language(); /* Test the language object */
                            lang = l;
                        } catch (MissingResourceException e) {
                        }
                    }
                    scopePsis.add(address);
                }
            }
            Name name;
            if (lang != null)
                name =  new Name(lang, nameIF.getValue(), variants);
            else
                name = new Name(nameIF.getValue(), variants);
            name.setScopePsis(scopePsis);
            return name;
        }

        public Locale getLang() {
            synchronized (this) {
                return lang;
            }
        }

        public void setLang(Locale lang) {
            synchronized (this) {
                if (this.lang != null) {
                    String url = getLanguageUrl(this.lang);
                    scopePsis.remove(url);
                }
                this.lang = lang;
                scopePsis.add(getLanguageUrl(lang));
            }
        }
    }

    /*
     * Just make sure that the implementation validates since we are not going to actually declare the Builder
     * interface on the Builder implementation because of inheritance typing conflicts.
     */
    public static class AnonymousTopicBuilder extends Builder<OntopiaTopic,AnonymousTopicBuilder> implements OntopiaTopic.AnonymousTopicBuilder {
        private TopicMap topicMap;
        private OntopiaTopicType type;

        public AnonymousTopicBuilder(TopicMap topicMap, OntopiaTopicType type)
        {
            super(topicMap, type);
            this.topicMap = topicMap;
            this.type = type;
        }

        @Override
        public OntopiaTopic createInstance() throws IOException {
            return new OntopiaTopicAbstract(topicMap, type) {
                @Override
                public void readInValues() {
                }
                @Override
                public void saveValues() {
                }
            };
        }

        @Override
        public void builder(OntopiaTopic object) {
        }
    }
    public abstract static class Builder<T extends OntopiaTopic, K extends OntopiaTopic.Builder> {
        private TopicMap topicMap;
        private OntopiaTopicType type;
        private String name = null;
        private String id = null;
        private Collection<Name> names = new ArrayList<Name>();
        private Collection<String> psis = new ArrayList<String>();

        public Builder(TopicMap topicMap, OntopiaTopicType type)
        {
            this.topicMap = topicMap;
            this.type = type;
        }
        public OntopiaTopicRef getRef()
        {
            return new OntopiaTopicCreator(topicMap, type);
        }

        public K name(String name)
        {
            this.name = name;
            return (K) this;
        }
        public K name(String name, Locale lang)
        {
            this.names.add(new Name(name, lang));
            return (K) this;
        }
        public K name(String name, Locale lang, boolean languageNeutral)
        {
            Name nameObj = new Name(name, lang);
            nameObj.languageNeutral = languageNeutral;
            this.names.add(nameObj);
            return (K) this;
        }

        public K psi(String psi)
        {
            this.psis.add(psi);
            return (K) this;
        }

        public K id(String id) {
            this.id = id;
            return (K) this;
        }

        public abstract T createInstance() throws IOException;
        public abstract void builder(T object);
        public T build() throws IOException {
            T object;

            object = createInstance();
            if (name != null) {
                OntopiaTopic.Name nameObj = object.setName(name);
                nameObj.setLanguageNeutral(true);
            }
            for (Name inName : names) {
                OntopiaTopic.Name nameObj = object.setName(inName.lang, inName.name);
                nameObj.setLanguageNeutral(inName.languageNeutral);
            }
            for (String psi : psis) {
                object.addPsi(psi);
            }
            builder(object);
            return object;
        }

        private static class Name {
            public String name;
            public Locale lang;
            public boolean languageNeutral = false;

            public Name(String name, Locale lang)
            {
                this.name = name;
                this.lang = lang;
            }
        }
    }
}
