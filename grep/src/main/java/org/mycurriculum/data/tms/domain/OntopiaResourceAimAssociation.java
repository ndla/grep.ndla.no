package org.mycurriculum.data.tms.domain;

import org.mycurriculum.data.tms.abstracts.OntopiaAssociation;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicRef;

import java.io.IOException;
import java.util.Date;

public interface OntopiaResourceAimAssociation {
    public OntopiaAssociation getAssociationObject();

    public String getHumanId();
    public void setHumanId(String humanId) throws IOException;

    public OntopiaTopicRef getResourceRef() throws IOException;
    public OntopiaResource getResource() throws IOException;
    public OntopiaTopicRef getAimRef() throws IOException;
    public OntopiaAim getAim() throws IOException;

    public String[] getAuthors() throws IOException;
    public void setAuthors(String[] authors) throws IOException;
    public String[] getAuthorUrls() throws IOException;
    public void setAuthorUrls(String[] authorUrls) throws IOException;
    public void setApprenticeRelevant(boolean apprenticeRelevant) throws IOException;
    public boolean getApprenticeRelevant() throws IOException;
    public boolean isApprenticeRelevant() throws IOException;
    public void setTimestamp(Long timestamp) throws IOException;
    public Long getTimestamp() throws IOException;
    public void setTimestampDate(Date date) throws IOException;
    public Date getTimestampDate() throws IOException;
    public void setCurriculumSet(OntopiaCurriculumSet curiculumSet) throws IOException;
    public OntopiaCurriculumSet getCurriculumSet() throws IOException;
    public void setLevel(OntopiaLevel level) throws IOException;
    public OntopiaLevel getLevel() throws IOException;
    public void setCurriculum(OntopiaCurriculum curriculum) throws IOException;
    public OntopiaCurriculum getCurriculum() throws IOException;
    public void setAimSet(OntopiaAimSet aimSet) throws IOException;
    public OntopiaAimSet getAimSet() throws IOException;
    public void setCourseId(String courseId) throws IOException;
    public String getCourseId() throws IOException;
    public void setCourseType(String courseType) throws IOException;
    public String getCourseType() throws IOException;

    public void remove() throws IOException;
    public void save() throws IOException;

    public interface Builder {
        public Builder humanId(String humanId);
        public Builder author(String author);
        public Builder authors(String[] authors);
        public Builder authorUrl(String authorUrl);
        public Builder authorUrls(String[] authorUrls);
        public Builder timestamp(long timestamp);
        public Builder timestampDate(Date date);
        public Builder apprenticeRelevant(boolean relevant);
        public Builder curriculumSet(OntopiaCurriculumSet curriculumSet);
        public Builder level(OntopiaLevel level);
        public Builder curriculum(OntopiaCurriculum curriculum);
        public Builder aimSet(OntopiaAimSet aimSet);
        public Builder courseId(String courseId);
        public Builder courseType(String courseType);

        public OntopiaResourceAimAssociation build() throws IOException;
    }
}
