package org.mycurriculum.data.tms.domain;

import java.util.Collection;

public interface OntopiaCurriculumSet extends OntopiaTopicCommon {
    public Collection<OntopiaCurriculum> getCurricula();
    public void addCurriculum(OntopiaCurriculum curriculum);
    public void removeCurriculum(OntopiaCurriculum curriculum);
    public Collection<OntopiaCourseStructure> getCourseStructures();
    public void setSetType(String setType);
    public String getSetType();

    public interface Builder extends OntopiaTopicCommon.Builder<OntopiaCurriculumSet,Builder> {
        public Builder curriculum(OntopiaCurriculum curriculum);
    }
}
