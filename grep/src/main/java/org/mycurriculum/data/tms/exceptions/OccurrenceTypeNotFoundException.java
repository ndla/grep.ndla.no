package org.mycurriculum.data.tms.exceptions;

import org.mycurriculum.data.tms.abstracts.OntopiaTopicAbstract;

public class OccurrenceTypeNotFoundException extends Exception {
    public OccurrenceTypeNotFoundException(String message) {
        super(message);
    }
    public OccurrenceTypeNotFoundException() {
        this("The occurrence type was not found (ontology issue)");
    }
}
