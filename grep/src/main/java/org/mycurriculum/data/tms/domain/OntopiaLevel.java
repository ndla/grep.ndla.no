package org.mycurriculum.data.tms.domain;

import java.io.IOException;
import java.util.Collection;

public interface OntopiaLevel extends OntopiaTopicCommon {
    public Collection<OntopiaCourseStructure> getCourseStructures() throws IOException;
    public void addCourseStructure(OntopiaCourseStructure courseStructure) throws IOException;
    public void removeCourseStructure(OntopiaCourseStructure courseStructure) throws IOException;
    public interface Builder extends OntopiaTopicCommon.Builder<OntopiaLevel,Builder> {
        public Builder courseStructure(OntopiaCourseStructure courseStructure);
    }
}
