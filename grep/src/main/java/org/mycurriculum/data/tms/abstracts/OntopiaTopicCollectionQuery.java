package org.mycurriculum.data.tms.abstracts;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import net.ontopia.topicmaps.query.core.*;
import net.ontopia.topicmaps.query.utils.QueryUtils;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.util.*;

public abstract class OntopiaTopicCollectionQuery<T> {
    public abstract T createInstance(OntopiaTopicRef ref) throws IOException;
    public Collection<T> getCollection(TopicMap topicMap, OntopiaTopicType type, TopicFilter topicFilter) throws IOException {
        final Set<String> locators = new HashSet<String>();
        TopicMapIF topicMapIF = topicMap.getTopicMap();
        try {
            TopicIF typeTopic = type.getTopic(topicMap.getTopicMap());
            String query = "instance-of($topic,i\""+typeTopic.getSubjectIdentifiers().iterator().next().getAddress()+"\")?";
            ParsedStatementIF statement;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMapIF, null, null);
            QueryResultIF tologResult;
            try {
                statement = queryProcessor.parse(query);
                if (statement != null) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    while (tologResult.next()) {
                        TopicIF topic = topicFilter.filter((TopicIF) tologResult.getValue("topic"));
                        if (topic != null) {
                            locators.add(topic.getObjectId());
                        }
                    }
                } else
                    throw new IOException("Invalid tolog query");
            } catch (InvalidQueryException e) {
                throw new IOException("Invalid tolog query", e);
            }
        } finally {
            topicMap.abort();
            topicMap.close();
        }
        List<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(locators.size());
        for (String id : locators) {
            refs.add(new OntopiaTopicFinderById(topicMap, id));
        }
        LazyLoaderCollectionBuilder<T,OntopiaTopicRef> lazyLoader = new LazyLoaderCollectionBuilder<T,OntopiaTopicRef>(refs) {
            @Override
            public T load(OntopiaTopicRef ref) throws IOException {
                return createInstance(ref);
            }
        };
        return lazyLoader.build();
    }
    public Collection<T> getCollection(TopicMap topicMap, OntopiaTopicType type) throws IOException {
        return getCollection(topicMap, type, new TopicFilter() {
            @Override
            public TopicIF filter(TopicIF input) {
                return input;
            }
        });
    }

    public interface TopicFilter {
        public TopicIF filter(TopicIF input);
    }
    public static abstract class TopicPsiFilter implements TopicFilter {
        public abstract boolean includeTopicWithPsis(Collection<String> psi);
        public TopicIF filter(TopicIF input) {
            Collection<LocatorIF> locators = input.getSubjectIdentifiers();
            Collection<String> addrs = new ArrayList<String>();
            for (LocatorIF locator : input.getSubjectIdentifiers()) {
                addrs.add(locator.getAddress());
            }
            if (includeTopicWithPsis(addrs)) {
                return input;
            } else {
                return null;
            }
        }
    }
}
