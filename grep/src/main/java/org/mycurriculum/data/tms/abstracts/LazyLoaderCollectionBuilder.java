package org.mycurriculum.data.tms.abstracts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Quick and dirty abstract class for building a lazy-load-collection.
 * Call new OntopiaTopicCollectionBuilder() {}.build();
 *
 * @param <T>
 */
public abstract class LazyLoaderCollectionBuilder<T, V> {
    private Collection<V> vectors;

    public LazyLoaderCollectionBuilder(Collection<V> vectors) {
        this.vectors = vectors;
    }

    public LazyLoaderCollection<T> build() {
        ArrayList<LazyLoaderCollection.LazyLoader<T>> lazyLoaderCollection = new ArrayList<LazyLoaderCollection.LazyLoader<T>>(vectors.size());
        for (V v : vectors)
            lazyLoaderCollection.add(new VectorLazyLoader<T,V>(v));
        return new LazyLoaderCollection<T>(lazyLoaderCollection);
    }

    public abstract T load(V ref) throws IOException;
    private Object loadFromVector(Object ref) {
        try {
            return load((V) ref);
        } catch (IOException e) {
            throw new RuntimeException("Failed to load object in collection", e);
        }
    }

    public class VectorLazyLoader<T,V> extends LazyLoaderCollection.LazyLoader<T> {
        private V vector;
        public VectorLazyLoader(V vector) {
            this.vector = vector;
        }

        @Override
        public T load() {
            return (T) loadFromVector(vector);
        }
    }
}
