package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class OntopiaAimResourceMetadataImpl extends OntopiaTopicAbstract {

    private static OntopiaTopicType type = null;

    @OccurrenceProperty(type = OntopiaPsis.authorsPsi)
    private String[] authors;
    @OccurrenceProperty(type = OntopiaPsis.authorUrlsPsi)
    private String[] authorUrls;
    @OccurrenceProperty(type = OntopiaPsis.apprenticeRelevantPsi)
    private boolean apprenticeRelevant;
    @OccurrenceProperty(type = OntopiaPsis.timestampPsi)
    private Long timestamp;
    @OccurrenceProperty(type = OntopiaPsis.courseIdPsi)
    private String courseId;
    @OccurrenceProperty(type = OntopiaPsis.courseTypePsi)
    private String courseType;

    private OntopiaTopicRef curriculumSetRef;
    private boolean curriculumSetChanged;
    private OntopiaTopicRef levelRef;
    private boolean levelChanged;
    private OntopiaTopicRef curriculumRef;
    private boolean curriculumChanged;
    private OntopiaTopicRef aimSetRef;
    private boolean aimSetChanged;

    public OntopiaAimResourceMetadataImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }

    public OntopiaAimResourceMetadataImpl(TopicMap topicMap) throws IOException {
        super(topicMap, getOntopiaType());
    }

    public OntopiaAimResourceMetadataImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    public static OntopiaTopicType getOntopiaType() {
        if (type == null) {
            try {
                type = new OntopiaTopicType(new URL(OntopiaPsis.aimResourceAssociationReifierTypePsi));
            } catch (MalformedURLException e) {
                throw new RuntimeException("Invalid url in OntopiaPsis.aimResourceAssociationReifierTypePsi", e);
            }
        }
        return type;
    }

    public OntopiaTopicNToNRelation getCurriculumSetRelation() {
        try {
            return new OntopiaTopicNToNRelation(
                    new URL(OntopiaPsis.aimResourceAssociationCurriculumSet),
                    new URL(OntopiaPsis.aimResourceAssociationReifierTypePsi),
                    new URL(OntopiaPsis.curriculumSetTypePsi)
            );
        } catch (MalformedURLException e) {
            throw new RuntimeException("Bad urls in OntopiaPsis", e);
        }
    }
    public OntopiaTopicNToNRelation getLevelRelation() {
        try {
            return new OntopiaTopicNToNRelation(
                    new URL(OntopiaPsis.aimResourceAssociationLevel),
                    new URL(OntopiaPsis.aimResourceAssociationReifierTypePsi),
                    new URL(OntopiaPsis.levelTypePsi)
            );
        } catch (MalformedURLException e) {
            throw new RuntimeException("Bad urls in OntopiaPsis", e);
        }
    }
    public OntopiaTopicNToNRelation getCurriculumRelation() {
        try {
            return new OntopiaTopicNToNRelation(
                    new URL(OntopiaPsis.aimResourceAssociationCurriculum),
                    new URL(OntopiaPsis.aimResourceAssociationReifierTypePsi),
                    new URL(OntopiaPsis.curriculumTypePsi)
            );
        } catch (MalformedURLException e) {
            throw new RuntimeException("Bad urls in OntopiaPsis", e);
        }
    }
    public OntopiaTopicNToNRelation getAimSetRelation() {
        try {
            return new OntopiaTopicNToNRelation(
                    new URL(OntopiaPsis.aimResourceAssociationAimSet),
                    new URL(OntopiaPsis.aimResourceAssociationReifierTypePsi),
                    new URL(OntopiaPsis.aimSetTypePsi)
            );
        } catch (MalformedURLException e) {
            throw new RuntimeException("Bad urls in OntopiaPsis", e);
        }
    }
    public OntopiaTopicRef getConnectedRef(OntopiaTopicNToNRelation relation) throws IOException {
        Collection<String> ids = getAssociatedIds(relation);
        if (ids.size() > 1)
            throw new IOException("Bad topicmap data. Expected 0 or 1 object, got "+ids.size());
        if (ids.size() == 1) {
            String id = ids.iterator().next();
            return new OntopiaTopicFinderById(getRef().getTopicMapReference(), id);
        } else
            return null;
    }
    public void saveConnectedTopic(OntopiaTopicNToNRelation relation, OntopiaTopicRef topic) throws IOException {
        Collection<TopicIF> list = new ArrayList<TopicIF>(1);
        if (topic != null) {
            if (getRef().getTopicMapReference() != topic.getTopicMapReference()) {
                topic.close();
                topic.resetTopicMap(getRef().getTopicMapReference());
            }
            list.add(topic.getTopic());
        }
        saveAssociatedObjects(relation, list);
    }
    public void readCurriculumSet() throws IOException {
        OntopiaTopicRef ref = getConnectedRef(getCurriculumSetRelation());
        curriculumSetRef = ref;
        curriculumSetChanged = false;
    }
    public void saveCurriculumSet() throws IOException {
        if (curriculumSetChanged) {
            saveConnectedTopic(getCurriculumSetRelation(), curriculumSetRef);
            curriculumSetChanged = false;
        }
    }
    public void readLevel() throws IOException {
        OntopiaTopicRef ref = getConnectedRef(getLevelRelation());
        levelRef = ref;
        levelChanged = false;
    }
    public void saveLevel() throws IOException {
        if (levelChanged) {
            saveConnectedTopic(getLevelRelation(), levelRef);
            levelChanged = false;
        }
    }
    public void readCurriculum() throws IOException {
        OntopiaTopicRef ref = getConnectedRef(getCurriculumRelation());
        curriculumRef = ref;
        curriculumChanged = false;
    }
    public void saveCurriculum() throws IOException {
        if (curriculumChanged) {
            saveConnectedTopic(getCurriculumRelation(), curriculumRef);
            curriculumChanged = false;
        }
    }
    public void readAimSet() throws IOException {
        OntopiaTopicRef ref = getConnectedRef(getAimSetRelation());
        aimSetRef = ref;
        aimSetChanged = false;
    }
    public void saveAimSet() throws IOException {
        if (aimSetChanged) {
            saveConnectedTopic(getAimSetRelation(), aimSetRef);
            aimSetChanged = false;
        }
    }

    @Override
    public void readInValues() throws IOException {
        readCurriculumSet();
        readLevel();
        readCurriculum();
        readAimSet();
    }

    @Override
    public void saveValues() throws IOException {
        saveCurriculumSet();
        saveLevel();
        saveCurriculum();
        saveAimSet();
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String[] getAuthorUrls() {
        return authorUrls;
    }

    public void setAuthorUrls(String[] authorUrls) {
        this.authorUrls = authorUrls;
    }

    public void setApprenticeRelevant(boolean apprenticeRelevant) {
        this.apprenticeRelevant = apprenticeRelevant;
    }

    public boolean getApprenticeRelevant() {
        return apprenticeRelevant;
    }

    public boolean isApprenticeRelevant() {
        return apprenticeRelevant;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestampDate(Date date) {
        setTimestamp(new Long(date.getTime() / 1000));
    }

    public Date getTimestampDate() {
        return new Date(getTimestamp().longValue() * 1000);
    }

    public void setCurriculumSet(OntopiaCurriculumSet curiculumSet) {
        OntopiaTopicRef ref = curiculumSet.getRef();
        if (ref instanceof OntopiaTopicCreator)
            throw new RuntimeException("The curriculum set must exist (in other words it must be saved first)");
        curriculumSetRef = ref;
        curriculumSetChanged = true;
    }

    public OntopiaCurriculumSet getCurriculumSet() throws IOException {
        if (curriculumSetRef != null)
            return new OntopiaCurriculumSetImpl(curriculumSetRef);
        else
            return null;
    }

    public void setLevel(OntopiaLevel level) {
        OntopiaTopicRef ref = level.getRef();
        if (ref instanceof OntopiaTopicCreator)
            throw new RuntimeException("The level must exist (in other words it must be saved first)");
        levelRef = ref;
        levelChanged = true;
    }

    public OntopiaLevel getLevel() throws IOException {
        if (levelRef != null)
            return new OntopiaLevelImpl(levelRef);
        else
            return null;
    }

    public void setCurriculum(OntopiaCurriculum curriculum) {
        OntopiaTopicRef ref = curriculum.getRef();
        if (ref instanceof OntopiaTopicCreator)
            throw new RuntimeException("The curriculum must exist (in other words it must be saved first)");
        curriculumRef = ref;
        curriculumChanged = true;
    }
    public OntopiaCurriculum getCurriculum() throws IOException {
        if (curriculumRef != null)
            return new OntopiaCurriculumImpl(curriculumRef);
        else
            return null;
    }

    public void setAimSet(OntopiaAimSet aimSet) {
        OntopiaTopicRef ref = aimSet.getRef();
        if (ref instanceof OntopiaTopicCreator)
            throw new RuntimeException("The aim set must exist (in other words it must be saved first)");
        aimSetRef = ref;
        aimSetChanged = true;
    }
    public OntopiaAimSet getAimSet() throws IOException {
        if (aimSetRef != null)
            return new OntopiaAimSetImpl(aimSetRef);
        else
            return null;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public static class Builder extends OntopiaTopicAbstract.Builder<OntopiaAimResourceMetadataImpl,OntopiaTopic.Builder> {
        private Set<String> authors = new HashSet<String>();
        private Set<String> authorUrls = new HashSet<String>();
        private Long timestamp = null;
        private OntopiaCurriculumSet curriculumSet = null;
        private OntopiaLevel level = null;
        private OntopiaCurriculum curriculum = null;
        private OntopiaAimSet aimSet = null;

        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaType());
        }

        @Override
        public OntopiaAimResourceMetadataImpl createInstance() throws IOException {
            return new OntopiaAimResourceMetadataImpl(getRef());
        }

        @Override
        public void builder(OntopiaAimResourceMetadataImpl object) {
            int i;
            String[] arrcpy;

            if (authors.size() > 0) {
                i = 0;
                arrcpy = new String[authors.size()];
                for (String author : authors) {
                    arrcpy[i++] = author;
                }
                object.setAuthors(arrcpy);
            }
            if (authorUrls.size() > 0) {
                i = 0;
                arrcpy = new String[authorUrls.size()];
                for (String authorUrl : authorUrls) {
                    arrcpy[i++] = authorUrl;
                }
                object.setAuthorUrls(arrcpy);
            }
            if (timestamp != null) {
                object.setTimestamp(timestamp);
            }
            if (curriculumSet != null) {
                object.setCurriculumSet(curriculumSet);
            }
            if (level != null) {
                object.setLevel(level);
            }
            if (curriculum != null) {
                object.setCurriculum(curriculum);
            }
            if (aimSet != null) {
                object.setAimSet(aimSet);
            }
        }

        public Builder author(String author) {
            authors.add(author);
            return this;
        }

        public Builder authors(String[] authors) {
            for (String author : authors)
                author(author);
            return this;
        }

        public Builder authorUrl(String authorUrl) {
            authorUrls.add(authorUrl);
            return this;
        }

        public Builder authorUrls(String[] authorUrls) {
            for (String authorUrl : authorUrls)
                authorUrl(authorUrl);
            return this;
        }

        public Builder timestamp(long timestamp) {
            timestamp = new Long(timestamp);
            return this;
        }

        public Builder timestampDate(Date date) {
            timestamp(date.getTime() / 1000);
            return this;
        }

        public Builder curriculumSet(OntopiaCurriculumSet curriculumSet) {
            this.curriculumSet = curriculumSet;
            return this;
        }

        public Builder level(OntopiaLevel level) {
            this.level = level;
            return this;
        }

        public Builder curriculum(OntopiaCurriculum curriculum) {
            this.curriculum = curriculum;
            return this;
        }

        public Builder aimSet(OntopiaAimSet aimSet) {
            this.aimSet = aimSet;
            return this;
        }
    }
}
