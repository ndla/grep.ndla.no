package org.mycurriculum.data.tms.abstracts;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.util.MutableTranslatedText;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Locale;

public interface OntopiaTopic {
    public TopicMapIF getTopicMap() throws IOException;
    public TopicIF getTopic() throws IOException;
    public OntopiaTopicRef getRef();

    /**
     * Move this object to a new topicmap connection.
     *
     * @param topicMap
     */
    public void resetTopicMap(TopicMap topicMap);

    @Override
    public boolean equals(Object obj);

    public String getId();
    public void setId(String id);
    public String getObjectId();
    public void addPsi(String url) throws MalformedURLException;
    public void addPsi(URL url);
    public void removePsi(String url) throws MalformedURLException;
    public void removePsi(URL url);
    public Collection<URL> getPsis();
    boolean hasPsi(String s);
    public URL getPsi() throws NotFoundException;

    public Collection<Name> getNameObjects();
    public Name getNameObject();
    public Name getNameObject(Locale locale);
    public void addNameObject(Name name);
    public void removeNameObject(Name name);
    public Name setName(String name);
    public String getName();
    public Name setName(Locale locale, String name);
    public String getName(Locale locale);
    public MutableTranslatedText getTranslatedName();

    public static class NotFoundException extends RuntimeException
    {
        public NotFoundException(String msg)
        {
            super(msg);
        }
    }

    public void save() throws IOException;
    public void remove() throws IOException;

    public interface Name {
        public void setName(String name);
        public String getName();
        public Collection<String> getScopes();
        public void addScope(String scope);
        public void removeScope(String scope);
        public void setLanguageNeutral(boolean languageNeutral);
        public boolean isLanguageNeutral();
    }

    /* Just for making sure that we have the correct methods available in the abstract implementation */
    public interface AnonymousTopicBuilder extends Builder<OntopiaTopic, AnonymousTopicBuilder> {
    }
    public interface Builder<T extends OntopiaTopic, K extends Builder> {
        public K name(String name);
        public K name(String name, Locale lang);
        public K name(String name, Locale lang, boolean languageNeutral);
        public K psi(String psi);
        public K id(String id);
        public T build() throws IOException;
    }
}
