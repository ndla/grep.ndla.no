package org.mycurriculum.data.tms.references;

import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.TopicMapIF;
import net.ontopia.topicmaps.core.TopicMapStoreIF;
import net.ontopia.topicmaps.entry.TopicMapReferenceIF;
import net.ontopia.topicmaps.entry.TopicMapRepositoryIF;
import net.ontopia.topicmaps.utils.ltm.LTMTemplateImporter;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicCollectionQuery;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicRef;
import org.mycurriculum.data.tms.connections.ontopia.OntopiaConnection;
import org.mycurriculum.data.tms.domain.*;

import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class TopicMapImpl implements TopicMapRoot {
    private TopicMapImpl lockObject = this;
    private CacheDao.CacheNamespace cache;
    private OntopiaConnection connection;
    private String topicMapId;
    private TopicMapRepositoryIF repository = null;
    private TopicMapStoreIF store = null;
    private TopicMapIF topicMap = null;
    private boolean keepaliveConnection = false;
    private Set<TransactionListener> transactionListeners = new HashSet<TransactionListener>();
    private TransactionListener transactionSignaler = new TransactionListener() {
        private Set<TransactionListener> transactionListeners() {
            Set<TransactionListener> set = new HashSet<TransactionListener>();
            synchronized (lockObject) {
                set.addAll(transactionListeners);
            }
            return set;
        }
        @Override
        public void comitted() {
            for (TransactionListener transactionListener : transactionListeners())
                transactionListener.comitted();
        }
        @Override
        public void aborted() {
            for (TransactionListener transactionListener : transactionListeners())
                transactionListener.aborted();
        }
        @Override
        public void closed() {
            for (TransactionListener transactionListener : transactionListeners())
                transactionListener.closed();
        }
    };

    public TopicMapImpl(OntopiaConnection connection, String topicMapId) {
        this.connection = connection;
        this.topicMapId = topicMapId;
    }
    public void addTransactionListener(TransactionListener transactionListener) {
        synchronized (lockObject) {
            transactionListeners.add(transactionListener);
        }
    }
    public void removeTransactionListener(TransactionListener transactionListener) {
        synchronized (lockObject) {
            transactionListeners.remove(transactionListener);
        }
    }
    protected TopicMapStoreIF getStore() throws IOException {
        synchronized (lockObject) {
            if (store == null) {
                TopicMapRepositoryIF repo = getRepository();
                if (repo == null)
                    throw new IOException("Failed to open topicmap repository");
                try {
                    TopicMapReferenceIF ref = repo.getReferenceByKey(topicMapId);
                    if (ref == null) {
                        /* Retry after refresh */
                        repo.refresh();
                        ref = repo.getReferenceByKey(topicMapId);
                        if (ref == null) {
                            return null;
                        }
                    }
                    store = ref.createStore(false);
                    repo = null;
                } finally {
                    if (repo != null) {
                        repo.close();
                    }
                }
            }
            return store;
        }
    }
    public TopicMapRepositoryIF getRepository() throws IOException {
        synchronized (lockObject) {
            if (repository == null) {
                repository = connection.getTopicMapRepository();
            }
            return repository;
        }
    }
    /*
     * Ontopia deadlocking issues.. trying to fix by disabling multithreading.
     */
    private static Object lockOneAtATime = new Object();
    public TopicMapIF getTopicMap() throws IOException {
        synchronized (lockOneAtATime) {
            TopicMapStoreIF store = getStore();
        }
        if (store == null)
            return null;
        TopicMapIF topicMap = null;
        try {
            topicMap = store.getTopicMap();
        } finally {
            if (topicMap == null) {
                store.close();
            }
        }
        return topicMap;
    }

    @Override
    public void abort() {
        boolean signal = false;
        synchronized (lockObject) {
            if (store != null) {
                store.abort();
                signal = true;
            }
        }
        if (signal)
            transactionSignaler.aborted();
    }

    @Override
    public void commit() {
        boolean signal = false;
        synchronized (lockObject) {
            if (store != null) {
                store.commit();
                signal = true;
            }
        }
        if (signal)
            transactionSignaler.comitted();
    }

    @Override
    public void close()
    {
        boolean signal;
        synchronized (lockObject) {
            if (keepaliveConnection)
                return;
            if (store != null)
                store.close();
            signal = (store != null);
            store = null;
        }
        if (signal)
            transactionSignaler.closed();
    }

    private Thread keepaliveOwner = null;
    @Override
    public void openPersistentConnection() throws IOException {
        synchronized (this) {
            if (keepaliveConnection)
                throw new IOException("Recursive keepalive connection is not allowed");
            keepaliveConnection = true;
            keepaliveOwner = Thread.currentThread();
        }
    }

    @Override
    public void closePersistentConnection() {
        synchronized (this) {
            keepaliveConnection = false;
        }
        close();
    }

    @Override
    public boolean inPersistentConnection() throws ConcurrentModificationException {
        synchronized (this) {
            if (keepaliveConnection) {
                if (keepaliveOwner != Thread.currentThread())
                    throw new ConcurrentModificationException("Using the connection on different threads at the same time does not work!");
                return true;
            } else
                return false;
        }
    }

    @Override
    public void finalize()
    {
        closePersistentConnection();
    }

    @Override
    public void importLtm(String ltm) throws IOException {
        boolean commited = false;
        TopicMapIF topicMap = getTopicMap();
        try {
            LTMTemplateImporter importer = new LTMTemplateImporter();
            importer.read(topicMap, ltm, new HashMap());
            commit();
            commited = true;
        } finally {
            if (!commited)
                abort();
            close();
        }
    }
    @Override
    public void importLtm(Reader ltm) throws IOException {
        String ltmData = "";
        char[] buf = new char[4096];
        while (true) {
            int res = ltm.read(buf);
            if (res < 0)
                break;
            if (res == 0) {
                try {
                    Thread.sleep(250); /* Should be blocking */
                } catch (InterruptedException e) {
                }
            }
            String bufStr;
            if (res < buf.length) {
                char[] data = new char[res];
                System.arraycopy(buf, 0, data, 0, res);
                bufStr = new String(data);
            } else
                bufStr = new String(buf);
            ltmData += bufStr;
        }
        importLtm(ltmData);
    }

    @Override
    public OntopiaLevel.Builder getLevelBuilder() {
        return new OntopiaLevelImpl.Builder(this);
    }

    @Override
    public OntopiaLevel.Builder getLevelBuilder(String humanId) {
        OntopiaLevel.Builder builder = getLevelBuilder();
        return builder.humanId(humanId).psi(OntopiaLevelImpl.generatePsi(humanId).toString());
    }

    @Override
    public OntopiaLevel createLevel() throws IOException {
        return getLevelBuilder().build();
    }

    @Override
    public OntopiaLevel getLevel(URL locator) throws IOException {
        try {
            return new OntopiaLevelImpl(this, new URILocator(locator.toString()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in converting from url to url");
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    @Override
    public OntopiaLevel getLevel(String humanId) throws IOException {
        return getLevel(OntopiaLevelImpl.generatePsi(humanId));
    }

    @Override
    public Collection<OntopiaLevel> getLevels() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaLevelImpl> query = new OntopiaTopicCollectionQuery<OntopiaLevelImpl>() {
            @Override
            public OntopiaLevelImpl createInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaLevelImpl(ref);
            }
        };
        Set<OntopiaLevel> res = new HashSet<OntopiaLevel>();
        res.addAll(query.getCollection(this, OntopiaLevelImpl.getOntopiaType()));
        return res;
    }

    @Override
    public OntopiaCourseStructure.Builder getCourseStructureBuilder() {
        return new OntopiaCourseStructureImpl.Builder(this);
    }

    @Override
    public OntopiaCourseStructure createCourseStructure() throws IOException {
        return getCourseStructureBuilder().build();
    }

    @Override
    public OntopiaCourseStructure getCourseStructure(URL locator) throws IOException {
        try {
            return new OntopiaCourseStructureImpl(this, new URILocator(locator.toString()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in converting from url to url");
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    @Override
    public OntopiaCourseStructure getCourseStructure(String humanId) throws IOException {
        return getCourseStructure(OntopiaCourseStructureImpl.generatePsi(humanId));
    }

    @Override
    public Collection<OntopiaCourseStructure> getCourseStructures() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaCourseStructure> query = new OntopiaTopicCollectionQuery<OntopiaCourseStructure>() {
            @Override
            public OntopiaCourseStructure createInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCourseStructureImpl(ref);
            }
        };
        return query.getCollection(this, OntopiaCourseStructureImpl.getOntopiaType());
    }

    @Override
    public OntopiaCurriculum.Builder getCurriculumBuilder() {
        return new OntopiaCurriculumImpl.Builder(this);
    }

    @Override
    public OntopiaCurriculum.Builder getCurriculumBuilder(String humanId) {
        OntopiaCurriculum.Builder builder = getCurriculumBuilder();
        return builder.humanId(humanId).psi(OntopiaCurriculumImpl.generatePsi(humanId).toString());
    }

    @Override
    public OntopiaCurriculum createCurriculum() throws IOException {
        return getCurriculumBuilder().build();
    }

    @Override
    public OntopiaCurriculum getCurriculum(URL locator) {
        try {
            return new OntopiaCurriculumImpl(this, new URILocator(locator));
        } catch (IOException e) {
            throw new RuntimeException("Error in converting from url to url", e);
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    @Override
    public OntopiaCurriculum getCurriculum(String humanId) {
        return getCurriculum(OntopiaCurriculumImpl.generatePsi(humanId));
    }

    @Override
    public Collection<OntopiaCurriculum> getCurricula() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaCurriculum> query = new OntopiaTopicCollectionQuery<OntopiaCurriculum>() {
            @Override
            public OntopiaCurriculum createInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCurriculumImpl(ref);
            }
        };
        return query.getCollection(this, OntopiaCurriculumImpl.getOntopiaType());
    }

    @Override
    public OntopiaAimSet.Builder getAimSetBuilder() {
        return new OntopiaAimSetImpl.Builder(this);
    }

    @Override
    public OntopiaAimSet.Builder getAimSetBuilder(String humanId) {
        OntopiaAimSet.Builder builder = getAimSetBuilder();
        return builder.humanId(humanId).psi(OntopiaAimSetImpl.generatePsi(humanId).toString());
    }

    @Override
    public OntopiaAimSet createAimSet() throws IOException {
        return getAimSetBuilder().build();
    }

    @Override
    public OntopiaAimSet getAimSet(URL locator) {
        try {
            return new OntopiaAimSetImpl(this, new URILocator(locator));
        } catch (IOException e) {
            throw new RuntimeException("Error in converting from url to url", e);
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    @Override
    public OntopiaAimSet getAimSet(String humanId) {
        return getAimSet(OntopiaAimSetImpl.generatePsi(humanId));
    }

    @Override
    public Collection<OntopiaAimSet> getAimSets() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaAimSet> query = new OntopiaTopicCollectionQuery<OntopiaAimSet>() {
            @Override
            public OntopiaAimSetImpl createInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimSetImpl(ref);
            }
        };
        return query.getCollection(this, OntopiaAimSetImpl.getOntopiaType());
    }

    @Override
    public OntopiaAim.Builder getAimBuilder() {
        return new OntopiaAimImpl.Builder(this);
    }

    @Override
    public OntopiaAim.Builder getAimBuilder(String humanId) {
        OntopiaAim.Builder builder = getAimBuilder();
        return builder.humanId(humanId).psi(OntopiaAimImpl.generatePsi(humanId).toString());
    }

    @Override
    public OntopiaAim createAim() throws IOException {
        return getAimBuilder().build();
    }

    @Override
    public OntopiaAim getAim(URL locator) {
        try {
            return new OntopiaAimImpl(this, new URILocator(locator));
        } catch (IOException e) {
            throw new RuntimeException("Error reading the competence aim", e);
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    @Override
    public OntopiaAim getAim(String humanId) {
        return getAim(OntopiaAimImpl.generatePsi(humanId));
    }

    @Override
    public Collection<OntopiaAim> getAims() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaAim> query = new OntopiaTopicCollectionQuery<OntopiaAim>() {
            @Override
            public OntopiaAimImpl createInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimImpl(ref);
            }
        };
        return query.getCollection(this, OntopiaAimImpl.getOntopiaType());
    }

    @Override
    public OntopiaCurriculumSet.Builder getCurriculumSetBuilder() {
        return new OntopiaCurriculumSetImpl.Builder(this);
    }

    @Override
    public OntopiaCurriculumSet.Builder getCurriculumSetBuilder(String humanId) {
        OntopiaCurriculumSet.Builder builder = getCurriculumSetBuilder();
        return builder.humanId(humanId).psi(OntopiaCurriculumSetImpl.generatePsi(humanId).toString());
    }

    @Override
    public OntopiaCurriculumSet createCurriculumSet() throws IOException {
        return getCurriculumSetBuilder().build();
    }

    @Override
    public OntopiaCurriculumSet getCurriculumSet(URL locator) {
        try {
            return new OntopiaCurriculumSetImpl(this, new URILocator(locator));
        } catch (IOException e) {
            throw new RuntimeException("Error in converting from url to url", e);
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    @Override
    public OntopiaCurriculumSet getCurriculumSet(String humanId) {
        return getCurriculumSet(OntopiaCurriculumSetImpl.generatePsi(humanId));
    }

    @Override
    public Collection<OntopiaCurriculumSet> getCurriculumSets() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaCurriculumSet> query = new OntopiaTopicCollectionQuery<OntopiaCurriculumSet>() {
            @Override
            public OntopiaCurriculumSet createInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCurriculumSetImpl(ref);
            }
        };
        return query.getCollection(this, OntopiaCurriculumSetImpl.getOntopiaType());
    }

    @Override
    public OntopiaResource.Builder getResourceBuilder() {
        return new OntopiaResourceImpl.Builder(this);
    }

    @Override
    public OntopiaResource createResource() throws IOException {
        return getResourceBuilder().build();
    }

    @Override
    public OntopiaResource getResource(URL locator) throws IOException {
        try {
            return new OntopiaResourceImpl(this, new URILocator(locator));
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error converting from url to url", e);
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    @Override
    public OntopiaResource getResource(String humanId) throws IOException {
        return getResource(OntopiaResourceImpl.generatePsi(humanId));
    }

    @Override
    public OntopiaResource getResource(OntopiaTopicRef ref) throws IOException {
        try {
            return new OntopiaResourceImpl(ref);
        } catch (OntopiaTopic.NotFoundException e) {
            return null;
        }
    }

    private boolean resourceTopicPsiFilter(Collection<String> psi) {
        if (psi.contains("http://psi.mycurriculum.org/#ndla-node"))
            return false;
        else if (psi.contains("http://psi.mycurriculum.org/#nygiv-node"))
            return false;
        else if (psi.contains("http://psi.mycurriculum.org/#deling-node"))
            return false;
        else
            return true;
    }
    @Override
    public Collection<OntopiaTopicRef> getResourceRefs() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaTopicRef> query = new OntopiaTopicCollectionQuery<OntopiaTopicRef>() {
            @Override
            public OntopiaTopicRef createInstance(OntopiaTopicRef ref) throws IOException {
                return ref;
            }
        };
        return query.getCollection(this, OntopiaResourceImpl.getOntopiaTopicType(), new OntopiaTopicCollectionQuery.TopicPsiFilter() {
            @Override
            public boolean includeTopicWithPsis(Collection<String> psi) {
                return resourceTopicPsiFilter(psi);
            }
        });
    }

    @Override
    public Collection<OntopiaResource> getResources() throws IOException {
        OntopiaTopicCollectionQuery<OntopiaResource> query = new OntopiaTopicCollectionQuery<OntopiaResource>() {
            @Override
            public OntopiaResource createInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaResourceImpl(ref);
            }
        };
        return query.getCollection(this, OntopiaResourceImpl.getOntopiaTopicType(), new OntopiaTopicCollectionQuery.TopicPsiFilter() {
            @Override
            public boolean includeTopicWithPsis(Collection<String> psi) {
                return resourceTopicPsiFilter(psi);
            }
        });
    }

    @Override
    public OntopiaResourceAimAssociation getResourceAimAssociation(String humanId) throws IOException {
        return OntopiaResourceAimAssociationImpl.getAssociationByHumanId(this, humanId);
    }

    @Override
    public void deleteTopics(Collection<? extends OntopiaTopic> topics) throws IOException {
        if (!inPersistentConnection()) {
            openPersistentConnection();
            try {
                deleteTopics(topics);
            } finally {
                closePersistentConnection();
            }
            return;
        }
        for (OntopiaTopic topic : topics) {
            topic.remove();
        }
    }

    @Override
    public CacheDao.CacheNamespace getCache() {
        return cache;
    }

    @Override
    public void setCache(CacheDao.CacheNamespace cache) {
        this.cache = cache;
    }
}
