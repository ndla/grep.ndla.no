package org.mycurriculum.data.tms.references;

import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicRef;
import org.mycurriculum.data.tms.domain.*;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.Collection;

public interface TopicMapRoot extends TopicMap {
    public void importLtm(String ltm) throws IOException;
    public void importLtm(Reader ltm) throws IOException;

    public void setCache(CacheDao.CacheNamespace cache);

    /* Level */
    public OntopiaLevel.Builder getLevelBuilder();
    public OntopiaLevel.Builder getLevelBuilder(String humanId);
    public OntopiaLevel createLevel() throws IOException;
    public OntopiaLevel getLevel(URL locator) throws IOException;
    public OntopiaLevel getLevel(String humanId) throws IOException;
    public Collection<OntopiaLevel> getLevels() throws IOException;

    /* CourseStructure */
    public OntopiaCourseStructure.Builder getCourseStructureBuilder();
    public OntopiaCourseStructure createCourseStructure() throws IOException;
    public OntopiaCourseStructure getCourseStructure(URL locator) throws IOException;
    public OntopiaCourseStructure getCourseStructure(String humanId) throws IOException;
    public Collection<OntopiaCourseStructure> getCourseStructures() throws IOException;

    /* Curriculum */
    public OntopiaCurriculum.Builder getCurriculumBuilder();
    public OntopiaCurriculum.Builder getCurriculumBuilder(String humanId);
    public OntopiaCurriculum createCurriculum() throws IOException;
    public OntopiaCurriculum getCurriculum(URL locator);
    public OntopiaCurriculum getCurriculum(String humanId);
    public Collection<OntopiaCurriculum> getCurricula() throws IOException;

    /* AimSet */
    public OntopiaAimSet.Builder getAimSetBuilder();
    public OntopiaAimSet.Builder getAimSetBuilder(String humanId);
    public OntopiaAimSet createAimSet() throws IOException;
    public OntopiaAimSet getAimSet(URL locator);
    public OntopiaAimSet getAimSet(String humanId);
    public Collection<OntopiaAimSet> getAimSets() throws IOException;

    /* Aim */
    public OntopiaAim.Builder getAimBuilder();
    public OntopiaAim.Builder getAimBuilder(String humanId);
    public OntopiaAim createAim() throws IOException;
    public OntopiaAim getAim(URL locator);
    public OntopiaAim getAim(String humanId);
    public Collection<OntopiaAim> getAims() throws IOException;

    /* CurriculumSet */
    public OntopiaCurriculumSet.Builder getCurriculumSetBuilder();
    public OntopiaCurriculumSet.Builder getCurriculumSetBuilder(String humanId);
    public OntopiaCurriculumSet createCurriculumSet() throws IOException;
    public OntopiaCurriculumSet getCurriculumSet(URL locator);
    public OntopiaCurriculumSet getCurriculumSet(String humanId);
    public Collection<OntopiaCurriculumSet> getCurriculumSets() throws IOException;

    /* Resource */
    public OntopiaResource.Builder getResourceBuilder();
    public OntopiaResource createResource() throws IOException;
    public OntopiaResource getResource(URL locator) throws IOException;
    public OntopiaResource getResource(String humanId) throws IOException;
    public OntopiaResource getResource(OntopiaTopicRef ref) throws IOException;
    public Collection<OntopiaTopicRef> getResourceRefs() throws IOException;
    public Collection<OntopiaResource> getResources() throws IOException;

    /* Resource - aim association */
    public OntopiaResourceAimAssociation getResourceAimAssociation(String humanId) throws IOException;

    public void deleteTopics(Collection<? extends OntopiaTopic> topics) throws IOException;
}
