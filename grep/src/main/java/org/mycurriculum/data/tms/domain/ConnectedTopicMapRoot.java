package org.mycurriculum.data.tms.domain;

import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.Closeable;
import java.io.IOException;

public class ConnectedTopicMapRoot implements AutoCloseable, Closeable {
    private TopicMapRoot topicMapRoot;

    public ConnectedTopicMapRoot(TopicMapRoot topicMapRoot) throws IOException {
        this.topicMapRoot = topicMapRoot;
        topicMapRoot.openPersistentConnection();
    }

    public TopicMapRoot getTopicMapRoot() {
        return topicMapRoot;
    }

    @Override
    public void close() throws IOException {
        topicMapRoot.closePersistentConnection();
    }
}
