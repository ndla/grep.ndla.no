package org.mycurriculum.data.tms.references;

import net.ontopia.topicmaps.core.TopicMapIF;
import org.mycurriculum.data.cache.CacheDao;

import java.io.IOException;
import java.util.ConcurrentModificationException;

public interface TopicMap {
    /* ontopia spesific features */
    public TopicMapIF getTopicMap() throws IOException;
    public CacheDao.CacheNamespace getCache();

    public void addTransactionListener(TransactionListener transactionListener);
    public void removeTransactionListener(TransactionListener transactionListener);

    public void abort();
    public void commit();
    public void close();

    public void openPersistentConnection() throws IOException;
    public void closePersistentConnection();
    public boolean inPersistentConnection() throws ConcurrentModificationException;

    public interface TransactionListener {
        public void comitted();
        public void aborted();
        public void closed();
    }
}
