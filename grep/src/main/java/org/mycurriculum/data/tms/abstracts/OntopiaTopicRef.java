package org.mycurriculum.data.tms.abstracts;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;

public abstract class OntopiaTopicRef {
    public abstract void resetTopicMap(TopicMap topicMap);
    public abstract TopicMap getTopicMapReference();
    public abstract TopicMapIF getTopicMap() throws IOException;
    public abstract TopicIF getTopic() throws IOException;
    public abstract void commit();
    public abstract void abort();
    public abstract void close();

    /**
     * Use this method when you know that the topic should exist. Ie.
     * in the save-method of a previously loaded topic.
     *
     * @return
     * @throws IOException
     */
    public TopicIF assertGetTopic() throws IOException
    {
        TopicIF topic = getTopic();
        if (topic == null)
            throw new RuntimeException("An expected-to-exist topic was not found when needed");
        return topic;
    }
}
