package org.mycurriculum.data.tms.connections.config.helpers;

public interface ConfigFileGeneratorSource {
    /**
     * Returns the contents of the config file as a string.
     *
     * @return
     */
    public String toString();
}
