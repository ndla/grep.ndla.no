package org.mycurriculum.data.tms;

import org.mycurriculum.data.tms.connections.TopicMapLocator;

import java.io.IOException;
import java.sql.SQLException;

public interface TopicMapCreator {
    public TopicMapLocator createTopicMap(String storeName, String topicMapName) throws IOException, SQLException;
    public TopicMapLocator getTopicMapFromConnectionString(String connectionString) throws IOException;
    public void deleteStore(String storeName) throws IOException, SQLException;
    public void deleteStoreByConnectionString(String connectionString) throws IOException, SQLException;
}
