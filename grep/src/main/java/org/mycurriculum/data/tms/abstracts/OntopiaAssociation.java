package org.mycurriculum.data.tms.abstracts;

import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.util.Collection;

public interface OntopiaAssociation {
    public TopicMap getTopicMap();
    public String getHumanId();
    public void setHumanId(String humanId) throws IOException;
    public String[] getRoles();
    public Collection<String> getPlayers(String role);
    public void addPlayer(String role, OntopiaTopic topic);
    public void removePlayer(String role, OntopiaTopic topic);
    public void setPlayers(String role, Collection<? extends OntopiaTopic> topics);
    public String getReifierId();
    public OntopiaTopicRef getReifierRef();
    public void setReifierId(String id);
    public void setReifier(OntopiaTopic topic);
    public void remove() throws IOException;
    public void save() throws IOException;

    public TestInterface getTestInterface();
    public interface TestInterface {
        public void createWithOldOntopiaMissingHumanId();
    }

    public static class Type {
        public String typePsi;
        public String[] rolePsis;

        public Type(String typePsi, String[] rolePsis) {
            this.typePsi = typePsi;
            this.rolePsis = rolePsis;
        }
    }
    public static abstract class ReifiedAssociation<R extends OntopiaTopic> {
        private OntopiaAssociation assoc;

        public ReifiedAssociation(OntopiaAssociation assoc) {
            this.assoc = assoc;
        }
        public OntopiaAssociation getAssociationObject() {
            return assoc;
        }
        public String getHumanId() {
            return assoc.getHumanId();
        }
        public void setHumanId(String humanId) throws IOException {
            assoc.setHumanId(humanId);
        }
        public String[] getRoles() {
            return assoc.getRoles();
        }
        public Collection<String> getPlayers(String role) {
            return assoc.getPlayers(role);
        }
        public void addPlayer(String role, OntopiaTopic topic) {
            assoc.addPlayer(role, topic);
        }
        public void removePlayer(String role, OntopiaTopic topic) {
            assoc.removePlayer(role, topic);
        }

        public abstract R createReifiedInstance(OntopiaTopicRef ref) throws IOException;

        public R getReifier() throws IOException {
            OntopiaTopicRef ref = assoc.getReifierRef();
            if (ref != null)
                return createReifiedInstance(ref);
            else
                return null;
        }
        public void setReifier(R reifier) {
            assoc.setReifier(reifier);
        }

        public void save() throws IOException {
            assoc.save();
        }
    }
}
