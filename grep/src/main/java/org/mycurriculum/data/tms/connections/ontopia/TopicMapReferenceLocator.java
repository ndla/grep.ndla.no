package org.mycurriculum.data.tms.connections.ontopia;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TopicMapReferenceLocator implements TopicMapLocator {
    private OntopiaConnection connection;
    private String id;

    public TopicMapReferenceLocator(OntopiaConnection connection, String id)
    {
        this.connection = connection;
        this.id = id;
    }

    @Override
    public TopicMap getTopicMap() throws IOException {
        return getTopicMapRoot();
    }

    @Override
    public TopicMapRoot getTopicMapRoot() throws IOException {
        return connection.getTopicMapByKey(id);
    }

    @Override
    public DBConnectionSetup getDBConnectionSetup() {
        return null;
    }

    @Override
    public String getConnectionString() {
        String dbConnStr;
        if ((dbConnStr = connection.getDatabaseConnectionString()) != null) {
            try {
                return "db="+ URLEncoder.encode(dbConnStr, "UTF-8")+"&tm="+URLEncoder.encode(id, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Unsupported UTF-8 encoding", e);
            }
        }
        throw new RuntimeException("The connection string is not available for the database. Can't create connection string for topicmap");
    }

    @Override
    public String getTopicMapId() {
        return id;
    }
}
