package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class OntopiaLevelImpl extends OntopiaTopicCommonImpl implements OntopiaLevel {
    private static OntopiaTopicType type;

    private boolean courseStructuresLoaded;
    private boolean courseStructuresChanged;
    private LazyLoaderCollection<OntopiaCourseStructure> courseStructures;

    public static OntopiaTopicType getOntopiaType()
    {
        synchronized (OntopiaLevelImpl.class) {
            if (type == null)
                type = createOntopiaType(OntopiaPsis.levelTypePsi);
            return type;
        }
    }

    public static URL generatePsi(String humanId) {
        try {
            return new URL(OntopiaPsis.getRootPsi()+"level#"+humanId);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unexpetected url exception", e);
        }
    }

    public OntopiaLevelImpl(TopicMap topicMap) throws IOException {
        super(topicMap, getOntopiaType());
    }

    public OntopiaLevelImpl(OntopiaTopicRef ref) throws IOException {
        super(ref);
    }

    public OntopiaLevelImpl(TopicMap topicMap, LocatorIF locator) throws IOException, OntopiaTopic.NotFoundException {
        super(topicMap, locator);
    }

    public void readInCourseStructures() throws IOException {
        synchronized (this) {
            if (courseStructuresLoaded)
                return;
            try {
                Collection<String> courseStructures = getAssociatedParents(OntopiaPsis.courseStructureHasLevelPsi);
                final TopicMap topicMap = getRef().getTopicMapReference();
                this.courseStructures = new LazyLoaderCollectionBuilder<OntopiaCourseStructure, String>(courseStructures) {
                    @Override
                    public OntopiaCourseStructure load(String ref) throws IOException {
                        return new OntopiaCourseStructureImpl(new OntopiaTopicFinderById(topicMap, ref));
                    }
                }.build();
                courseStructuresLoaded = true;
            } finally {
                getRef().close();
            }
        }
    }
    public void saveCourseStructures() throws IOException {
        if (courseStructuresChanged) {
            courseStructuresChanged = false;
            Collection<TopicIF> parents = new ArrayList<TopicIF>(courseStructures.size());
            for (OntopiaCourseStructure courseStructure : courseStructures) {
                parents.add(courseStructure.getTopic());
            }
            saveAssociatedParents(OntopiaPsis.courseStructureHasLevelPsi, parents);
        }
    }

    @Override
    public void readInValues() throws IOException {
        courseStructuresLoaded = false;
        courseStructuresChanged = false;
    }

    @Override
    public void saveValues() throws IOException {
        saveCourseStructures();
    }

    @Override
    public Collection<OntopiaCourseStructure> getCourseStructures() throws IOException {
        readInCourseStructures();
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaCourseStructure>) courseStructures.clone();
        }
    }

    @Override
    public void addCourseStructure(OntopiaCourseStructure courseStructure) throws IOException {
        readInCourseStructures();
        synchronized (this) {
            courseStructures.add(courseStructure);
            courseStructuresChanged = true;
        }
    }

    @Override
    public void removeCourseStructure(OntopiaCourseStructure courseStructure) throws IOException {
        readInCourseStructures();
        synchronized (this) {
            courseStructures.remove(courseStructure);
            courseStructuresChanged = true;
        }
    }

    public static class Builder extends OntopiaTopicCommonImpl.Builder<OntopiaLevelImpl,Builder> implements OntopiaLevel.Builder {
        private Collection<OntopiaCourseStructure> courseStructures = new ArrayList<OntopiaCourseStructure>();

        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaType());
        }

        @Override
        public OntopiaLevelImpl createInstance() throws IOException {
            return new OntopiaLevelImpl(getRef());
        }

        @Override
        public void builder(OntopiaLevelImpl object) {
            super.builder(object);
            for (OntopiaCourseStructure courseStructure : courseStructures) {
                try {
                    object.addCourseStructure(courseStructure);
                } catch (IOException e) {
                    throw new RuntimeException("Failed to create a level object due to failing course structure association", e);
                }
            }
        }

        @Override
        public OntopiaLevel.Builder courseStructure(OntopiaCourseStructure courseStructure) {
            courseStructures.add(courseStructure);
            return this;
        }
    }
}
