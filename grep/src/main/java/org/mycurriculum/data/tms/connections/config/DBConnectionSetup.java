package org.mycurriculum.data.tms.connections.config;

import org.mycurriculum.data.tms.connections.config.helpers.ConfigFileGeneratorSource;

import java.util.Properties;

public interface DBConnectionSetup extends ConfigFileGeneratorSource {
    public String getDriver();

    public String getDatabase();

    public void setConnectionString(String connectionString);

    public String getConnectionString();

    public Properties getJDBCProperties();

    public DBConnectionSetup clone();

    public String getFullConnectionString();
}
