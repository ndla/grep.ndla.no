package org.mycurriculum.data.tms.connections;

import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;

public interface TopicMapStore {
    public String[] getTopicMaps() throws IOException;
    public TopicMapLocator getTopicMapLocator(String topicMap);
    public TopicMapRoot getTopicMap(String topicMap) throws IOException;
    public TopicMapLocator getTopicMapByName(String topicMap) throws IOException;
    public TopicMapLocator createTopicMap(String topicMap) throws IOException;
    public CacheDao getCacheDao();
}
