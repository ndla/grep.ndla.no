package org.mycurriculum.data.tms.domain;

import org.mycurriculum.data.tms.exceptions.InvalidDataException;
import org.mycurriculum.data.tms.exceptions.RecoverableInvalidDataException;
import org.mycurriculum.data.util.MutableTranslatedText;
import org.mycurriculum.data.util.TranslatedText;

import java.io.IOException;
import java.util.Collection;

public interface OntopiaResource extends OntopiaTopicCommon {
    public Collection<OntopiaResourceAimAssociation> getAimAssociations() throws IOException;
    public Collection<OntopiaResourceAimAssociation> getAimAssociations(OntopiaAim aim) throws IOException;
    public OntopiaResourceAimAssociation.Builder buildAimAssociation(OntopiaAim aim);
    public String[] getAuthors();
    public void setAuthors(String[] authors) throws RecoverableInvalidDataException;
    public String[] getAuthorUrls();
    public void setAuthorUrls(String[] authorUrls) throws RecoverableInvalidDataException;
    public String getNodeType();
    public void setNodeType(String nodeType);
    public MutableTranslatedText getIngress();
    public void setIngress(TranslatedText ingress);
    public MutableTranslatedText getLicense();
    public void setLicense(TranslatedText license);
    public String getStatus();
    public void setStatus(String status);
    public interface Builder extends OntopiaTopicCommon.Builder<OntopiaResource,Builder> {
        public Builder author(String author) throws InvalidDataException;
        public Builder authors(String[] authors) throws InvalidDataException;
        public Builder authorUrl(String authorUrl) throws InvalidDataException;
        public Builder authorUrls(String[] authorUrls) throws InvalidDataException;
        public Builder nodeType(String nodeType);
        public Builder ingress(TranslatedText ingress);
        public Builder status(String status);
    }
}
