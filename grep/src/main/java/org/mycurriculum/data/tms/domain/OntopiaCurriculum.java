package org.mycurriculum.data.tms.domain;

import java.io.IOException;
import java.util.Collection;

public interface OntopiaCurriculum extends OntopiaTopicCommon {
    public Collection<OntopiaAimSet> getAimSets();
    public void addAimSet(OntopiaAimSet aimSet);
    public void removeAimSet(OntopiaAimSet aimSet);
    public Collection<OntopiaLevel> getLevels();
    public void addLevel(OntopiaLevel level);
    public void removeLevel(OntopiaLevel level);
    public Collection<OntopiaCurriculumSet> getCurriculumSets();

    /**
     * Gets the collection of psis for this curriculums type.
     * @return Collection of psis for one single type topic.
     */
    public Collection<String> getType();
    public void setType(String type) throws IOException;
    public interface Builder extends OntopiaTopicCommon.Builder<OntopiaCurriculum,Builder> {
        public Builder aimSet(OntopiaAimSet aimSet);
        public Builder level(OntopiaLevel level);
    }
}
