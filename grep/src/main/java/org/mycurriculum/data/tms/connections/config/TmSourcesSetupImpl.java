package org.mycurriculum.data.tms.connections.config;

import org.mycurriculum.data.tms.connections.config.helpers.TmSourcesGeneratorAbstract;

public class TmSourcesSetupImpl extends TmSourcesGeneratorAbstract {
    private String className = "net.ontopia.topicmaps.impl.rdbms.RDBMSTopicMapSource";
    private String propertyFile;
    private String id;
    private String title;
    private boolean supportsCreate;
    private boolean supportsDelete;

    public TmSourcesSetupImpl() {
        /* "reasonable" defaults */
        setPropertyFile("db.postgresql.props");
        setId("postgresql");
        setTitle("PostgreSQL database");
        setSupportsCreate(true);
        setSupportsDelete(true);
    }

    @Override
    public TmSourcesSetupImpl clone()
    {
        return (TmSourcesSetupImpl) super.clone();
    }

    @Override
    public void setPropertyFile(String propertyFile) {
        this.propertyFile = propertyFile;
    }

    @Override
    public String getPropertyFile() {
        return propertyFile;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setSupportsCreate(boolean supportsCreate) {
        this.supportsCreate = supportsCreate;
    }

    @Override
    public boolean getSupportsCreate() {
        return supportsCreate;
    }

    @Override
    public void setSupportsDelete(boolean supportsDelete) {
        this.supportsDelete = supportsDelete;
    }

    @Override
    public boolean getSupportsDelete() {
        return supportsDelete;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
