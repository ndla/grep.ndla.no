package org.mycurriculum.data.tms.abstracts;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;

public class OntopiaTopicCreator extends OntopiaTopicRef {
    private OntopiaTopicCreator lockObject = this;
    private TopicMap topicMap;
    private OntopiaTopicType type;
    private TopicIF instance = null;

    public OntopiaTopicCreator(TopicMap topicMap, OntopiaTopicType type)
    {
        this.topicMap = topicMap;
        this.type = type;
    }

    @Override
    public void resetTopicMap(TopicMap topicMap) {
        if (instance != null)
            throw new RuntimeException("Reset topicmap after an instance has been created is not possible");
        this.topicMap = topicMap;
    }

    @Override
    public TopicMap getTopicMapReference() {
        return topicMap;
    }

    @Override
    public TopicMapIF getTopicMap() throws IOException {
        return topicMap.getTopicMap();
    }

    @Override
    public TopicIF getTopic() throws IOException {
        TopicMapIF topicMapIF = this.topicMap.getTopicMap();
        if (instance == null) {
            TopicMap.TransactionListener transactionListener = new TopicMap.TransactionListener() {
                private void reset() {
                    synchronized (lockObject) {
                        instance = null;
                        topicMap.removeTransactionListener(this);
                    }
                }
                @Override
                public void comitted() {
                    reset();
                }

                @Override
                public void aborted() {
                    reset();
                }

                @Override
                public void closed() {
                    reset();
                }
            };
            synchronized (lockObject) {
                topicMap.addTransactionListener(transactionListener);
                instance = topicMapIF.getBuilder().makeTopic(type.getTopic(topicMapIF));
            }
        }
        return instance;
    }

    public OntopiaTopicType getType() {
        return type;
    }

    @Override
    public void commit() {
        topicMap.commit();
    }

    @Override
    public void abort() {
        topicMap.abort();
    }

    @Override
    public void close() {
        topicMap.close();
    }
}
