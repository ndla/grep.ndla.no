package org.mycurriculum.data.tms.connections.config;

public interface PostgresqlConnectionSetup extends DBConnectionSetup {
    public PostgresqlConnectionSetup clone();

    public void setUsername(String username);

    public String getUsername();

    public void setPassword(String password);

    public String getPassword();

    public void setConnectionPooling(boolean connectionPooling);

    public boolean getConnectionPooling();
}
