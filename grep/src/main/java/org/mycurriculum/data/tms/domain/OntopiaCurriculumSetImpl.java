package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.exceptions.OccurrenceTypeNotFoundException;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class OntopiaCurriculumSetImpl extends OntopiaTopicCommonImpl implements OntopiaCurriculumSet {
    private static OntopiaTopicType type = null;

    private boolean curriculaChanged = false;
    private LazyLoaderCollection<OntopiaCurriculum> curricula;
    private LazyLoaderCollection<OntopiaCourseStructure> courseStructures;
    private boolean setTypeChanged = false;
    private String setType;

    public OntopiaCurriculumSetImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }

    public OntopiaCurriculumSetImpl(TopicMap topicMap) throws IOException {
        super(topicMap, getOntopiaType());
    }

    public OntopiaCurriculumSetImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    public static OntopiaTopicType getOntopiaType() {
        String psiStr = OntopiaPsis.curriculumSetTypePsi;
        synchronized (OntopiaCurriculumSetImpl.class) {
            if (type == null)
                type = createOntopiaType(psiStr);
            return type;
        }
    }

    public static URL generatePsi(String humanId) {
        try {
            return new URL(OntopiaPsis.getRootPsi()+"curriculum-set#"+humanId);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unexpetected url exception", e);
        }
    }

    @Override
    public Collection<OntopiaCurriculum> getCurricula() {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaCurriculum>) curricula.clone();
        }
    }

    @Override
    public void addCurriculum(OntopiaCurriculum curriculum) {
        synchronized (this) {
            curricula.add(curriculum);
            curriculaChanged = true;
        }
    }

    @Override
    public void removeCurriculum(OntopiaCurriculum curriculum) {
        synchronized (this) {
            curricula.remove(curriculum);
            curriculaChanged = true;
        }
    }

    @Override
    public Collection<OntopiaCourseStructure> getCourseStructures() {
        synchronized (this) {
            return (Collection<OntopiaCourseStructure>) courseStructures.clone();
        }
    }

    private void readInCurricula() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaPsis.curriculumSetHasCurriculumPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String child : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), child);
            refs.add(ref);
        }
        curricula = new LazyLoaderCollectionBuilder<OntopiaCurriculum,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaCurriculum load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCurriculumImpl(ref);
            }
        }.build();
        curriculaChanged = false;
    }

    private void saveCurricula() throws IOException {
        if (curriculaChanged) {
            curriculaChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(curricula.size());
            for (OntopiaCurriculum curriculum : curricula) {
                TopicIF topic = curriculum.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaPsis.curriculumSetHasCurriculumPsi, children);
        }
    }

    public void readInCourseStructures() throws IOException {
        Collection<String> parents = getAssociatedParents(OntopiaPsis.courseStructureHasCurriculumSetPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>();
        for (String parent : parents) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), parent);
            refs.add(ref);
        }
        courseStructures = new LazyLoaderCollectionBuilder<OntopiaCourseStructure,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaCourseStructure load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCourseStructureImpl(ref);
            }
        }.build();
    }

    public void readInOccurences() throws IOException {
        try {
            setType = loadOccurence(OntopiaPsis.setTypePsi);
        } catch (OccurrenceTypeNotFoundException e) {
            throw new IOException("Ontology error: Occurrence type not found: "+OntopiaPsis.setTypePsi, e);
        }
        setTypeChanged = false;
    }
    public void saveOccurences() throws IOException {
        if (setTypeChanged) {
            try {
                saveOccurence(OntopiaPsis.setTypePsi, setType);
                setTypeChanged = false;
            } catch (OccurrenceTypeNotFoundException e) {
                throw new IOException("Ontology error: Occurrence type not found: "+OntopiaPsis.setTypePsi, e);
            }
        }
    }

    @Override
    public void readInValues() throws IOException {
        readInCurricula();
        readInCourseStructures();
        readInOccurences();
    }

    @Override
    public void saveValues() throws IOException {
        saveCurricula();
        saveOccurences();
    }

    @Override
    public String getSetType() {
        synchronized (this) {
            return setType;
        }
    }

    @Override
    public void setSetType(String setType) {
        synchronized (this) {
            this.setType = setType;
            this.setTypeChanged = true;
        }
    }

    public static class Builder extends OntopiaTopicCommonImpl.Builder<OntopiaCurriculumSetImpl,Builder> implements OntopiaCurriculumSet.Builder {
        private List<OntopiaCurriculum> curricula = new ArrayList<OntopiaCurriculum>();

        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaType());
        }

        @Override
        public Builder curriculum(OntopiaCurriculum curriculum) {
            curricula.add(curriculum);
            return this;
        }

        @Override
        public OntopiaCurriculumSetImpl createInstance() throws IOException {
            return new OntopiaCurriculumSetImpl(getRef());
        }

        @Override
        public void builder(OntopiaCurriculumSetImpl object) {
            super.builder(object);
            for (OntopiaCurriculum curriculum : curricula) {
                object.addCurriculum(curriculum);
            }
        }
    }
}
