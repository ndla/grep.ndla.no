package org.mycurriculum.data.tms.exceptions;

public class InvalidDataException extends Exception {
    public InvalidDataException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalidDataException(String message) {
        super(message);
    }
    public boolean isRecoverable() {
        return false;
    }
}
