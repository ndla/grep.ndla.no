package org.mycurriculum.data.tms.connections.ontopia;

import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.connections.TopicMapStore;
import org.mycurriculum.data.tms.connections.TopicMapStoreLocator;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;

import java.io.IOException;

public class TopicMapStoreLocatorImpl implements TopicMapStoreLocator {
    private OntopiaConnectionFactory connectionFactory;
    private DBConnectionSetup dbConnectionSetup;
    private TmSourcesSetup tmSourcesSetup;
    private CacheDao cacheDao;

    public TopicMapStoreLocatorImpl(OntopiaConnectionFactory connectionFactory, CacheDao cacheDao) {
        this.connectionFactory = connectionFactory;
        this.cacheDao = cacheDao;
    }

    @Override
    public TopicMapStore getTopicMapStore() throws IOException {
        OntopiaConnectionFactory connectionFactory = this.connectionFactory;

        return connectionFactory.getTopicMapStore(tmSourcesSetup, dbConnectionSetup);
    }

    @Override
    public String getConnectionString() {
        return dbConnectionSetup.getFullConnectionString();
    }

    @Override
    public DBConnectionSetup getDbConnectionSetup() {
        return dbConnectionSetup;
    }

    public void setDbConnectionSetup(DBConnectionSetup dbConnectionSetup) {
        this.dbConnectionSetup = dbConnectionSetup;
    }

    @Override
    public TmSourcesSetup getTmSourcesSetup() {
        return tmSourcesSetup;
    }

    public void setTmSourcesSetup(TmSourcesSetup tmSourcesSetup) {
        this.tmSourcesSetup = tmSourcesSetup;
    }

    public CacheDao getCacheDao() {
        return cacheDao;
    }

    public void setCacheDao(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }
}
