package org.mycurriculum.data.tms.connections.topicmapstore.postgresql;

import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.connections.TopicMapStoreLocator;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;
import org.mycurriculum.data.tms.connections.ontopia.OntopiaConnectionFactory;
import org.mycurriculum.data.tms.connections.ontopia.TopicMapStoreLocatorImpl;
import org.mycurriculum.data.tms.connections.topicmapstore.TopicMapStoreCreator;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PostgresqlTopicMapStoreCreator implements TopicMapStoreCreator {
    private OntopiaConnectionFactory connectionFactory;
    private TopicMapStoreLocator topicMapStoreLocator;
    private CacheDao cacheDao;

    public PostgresqlTopicMapStoreCreator(OntopiaConnectionFactory connectionFactory, CacheDao cacheDao) {
        this.connectionFactory = connectionFactory;
        this.cacheDao = cacheDao;
    }

    public DBConnectionSetup getDBConnection() {
        return topicMapStoreLocator.getDbConnectionSetup();
    }

    public TmSourcesSetup getTmSourcesSetup() {
        return topicMapStoreLocator.getTmSourcesSetup();
    }

    public Connection getTemplateConnection() throws SQLException {
        Connection conn = DriverManager.getConnection(getDBConnection().getConnectionString(), getDBConnection().getJDBCProperties());
        return conn;
    }
    @Override
    public void testTemplateConnection() throws SQLException {
        getTemplateConnection().close();
    }
    @Override
    public void createStore(String name) throws SQLException, IOException {
        synchronized (this) {
            String templateDatabase = getTemplateDatabaseName();
            Connection conn = getTemplateConnection();
            try {
                PreparedStatement statement = conn.prepareStatement("CREATE DATABASE " + name + " WITH TEMPLATE " + templateDatabase + " ENCODING 'UNICODE'");
                try {
                    statement.execute();
                } finally {
                    statement.close();
                }
            } finally {
                conn.close();
            }
        }
    }

    @Override
    public void deleteStore(String name) throws SQLException {
        synchronized (this) {
            Connection conn = getTemplateConnection();
            try {
                PreparedStatement statement = conn.prepareStatement("DROP DATABASE " + name);
                try {
                    statement.execute();
                } finally {
                    statement.close();
                }
            } finally {
                conn.close();
            }
        }
    }

    @Override
    public boolean isOpenStore(String name) throws IOException {
        TopicMapStoreLocator locator = getStoreLocator(name);
        return connectionFactory.isOpen(locator.getDbConnectionSetup().getFullConnectionString());
    }

    @Override
    public void closeStore(String name) throws IOException {
        TopicMapStoreLocator locator = getStoreLocator(name);
        connectionFactory.close(locator.getDbConnectionSetup().getFullConnectionString());
    }

    @Override
    public void deleteStoreByConnectionString(String connectionString) throws IOException, SQLException {
        TopicMapStoreLocator locator = getStoreLocatorFromConnectionString(connectionString); /* validate */
        String dbName = getDatabaseNameFromConnectionString(connectionString);
        deleteStore(dbName);
    }

    @Override
    public String getDatabaseNameFromConnectionString(String connectionString) throws IOException {
        int queryStrPos = connectionString.indexOf('?');
        String query = "";
        if (queryStrPos >= 0) {
            connectionString = connectionString.substring(0, queryStrPos);
        }
        int preDb = connectionString.lastIndexOf('/');
        if (preDb < 0)
            throw new IOException("Failed to extract the database name from the connectionString");
        return connectionString.substring(preDb + 1);
    }
    private String getTemplateDatabaseName() throws IOException {
        String connectionString = getDBConnection().getConnectionString();
        return getDatabaseNameFromConnectionString(connectionString);
    }

    @Override
    public TopicMapStoreLocator getStoreLocator(String name) throws IOException {
        DBConnectionSetup setup = getDBConnection().clone();
        TmSourcesSetup tmSources = getTmSourcesSetup().clone();

        String connectionString = setup.getConnectionString();
        int queryStrPos = connectionString.indexOf('?');
        String query = "";
        if (queryStrPos >= 0) {
            query = connectionString.substring(queryStrPos);
            connectionString = connectionString.substring(0, queryStrPos);
        }
        int preDb = connectionString.lastIndexOf('/');
        if (preDb < 0)
            throw new IOException("Failed to point the connection string at the new database");
        connectionString = connectionString.substring(0, preDb) + "/" + name + query;
        setup.setConnectionString(connectionString);

        TopicMapStoreLocatorImpl locator = new TopicMapStoreLocatorImpl(connectionFactory, cacheDao);
        locator.setDbConnectionSetup(setup);
        locator.setTmSourcesSetup(tmSources);
        return locator;
    }

    @Override
    public TopicMapStoreLocator getStoreLocatorFromConnectionString(String connectionString) throws IOException {
        int pos = connectionString.indexOf(':');
        if (pos < 0)
            throw new IOException("Invalid connection string, expected classname followed by colon: "+connectionString);
        String className = connectionString.substring(0, pos);
        String classConnectionString = connectionString.substring(pos + 1);

        int queryStrPos = classConnectionString.indexOf('?');
        String query = "";
        if (queryStrPos >= 0) {
            query = classConnectionString.substring(queryStrPos);
            classConnectionString = classConnectionString.substring(0, queryStrPos);
        }
        int preDb = classConnectionString.lastIndexOf('/');
        if (preDb < 0)
            throw new IOException("No database part of the connection string");
        String dbName = classConnectionString.substring(preDb + 1);
        String bareConnectionString = classConnectionString.substring(0, preDb);

        /* Just check that this connection string corresponds with the template
         * and use additional parameters from there.
         */
        TopicMapStoreLocator locator = getStoreLocator(dbName);
        if (locator.getConnectionString().equals(connectionString))
            return locator;
        else
            throw new IOException("Cannot locate connection parameters for connection string: "+connectionString);
    }

    public TopicMapStoreLocator getTopicMapStoreLocator() {
        return topicMapStoreLocator;
    }

    public void setTopicMapStoreLocator(TopicMapStoreLocator topicMapStoreLocator) {
        this.topicMapStoreLocator = topicMapStoreLocator;
    }

    public CacheDao getCacheDao() {
        return cacheDao;
    }

    public void setCacheDao(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }
}
