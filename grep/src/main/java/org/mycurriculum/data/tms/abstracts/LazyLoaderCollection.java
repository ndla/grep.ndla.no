package org.mycurriculum.data.tms.abstracts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class LazyLoaderCollection<T> implements Collection<T>, Cloneable {
    private Collection<LazyLoader<T>> collection;
    public LazyLoaderCollection(Collection<LazyLoader<T>> collection)
    {
        this.collection = collection;
    }

    public void setInternalCollection(Collection<LazyLoader<T>> collection) {
        this.collection = collection;
    }

    @Override
    public Object clone()
    {
        LazyLoaderCollection<T> collection;
        synchronized (this) {
            try {
                collection = (LazyLoaderCollection<T>) super.clone();
                Collection<LazyLoader<T>> mirror = new ArrayList<LazyLoader<T>>(this.collection.size());
                for (LazyLoader<T> loader : this.collection)
                    mirror.add(loader);
                collection.setInternalCollection(mirror);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                throw new RuntimeException("Cloning not supported");
            }
        }
        return collection;
    }

    @Override
    public int size() {
        return collection.size();
    }

    @Override
    public boolean isEmpty() {
        return collection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        Iterator<T> iterator = iterator();
        while (iterator.hasNext()) {
            if (iterator.next().equals(o))
                return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new LazyLoaderIterator<T>(collection.iterator());
    }

    @Override
    public Object[] toArray() {
        ArrayList<T> array = new ArrayList<T>(collection.size());
        Iterator<T> iterator = iterator();
        while (iterator.hasNext()) {
            array.add(iterator.next());
        }
        return array.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] array = (T1[]) toArray();
        for (int i = 0; i < array.length && i < a.length; i++)
            a[i] = array[i];
        return a;
    }

    @Override
    public boolean add(T t) {
        synchronized (this) {
            return collection.add(new DummyLazyLoader<T>(t));
        }
    }

    @Override
    public boolean remove(Object o) {
        Iterator<LazyLoader<T>> iterator = collection.iterator();
        while (iterator.hasNext()) {
            LazyLoader<T> loader = iterator.next();
            if (loader.get().equals(o)) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object object : c) {
            if (!contains(object))
                return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean changed = false;
        for (T obj : c) {
            if (add(obj))
                changed = true;
        }
        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object obj : c) {
            if (remove(obj))
                changed = true;
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        Iterator<T> iterator = iterator();
        while (iterator.hasNext()) {
            T obj = iterator.next();
            if (!c.contains(obj)) {
                iterator.remove();
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        collection.clear();
    }

    public static abstract class LazyLoader<T> {
        private T obj = null;
        public abstract T load();
        public T get() {
            synchronized (this) {
                if (obj == null)
                    obj = load();
            }
            return obj;
        }
    }
    private static class DummyLazyLoader<T> extends LazyLoader<T> {
        private T object;
        public DummyLazyLoader(T object)
        {
            this.object = object;
        }

        @Override
        public T load() {
            return object;
        }
    }
    private static class LazyLoaderIterator<T> implements Iterator<T> {
        private Iterator<LazyLoader<T>> iterator;

        public LazyLoaderIterator(Iterator<LazyLoader<T>> iterator) {
            this.iterator = iterator;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public T next() {
            return iterator.next().get();
        }

        @Override
        public void remove() {
            iterator.remove();
        }
    }
}
