package org.mycurriculum.data.tms.connections.topicmapstore;

import org.mycurriculum.data.tms.connections.TopicMapStoreLocator;

import java.io.IOException;
import java.sql.SQLException;

public interface TopicMapStoreCreator {
    public void testTemplateConnection() throws SQLException;
    public void createStore(String name) throws SQLException, IOException;
    public void deleteStore(String name) throws IOException, SQLException;
    public boolean isOpenStore(String name) throws IOException;
    public void closeStore(String name) throws IOException;
    public void deleteStoreByConnectionString(String connectionString) throws IOException, SQLException;
    public TopicMapStoreLocator getStoreLocator(String name) throws IOException;
    public TopicMapStoreLocator getStoreLocatorFromConnectionString(String name) throws IOException;
    public String getDatabaseNameFromConnectionString(String connectionString) throws IOException;
}
