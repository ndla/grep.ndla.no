package org.mycurriculum.data.tms.connections.config.helpers;

import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringEscapeUtils.escapeXml11;

public abstract class TmSourcesGeneratorAbstract implements TmSourcesSetup, Cloneable {
    private String booleanValue(boolean value) {
        return (value ? "true" : "false");
    }

    @Override
    public Map<String, String> getParams() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("propertyFile", getPropertyFile());
        map.put("id", getId());
        map.put("title", getTitle());
        map.put("supportsCreate", booleanValue(getSupportsCreate()));
        map.put("supportsDelete", booleanValue(getSupportsDelete()));
        return map;
    }

    private String escapeXml(String value)
    {
        return escapeXml11(value); /* Imported static methods from class StringEscapeUtils */
    }
    private String getXmlKeyValue() {
        Map<String, String> map = getParams();
        String xml = "";
        for (Map.Entry<String, String> entry : map.entrySet()) {
            xml += "<param name=\"" + escapeXml(entry.getKey()) + "\" value=\"" + escapeXml(entry.getValue()) + "\"/>\n";
        }
        return xml;
    }

    private String getTmSourcesTemplate() throws IOException {
        String filename = "tm-sources.xml.template";
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(filename);
        byte[] chunk = new byte[4096];
        byte[] tpl = new byte[0];
        int rd;

        while ((rd = inputStream.read(chunk)) >= 0) {
            if (rd == 0) {
                /* Should be blocking read, wait and avoid 100% cpu */
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                continue;
            }
            byte[] newTpl = new byte[tpl.length + rd];
            System.arraycopy(tpl, 0, newTpl, 0, tpl.length);
            System.arraycopy(chunk, 0, newTpl, tpl.length, rd);
            tpl = newTpl;
        }
        try {
            return new String(tpl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new RuntimeException("Hey, UTF-8 decoding from byte array of classpath:"+filename+" does not work!");
        }
    }
    private String tmSourcesTemplateFilter(String sources) throws IOException {
        String template = getTmSourcesTemplate();
        return template.replaceFirst("%dynamicSources%", sources);
    }
    @Override
    public String toString() {
        String xml = "<source class=\""+escapeXml(getClassName())+"\">\n";
        xml += getXmlKeyValue();
        xml += "</source>";
        try {
            return tmSourcesTemplateFilter(xml);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to parse StringTemplate: IOException: "+e.getMessage());
        }
    }

    @Override
    public TmSourcesGeneratorAbstract clone() {
        try {
            return (TmSourcesGeneratorAbstract) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to clone TmSourcesGeneratorAbstract object");
        }
    }
}
