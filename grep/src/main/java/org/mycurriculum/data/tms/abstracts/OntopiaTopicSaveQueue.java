package org.mycurriculum.data.tms.abstracts;

import net.ontopia.topicmaps.core.UniquenessViolationException;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class OntopiaTopicSaveQueue {
    public final static int RECONNECT_INTERVAL = 2000;

    public interface SavedEvent {
        public void saved(OntopiaTopic topic);
    }
    public interface ConflictEvent {
        void conflict(OntopiaTopic topic, UniquenessViolationException e);
    }
    private static class OntopiaTopicRef {
        public OntopiaTopic topic;
        public SavedEvent savedEvent;
        public ConflictEvent conflictEvent;
        public OntopiaTopicRef(OntopiaTopic topic, SavedEvent savedEvent, ConflictEvent conflictEvent) {
            this.topic = topic;
            this.savedEvent = savedEvent;
            this.conflictEvent = conflictEvent;
        }
    }
    private TopicMapLocator dst;
    private BlockingQueue<OntopiaTopicRef> objects = new LinkedBlockingQueue<OntopiaTopicRef>();
    private Object saveQueueReadLock = new Object();
    private boolean saveQueueRunning = true;
    private IOException ioException = null;
    private QueueRunnerThread[] threads;

    public OntopiaTopicSaveQueue(TopicMapLocator dst, int numThreads) {
        this.dst = dst;
        this.threads = new QueueRunnerThread[numThreads];
        for (int i = 0; i < numThreads; i++) {
            this.threads[i] = new QueueRunnerThread();
            this.threads[i].start();
        }
    }
    public void saveTopic(OntopiaTopic topic, SavedEvent savedEvent, ConflictEvent conflictEvent) throws IOException {
        synchronized (this) {
            if (ioException != null)
                throw new IOException("Asynchronous IO-exception", ioException);
            try {
                objects.put(new OntopiaTopicRef(topic, savedEvent, conflictEvent));
            } catch (InterruptedException e) {
                throw new IOException("Interrupted", e);
            }
        }
    }
    public void spawnNewThread() throws IOException {
        synchronized (this) {
            if (ioException != null)
                throw new IOException("Asynchronous IO-exception", ioException);
            QueueRunnerThread[] newThreads = new QueueRunnerThread[threads.length + 1];
            System.arraycopy(threads, 0, newThreads, 0, threads.length);
            newThreads[threads.length] = new QueueRunnerThread();
            newThreads[threads.length].start();
            threads = newThreads;
        }
    }
    public void spawnNewThreads(int num) throws IOException {
        while (num > 0) {
            spawnNewThread();
            num--;
        }
    }
    public void endSaveQueue() throws IOException {
        synchronized (this) {
            if (ioException != null)
                throw new IOException("Asynchronous IO-exception", ioException);
            try {
                objects.put(new OntopiaTopicRef(null, null, null));
            } catch (InterruptedException e) {
                throw new IOException("Interrupted", e);
            }
        }
        try {
            for (int i = 0; i < threads.length; i++)
                threads[i].join();
        } catch (InterruptedException e) {
            throw new IOException("Interrupted", e);
        }
        synchronized (this) {
            if (ioException != null) {
                throw new IOException("Asynchronous IO-exception", ioException);
            }
        }
    }
    private void saveRunner() {
        boolean running = true;
        while (running) {
            int reconnectCounter = RECONNECT_INTERVAL;
            try {
                TopicMapRoot dst = this.dst.getTopicMapRoot();
                dst.openPersistentConnection();
                try {
                    while (reconnectCounter > 0) {
                        OntopiaTopicRef topicRef;
                        synchronized (saveQueueReadLock) {
                            if (!saveQueueRunning || ioException != null) {
                                running = false;
                                break;
                            }
                            topicRef = objects.take();
                            if (topicRef.topic == null) {
                                saveQueueRunning = false;
                                running = false;
                                break;
                            }
                        }
                        reconnectCounter--;
                        topicRef.topic.resetTopicMap(dst);
                        try {
                            topicRef.topic.save();
                            try {
                                topicRef.savedEvent.saved(topicRef.topic);
                            } catch (Exception e) {
                                throw new IOException("Exception thrown from saved event method", e);
                            }
                        } catch (NullPointerException e) {
                            throw new IOException("Npe in save: "+e.getMessage(), e);
                        } catch (UniquenessViolationException e) {
                            if (topicRef.conflictEvent != null) {
                                topicRef.conflictEvent.conflict(topicRef.topic, e);
                            } else {
                                throw new IOException("Conflict in save", e);
                            }
                        } catch (RuntimeException e) {
                            throw new IOException("Runtime exception in save: "+e.getMessage(), e);
                        }
                    }
                } catch (InterruptedException e) {
                    throw new IOException("Interrupted", e);
                } finally {
                    dst.closePersistentConnection();
                }
            } catch (IOException e) {
                ioException = e;
            }
        }
    }

    private class QueueRunnerThread extends Thread {
        public void run() {
            saveRunner();
        }
    }
}
