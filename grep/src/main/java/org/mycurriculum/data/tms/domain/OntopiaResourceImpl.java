package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import org.mycurriculum.data.tms.abstracts.OccurrenceProperty;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicRef;
import org.mycurriculum.data.tms.abstracts.OntopiaTopicType;
import org.mycurriculum.data.tms.exceptions.InvalidDataException;
import org.mycurriculum.data.tms.exceptions.RecoverableInvalidDataException;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.util.MutableTranslatedText;
import org.mycurriculum.data.util.MutableTranslatedTextImpl;
import org.mycurriculum.data.util.TranslatedText;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class OntopiaResourceImpl extends OntopiaTopicCommonImpl implements OntopiaResource {
    private static OntopiaTopicType type = null;

    @OccurrenceProperty(type = OntopiaPsis.authorsPsi)
    private String[] authors;
    @OccurrenceProperty(type = OntopiaPsis.authorUrlsPsi)
    private String[] authorUrls;

    //private String status; missing psi from ontology

    // private String creation; missing psi from ontology

    @OccurrenceProperty(type = OntopiaPsis.nodeTypePsi)
    private String nodeType;

    @OccurrenceProperty(type = OntopiaPsis.ingressPsi)
    private TranslatedText ingress;

    @OccurrenceProperty(type = OntopiaPsis.licensePsi)
    private TranslatedText license;

    @OccurrenceProperty(type = OntopiaPsis.statusPsi)
    private String status;

    public OntopiaResourceImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }

    public OntopiaResourceImpl(TopicMap topicMap) throws IOException {
        super(topicMap, getOntopiaTopicType());
    }

    public OntopiaResourceImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    public static OntopiaTopicType getOntopiaTopicType() {
        if (type == null) {
            try {
                type = new OntopiaTopicType(OntopiaPsis.resourceTypePsi);
            } catch (MalformedURLException e) {
                throw new RuntimeException("Problem with url", e);
            }
        }
        return type;
    }

    public static URL generatePsi(String humanId) {
        try {
            return new URL(OntopiaPsis.getRootPsi()+"resource#"+humanId);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unexpetected url exception", e);
        }
    }

    @Override
    public void readInValues() throws IOException {
        super.readInValues();
    }

    @Override
    public void saveValues() throws IOException {
        super.saveValues();
    }

    @Override
    public Collection<OntopiaResourceAimAssociation> getAimAssociations() throws IOException {
        return OntopiaResourceAimAssociationImpl.getAssociations(this);
    }

    @Override
    public Collection<OntopiaResourceAimAssociation> getAimAssociations(OntopiaAim aim) throws IOException {
        Collection<OntopiaResourceAimAssociation> list = new ArrayList<OntopiaResourceAimAssociation>();
        list.addAll(OntopiaResourceAimAssociationImpl.getAssociations(this, aim));
        return list;
    }

    @Override
    public OntopiaResourceAimAssociation.Builder buildAimAssociation(OntopiaAim aim) {
        return new OntopiaResourceAimAssociationImpl.Builder(this, aim);
    }

    @Override
    public String[] getAuthors() {
        return authors;
    }

    @Override
    public void setAuthors(String[] authors) throws RecoverableInvalidDataException {
        for (String author : authors) {
            if (author == null) {
                List<String> recoveredList = new ArrayList<String>();
                for (String authorVal : authors) {
                    if (authorVal != null) {
                        recoveredList.add(authorVal);
                    }
                }
                Object[] untypedRecoveredArray = recoveredList.toArray();
                String[] recoveredArray = new String[untypedRecoveredArray.length];
                for (int i = 0; i < recoveredArray.length; i++) {
                    recoveredArray[i] = (String) untypedRecoveredArray[i];
                }
                setAuthors(recoveredArray);
                throw new RecoverableInvalidDataException("Invalid authors (null value in array)");
            }
        }
        this.authors = authors;
    }

    @Override
    public String getNodeType() {
        return nodeType;
    }

    @Override
    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    @Override
    public String[] getAuthorUrls() {
        return authorUrls;
    }

    @Override
    public void setAuthorUrls(String[] authorUrls) throws RecoverableInvalidDataException {
        for (String authorUrl : authorUrls) {
            if (authorUrl == null) {
                List<String> recoveredList = new ArrayList<String>();
                for (String authorVal : authorUrls) {
                    if (authorVal != null) {
                        recoveredList.add(authorVal);
                    }
                }
                Object[] untypedRecoveredArray = recoveredList.toArray();
                String[] recoveredArray = new String[untypedRecoveredArray.length];
                for (int i = 0; i < recoveredArray.length; i++) {
                    recoveredArray[i] = (String) untypedRecoveredArray[i];
                }
                setAuthorUrls(recoveredArray);
                throw new RecoverableInvalidDataException("Invalid authors (null value in array)");
            }
        }
        this.authorUrls = authorUrls;
    }

    @Override
    public MutableTranslatedText getIngress() {
        synchronized (this) {
            return new MutableTranslatedTextImpl(ingress);
        }
    }

    @Override
    public void setIngress(TranslatedText ingress) {
        synchronized (this) {
            if (ingress != null)
                this.ingress = new MutableTranslatedTextImpl(ingress);
            else
                this.ingress = null;
        }
    }
    public void setIngress(MutableTranslatedText ingress) {
        setIngress((TranslatedText) ingress);
    }

    @Override
    public MutableTranslatedText getLicense() {
        synchronized (this) {
            if (license != null)
                return new MutableTranslatedTextImpl(license);
            else
                return null;
        }
    }

    @Override
    public void setLicense(TranslatedText license) {
        synchronized (this) {
            this.license = new MutableTranslatedTextImpl(license);
        }
    }

    public void setLicense(MutableTranslatedText license) {
        synchronized (this) {
            this.license = new MutableTranslatedTextImpl(license);
        }
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    public static class Builder extends OntopiaTopicCommonImpl.Builder<OntopiaResourceImpl,Builder> implements OntopiaResource.Builder {
        private Set<String> authors = new HashSet<String>();
        private Set<String> authorUrls = new HashSet<String>();
        private String nodeType = null;
        private TranslatedText ingress = null;
        private String status = null;

        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaTopicType());
        }

        @Override
        public OntopiaResourceImpl createInstance() throws IOException {
            return new OntopiaResourceImpl(getRef());
        }

        @Override
        public void builder(OntopiaResourceImpl object) {
            super.builder(object);

            int i;
            String[] arrcpy;

            if (authors.size() > 0) {
                i = 0;
                arrcpy = new String[authors.size()];
                for (String author : authors) {
                    arrcpy[i++] = author;
                }
                try {
                    object.setAuthors(arrcpy);
                } catch (InvalidDataException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            if (authorUrls.size() > 0) {
                i = 0;
                arrcpy = new String[authorUrls.size()];
                for (String authorUrl : authorUrls) {
                    arrcpy[i++] = authorUrl;
                }
                try {
                    object.setAuthorUrls(arrcpy);
                } catch (InvalidDataException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            if (nodeType != null)
                object.setNodeType(nodeType);
            if (ingress != null)
                object.setIngress(ingress);
            if (status != null)
                object.setStatus(status);
        }

        @Override
        public OntopiaResource.Builder author(String author) throws InvalidDataException {
            if (author == null) {
                throw new InvalidDataException("Author must not be null");
            }
            authors.add(author);
            return this;
        }

        @Override
        public OntopiaResource.Builder authors(String[] authors) throws InvalidDataException {
            for (String author : authors)
                author(author);
            return this;
        }

        @Override
        public OntopiaResource.Builder authorUrl(String authorUrl) throws InvalidDataException {
            if (authorUrl == null) {
                throw new InvalidDataException("Author url must not be null");
            }
            authorUrls.add(authorUrl);
            return this;
        }

        @Override
        public OntopiaResource.Builder authorUrls(String[] authorUrls) throws InvalidDataException {
            for (String authorUrl : authorUrls)
                authorUrl(authorUrl);
            return this;
        }

        @Override
        public OntopiaResource.Builder nodeType(String nodeType) {
            this.nodeType = nodeType;
            return this;
        }

        @Override
        public OntopiaResource.Builder ingress(TranslatedText ingress) {
            this.ingress = new MutableTranslatedTextImpl(ingress);
            return this;
        }

        @Override
        public OntopiaResource.Builder status(String status) {
            this.status = status;
            return this;
        }
    }
}
