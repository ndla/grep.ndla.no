package org.mycurriculum.data.tms.connections.config;

import org.mycurriculum.data.tms.connections.config.helpers.DBConnectionSetupAbstract;

import java.util.Properties;

public class PostgresqlConnectionSetupImpl extends DBConnectionSetupAbstract implements PostgresqlConnectionSetup, Cloneable {
    private String username;
    private String password;
    private boolean connectionPooling;
    private int minimumSize;
    private int maximumSize;
    private boolean softMaximum;

    public PostgresqlConnectionSetupImpl() {
        /* "reasonable" defaults */
        setConnectionString("jdbc:postgresql:ontopia");
        setUsername(null);
        setPassword(null);
        setConnectionPooling(true);
        setMinimumSize(4);
        setMaximumSize(15);
        setSoftMaximum(true);
    }

    @Override
    public PostgresqlConnectionSetupImpl clone()
    {
        return (PostgresqlConnectionSetupImpl) super.clone();
    }

    @Override
    public String getDriver() {
        return "org.postgresql.Driver";
    }

    @Override
    public String getDatabase() {
        return "postgresql";
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setConnectionPooling(boolean connectionPooling) {
        this.connectionPooling = connectionPooling;
    }

    @Override
    public boolean getConnectionPooling() {
        return connectionPooling;
    }

    @Override
    public String toString() {
        String data = super.toString();
        if (getUsername() != null)
            data += "net.ontopia.topicmaps.impl.rdbms.UserName=" + getUsername() + "\n";
        if (getPassword() != null)
            data += "net.ontopia.topicmaps.impl.rdbms.Password=" + getPassword() + "\n";
        data += "net.ontopia.topicmaps.impl.rdbms.ConnectionPool=" + (getConnectionPooling() ? "true" : "false") + "\n";
        data += "net.ontopia.topicmaps.impl.rdbms.ConnectionPool.MinimumSize = "+getMinimumSize()+"\n";
        data += "net.ontopia.topicmaps.impl.rdbms.ConnectionPool.MaximumSize = "+getMaximumSize()+"\n";
        data += "net.ontopia.topicmaps.impl.rdbms.StorePool.SoftMaximum = "+(isSoftMaximum() ? "true" : "false")+"\n";
        return data;
    }

    @Override
    public Properties getJDBCProperties() {
        Properties props = new Properties();
        props.setProperty("user", getUsername());
        props.setProperty("password", getPassword());
        return props;
    }

    public int getMinimumSize() {
        return minimumSize;
    }

    public void setMinimumSize(int minimumSize) {
        this.minimumSize = minimumSize;
    }

    public int getMaximumSize() {
        return maximumSize;
    }

    public void setMaximumSize(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public boolean isSoftMaximum() {
        return softMaximum;
    }

    public void setSoftMaximum(boolean softMaximum) {
        this.softMaximum = softMaximum;
    }
}
