package org.mycurriculum.data.tms.domain;

public class OntopiaPsis {
    public static final String ontologyRootPsi = "http://psi.mycurriculum.org/";

    public static final String universalTypePsi = ontologyRootPsi + "#universal-type";
    /* Topic types */
    public static final String aimSetTypePsi = ontologyRootPsi + "#competenceaimset";
    public static final String curriculumTypePsi = ontologyRootPsi +"#curriculum";
    public static final String curriculumSetTypePsi = ontologyRootPsi +"#curriculumset";
    public static final String courseStructureTypePsi = ontologyRootPsi +"#course_structure";
    public static final String resourceTypePsi = ontologyRootPsi + "#resource";
    public static final String aimTypePsi = ontologyRootPsi + "#competenceaim";
    public static final String levelTypePsi = ontologyRootPsi + "#level";
    public static final String aimResourceAssociationReifierTypePsi = ontologyRootPsi + "#resource-aim-association-reifier";
    /* Occurences */
    public static final String upstreamIdPsi = ontologyRootPsi + "#upstream-id";
    public static final String setTypePsi = ontologyRootPsi +"#set-type";
    public static final String humanIdPsi = ontologyRootPsi +"#human-id";
    public static final String authorsPsi = ontologyRootPsi +"#node-author";
    public static final String nodeTypePsi = ontologyRootPsi +"#node-type";
    public static final String authorUrlsPsi = ontologyRootPsi +"#node-author-url";
    public static final String ingressPsi = ontologyRootPsi + "#node-ingress";
    public static final String licensePsi = ontologyRootPsi + "#resource-usufruct";
    public static final String sortingOrderPsi = ontologyRootPsi + "#sorting-order";
    public static final String apprenticeRelevantPsi = ontologyRootPsi + "#apprentice-relevant";
    public static final String timestampPsi = ontologyRootPsi + "#timestamp";
    public static final String statusPsi = ontologyRootPsi + "#status";
    public static final String courseIdPsi = ontologyRootPsi + "#course-id";
    public static final String courseTypePsi = ontologyRootPsi + "#course-type";
    /* Associations */
    public static final String courseStructureHasCourseStructurePsi = ontologyRootPsi +"#course_structure-has-course_structure";
    public static final String courseStructureHasLevelPsi = ontologyRootPsi +"#course_structure-has-level";
    public static final String courseStructureHasCurriculumSetPsi = ontologyRootPsi +"#course_structure-has-curriculumset";
    public static final String curriculumSetHasCurriculumPsi = ontologyRootPsi +"#curriculumset-has-curriculum";
    public static final String curriculumHasCompetenceAimSetsPsi = ontologyRootPsi +"competenceaimset_etter_curriculum";
    public static final String aimSetHasAimSetPsi = ontologyRootPsi +"competenceaimset_har_competenceaimset";
    public static final String aimHasResourceRelatedPsi = ontologyRootPsi + "#related";
    public static final String aimSetHasLevelPsi = ontologyRootPsi +"#competenceaimset-has-level";
    public static final String aimResourceAssociationCurriculumSet = ontologyRootPsi +"#aim-resource-association-curriculum-set";
    public static final String aimResourceAssociationLevel = ontologyRootPsi +"#aim-resource-association-level";
    public static final String aimResourceAssociationCurriculum = ontologyRootPsi +"#aim-resource-association-curriculum";
    public static final String aimResourceAssociationAimSet = ontologyRootPsi +"#aim-resource-association-competenceaimset";

    public static String getRootPsi() {
        return ontologyRootPsi;
    }
}
