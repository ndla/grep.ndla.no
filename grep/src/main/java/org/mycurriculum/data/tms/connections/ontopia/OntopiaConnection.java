package org.mycurriculum.data.tms.connections.ontopia;

import net.ontopia.topicmaps.entry.TopicMapRepositoryIF;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.util.Collection;

public interface OntopiaConnection {
    public Collection<String> getTopicMapKeys() throws IOException;
    public TopicMapRoot getTopicMapByKey(String key) throws IOException;
    public TopicMapRepositoryIF getTopicMapRepository() throws IOException;

    public String getDatabaseConnectionString();
}
