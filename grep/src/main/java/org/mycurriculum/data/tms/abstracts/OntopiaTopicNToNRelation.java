package org.mycurriculum.data.tms.abstracts;

import java.net.URL;

public class OntopiaTopicNToNRelation {
    private URL assoc, src, dst;

    public OntopiaTopicNToNRelation(URL assoc, URL src, URL dst) {
        this.assoc = assoc;
        this.src = src;
        this.dst = dst;
    }
    public URL getAssociation() {
        return assoc;
    }
    public URL getSourcePsi() {
        return src;
    }
    public URL getDestinationPsi() {
        return dst;
    }
    public OntopiaTopicNToNRelation getReverse() {
        return new OntopiaTopicNToNRelation(assoc, dst, src);
    }
}
