package org.mycurriculum.data.tms.connections.ontopia;

import net.ontopia.topicmaps.entry.TopicMapRepositoryIF;
import net.ontopia.topicmaps.entry.XMLConfigSource;
import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.connections.TopicMapStore;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;
import org.mycurriculum.data.tms.connections.config.helpers.TmpConfigFileGenerator;
import org.mycurriculum.data.tms.connections.config.helpers.TmpConfigFileGeneratorImpl;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class OntopiaConnectionFactoryImpl implements OntopiaConnectionFactory, Closeable, AutoCloseable {
    private CacheDao cacheDao;
    private Map<String,TopicMapRepositoryIF> repositories = new HashMap<String,TopicMapRepositoryIF>();

    public OntopiaConnectionFactoryImpl(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }

    private TopicMapRepositoryIF getTopicMapRepositoryIf(TmpConfigFileGenerator config) throws IOException {
        TmpConfigFileGenerator configGenerator = new TmpConfigFileGeneratorImpl();
        OntopiaConnectionImpl connection;
        File tmSources;

        tmSources = config.getTmSourcesFile();
        return XMLConfigSource.getRepository(tmSources.getAbsolutePath());
    }
    @Override
    public TopicMapRepositoryIF getOntopiaRepository(TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) throws IOException {
        TopicMapRepositoryIF repository;

        synchronized (this) {
            if (repositories.containsKey(dbConnectionSetup.getFullConnectionString())) {
                repository = repositories.get(dbConnectionSetup.getFullConnectionString());
            } else {
                TmpConfigFileGenerator configGenerator = new TmpConfigFileGeneratorImpl();
                configGenerator.setDBConnectionSetup(dbConnectionSetup);
                configGenerator.setTmSourcesSetup(setup);
                repository = getTopicMapRepositoryIf(configGenerator);
                repositories.put(dbConnectionSetup.getFullConnectionString(), repository);
            }
        }

        return repository;
    }
    private OntopiaConnectionImpl getConnectionImpl(TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) throws IOException {
        return new OntopiaConnectionImpl(this, cacheDao, setup, dbConnectionSetup);
    }

    @Override
    public OntopiaConnection getConnection(TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) throws IOException {
        return getConnectionImpl(setup, dbConnectionSetup);
    }

    @Override
    public TopicMapStore getTopicMapStore(TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) throws IOException {
        return getConnectionImpl(setup, dbConnectionSetup);
    }

    @Override
    public boolean isOpen(String fullDbConnectionString) {
        synchronized (this) {
            return repositories.containsKey(fullDbConnectionString);
        }
    }

    public CacheDao getCacheDao() {
        return cacheDao;
    }

    public void setCacheDao(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }

    @Override
    public void close(TopicMapRepositoryIF repository) {
        synchronized (this) {
            Iterator<Map.Entry<String,TopicMapRepositoryIF>> iterator = repositories.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String,TopicMapRepositoryIF> entry = iterator.next();
                if (entry.getValue() == repository) {
                    iterator.remove();
                    repository.close();
                    return;
                }
            }
        }
        repository.close();
        throw new RuntimeException("TopicMapRepositoryIF was not found");
    }
    @Override
    public void close(String fullConnectionString) {
        TopicMapRepositoryIF repository;
        synchronized (this) {
            repository = repositories.get(fullConnectionString);
            repositories.remove(repository);
        }
        repository.close();
    }
    @Override
    public void close() throws IOException {
        synchronized (this) {
            for (Map.Entry<String,TopicMapRepositoryIF> entry : repositories.entrySet()) {
                entry.getValue().close();
            }
            repositories.clear();
        }
    }
}
