package org.mycurriculum.data.tms.abstracts;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapBuilderIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class OntopiaTopicType {
    private LocatorIF locator;

    public OntopiaTopicType(LocatorIF locator)
    {
        this.locator = locator;
    }
    public OntopiaTopicType(String locator) throws MalformedURLException {
        this(new URILocator(locator));
    }
    public static URILocator urlToLocator(URL input)
    {
        try {
            return new URILocator(input);
        } catch (MalformedURLException e) {
            throw new RuntimeException("URL to URL converting failure");
        }
    }
    public OntopiaTopicType(URL locator)
    {
        this(urlToLocator(locator));
    }

    public TopicIF getTopic(TopicMapIF topicMap) throws IOException {
        TopicIF topic = topicMap.getTopicBySubjectIdentifier(locator);
        if (topic == null)
            throw new IOException("Cannot locate type in topicmap: "+locator.toString());
        return topic;
    }

    public LocatorIF getLocator() {
        return locator;
    }

    public void createTopicIfNotExists(TopicMap topicMapRoot) throws IOException {
        TopicMapIF topicMap = topicMapRoot.getTopicMap();
        boolean ok = false;
        try {
            TopicIF topic = topicMap.getTopicBySubjectIdentifier(locator);
            if (topic == null) {
                TopicMapBuilderIF builder = topicMap.getBuilder();
                topic = builder.makeTopic();
                topic.addSubjectIdentifier(locator);
                builder.makeTopicName(topic, "Type: "+locator.getAddress());
            }
            ok = true;
            topicMapRoot.commit();
        } finally {
            if (!ok)
                topicMapRoot.abort();
            topicMapRoot.close();
        }
    }
}
