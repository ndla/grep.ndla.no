package org.mycurriculum.data.tms.connections.ontopia;

import net.ontopia.topicmaps.entry.TopicMapRepositoryIF;
import org.mycurriculum.data.tms.connections.TopicMapStore;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;

import java.io.IOException;

public interface OntopiaConnectionFactory {
    TopicMapRepositoryIF getOntopiaRepository(TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) throws IOException;
    public OntopiaConnection getConnection(TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) throws IOException;
    public TopicMapStore getTopicMapStore(TmSourcesSetup setup, DBConnectionSetup dbConnectionSetup) throws IOException;
    public boolean isOpen(String fullDbConnectionString);
    public void close(TopicMapRepositoryIF repositoryIf);
    public void close(String fullDbConnectionString);
}
