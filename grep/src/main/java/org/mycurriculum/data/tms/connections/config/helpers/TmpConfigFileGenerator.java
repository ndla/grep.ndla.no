package org.mycurriculum.data.tms.connections.config.helpers;

import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;

import java.io.File;
import java.io.IOException;

public interface TmpConfigFileGenerator {
    public DBConnectionSetup getDBConnectionSetup();

    public void setDBConnectionSetup(DBConnectionSetup dbconn);

    public TmSourcesSetup getTmSourcesSetup();

    public void setTmSourcesSetup(TmSourcesSetup tmSources);

    File getTmSourcesFile() throws IOException;
}
