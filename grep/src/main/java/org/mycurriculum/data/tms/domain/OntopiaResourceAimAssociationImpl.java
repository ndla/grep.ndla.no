package org.mycurriculum.data.tms.domain;

import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.util.*;

public class OntopiaResourceAimAssociationImpl implements OntopiaResourceAimAssociation {
    private OntopiaAssociation.ReifiedAssociation<OntopiaAimResourceMetadataImpl> association;
    private OntopiaAimResourceMetadataImpl metadata = null;

    public OntopiaResourceAimAssociationImpl(OntopiaAssociation association) {
        this.association = new OntopiaAssociation.ReifiedAssociation<OntopiaAimResourceMetadataImpl>(association) {
            @Override
            public OntopiaAimResourceMetadataImpl createReifiedInstance(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimResourceMetadataImpl(ref);
            }
        };
    }
    public OntopiaResourceAimAssociationImpl(OntopiaResource resource, OntopiaAim aim) throws IOException {
        this(createAssociationFor(resource, aim));
    }
    public OntopiaResourceAimAssociationImpl(TopicMap topicMap, String humanId) throws IOException {
        this(getExistingAssociation(topicMap, humanId));
    }

    public static OntopiaAssociation getExistingAssociation(TopicMap topicMap, String humanId) throws IOException {
        String[] roleTypes = {OntopiaPsis.aimTypePsi, OntopiaPsis.resourceTypePsi};
        OntopiaAssociation.Type typeDef = new OntopiaAssociation.Type(OntopiaPsis.aimHasResourceRelatedPsi, roleTypes);
        return OntopiaAssociationImpl.getAssociationById(typeDef, topicMap, humanId);
    }
    public static Collection<OntopiaAssociation> getExistingAssociationsFor(OntopiaResource resource, OntopiaAim aim) throws IOException {
        Collection<OntopiaResourceAimAssociation> associations = getAssociations(resource);
        Collection<OntopiaAssociation> list = new ArrayList<OntopiaAssociation>();
        for (OntopiaResourceAimAssociation association : associations) {
            OntopiaAim assocAim = association.getAim();
            if (assocAim != null && assocAim.equals(aim)) {
                list.add(association.getAssociationObject());
            }
        }
        return list;
    }
    public static Collection<OntopiaResourceAimAssociationImpl> getAssociations(OntopiaResource resource, OntopiaAim aim) throws IOException {
        Collection<OntopiaAssociation> associations = getExistingAssociationsFor(resource, aim);
        Collection<OntopiaResourceAimAssociationImpl> objects = new ArrayList<OntopiaResourceAimAssociationImpl>();
        for (OntopiaAssociation association : associations) {
            objects.add(new OntopiaResourceAimAssociationImpl(association));
        }
        return objects;
    }
    public static OntopiaAssociation createAssociationFor(OntopiaResource resource, OntopiaAim aim) throws IOException {
        if (resource == null)
            throw new RuntimeException("Resource is null");
        if (aim == null)
            throw new RuntimeException("Aim is null");
        String[] roleTypes = {OntopiaPsis.aimTypePsi, OntopiaPsis.resourceTypePsi};
        OntopiaAssociation.Type typeDef = new OntopiaAssociation.Type(OntopiaPsis.aimHasResourceRelatedPsi, roleTypes);
        OntopiaAssociation  association = new OntopiaAssociationImpl(typeDef, resource.getRef().getTopicMapReference());
        association.addPlayer(OntopiaPsis.resourceTypePsi, resource);
        association.addPlayer(OntopiaPsis.aimTypePsi, aim);
        return association;
    }
    public static Collection<? extends OntopiaAssociation> getAssociationObjects(OntopiaTopic topic) throws IOException {
        String[] roleTypes = {OntopiaPsis.aimTypePsi, OntopiaPsis.resourceTypePsi};
        OntopiaAssociation.Type typeDef = new OntopiaAssociation.Type(OntopiaPsis.aimHasResourceRelatedPsi, roleTypes);
        return OntopiaAssociationImpl.getAssociationObjects(typeDef, topic.getRef().getTopicMapReference(), topic.getObjectId());
    }
    private static Collection<OntopiaResourceAimAssociation> getAssociations(OntopiaTopic topic) throws IOException {
        Collection<OntopiaResourceAimAssociation> associations = new ArrayList<OntopiaResourceAimAssociation>();
        Collection<? extends OntopiaAssociation> associationObjects = getAssociationObjects(topic);
        for (OntopiaAssociation associationObject : associationObjects) {
            OntopiaResourceAimAssociation association = new OntopiaResourceAimAssociationImpl(associationObject);
            associations.add(association);
        }
        return associations;
    }
    public static Collection<OntopiaResourceAimAssociation> getAssociations(OntopiaResource resource) throws IOException {
        return getAssociations((OntopiaTopic) resource);
    }
    public static Collection<OntopiaResourceAimAssociation> getAssociations(OntopiaAim aim) throws IOException {
        return getAssociations((OntopiaTopic) aim);
    }

    @Override
    public OntopiaAssociation getAssociationObject() {
        return association.getAssociationObject();
    }

    @Override
    public String getHumanId() {
        return association.getHumanId();
    }

    @Override
    public void setHumanId(String humanId) throws IOException {
        association.setHumanId(humanId);
    }

    private OntopiaTopicRef getPlayerRef(String type) throws IOException {
        Collection<String> rolePlayerIds = association.getPlayers(type);
        if (rolePlayerIds.size() != 1) {
            throw new IOException("Bad topicmap data. (expeted 1 role player, got "+rolePlayerIds.size()+")");
        }
        String rolePlayerId = rolePlayerIds.iterator().next();
        return new OntopiaTopicFinderById(association.getAssociationObject().getTopicMap(), rolePlayerId);
    }

    @Override
    public OntopiaTopicRef getResourceRef() throws IOException {
        return getPlayerRef(OntopiaPsis.resourceTypePsi);
    }

    @Override
    public OntopiaResource getResource() throws IOException {
        return new OntopiaResourceImpl(getResourceRef());
    }

    @Override
    public OntopiaTopicRef getAimRef() throws IOException {
        return getPlayerRef(OntopiaPsis.aimTypePsi);
    }

    @Override
    public OntopiaAim getAim() throws IOException {
        return new OntopiaAimImpl(getAimRef());
    }

    public static OntopiaResourceAimAssociationImpl getAssociationByHumanId(TopicMap topicMap, String humanId) throws IOException {
        OntopiaAssociation assoc = getExistingAssociation(topicMap, humanId);
        if (assoc != null) {
            return new OntopiaResourceAimAssociationImpl(assoc);
        }
        return null;
    }

    private class MetadataRef {
        public OntopiaAimResourceMetadataImpl metadataRef;
    }
    private MetadataRef metadataRefObject = new MetadataRef();
    private void prepareMetadata() throws IOException {
        synchronized (metadataRefObject) {
            if (metadata == null) {
                metadata = association.getReifier();
                if (metadata == null) {
                    final OntopiaAssociation.ReifiedAssociation<OntopiaAimResourceMetadataImpl> reified = association;
                    final MetadataRef metadataRef = metadataRefObject;
                    metadataRef.metadataRef = new OntopiaAimResourceMetadataImpl(association.getAssociationObject().getTopicMap()) {
                        public void save() throws IOException {
                            super.save();
                            reified.setReifier(metadataRef.metadataRef);
                            reified.save();
                        }
                    };
                    metadata = metadataRef.metadataRef;
                }
            }
        }
    }

    @Override
    public String[] getAuthors() throws IOException {
        prepareMetadata();
        return metadata.getAuthors();
    }

    @Override
    public void setAuthors(String[] authors) throws IOException {
        prepareMetadata();
        metadata.setAuthors(authors);
    }

    @Override
    public String[] getAuthorUrls() throws IOException {
        prepareMetadata();
        return metadata.getAuthorUrls();
    }

    @Override
    public void setAuthorUrls(String[] authorUrls) throws IOException {
        prepareMetadata();
        metadata.setAuthorUrls(authorUrls);
    }

    @Override
    public void setApprenticeRelevant(boolean apprenticeRelevant) throws IOException {
        prepareMetadata();
        metadata.setApprenticeRelevant(apprenticeRelevant);
    }

    @Override
    public boolean getApprenticeRelevant() throws IOException {
        prepareMetadata();
        return metadata.getApprenticeRelevant();
    }

    @Override
    public boolean isApprenticeRelevant() throws IOException {
        prepareMetadata();
        return metadata.isApprenticeRelevant();
    }

    @Override
    public void setTimestamp(Long timestamp) throws IOException {
        prepareMetadata();
        metadata.setTimestamp(timestamp);
    }

    @Override
    public Long getTimestamp() throws IOException {
        prepareMetadata();
        return metadata.getTimestamp();
    }

    @Override
    public void setTimestampDate(Date date) throws IOException {
        prepareMetadata();
        metadata.setTimestampDate(date);
    }

    @Override
    public Date getTimestampDate() throws IOException {
        prepareMetadata();
        return metadata.getTimestampDate();
    }

    @Override
    public void setCurriculumSet(OntopiaCurriculumSet curiculumSet) throws IOException {
        prepareMetadata();
        metadata.setCurriculumSet(curiculumSet);
    }

    @Override
    public OntopiaCurriculumSet getCurriculumSet() throws IOException {
        prepareMetadata();
        return metadata.getCurriculumSet();
    }

    @Override
    public void setLevel(OntopiaLevel level) throws IOException {
        prepareMetadata();
        metadata.setLevel(level);
    }

    @Override
    public OntopiaLevel getLevel() throws IOException {
        prepareMetadata();
        return metadata.getLevel();
    }

    @Override
    public void setCurriculum(OntopiaCurriculum curriculum) throws IOException {
        prepareMetadata();
        metadata.setCurriculum(curriculum);
    }

    @Override
    public OntopiaCurriculum getCurriculum() throws IOException {
        prepareMetadata();
        return metadata.getCurriculum();
    }

    @Override
    public void setAimSet(OntopiaAimSet aimSet) throws IOException {
        prepareMetadata();
        metadata.setAimSet(aimSet);
    }

    @Override
    public OntopiaAimSet getAimSet() throws IOException {
        prepareMetadata();
        return metadata.getAimSet();
    }

    @Override
    public void setCourseId(String courseId) throws IOException {
        prepareMetadata();
        metadata.setCourseId(courseId);
    }

    @Override
    public String getCourseId() throws IOException {
        prepareMetadata();
        return metadata.getCourseId();
    }

    @Override
    public void setCourseType(String courseType) throws IOException {
        prepareMetadata();
        metadata.setCourseType(courseType);
    }

    @Override
    public String getCourseType() throws IOException {
        prepareMetadata();
        return metadata.getCourseType();
    }

    @Override
    public void remove() throws IOException {
        association.getAssociationObject().remove();
    }

    @Override
    public void save() throws IOException {
        association.save();
        if (metadata != null) {
            metadata.save();
        }
    }

    public static class Builder implements OntopiaResourceAimAssociation.Builder {
        private OntopiaResource resource;
        private OntopiaAim aim;
        private Set<String> authors = new HashSet<String>();
        private Set<String> authorUrls = new HashSet<String>();
        private Long timestamp = null;
        private Boolean apprenticeRelevant = null;
        private OntopiaCurriculumSet curriculumSet = null;
        private OntopiaLevel level = null;
        private OntopiaCurriculum curriculum = null;
        private OntopiaAimSet aimSet = null;
        private String courseId = null;
        private String courseType = null;
        private String humanId = null;

        public Builder(OntopiaResource resource, OntopiaAim aim) {
            if (resource == null)
                throw new RuntimeException("Resource is null");
            if (aim == null)
                throw new RuntimeException("Aim is null");
            this.resource = resource;
            this.aim = aim;
        }

        @Override
        public Builder humanId(String humanId) {
            this.humanId = humanId;
            return this;
        }

        @Override
        public Builder author(String author) {
            authors.add(author);
            return this;
        }

        @Override
        public Builder authors(String[] authors) {
            for (String author : authors)
                author(author);
            return this;
        }

        @Override
        public Builder authorUrl(String authorUrl) {
            authorUrls.add(authorUrl);
            return this;
        }

        @Override
        public Builder authorUrls(String[] authorUrls) {
            for (String authorUrl : authorUrls)
                authorUrl(authorUrl);
            return this;
        }

        @Override
        public Builder timestamp(long timestamp) {
            this.timestamp = new Long(timestamp);
            return this;
        }

        @Override
        public Builder timestampDate(Date date) {
            timestamp(date.getTime() / 1000);
            return this;
        }

        @Override
        public Builder apprenticeRelevant(boolean relevant) {
            this.apprenticeRelevant = new Boolean(relevant);
            return this;
        }

        @Override
        public Builder curriculumSet(OntopiaCurriculumSet curriculumSet) {
            this.curriculumSet = curriculumSet;
            return this;
        }

        @Override
        public Builder level(OntopiaLevel level) {
            this.level = level;
            return this;
        }

        @Override
        public Builder curriculum(OntopiaCurriculum curriculum) {
            this.curriculum = curriculum;
            return this;
        }

        @Override
        public Builder aimSet(OntopiaAimSet aimSet) {
            this.aimSet = aimSet;
            return this;
        }

        @Override
        public Builder courseId(String courseId) {
            this.courseId = courseId;
            return this;
        }

        @Override
        public Builder courseType(String courseType) {
            this.courseType = courseType;
            return this;
        }

        @Override
        public OntopiaResourceAimAssociation build() throws IOException {
            OntopiaResourceAimAssociation association = new OntopiaResourceAimAssociationImpl(resource, aim);
            String[] authors = new String[this.authors.size()];
            int i = 0;
            for (String author : this.authors) {
                authors[i] = author;
                i++;
            }
            String[] authorUrls = new String[this.authorUrls.size()];
            i = 0;
            for (String authorUrl : this.authorUrls) {
                authorUrls[i] = authorUrl;
                i++;
            }
            association.setAuthors(authors);
            association.setAuthorUrls(authorUrls);
            if (timestamp != null)
                association.setTimestamp(timestamp);
            if (apprenticeRelevant != null)
                association.setApprenticeRelevant(apprenticeRelevant);
            if (curriculumSet != null)
                association.setCurriculumSet(curriculumSet);
            if (level != null)
                association.setLevel(level);
            if (curriculum != null)
                association.setCurriculum(curriculum);
            if (aimSet != null)
                association.setAimSet(aimSet);
            if (courseId != null)
                association.setCourseId(courseId);
            if (courseType != null)
                association.setCourseType(courseType);
            if (humanId != null)
                association.setHumanId(humanId);
            return association;
        }
    }
}
