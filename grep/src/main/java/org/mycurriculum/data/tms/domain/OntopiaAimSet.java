package org.mycurriculum.data.tms.domain;

import java.util.Collection;

public interface OntopiaAimSet extends OntopiaTopicCommon {
    /**
     * there shold normally be one curriculum (parent).
     */
    public Collection<OntopiaCurriculum> getAllContainingCurricula();
    public Collection<OntopiaCurriculum> getCurricula();
    public void addCurriculum(OntopiaCurriculum curriculum);
    public void removeCurriculum(OntopiaCurriculum curriculum);
    public Collection<OntopiaAim> getAims();
    public void addAim(OntopiaAim aim);
    public void removeAim(OntopiaAim aim);
    public Collection<OntopiaAimSet> getAimSets();
    public void addAimSet(OntopiaAimSet aimSet);
    public void removeAimSet(OntopiaAimSet aimSet);
    public Collection<OntopiaAimSet> getParentAimSets();
    public Collection<OntopiaLevel> getLevels();
    public void addLevel(OntopiaLevel level);
    public void removeLevel(OntopiaLevel level);
    public void setSetType(String setType);
    public String getSetType();
    public void setSortingOrder(Integer sortingOrder);
    public Integer getSortingOrder();
    public String getUpstreamId();
    public void setUpstreamId(String upstreamId);
    public interface Builder extends OntopiaTopicCommon.Builder<OntopiaAimSet,Builder> {
        public Builder aimSet(OntopiaAimSet aimSet);
        public Builder addCurriculum(OntopiaCurriculum curriculum);
        public Builder sortingOrder(Integer sortingOrder);
        public Builder aim(OntopiaAim aim);
        public Builder upstreamId(String upstreamId);
    }
}
