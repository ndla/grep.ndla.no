package org.mycurriculum.data.tms.connections;

import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;

public interface TopicMapLocator {
    public TopicMap getTopicMap() throws IOException;
    public TopicMapRoot getTopicMapRoot() throws IOException;
    public DBConnectionSetup getDBConnectionSetup(); /* May return null if not available */
    public String getConnectionString() throws IOException;
    public String getTopicMapId() throws IOException;
}
