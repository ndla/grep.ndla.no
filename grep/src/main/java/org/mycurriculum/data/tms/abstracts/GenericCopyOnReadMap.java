package org.mycurriculum.data.tms.abstracts;

import java.util.*;

public abstract class GenericCopyOnReadMap<K,V> implements Map<K,V> {
    private Map<K,V> map;
    private boolean cloned;
    private Set<K> copied;

    public GenericCopyOnReadMap(Map<K, V> map) {
        this.map = map;
        this.copied = new HashSet<K>();
        this.cloned = false;
    }

    public abstract V copyValue(V value);
    public Map<K,V> getMap() {
        Map<K,V> rev;
        synchronized (this) {
            rev = map;
            cloned = false;
        }
        return rev;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    private void requireCloned() {
        synchronized (this) {
            if (!cloned) {
                Map<K, V> cp = new HashMap<K, V>();
                cp.putAll(map);
                map = cp;
                cloned = true;
            }
        }
    }
    @Override
    public V get(Object key) {
        synchronized (this) {
            if (!copied.contains(key)) {
                requireCloned();
                V orig = map.get(key);
                if (orig == null)
                    return null;
                V cp = copyValue(orig);
                map.put((K) key, cp);
                copied.add((K) key);
                return cp;
            }
        }
        return map.get(key);
    }

    @Override
    public V put(K key, V value) {
        synchronized (this) {
            requireCloned();
            if (copied.contains(key)) {
                copied.remove(key);
            }
            return map.put(key, value);
        }
    }

    @Override
    public V remove(Object key) {
        synchronized (this) {
            requireCloned();
            return map.remove(key);
        }
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        synchronized (this) {
            requireCloned();
            for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
                if (copied.contains(entry.getKey())) {
                    copied.remove(entry.getKey());
                }
            }
            map.putAll(m);
        }
    }

    @Override
    public void clear() {
        synchronized (this) {
            requireCloned();
            copied.clear();
            map.clear();
        }
    }

    @Override
    public Set<K> keySet() {
        return map.keySet();
    }

    @Override
    public Collection<V> values() {
        Collection<V> coll = new ArrayList<V>();
        for (K key : keySet()) {
            coll.add(get(key));
        }
        return coll;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K,V>> output = new HashSet<Entry<K,V>>();
        for (final K key : keySet()) {
            output.add(new Entry<K,V>() {
                @Override
                public K getKey() {
                    return key;
                }

                @Override
                public V getValue() {
                    return get(key);
                }

                @Override
                public V setValue(V value) {
                    return put(key, value);
                }
            });
        }
        return output;
    }
}
