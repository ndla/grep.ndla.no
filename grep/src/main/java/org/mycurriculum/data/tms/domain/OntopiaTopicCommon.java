package org.mycurriculum.data.tms.domain;

import org.mycurriculum.data.tms.abstracts.OntopiaTopic;

public interface OntopiaTopicCommon extends OntopiaTopic {
    public void setHumanId(String humanId);
    public String getHumanId();
    public interface AnonymousBuilder extends Builder<OntopiaTopicCommon,AnonymousBuilder> {
    }
    public interface Builder<T extends OntopiaTopicCommon, K extends Builder> extends OntopiaTopic.Builder<T, K> {
        public K humanId(String humanId);
    }
}
