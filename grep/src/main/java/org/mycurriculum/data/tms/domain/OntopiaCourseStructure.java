package org.mycurriculum.data.tms.domain;

import java.io.IOException;
import java.util.Collection;

public interface OntopiaCourseStructure extends OntopiaTopicCommon {
    public void addCourseStructure(OntopiaCourseStructure courseStructure);
    public void removeCourseStructure(OntopiaCourseStructure courseStructure);
    public Collection<OntopiaCourseStructure> getCourseStructures();

    public void addCourseStructureParent(OntopiaCourseStructure courseStructure);
    public void removeCourseStructureParent(OntopiaCourseStructure courseStructure);
    public Collection<OntopiaCourseStructure> getCourseStructureParents();

    public void addCurriculumSet(OntopiaCurriculumSet curriculumSet);
    public void removeCurriculumSet(OntopiaCurriculumSet curriculumSet);
    public Collection<OntopiaCurriculumSet> getCurriculumSets();

    public void addLevel(OntopiaLevel level);
    public void removeLevel(OntopiaLevel level);
    public Collection<OntopiaLevel> getLevels() throws IOException;

    public void setSetType(String setType);
    public String getSetType();

    public interface Builder extends OntopiaTopicCommon.Builder<OntopiaCourseStructure,Builder> {
        public Builder addCourseStructure(OntopiaCourseStructure courseStructure);
        public Builder addCourseStructureParent(OntopiaCourseStructure courseStructure);
        public Builder addLevel(OntopiaLevel level);
    }
}
