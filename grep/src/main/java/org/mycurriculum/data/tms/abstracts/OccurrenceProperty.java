package org.mycurriculum.data.tms.abstracts;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OccurrenceProperty {
    public String type();
    public boolean addedFieldTypeCompatibility() default false;
}
