package org.mycurriculum.data.tms.connections.config;

import org.mycurriculum.data.tms.connections.config.helpers.ConfigFileGeneratorSource;

import java.util.Map;

public interface TmSourcesSetup extends ConfigFileGeneratorSource {
    public void setPropertyFile(String propertyFile);

    public String getPropertyFile();

    public void setId(String id);

    public String getId();

    public void setTitle(String title);

    public String getTitle();

    public void setSupportsCreate(boolean supportsCreate);

    public boolean getSupportsCreate();

    public void setSupportsDelete(boolean supportsDelete);

    public boolean getSupportsDelete();

    public void setClassName(String className);

    public String getClassName();

    Map<String, String> getParams();

    public TmSourcesSetup clone();
}
