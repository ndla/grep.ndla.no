package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.TopicIF;
import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class OntopiaCurriculumImpl extends OntopiaTopicCommonImpl implements OntopiaCurriculum {
    private static OntopiaTopicType type = null;

    private boolean aimSetsChanged = false;
    private LazyLoaderCollection<OntopiaAimSet> aimSets;
    private boolean levelsChanged = false;
    private LazyLoaderCollection<OntopiaLevel> levels;
    private boolean curriculumTypeChanged = false;
    private LazyLoaderCollection<OntopiaCurriculumSet> curriculumSets; /* Read only! */
    private Collection<String> curriculumTypePsis; /* This may be a collecion of psis for the type object */

    public OntopiaCurriculumImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }

    public OntopiaCurriculumImpl(TopicMap topicMap, OntopiaTopicType type) throws IOException {
        super(topicMap, type);
    }

    public OntopiaCurriculumImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    public static OntopiaTopicType getOntopiaType()
    {
        synchronized (OntopiaCourseStructureImpl.class) {
            if (type == null)
                type = createOntopiaType(OntopiaPsis.curriculumTypePsi);
            return type;
        }
    }

    public static URL generatePsi(String humanId) {
        try {
            return new URL(OntopiaPsis.getRootPsi()+"curriculum#"+humanId);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unexpetected url exception", e);
        }
    }

    public void readInAimSets() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaPsis.curriculumHasCompetenceAimSetsPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String id : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), id);
            refs.add(ref);
        }
        aimSets = new LazyLoaderCollectionBuilder<OntopiaAimSet,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaAimSet load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimSetImpl(ref);
            }
        }.build();
        aimSetsChanged = false;
    }
    public void saveAimSets() throws IOException {
        if (aimSetsChanged) {
            aimSetsChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(aimSets.size());
            for (OntopiaAimSet aimSet : aimSets) {
                TopicIF topic = aimSet.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaPsis.curriculumHasCompetenceAimSetsPsi, children);
        }
    }

    public void readInLevels() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaPsis.aimSetHasLevelPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String child : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), child);
            refs.add(ref);
        }
        levels = new LazyLoaderCollectionBuilder<OntopiaLevel,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaLevel load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaLevelImpl(ref);
            }
        }.build();
        levelsChanged = false;
    }
    public void saveLevels() throws IOException {
        if (levelsChanged) {
            levelsChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(levels.size());
            for (OntopiaLevel level : levels) {
                TopicIF topic = level.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaPsis.aimSetHasLevelPsi, children);
        }
    }

    private void readInType() throws IOException {
        String type = getOntopiaType().getTopic(getRef().getTopicMap()).getObjectId();
        Collection<String> types = getTypes();
        if (types.contains(type)) {
            types.remove(type);
        }
        if (types.size() == 1) {
            TopicIF topic = (TopicIF) getRef().getTopicMap().getObjectById(types.iterator().next());
            curriculumTypePsis = new ArrayList<String>();
            for (LocatorIF locator : topic.getSubjectIdentifiers()) {
                curriculumTypePsis.add(locator.getAddress());
            }
        } else {
            curriculumTypePsis = null;
        }
        curriculumTypeChanged = false;
    }
    private void saveType() throws IOException {
        if (curriculumTypeChanged) {
            String type = getOntopiaType().getTopic(getRef().getTopicMap()).getObjectId();
            Collection<String> types = getTypes();
            if (types.contains(type)) {
                types.remove(type);
            }
            TopicIF newType = getRef().getTopicMap().getTopicBySubjectIdentifier(new URILocator(this.curriculumTypePsis.iterator().next()));
            if (types.size() == 1) {
                TopicIF topic = (TopicIF) getRef().getTopicMap().getObjectById(types.iterator().next());
                Collection<String> curriculumTypePsis = new ArrayList<String>();
                for (LocatorIF locator : topic.getSubjectIdentifiers()) {
                    curriculumTypePsis.add(locator.getAddress());
                }
                if (this.curriculumTypePsis != null && this.curriculumTypePsis.size() > 0) {
                    if (!curriculumTypePsis.contains(this.curriculumTypePsis.iterator().next())) {
                        /* Replacement of type needed */
                        removeType(topic.getObjectId());
                        addType(newType.getObjectId());
                        curriculumTypePsis = new ArrayList<String>();
                        for (LocatorIF locator : newType.getSubjectIdentifiers()) {
                            curriculumTypePsis.add(locator.getAddress());
                        }
                    }
                    this.curriculumTypePsis = curriculumTypePsis;
                } else {
                    removeType(topic.getObjectId());
                }
            } else if (types.size() == 0) {
                addType(newType.getObjectId());
                curriculumTypePsis = new ArrayList<String>();
                for (LocatorIF locator : newType.getSubjectIdentifiers()) {
                    curriculumTypePsis.add(locator.getAddress());
                }
            } else {
                curriculumTypePsis = null;
            }
            curriculumTypeChanged = false;
        }
    }

    public void readInCurriculumSets() throws IOException {
        Collection<String> parents = getAssociatedParents(OntopiaPsis.curriculumSetHasCurriculumPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>();
        for (String parent : parents) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), parent);
            refs.add(ref);
        }
        curriculumSets = new LazyLoaderCollectionBuilder<OntopiaCurriculumSet,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaCurriculumSet load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCurriculumSetImpl(ref);
            }
        }.build();
    }

    @Override
    public void readInValues() throws IOException {
        super.readInValues();
        readInAimSets();
        readInLevels();
        readInType();
        readInCurriculumSets();
    }

    @Override
    public void saveValues() throws IOException {
        super.saveValues();
        saveAimSets();
        saveLevels();
        saveType();
    }

    @Override
    public Collection<OntopiaAimSet> getAimSets() {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaAimSet>) aimSets.clone();
        }
    }

    @Override
    public void addAimSet(OntopiaAimSet aimSet) {
        synchronized (this) {
            aimSets.add(aimSet);
            aimSetsChanged = true;
        }
    }

    @Override
    public void removeAimSet(OntopiaAimSet aimSet) {
        synchronized (this) {
            aimSets.remove(aimSet);
            aimSetsChanged = true;
        }
    }

    @Override
    public Collection<OntopiaLevel> getLevels() {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaLevel>) levels.clone();
        }
    }

    @Override
    public void addLevel(OntopiaLevel level) {
        synchronized (this) {
            levels.add(level);
            levelsChanged = true;
        }
    }

    @Override
    public void removeLevel(OntopiaLevel level) {
        synchronized (this) {
            levels.remove(level);
            levelsChanged = true;
        }
    }

    @Override
    public Collection<OntopiaCurriculumSet> getCurriculumSets() {
        synchronized (this) {
            return (Collection<OntopiaCurriculumSet>) curriculumSets.clone();
        }
    }

    @Override
    public Collection<String> getType() {
        synchronized (this) {
            if (curriculumTypePsis == null)
                return null;
            Collection<String> retv = new ArrayList<String>(curriculumTypePsis.size());
            retv.addAll(curriculumTypePsis);
            return retv;
        }
    }

    @Override
    public void setType(String type) throws IOException {
        synchronized (this) {
            if (type != null) {
                curriculumTypePsis = new ArrayList<String>();
                curriculumTypePsis.add(type);
            } else
                curriculumTypePsis = null;
            curriculumTypeChanged = true;
        }
    }

    public static class Builder extends OntopiaTopicCommonImpl.Builder<OntopiaCurriculumImpl,Builder> implements OntopiaCurriculum.Builder {
        private Set<OntopiaAimSet> aimSets = new HashSet<OntopiaAimSet>();
        private Set<OntopiaLevel> levels = new HashSet<OntopiaLevel>();
        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaType());
        }

        @Override
        public OntopiaCurriculumImpl createInstance() throws IOException {
            return new OntopiaCurriculumImpl(getRef());
        }

        @Override
        public void builder(OntopiaCurriculumImpl object) {
            super.builder(object);
            for (OntopiaAimSet aimSet : aimSets) {
                object.addAimSet(aimSet);
            }
            for (OntopiaLevel level : levels) {
                object.addLevel(level);
            }
        }

        @Override
        public Builder aimSet(OntopiaAimSet aimSet) {
            aimSets.add(aimSet);
            return this;
        }

        @Override
        public OntopiaCurriculum.Builder level(OntopiaLevel level) {
            levels.add(level);
            return this;
        }
    }
}
