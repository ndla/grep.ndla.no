package org.mycurriculum.data.tms;

import org.mycurriculum.data.tms.connections.TopicMapStore;
import org.mycurriculum.data.tms.connections.TopicMapStoreLocator;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.ontopia.OntopiaConnectionFactory;
import org.mycurriculum.data.tms.connections.topicmapstore.TopicMapStoreCreator;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class TopicMapCreatorImpl implements TopicMapCreator {
    private URL ltmFile;
    private TopicMapStoreCreator storeCreator;
    private OntopiaConnectionFactory connectionFactory;

    public TopicMapCreatorImpl(URL ltmFile, TopicMapStoreCreator storeCreator, OntopiaConnectionFactory connectionFactory) {
        this.ltmFile = ltmFile;
        this.storeCreator = storeCreator;
        this.connectionFactory = connectionFactory;
    }

    public Reader getLtmFileReader() throws IOException {
        if (ltmFile == null)
            return null;
        Reader reader = new InputStreamReader(ltmFile.openConnection().getInputStream(), "UTF-8");
        return reader;
    }

    public org.mycurriculum.data.tms.connections.TopicMapLocator createTopicMap(TopicMapStore store, String name) throws IOException {
        org.mycurriculum.data.tms.connections.TopicMapLocator locator = store.createTopicMap(name);
        locator.getTopicMapRoot().importLtm(getLtmFileReader());
        locator.getTopicMapRoot().getCache().clearCache();
        return locator;
    }

    public TopicMapLocator getTopicMap(TopicMapStore store, String name) {
        return new TopicMapLocator(store, name);
    }

    @Override
    public org.mycurriculum.data.tms.connections.TopicMapLocator createTopicMap(String storeName, String topicMapName) throws IOException, SQLException {
        storeCreator.createStore(storeName);
        org.mycurriculum.data.tms.connections.TopicMapLocator locator = null;
        TopicMapStoreLocator storeLocator = null;
        try {
            storeLocator = storeCreator.getStoreLocator(storeName);
            locator = createTopicMap(storeLocator.getTopicMapStore(), topicMapName);
        } finally {
            if (locator == null) {
                if (storeLocator != null) {
                    DBConnectionSetup dbConnectionSetup = storeLocator.getDbConnectionSetup();
                    if (dbConnectionSetup != null) {
                        connectionFactory.close(dbConnectionSetup.getFullConnectionString());
                    }
                }
                storeCreator.deleteStore(storeName);
            }
        }
        return locator;
    }

    private Map<String,String> decodeConnectionString(String connectionString) {
        String[] queryPairs = connectionString.split("&");
        Map<String,String> map = new HashMap<String,String>();
        for (String queryPair : queryPairs) {
            int pos = queryPair.indexOf('=');
            String name, value;
            if (pos >= 0) {
                name = queryPair.substring(0, pos);
                value = queryPair.substring(pos + 1);
            } else {
                name = queryPair;
                value = "";
            }
            try {
                name = URLDecoder.decode(name, "UTF-8");
                value = URLDecoder.decode(value, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Java install does not support UTF-8", e);
            }
            map.put(name, value);
        }
        return map;
    }

    @Override
    public org.mycurriculum.data.tms.connections.TopicMapLocator getTopicMapFromConnectionString(String connectionString) throws IOException {
        Map<String,String> map = decodeConnectionString(connectionString);
        if (map.containsKey("db")) {
            if (map.containsKey("tm")) {
                String db = map.get("db");
                String tm = map.get("tm");
                TopicMapStoreLocator storeLocator = storeCreator.getStoreLocatorFromConnectionString(db);
                TopicMapStore store = storeLocator.getTopicMapStore();
                return store.getTopicMapLocator(tm);
            } else
                throw new IOException("The connection string has to contain a tm-parameter (topicmap id)");
        } else
            throw new IOException("The connection string has to contain a db-parameter");
    }

    @Override
    public void deleteStore(String storeName) throws IOException, SQLException {
        storeCreator.deleteStore(storeName);
    }

    @Override
    public void deleteStoreByConnectionString(String connectionString) throws IOException, SQLException {
        Map<String,String> map = decodeConnectionString(connectionString);
        if (map.containsKey("db"))
            storeCreator.deleteStoreByConnectionString(map.get("db"));
        else
            throw new IOException("The connection string has to contain a db-parameter");
    }

    public static class TopicMapLocator implements org.mycurriculum.data.tms.connections.TopicMapLocator {
        private TopicMapStoreLocator storeLocator;
        private TopicMapStore store;
        private String name;

        public TopicMapLocator(TopicMapStoreCreator storeCreator, String storeName, String name) throws IOException {
            this(storeCreator.getStoreLocator(storeName), name);
        }
        public TopicMapLocator(TopicMapStoreLocator storeLocator, String name) throws IOException {
            this(storeLocator.getTopicMapStore(), name);
            this.storeLocator = storeLocator;
        }
        public TopicMapLocator(TopicMapStore store, String name)
        {
            this.store = store;
            this.name = name;
        }

        @Override
        public TopicMap getTopicMap() throws IOException {
            return getTopicMapRoot();
        }

        @Override
        public TopicMapRoot getTopicMapRoot() throws IOException {
            return store.getTopicMapByName(name).getTopicMapRoot();
        }

        @Override
        public DBConnectionSetup getDBConnectionSetup() {
            return null;
        }

        @Override
        public String getConnectionString() throws IOException {
            try {
                return "db="+ URLEncoder.encode(storeLocator.getConnectionString(), "UTF-8")+"&tm="+URLEncoder.encode(getTopicMapId(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Unsupported UTF-8 encoding", e);
            }
        }

        @Override
        public String getTopicMapId() throws IOException {
            return store.getTopicMapByName(name).getTopicMapId();
        }
    }
}
