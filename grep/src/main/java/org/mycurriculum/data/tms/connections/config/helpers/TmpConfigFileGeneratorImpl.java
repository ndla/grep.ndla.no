package org.mycurriculum.data.tms.connections.config.helpers;

import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;

import java.io.File;
import java.io.IOException;

/**
 * Created by janespen on 4/25/14.
 */
public class TmpConfigFileGeneratorImpl implements TmpConfigFileGenerator {
    private DBConnectionSetup connectionSetup;
    private TmSourcesSetup tmSources;
    @Override
    public DBConnectionSetup getDBConnectionSetup() {
        return connectionSetup;
    }

    @Override
    public void setDBConnectionSetup(DBConnectionSetup dbconn) {
        this.connectionSetup = dbconn;
    }

    @Override
    public TmSourcesSetup getTmSourcesSetup() {
        return tmSources;
    }

    @Override
    public void setTmSourcesSetup(TmSourcesSetup tmSources) {
        this.tmSources = tmSources;
    }

    @Override
    public File getTmSourcesFile() throws IOException {
        ConfigFileGenerator genDb = new ConfigFileGeneratorImpl(getDBConnectionSetup());
        TmSourcesSetup tmSources = getTmSourcesSetup();
        ConfigFileGenerator genTmSources = new ConfigFileGeneratorImpl(tmSources);
        File dbFile = genDb.generateTempFile("db.gen", ".props");
        tmSources.setPropertyFile(dbFile.getAbsolutePath());
        return genTmSources.generateTempFile("tm-sources", ".xml");
    }
}
