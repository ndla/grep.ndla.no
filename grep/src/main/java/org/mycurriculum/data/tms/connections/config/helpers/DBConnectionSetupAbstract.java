package org.mycurriculum.data.tms.connections.config.helpers;

import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;

import java.util.Properties;

public abstract class DBConnectionSetupAbstract implements DBConnectionSetup, Cloneable {
    private String connectionString;

    @Override
    public abstract String getDriver();

    @Override
    public abstract String getDatabase();

    @Override
    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    @Override
    public String getConnectionString() {
        return connectionString;
    }

    @Override
    public String toString() {
        String data = "";
        data += "net.ontopia.topicmaps.impl.rdbms.Database=" + getDatabase() + "\n";
        data += "net.ontopia.topicmaps.impl.rdbms.ConnectionString=" + getConnectionString() + "\n";
        data += "net.ontopia.topicmaps.impl.rdbms.DriverClass=" + getDriver() + "\n";
        return data;
    }

    @Override
    public abstract Properties getJDBCProperties();

    @Override
    public DBConnectionSetupAbstract clone() {
        try {
            return (DBConnectionSetupAbstract) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to clone DBConnectionSetupAbstract object");
        }
    }

    @Override
    public String getFullConnectionString() {
        return this.getClass().getName()+":"+getConnectionString();
    }
}
