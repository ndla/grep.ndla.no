package org.mycurriculum.data.tms.connections.ontopia;

import org.mycurriculum.data.cache.CacheDao;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.tms.references.TopicMapRoot;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TopicMapLocatorImpl implements TopicMapLocator {
    private OntopiaConnectionFactory connectionFactory;
    private DBConnectionSetup dbConnectionSetup;
    private TmSourcesSetup tmSourcesSetup;
    private CacheDao cacheDao;
    private String topicMapId;

    public TopicMapLocatorImpl(OntopiaConnectionFactory connectionFactory, CacheDao cacheDao) {
        this.connectionFactory = connectionFactory;
        this.cacheDao = cacheDao;
    }

    @Override
    public TopicMap getTopicMap() throws IOException {
        return getTopicMapRoot();
    }

    @Override
    public TopicMapRoot getTopicMapRoot() throws IOException {
        OntopiaConnection connection = connectionFactory.getConnection(tmSourcesSetup, dbConnectionSetup);
        return connection.getTopicMapByKey(topicMapId);
    }

    @Override
    public DBConnectionSetup getDBConnectionSetup() {
        return dbConnectionSetup;
    }

    @Override
    public String getConnectionString() {
        try {
            return "db="+ URLEncoder.encode(dbConnectionSetup.getFullConnectionString(), "UTF-8")+"&tm="+URLEncoder.encode(topicMapId, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unsupported UTF-8 encoding", e);
        }
    }

    public DBConnectionSetup getDbConnectionSetup() {
        return dbConnectionSetup;
    }

    public void setDbConnectionSetup(DBConnectionSetup dbConnectionSetup) {
        this.dbConnectionSetup = dbConnectionSetup;
    }

    public TmSourcesSetup getTmSourcesSetup() {
        return tmSourcesSetup;
    }

    public void setTmSourcesSetup(TmSourcesSetup tmSourcesSetup) {
        this.tmSourcesSetup = tmSourcesSetup;
    }

    public String getTopicMapId() {
        return topicMapId;
    }

    public void setTopicMapId(String topicMapId) {
        this.topicMapId = topicMapId;
    }

    public CacheDao getCacheDao() {
        return cacheDao;
    }

    public void setCacheDao(CacheDao cacheDao) {
        this.cacheDao = cacheDao;
    }
}
