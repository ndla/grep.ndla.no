package org.mycurriculum.data.tms.abstracts;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class CopyOnWriteSet<T> extends CopyOnWriteCollection<T> implements Set<T> {
    public CopyOnWriteSet(Set<T> input) {
        super(input);
    }

    protected void readyForWrite() {
        synchronized (this) {
            if (!writeable) {
                Collection<T> newCollection = new HashSet<T>(store.size());
                newCollection.addAll(store);
                store = newCollection;
                writeable = true;
            }
        }
    }
}
