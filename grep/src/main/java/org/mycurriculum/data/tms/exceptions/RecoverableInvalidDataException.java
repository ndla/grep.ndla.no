package org.mycurriculum.data.tms.exceptions;

/**
 * Used when the data is invalid but execution can continue because the
 * data has been converted or reduced to something valid.
 */
public class RecoverableInvalidDataException extends InvalidDataException {
    public RecoverableInvalidDataException(String message, Throwable cause) {
        super(message, cause);
    }
    public RecoverableInvalidDataException(String message) {
        super(message);
    }
    @Override
    public boolean isRecoverable() {
        return true;
    }
}
