package org.mycurriculum.data.tms.abstracts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class CopyOnWriteCollection<T> implements Collection<T>, Cloneable {
    protected Collection store;
    protected boolean writeable;

    public CopyOnWriteCollection(Collection<T> input) {
        store = input;
        writeable = false;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        synchronized (this) {
            /* Set both to COW */
            writeable = false;
            return super.clone();
        }
    }

    protected void readyForWrite() {
        synchronized (this) {
            if (!writeable) {
                Collection<T> newCollection = new ArrayList<T>(store.size());
                newCollection.addAll(store);
                store = newCollection;
                writeable = true;
            }
        }
    }

    @Override
    public int size() {
        return store.size();
    }

    @Override
    public boolean isEmpty() {
        return store.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return store.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        synchronized (this) {
            return new COWIterator<T>();
        }
    }

    @Override
    public Object[] toArray() {
        return store.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[]) store.toArray(a);
    }

    @Override
    public boolean add(T t) {
        synchronized (this) {
            readyForWrite();
            return store.add(t);
        }
    }

    @Override
    public boolean remove(Object o) {
        synchronized (this) {
            readyForWrite();
            return store.remove(o);
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return store.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        synchronized (this) {
            readyForWrite();
            return store.addAll(c);
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        synchronized (this) {
            readyForWrite();
            return store.removeAll(c);
        }
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        synchronized (this) {
            readyForWrite();
            return store.retainAll(c);
        }
    }

    @Override
    public void clear() {
        synchronized (this) {
            readyForWrite();
            store.clear();
        }
    }

    private class COWIterator<T> implements Iterator<T> {
        private Iterator<T> iterator;
        private T removeObj;

        public COWIterator() {
            writeable = false;
            iterator = store.iterator();
            removeObj = null;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public T next() {
            return (removeObj = iterator.next());
        }

        @Override
        public void remove() {
            if (removeObj == null)
                throw new ConcurrentModificationException("No item to remove (iter.remove)");
            synchronized (this) {
                readyForWrite();
                store.remove(removeObj);
                removeObj = null;
            }
        }
    }
}
