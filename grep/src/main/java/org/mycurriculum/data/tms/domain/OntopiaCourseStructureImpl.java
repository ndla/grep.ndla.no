package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.exceptions.OccurrenceTypeNotFoundException;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class OntopiaCourseStructureImpl extends OntopiaTopicCommonImpl implements OntopiaCourseStructure {
    private static OntopiaTopicType type = null;

    private boolean courseStructuresChanged = false;
    private LazyLoaderCollection<OntopiaCourseStructure> courseStructures;
    private boolean courseStructureParentsChanged = false;
    private LazyLoaderCollection<OntopiaCourseStructure> courseStructureParents;
    private boolean curriculumSetsChanged = false;
    private LazyLoaderCollection<OntopiaCurriculumSet> curriculumSets;
    private boolean levelsChanged = false;
    private LazyLoaderCollection<OntopiaLevel> levels;
    private boolean setTypeChanged = false;
    private String setType;

    public OntopiaCourseStructureImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }
    public OntopiaCourseStructureImpl(TopicMap topicMap) throws IOException {
        super(topicMap, getOntopiaType());
    }
    public OntopiaCourseStructureImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    public static OntopiaTopicType getOntopiaType()
    {
        synchronized (OntopiaCourseStructureImpl.class) {
            if (type == null)
                type = createOntopiaType(OntopiaPsis.courseStructureTypePsi);
            return type;
        }
    }

    public static URL generatePsi(String humanId) {
        try {
            return new URL(OntopiaPsis.getRootPsi()+"course-structure#"+humanId);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unexpetected url exception", e);
        }
    }

    private void readInCourseStructureHasCourseStructure() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaPsis.courseStructureHasCourseStructurePsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String child : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), child);
            refs.add(ref);
        }
        courseStructures = new LazyLoaderCollectionBuilder<OntopiaCourseStructure,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaCourseStructure load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCourseStructureImpl(ref);
            }
        }.build();
        courseStructuresChanged = false;

        Collection<String> parents = getAssociatedParents(OntopiaPsis.courseStructureHasCourseStructurePsi);
        Collection<OntopiaTopicRef> parentRefs = new ArrayList<OntopiaTopicRef>(parents.size());
        for (String parent : parents) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), parent);
            parentRefs.add(ref);
        }
        courseStructureParents = new LazyLoaderCollectionBuilder<OntopiaCourseStructure,OntopiaTopicRef>(parentRefs) {
            @Override
            public OntopiaCourseStructure load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCourseStructureImpl(ref);
            }
        }.build();
        courseStructureParentsChanged = false;
    }
    private void readInCourseStructureHasLevel() throws IOException {
        Collection<String> levels = getAssociatedChildren(OntopiaPsis.courseStructureHasLevelPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(levels.size());
        for (String level : levels) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), level);
            refs.add(ref);
        }
        this.levels = new LazyLoaderCollectionBuilder<OntopiaLevel,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaLevel load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaLevelImpl(ref);
            }
        }.build();
        levelsChanged = false;
    }
    private void saveCourseStructureHasCourseStructure() throws IOException {
        if (courseStructuresChanged) {
            courseStructuresChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(courseStructures.size());
            for (OntopiaCourseStructure courseStructure : courseStructures) {
                TopicIF topic = courseStructure.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaPsis.courseStructureHasCourseStructurePsi, children);
        }

        if (courseStructureParentsChanged) {
            courseStructureParentsChanged = false;
            Collection<TopicIF> parents = new ArrayList<TopicIF>(courseStructureParents.size());
            for (OntopiaCourseStructure courseStructure : courseStructureParents) {
                TopicIF topic = courseStructure.getTopic();
                parents.add(topic);
            }
            saveAssociatedParents(OntopiaPsis.courseStructureHasCourseStructurePsi, parents);
        }
    }
    private void saveCourseStructureHasLevel() throws IOException {
        if (levelsChanged) {
            levelsChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(levels.size());
            for (OntopiaLevel level : levels) {
                children.add(level.getTopic());
            }
            saveAssociatedChildren(OntopiaPsis.courseStructureHasLevelPsi, children);
        }
    }
    private void readInCurriculumSets() throws IOException {
        Collection<String> children = getAssociatedChildren(OntopiaPsis.courseStructureHasCurriculumSetPsi);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(children.size());
        for (String child : children) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), child);
            refs.add(ref);
        }
        curriculumSets = new LazyLoaderCollectionBuilder<OntopiaCurriculumSet,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaCurriculumSet load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaCurriculumSetImpl(ref);
            }
        }.build();
        curriculumSetsChanged = false;
    }
    private void saveCurriculumSets() throws IOException {
        if (curriculumSetsChanged) {
            curriculumSetsChanged = false;
            Collection<TopicIF> children = new ArrayList<TopicIF>(curriculumSets.size());
            for (OntopiaCurriculumSet curriculumSet : curriculumSets) {
                TopicIF topic = curriculumSet.getTopic();
                children.add(topic);
            }
            saveAssociatedChildren(OntopiaPsis.courseStructureHasCurriculumSetPsi, children);
            curriculumSetsChanged = false;
        }
    }
    private void readInOccurences() throws IOException {
        try {
            setType = loadOccurence(OntopiaPsis.setTypePsi);
        } catch (OccurrenceTypeNotFoundException e) {
            throw new IOException("Ontology error: Occurrence type not found: "+OntopiaPsis.setTypePsi);
        }
        setTypeChanged = false;
    }
    private void saveOccurences() throws IOException {
        if (setTypeChanged) {
            try {
                saveOccurence(OntopiaPsis.setTypePsi, setType);
                setTypeChanged = false;
            } catch (OccurrenceTypeNotFoundException e) {
                throw new IOException("Ontology error: Occurrence type not found: "+OntopiaPsis.setTypePsi);
            }
        }
    }

    @Override
    public void readInValues() throws IOException {
        readInCourseStructureHasCourseStructure();
        readInCourseStructureHasLevel();
        readInCurriculumSets();
        readInOccurences();
    }

    @Override
    public void saveValues() throws IOException {
        saveCourseStructureHasCourseStructure();
        saveCourseStructureHasLevel();
        saveCurriculumSets();
        saveOccurences();
    }

    @Override
    public void addCourseStructure(OntopiaCourseStructure courseStructure) {
        synchronized (this) {
            courseStructures.add(courseStructure);
            courseStructuresChanged = true;
        }
    }

    @Override
    public void removeCourseStructure(OntopiaCourseStructure courseStructure) {
        /* Removes based on equals method which we have defined for topic objects */
        synchronized (this) {
            courseStructures.remove(courseStructure);
            courseStructuresChanged = true;
        }
    }

    @Override
    public Collection<OntopiaCourseStructure> getCourseStructures() {
        return (LazyLoaderCollection<OntopiaCourseStructure>) courseStructures.clone();
    }

    @Override
    public void addCourseStructureParent(OntopiaCourseStructure courseStructure) {
        synchronized (this) {
            courseStructureParents.add(courseStructure);
            courseStructureParentsChanged = true;
        }
    }

    @Override
    public void removeCourseStructureParent(OntopiaCourseStructure courseStructure) {
        synchronized (this) {
            courseStructureParents.remove(courseStructure);
            courseStructureParentsChanged = true;
        }
    }

    @Override
    public Collection<OntopiaCourseStructure> getCourseStructureParents() {
        return (LazyLoaderCollection<OntopiaCourseStructure>) courseStructureParents.clone();
    }

    @Override
    public void addCurriculumSet(OntopiaCurriculumSet curriculumSet) {
        synchronized (this) {
            curriculumSets.add(curriculumSet);
            curriculumSetsChanged = true;
        }
    }

    @Override
    public void removeCurriculumSet(OntopiaCurriculumSet curriculumSet) {
        synchronized (this) {
            curriculumSets.remove(curriculumSet);
            curriculumSetsChanged = true;
        }
    }

    @Override
    public LazyLoaderCollection<OntopiaCurriculumSet> getCurriculumSets() {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaCurriculumSet>) curriculumSets.clone();
        }
    }

    @Override
    public void addLevel(OntopiaLevel level) {
        synchronized (this) {
            levelsChanged = true;
            levels.add(level);
        }
    }

    @Override
    public void removeLevel(OntopiaLevel level) {
        synchronized (this) {
            levelsChanged = true;
            levels.remove(level);
        }
    }

    @Override
    public Collection<OntopiaLevel> getLevels() throws IOException {
        synchronized (this) {
            return (LazyLoaderCollection<OntopiaLevel>) levels.clone();
        }
    }

    @Override
    public String getSetType() {
        synchronized (this) {
            return setType;
        }
    }

    @Override
    public void setSetType(String setType) {
        synchronized (this) {
            this.setTypeChanged = true;
            this.setType = setType;
        }
    }

    public static class Builder extends OntopiaTopicCommonImpl.Builder<OntopiaCourseStructureImpl,Builder> implements OntopiaCourseStructure.Builder {
        private Collection<OntopiaCourseStructure> courseStructures = new ArrayList<OntopiaCourseStructure>();
        private Collection<OntopiaCourseStructure> courseStructureParents = new ArrayList<OntopiaCourseStructure>();
        private Collection<OntopiaLevel> levels = new ArrayList<OntopiaLevel>();

        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaType());
        }

        @Override
        public OntopiaCourseStructureImpl createInstance() throws IOException {
            return new OntopiaCourseStructureImpl(getRef());
        }

        @Override
        public void builder(OntopiaCourseStructureImpl object) {
            super.builder(object);
            for (OntopiaCourseStructure courseStructure : courseStructures) {
                object.addCourseStructure(courseStructure);
            }
            for (OntopiaCourseStructure courseStructure : courseStructureParents) {
                object.addCourseStructureParent(courseStructure);
            }
            for (OntopiaLevel level : levels) {
                object.addLevel(level);
            }
        }

        @Override
        public Builder addCourseStructure(OntopiaCourseStructure courseStructure) {
            courseStructures.add(courseStructure);
            return this;
        }

        @Override
        public Builder addCourseStructureParent(OntopiaCourseStructure courseStructure) {
            courseStructureParents.add(courseStructure);
            return this;
        }

        @Override
        public OntopiaCourseStructure.Builder addLevel(OntopiaLevel level) {
            levels.add(level);
            return this;
        }
    }
}
