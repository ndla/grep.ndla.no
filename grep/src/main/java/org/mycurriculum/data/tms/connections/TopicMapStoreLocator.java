package org.mycurriculum.data.tms.connections;

import org.mycurriculum.data.tms.connections.config.DBConnectionSetup;
import org.mycurriculum.data.tms.connections.config.TmSourcesSetup;

import java.io.IOException;

public interface TopicMapStoreLocator {
    public DBConnectionSetup getDbConnectionSetup();
    public TmSourcesSetup getTmSourcesSetup();

    public TopicMapStore getTopicMapStore() throws IOException;

    public String getConnectionString();
}
