package org.mycurriculum.data.tms.domain;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import org.mycurriculum.data.tms.abstracts.*;
import org.mycurriculum.data.tms.references.TopicMap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class OntopiaAimImpl extends OntopiaTopicCommonImpl implements OntopiaAim {
    private static OntopiaTopicType type = null;
    public final static String aimSetHasAim = "http://psi.mycurriculum.org/competenceaimset_har_competenceaim";

    private boolean aimSetsChanged = false;
    private LazyLoaderCollection<OntopiaAimSet> aimSets;

    @OccurrenceProperty(type = OntopiaPsis.sortingOrderPsi)
    private Integer sortingOrder;

    public OntopiaAimImpl(OntopiaTopicRef topicIfRef) throws IOException {
        super(topicIfRef);
    }

    public OntopiaAimImpl(TopicMap topicMap) throws IOException {
        super(topicMap, getOntopiaType());
    }

    public OntopiaAimImpl(TopicMap topicMap, LocatorIF subjectIdentifier) throws IOException, NotFoundException {
        super(topicMap, subjectIdentifier);
    }

    public static OntopiaTopicType getOntopiaType() {
        synchronized (OntopiaAimImpl.class) {
            if (type == null)
                type = createOntopiaType(OntopiaPsis.aimTypePsi);
            return type;
        }
    }

    public static URL generatePsi(String humanId) {
        try {
            return new URL(OntopiaPsis.getRootPsi()+"competence-aim#"+humanId);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unexpetected url exception", e);
        }
    }

    @Override
    public void addToAimSet(OntopiaAimSet aimSet) {
        synchronized (this) {
            aimSets.add(aimSet);
            aimSetsChanged = true;
        }
    }

    @Override
    public void removeFromAimSet(OntopiaAimSet aimSet) {
        synchronized (this) {
            aimSets.remove(aimSet);
            aimSetsChanged = false;
        }
    }

    @Override
    public Collection<OntopiaAimSet> getAimSets() {
        synchronized (this) {
            return (Collection<OntopiaAimSet>) aimSets.clone();
        }
    }

    @Override
    public Collection<OntopiaCurriculum> getCurricula() {
        HashSet<OntopiaCurriculum> curricula = new HashSet<OntopiaCurriculum>();
        Collection<OntopiaAimSet> aimSets = getAimSets();
        for (OntopiaAimSet aimSet : aimSets) {
            for (OntopiaCurriculum curriculum : aimSet.getAllContainingCurricula()) {
                if (!curricula.contains(curriculum))
                    curricula.add(curriculum);
            }
        }
        return curricula;
    }

    @Override
    public Collection<OntopiaResourceAimAssociation> getResourceAssociations() throws IOException {
        return OntopiaResourceAimAssociationImpl.getAssociations(this);
    }

    @Override
    public Collection<OntopiaResource> getResources() throws IOException {
        Collection<OntopiaTopicRef> resources = new ArrayList<OntopiaTopicRef>();
        Collection<OntopiaResourceAimAssociation> associations = getResourceAssociations();
        for (OntopiaResourceAimAssociation association : associations) {
            OntopiaTopicRef resource = association.getResourceRef();
            if (resource != null)
                resources.add(resource);
        }
        return new LazyLoaderCollectionBuilder<OntopiaResource,OntopiaTopicRef>(resources) {
            @Override
            public OntopiaResource load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaResourceImpl(ref);
            }
        }.build();
    }

    private void readAimSets() throws IOException {
        Collection<String> parents = getAssociatedParents(aimSetHasAim);
        Collection<OntopiaTopicRef> refs = new ArrayList<OntopiaTopicRef>(parents.size());
        for (String parent : parents) {
            OntopiaTopicRef ref = new OntopiaTopicFinderById(getRef().getTopicMapReference(), parent);
            refs.add(ref);
        }
        aimSets = new LazyLoaderCollectionBuilder<OntopiaAimSet,OntopiaTopicRef>(refs) {
            @Override
            public OntopiaAimSet load(OntopiaTopicRef ref) throws IOException {
                return new OntopiaAimSetImpl(ref);
            }
        }.build();
        aimSetsChanged = false;
    }
    private void saveAimSets() throws IOException {
        if (aimSetsChanged) {
            aimSetsChanged = false;
            Collection<TopicIF> parents = new ArrayList<TopicIF>(aimSets.size());
            for (OntopiaAimSet aimSet : aimSets) {
                TopicIF topic = aimSet.getTopic();
                parents.add(topic);
            }
            saveAssociatedParents(aimSetHasAim, parents);
        }
    }

    @Override
    public void readInValues() throws IOException {
        readAimSets();
    }

    @Override
    public void saveValues() throws IOException {
        saveAimSets();
    }

    @Override
    public Integer getSortingOrder() {
        return sortingOrder;
    }

    @Override
    public void setSortingOrder(Integer sortingOrder) {
        this.sortingOrder = sortingOrder;
    }

    public static class Builder extends OntopiaTopicCommonImpl.Builder<OntopiaAimImpl,Builder> implements OntopiaAim.Builder {
        private Set<OntopiaAimSet> aimSets = new HashSet<OntopiaAimSet>();
        private Integer sortingOrder = null;

        public Builder(TopicMap topicMap) {
            super(topicMap, getOntopiaType());
        }

        @Override
        public OntopiaAim.Builder aimSet(OntopiaAimSet aimSet) {
            aimSets.add(aimSet);
            return this;
        }

        @Override
        public OntopiaAim.Builder sortingOrder(Integer sortingOrder) {
            this.sortingOrder = sortingOrder;
            return this;
        }

        @Override
        public OntopiaAimImpl createInstance() throws IOException {
            return new OntopiaAimImpl(getRef());
        }

        @Override
        public void builder(OntopiaAimImpl object) {
            super.builder(object);
            for (OntopiaAimSet aimSet : aimSets) {
                object.addToAimSet(aimSet);
            }
            if (sortingOrder != null)
                object.setSortingOrder(sortingOrder);
        }
    }
}
