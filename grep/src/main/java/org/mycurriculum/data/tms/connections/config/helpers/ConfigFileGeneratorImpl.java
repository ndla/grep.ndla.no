package org.mycurriculum.data.tms.connections.config.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by janespen on 4/25/14.
 */
public class ConfigFileGeneratorImpl implements ConfigFileGenerator {
    private ConfigFileGeneratorSource source;

    public ConfigFileGeneratorImpl(ConfigFileGeneratorSource source) {
        this.source = source;
    }

    @Override
    public File generateTempFile(String prefix, String suffix) throws IOException {
        File temp = File.createTempFile(prefix, suffix, null);
        FileOutputStream output = new FileOutputStream(temp);
        output.write(source.toString().getBytes("utf-8"));
        return temp;
    }
}
