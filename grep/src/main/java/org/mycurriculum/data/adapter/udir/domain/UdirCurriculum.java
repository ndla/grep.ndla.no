package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;

public interface UdirCurriculum extends UdirTranslatedTitledObject {
    public String getCurriculumType() throws IOException;

    public boolean isPublished();

    public String getKode();

    public UdirAimSet[] getAimSets();
}
