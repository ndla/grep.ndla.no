package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;

public interface UdirAimRef extends UdirTitledObject {
    public UdirAim getAim() throws IOException;
    public int getSortingOrder();
}
