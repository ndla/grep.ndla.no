package org.mycurriculum.data.adapter.udir.domain;

public interface UdirAim extends UdirTranslatedTitledObject {
    public UdirCurriculumRef[] getCurriculumReferences();

    public String getKode();
}
