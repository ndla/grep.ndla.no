package org.mycurriculum.data.adapter.udir.domain;

public interface UdirTitledObject extends UdirObject {
    public String getTitle();
}
