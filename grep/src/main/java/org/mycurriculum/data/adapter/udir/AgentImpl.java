/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 28, 2014
 */

package org.mycurriculum.data.adapter.udir;

import org.mycurriculum.data.adapter.Agent;

public class AgentImpl implements Agent {

    private String uri;

    public AgentImpl(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String consume() {
        return null;
    }

    @Override
    public String parse() {
        return null;
    }
}
