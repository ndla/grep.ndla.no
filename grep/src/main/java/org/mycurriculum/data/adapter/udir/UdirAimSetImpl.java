package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTranslatedTitledObjectImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirAimSet;
import org.mycurriculum.data.adapter.udir.domain.UdirAimsByArea;
import org.mycurriculum.data.adapter.udir.domain.UdirLevelRef;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirAimSetImpl extends UdirTranslatedTitledObjectImpl implements UdirAimSet {
    @JsonProperty("etter-aarstrinn")
    private List<UdirLevelRefImpl> levelRefList;

    @JsonProperty("hovedomraader")
    private List<UdirAimsByAreaImpl> aimsByArea;

    @JsonProperty("rekkefoelge")
    private int sortingOrder;

    public List<UdirAimsByAreaImpl> getAimsByAreaList() {
        return aimsByArea;
    }

    @Override
    public UdirAimsByArea[] getAimsByArea() {
        List<UdirAimsByAreaImpl> list = getAimsByAreaList();
        UdirAimsByArea[] array = new UdirAimsByArea[list.size()];
        int i = 0;
        for (UdirAimsByAreaImpl area : list) {
            area.setRootReference(getRootReference());
            area.setWebFetcher(getWebFetcher());
            array[i] = area;
            i++;
        }
        return array;
    }

    public void setAimsByArea(List<UdirAimsByAreaImpl> aimsByArea) {
        this.aimsByArea = aimsByArea;
    }

    @Override
    public UdirLevelRef[] getLevelRefs() {
        UdirLevelRef[] refs = new UdirLevelRef[levelRefList.size()];
        int i = 0;
        for (UdirLevelRefImpl ref : levelRefList) {
            ref.setRootReference(getRootReference());
            refs[i] = ref;
            i++;
        }
        return refs;
    }

    public List<UdirLevelRefImpl> getLevelRefList() {
        return levelRefList;
    }

    public void setLevelRefList(List<UdirLevelRefImpl> levelRefList) {
        this.levelRefList = levelRefList;
    }

    @Override
    public int getSortingOrder() {
        return sortingOrder;
    }

    public void setSortingOrder(int sortingOrder) {
        this.sortingOrder = sortingOrder;
    }
}
