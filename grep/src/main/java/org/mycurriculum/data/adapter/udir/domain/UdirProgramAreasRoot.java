package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;
import java.util.List;

public interface UdirProgramAreasRoot {
    public List<UdirProgramAreaRef> getProgramAreaReferences() throws IOException;
}
