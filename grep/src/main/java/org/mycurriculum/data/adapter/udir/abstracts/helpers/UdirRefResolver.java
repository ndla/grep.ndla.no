package org.mycurriculum.data.adapter.udir.abstracts.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mycurriculum.data.adapter.WebFetcherInstance;

import java.io.IOException;
import java.net.URL;

public class UdirRefResolver<T> {
    private UdirRef ref;

    public UdirRefResolver(UdirRef ref) {
        this.ref = ref;
    }

    public T getObject(Class<T> cls) throws IOException {
        WebFetcherInstance downloader;
        ObjectMapper mapper = new ObjectMapper();
        T object;
        String data;

        downloader = ref.getWebFetcher().getFetcher(new URL(ref.getDataUrl()));
        data = downloader.getContents();
        object = mapper.readValue(data, cls);
        return object;
    }
}
