package org.mycurriculum.data.adapter.udir;

import org.mycurriculum.data.adapter.udir.abstracts.UdirRefAbstract;
import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgram;
import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgramRef;

import java.io.IOException;

public class UdirEducationalProgramRefImpl extends UdirRefAbstract<UdirEducationalProgramImpl> implements UdirEducationalProgramRef {
    @Override
    public UdirEducationalProgram getEducationalProgram() throws IOException {
        return getRefObject(UdirEducationalProgramImpl.class);
    }
}
