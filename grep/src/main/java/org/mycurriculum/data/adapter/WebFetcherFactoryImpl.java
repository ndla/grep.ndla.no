package org.mycurriculum.data.adapter;

import java.io.File;
import java.io.IOException;

public class WebFetcherFactoryImpl implements WebFetcherFactory {
    private RateLimiter rateLimiter = null;
    private ConcurrencyLimiter concurrencyLimiter = null;
    private File develCacheFile = null;

    public WebFetcherFactoryImpl() {
        String sysprop = "org.mycurriculum.webFetcherDevelCacheFilename";
        String filename = System.getProperty(sysprop);
        if (filename != null) {
            setDevelCacheFile(new File(filename));
        }
    }

    public RateLimiter getRateLimiter() {
        return rateLimiter;
    }

    public void setRateLimiter(RateLimiter rateLimiter) {
        this.rateLimiter = rateLimiter;
    }

    @Override
    public WebFetcher createWebFetcher() {
        WebFetcherInterceptor webFetcher = new WebFetcherInterceptor();
        webFetcher.setRateLimiter(getRateLimiter());
        webFetcher.setConcurrencyLimiter(getConcurrencyLimiter());
        if (develCacheFile != null)
            webFetcher.setDevelCacheFile(develCacheFile);
        return webFetcher;
    }

    public ConcurrencyLimiter getConcurrencyLimiter() {
        return concurrencyLimiter;
    }

    @Override
    public void setConcurrencyLimiter(ConcurrencyLimiter concurrencyLimiter) {
        this.concurrencyLimiter = concurrencyLimiter;
    }

    public File getDevelCacheFile() {
        return develCacheFile;
    }

    @Override
    public void setDevelCacheFile(File develCacheFile) {
        this.develCacheFile = develCacheFile;
    }

    private class WebFetcherInterceptor extends WebFetcherImpl {
        private File develCache = null;

        private void setDevelCacheFile(File develCache) {
            System.out.println("Reading devel cache from "+develCache.getAbsolutePath());
            this.develCache = develCache;
            if (develCache.isFile() && develCache.canRead()) {
                try {
                    injectCache(develCache);
                } catch (IOException e) {
                    throw new RuntimeException("Unable to read devel cache", e);
                }
            }
        }

        @Override
        public void close() {
            if (develCache != null && (!develCache.exists() || develCache.canWrite())) {
                System.out.println("Writing devel cache to "+develCache.getAbsolutePath());
                try {
                    extractCache(develCache);
                } catch (IOException e) {
                    throw new RuntimeException("Unable to write devel cache", e);
                } finally {
                    develCache = null;
                }
            }
            super.close();
        }
        @Override
        public void finalize() {
            close();
        }
    }
}
