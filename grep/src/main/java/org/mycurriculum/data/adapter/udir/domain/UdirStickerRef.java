package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;

public interface UdirStickerRef {
    public String getKode();
    public UdirSticker getSticker() throws IOException;
}
