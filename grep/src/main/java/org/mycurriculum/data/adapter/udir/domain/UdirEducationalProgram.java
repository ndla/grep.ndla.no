package org.mycurriculum.data.adapter.udir.domain;

public interface UdirEducationalProgram extends UdirTranslatedTitledObject {
    public String getKode();
}
