package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.WebFetcherInstance;
import org.mycurriculum.data.adapter.udir.domain.UdirProgramAreaRef;
import org.mycurriculum.data.adapter.udir.domain.UdirProgramAreasRoot;
import org.mycurriculum.data.adapter.udir.domain.UdirRootReference;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UdirProgramAreasRootImpl implements UdirProgramAreasRoot {
    private List<UdirProgramAreaRefImpl> programAreaRefs = null;
    private URL rootUrl;
    private WebFetcher webFetcher;
    private UdirRootReference udirRootReference;

    public UdirProgramAreasRootImpl(URL rootUrl) {
        this.rootUrl = rootUrl;
    }

    @Override
    public List<UdirProgramAreaRef> getProgramAreaReferences() throws IOException {
        ArrayList<UdirProgramAreaRef> arrayList;

        if (programAreaRefs == null) {
            WebFetcherInstance downloader = webFetcher.getFetcher(rootUrl);
            String data = downloader.getContents();
            ObjectMapper mapper = new ObjectMapper();
            programAreaRefs = mapper.readValue(data, new TypeReference<List<UdirProgramAreaRefImpl>>() {
            });
            for (UdirProgramAreaRefImpl programAreaRef : programAreaRefs) {
                programAreaRef.setWebFetcher(getWebFetcher());
                programAreaRef.setRootReference(getUdirRootReference());
            }
        }
        arrayList = new ArrayList<UdirProgramAreaRef>();
        arrayList.addAll(programAreaRefs);
        return arrayList;
    }

    public WebFetcher getWebFetcher() {
        return webFetcher;
    }

    public void setWebFetcher(WebFetcher webFetcher) {
        this.webFetcher = webFetcher;
    }

    public UdirRootReference getUdirRootReference() {
        return udirRootReference;
    }

    public void setUdirRootReference(UdirRootReference udirRootReference) {
        this.udirRootReference = udirRootReference;
    }
}
