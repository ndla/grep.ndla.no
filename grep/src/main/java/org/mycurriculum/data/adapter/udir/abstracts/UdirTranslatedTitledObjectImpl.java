package org.mycurriculum.data.adapter.udir.abstracts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.UdirTranslatedTextImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirTranslatedTitledObject;
import org.mycurriculum.data.util.TranslatedText;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirTranslatedTitledObjectImpl extends UdirObjectImpl implements UdirTranslatedTitledObject {
    @JsonProperty("tittel")
    private List<UdirTranslatedTextImpl> titleList;

    public List<UdirTranslatedTextImpl> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<UdirTranslatedTextImpl> titleList) {
        this.titleList = titleList;
    }

    public TranslatedText getTranslatedTitle() {
        return UdirTranslatedTextImpl.getList(titleList);
    }
}
