package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTranslatedTitledObjectImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirAim;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumRef;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class UdirAimImpl extends UdirTranslatedTitledObjectImpl implements UdirAim {
    @JsonProperty("laereplan-referanser")
    private List<UdirCurriculumRefImpl> curriculumRefList;

    @JsonProperty("kode")
    private String kode;

    public List<UdirCurriculumRefImpl> getCurriculumRefList() {
        return curriculumRefList;
    }

    public void setCurriculumRefList(List<UdirCurriculumRefImpl> curriculumRefList) {
        this.curriculumRefList = curriculumRefList;
    }

    public UdirCurriculumRef[] getCurriculumReferences() {
        List<UdirCurriculumRefImpl> list = getCurriculumRefList();
        UdirCurriculumRef[] refs = new UdirCurriculumRef[list.size()];
        int i = 0;
        for (UdirCurriculumRefImpl ref : list) {
            ref.setRootReference(getRootReference());
            ref.setWebFetcher(getWebFetcher());
            refs[i] = ref;
            i++;
        }
        return refs;
    }

    @Override
    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
}
