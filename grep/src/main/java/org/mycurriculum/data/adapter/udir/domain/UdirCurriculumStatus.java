package org.mycurriculum.data.adapter.udir.domain;

public interface UdirCurriculumStatus {
    public boolean isPublished();
}
