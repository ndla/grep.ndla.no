package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;

public interface UdirEducationalProgramRef extends UdirObject {
    public UdirEducationalProgram getEducationalProgram() throws IOException;
}
