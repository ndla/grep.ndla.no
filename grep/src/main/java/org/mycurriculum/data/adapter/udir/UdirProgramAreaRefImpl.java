package org.mycurriculum.data.adapter.udir;

import org.mycurriculum.data.adapter.udir.abstracts.UdirRefAbstract;
import org.mycurriculum.data.adapter.udir.domain.UdirProgramArea;
import org.mycurriculum.data.adapter.udir.domain.UdirProgramAreaRef;

import java.io.IOException;

public class UdirProgramAreaRefImpl extends UdirRefAbstract<UdirProgramAreaImpl> implements UdirProgramAreaRef {
    @Override
    public UdirProgramArea getProgramArea() throws IOException {
        UdirProgramAreaImpl obj = getRefObject(UdirProgramAreaImpl.class);
        obj.setWebFetcher(getWebFetcher());
        obj.setRootReference(getRootReference());
        return obj;
    }
}
