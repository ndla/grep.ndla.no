package org.mycurriculum.data.adapter.udir.domain;

public interface UdirAimSet extends UdirTranslatedTitledObject {
    public UdirAimsByArea[] getAimsByArea();
    public UdirLevelRef[] getLevelRefs();
    public int getSortingOrder();
}
