package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.WebFetcherInstance;
import org.mycurriculum.data.adapter.udir.domain.UdirLevel;
import org.mycurriculum.data.adapter.udir.domain.UdirLevelsRoot;
import org.mycurriculum.data.adapter.udir.domain.UdirRootReference;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UdirLevelsRootImpl implements UdirLevelsRoot {
    private List<UdirLevelImpl> levels = null;
    private WebFetcher webFetcher;
    private URL rootUrl;
    private UdirRootReference udirRootReference;

    public UdirLevelsRootImpl(URL rootUrl)
    {
        this.rootUrl = rootUrl;
    }

    @Override
    public List<UdirLevel> getUdirLevels() throws IOException {
        synchronized (this) {
            if (levels == null) {
                WebFetcherInstance downloader = webFetcher.getFetcher(rootUrl);
                String data = downloader.getContents();
                ObjectMapper mapper = new ObjectMapper();
                levels = mapper.readValue(data, new TypeReference<List<UdirLevelImpl>>() {
                });
            }
        }
        List<UdirLevel> levelsOutput = new ArrayList<UdirLevel>(levels.size());
        levelsOutput.addAll(levels);
        return levelsOutput;
    }

    public void setWebFetcher(WebFetcher webFetcher) {
        this.webFetcher = webFetcher;
    }
    public WebFetcher getWebFetcher() {
        return webFetcher;
    }

    public void setRootUrl(URL rootUrl) {
        this.rootUrl = rootUrl;
    }
    public URL getRootUrl()
    {
        return rootUrl;
    }

    public UdirRootReference getUdirRootReference() {
        return udirRootReference;
    }

    public void setUdirRootReference(UdirRootReference udirRootReference) {
        this.udirRootReference = udirRootReference;
    }
}
