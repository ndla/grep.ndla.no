package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTranslatedTitledObjectImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgram;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirEducationalProgramImpl  extends UdirTranslatedTitledObjectImpl implements UdirEducationalProgram {
    @JsonProperty("kode")
    private String kode;

    @Override
    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
}
