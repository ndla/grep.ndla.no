package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.domain.UdirTranslatedText;
import org.mycurriculum.data.util.MutableTranslatedTextImpl;
import org.mycurriculum.data.util.TranslatedText;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@XmlRootElement
public class UdirTranslatedTextImpl implements UdirTranslatedText {
    @JsonProperty("noekkel")
    private String langId;
    @JsonProperty("verdi")
    private String text;

    public String getLangId() {
        return langId;
    }

    public void setLangId(String langId) {
        this.langId = langId;
    }

    public String getText() {
        return text;
    }

    @Override
    public Locale getLocale() {
        return getLocaleFromPsi(langId);
    }

    public void setText(String text) {
        this.text = text;
    }

    public static TranslatedText getList(List<UdirTranslatedTextImpl> texts) {
        return new UdirTranslatedListImpl(texts);
    }

    public static Locale getLocaleFromPsi(String psi) {
        return MutableTranslatedTextImpl.getLocaleFromPsi(psi);
    }
    public static String getPsiFromLocale(Locale locale) {
        return MutableTranslatedTextImpl.getPsiFromLocale(locale);
    }

    private static class UdirTranslatedListImpl implements TranslatedText {
        private UdirTranslatedText[] texts;
        private UdirTranslatedText defaultText = null;

        public UdirTranslatedListImpl(UdirTranslatedText[] texts) {
            this.texts = texts;
            if (texts.length > 0) {
                defaultText = texts[0];
                for (int i = 0; i < texts.length; i++) {
                    if (texts[i].getLangId().compareTo("default") == 0)
                        defaultText = texts[i];
                }
            }
        }

        public UdirTranslatedListImpl(List<UdirTranslatedTextImpl> texts) {
            this(listToArray(texts));
        }

        private static UdirTranslatedText[] listToArray(List<UdirTranslatedTextImpl> texts) {
            UdirTranslatedText[] array;
            int i = 0;

            array = new UdirTranslatedText[texts.size()];
            for (UdirTranslatedTextImpl text : texts) {
                array[i] = text;
                i++;
            }
            return array;
        }

        @Override
        public String getDefaultText() {
            if (defaultText != null)
                return defaultText.getText();
            return null;
        }

        @Override
        public Locale getDefaultLocale() {
            return null;
        }

        @Override
        public String getDefaultLocalePsi() {
            return null;
        }

        @Override
        public String getText(Locale locale) {
            String url = getPsiFromLocale(locale);

            for (int i = 0; i < texts.length; i++) {
                if (texts[i].getLangId().compareTo(url) == 0)
                    return texts[i].getText();
            }
            return null;
        }

        @Override
        public String getText(String iso639lang) {
            return getText(new Locale(iso639lang));
        }

        @Override
        public Map<Locale, String> getTextMap() {
            Locale[] locales = getLanguages();
            Map<Locale,String> map = new HashMap<Locale,String>();
            for (Locale locale : locales) {
                map.put(locale, getText(locale));
            }
            return map;
        }

        @Override
        public Map<String, String> getPsiTextMap() {
            Map<Locale,String> localeMap = getTextMap();
            if (localeMap == null)
                return null;
            Map<String,String> map = new HashMap<String,String>();
            for (Map.Entry<Locale,String> entry : localeMap.entrySet()) {
                map.put(getPsiFromLocale(entry.getKey()), entry.getValue());
            }
            return map;
        }

        @Override
        public Locale[] getLanguages() {
            Locale[] availLanguages = new Locale[texts.length];
            int i = 0;
            for (UdirTranslatedText translated : texts) {
                Locale locale = translated.getLocale();
                if (locale == null)
                    continue;
                availLanguages[i] = locale;
                i++;
            }
            if (i < availLanguages.length) {
                Locale[] languages = new Locale[i];
                System.arraycopy(availLanguages, 0, languages, 0, i);
                return languages;
            }
            return availLanguages;
        }
    }
}
