package org.mycurriculum.data.adapter;

public interface ConcurrencyLimiter {
    public void setConcurrency(int concurrency);
    public int getConcurrency();
    public void setIdlePercentage(int idlePercentage);
    public int getIdlePercentage();
    public Handle acquire();

    public interface Handle {
        public void release();
    }
}
