package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.util.TranslatedText;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirCurriculumTypeData {
    @JsonProperty("noekkel")
    private String typePsi;

    @JsonProperty("beskrivelse")
    private List<UdirTranslatedTextImpl> descriptionList;

    public String getTypePsi() {
        return typePsi;
    }

    public void setTypePsi(String typePsi) {
        this.typePsi = typePsi;
    }

    public TranslatedText getDescription() {
        return UdirTranslatedTextImpl.getList(descriptionList);
    }

    public List<UdirTranslatedTextImpl> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<UdirTranslatedTextImpl> descriptionList) {
        this.descriptionList = descriptionList;
    }
}
