package org.mycurriculum.data.adapter.udir.domain;

public interface UdirAimsByArea extends UdirTranslatedTitledObject {
    public UdirAimRef[] getAimRefs();
    public String getKode();
    public Integer getSortingOrder();
}
