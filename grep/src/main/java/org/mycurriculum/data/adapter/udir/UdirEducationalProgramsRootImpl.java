package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.WebFetcherInstance;
import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgramRef;
import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgramsRoot;
import org.mycurriculum.data.adapter.udir.domain.UdirRootReference;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UdirEducationalProgramsRootImpl implements UdirEducationalProgramsRoot {
    private List<UdirEducationalProgramRefImpl> educationalProgramRefs;
    private URL rootUrl;
    private WebFetcher webFetcher;
    private UdirRootReference udirRootReference;

    public UdirEducationalProgramsRootImpl(URL rootUrl)
    {
        this.rootUrl = rootUrl;
    }

    @Override
    public List<UdirEducationalProgramRef> getEducationalProgramReferences() throws IOException {
        ArrayList<UdirEducationalProgramRef> arrayList;

        if (educationalProgramRefs == null) {
            WebFetcherInstance downloader = webFetcher.getFetcher(rootUrl);
            String data = downloader.getContents();
            ObjectMapper mapper = new ObjectMapper();
            educationalProgramRefs = mapper.readValue(data, new TypeReference<List<UdirEducationalProgramRefImpl>>() {
            });
            for (UdirEducationalProgramRefImpl educationalProgramRef : educationalProgramRefs) {
                educationalProgramRef.setWebFetcher(getWebFetcher());
                educationalProgramRef.setRootReference(getUdirRootReference());
            }
        }
        arrayList = new ArrayList<UdirEducationalProgramRef>();
        arrayList.addAll(educationalProgramRefs);
        return arrayList;
    }

    public WebFetcher getWebFetcher() {
        return webFetcher;
    }

    public void setWebFetcher(WebFetcher webFetcher) {
        this.webFetcher = webFetcher;
    }

    public UdirRootReference getUdirRootReference() {
        return udirRootReference;
    }

    public void setUdirRootReference(UdirRootReference udirRootReference) {
        this.udirRootReference = udirRootReference;
    }
}
