/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 28, 2014
 */

package org.mycurriculum.data.adapter;

public interface Agent {

    public String consume();

    public String parse();
}
