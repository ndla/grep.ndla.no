package org.mycurriculum.data.adapter;

import java.util.Date;

public class PerSecondRateLimiterImpl implements RateLimiter {
    private static class RateUsage {
        public Date when = new Date();
        public float weight;

        public RateUsage(float weight) {
            this.weight = weight;
        }

        public float getSecondsAgo() {
            Date now = new Date();
            long diff;

            diff = now.getTime() - when.getTime();
            return ((float) diff) / 1000.0f;
        }
    }

    private static class WaitingObject {
    }

    private class RatelimiterManager {
        private WaitingObject[] queue = new WaitingObject[0];

        public boolean hasQueue() {
            synchronized (this) {
                return (queue.length > 0);
            }
        }

        public void getInLine(WaitingObject obj) {
            synchronized (this) {
                WaitingObject[] newQueue = new WaitingObject[queue.length + 1];
                System.arraycopy(queue, 0, newQueue, 0, queue.length);
                newQueue[queue.length] = obj;
                queue = newQueue;
            }
        }

        public void getOutOfLine(WaitingObject obj) {
            synchronized (this) {
                if (queue.length == 0)
                    return; /* Btw. Not found */
                WaitingObject[] newQueue = new WaitingObject[queue.length - 1];
                for (int i = 0; i < queue.length; i++) {
                    if (queue[i] == obj) {
                        if (i < newQueue.length)
                            System.arraycopy(queue, i + 1, newQueue, i, newQueue.length - i);
                        queue = newQueue;
                        return;
                    }
                    if (i == newQueue.length)
                        return; /* Not found actually */
                    newQueue[i] = queue[i];
                }
            }
        }

        public void waitInLine(WaitingObject obj, int milliseconds) {
            do {
                try {
                    Thread.sleep(milliseconds);
                } catch (InterruptedException e) {
                }
            } while (obj != queue[0]);
        }
    }

    private float maxPerSecond = 1.0f;
    private float averagingSeconds = 5.0f;
    private float ratelimitLimit = maxPerSecond * averagingSeconds;
    private RateUsage[] usages = new RateUsage[0];
    private RatelimiterManager manager = new RatelimiterManager();

    private synchronized void addUsage(RateUsage usage) {
        RateUsage[] newUsages = new RateUsage[usages.length + 1];

        System.arraycopy(usages, 0, newUsages, 0, usages.length);
        newUsages[usages.length] = usage;
        usages = newUsages;
    }

    private synchronized void rotateUsages() {
        int i;

        for (i = 0; i < usages.length; i++) {
            if (usages[i].getSecondsAgo() < averagingSeconds)
                break;
        }
        if (i > 0) {
            RateUsage[] stillUsages = new RateUsage[usages.length - i];
            if (stillUsages.length > 0)
                System.arraycopy(usages, i, stillUsages, 0, stillUsages.length);
            usages = stillUsages;
        }
    }

    private synchronized float getTotalUsage() {
        float consumed = 0.0f;

        rotateUsages();
        for (int i = 0; i < usages.length; i++) {
            consumed += usages[i].weight;
        }
        return consumed;
    }

    public float getMaxPerSecond() {
        return maxPerSecond;
    }

    public void setMaxPerSecond(float maxPerSecond) {
        this.maxPerSecond = maxPerSecond;
        this.ratelimitLimit = this.maxPerSecond * this.averagingSeconds;
    }

    public float getAveragingSeconds() {
        return averagingSeconds;
    }

    public void setAveragingSeconds(float averagingSeconds) {
        this.averagingSeconds = averagingSeconds;
        this.ratelimitLimit = this.maxPerSecond * this.averagingSeconds;
    }

    public void rateLimitWait(float weight) {
        synchronized (this) {
            if (getTotalUsage() <= ratelimitLimit && !manager.hasQueue()) {
                addUsage(new RateUsage(weight));
                return;
            }
        }
        WaitingObject obj = new WaitingObject();

        manager.getInLine(obj);
        try {
            do {
                manager.waitInLine(obj, 500);
            } while (getTotalUsage() > ratelimitLimit);
        } finally {
            manager.getOutOfLine(obj);
        }
        addUsage(new RateUsage(weight));
    }

    public void rateLimitWait() {
        rateLimitWait(1.0f);
    }
}
