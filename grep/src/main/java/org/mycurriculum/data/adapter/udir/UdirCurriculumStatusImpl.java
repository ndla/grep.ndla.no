package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumStatus;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirCurriculumStatusImpl implements UdirCurriculumStatus {
    @JsonProperty("noekkel")
    private String key;

    @Override
    public boolean isPublished() {
        String key = getKey();
        if (key != null && key.equals("http://psi.udir.no/ontologi/status/publisert")) {
            return true;
        } else {
            return false;
        }
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
}
