package org.mycurriculum.data.adapter.udir.domain;

import java.util.List;

public interface UdirProgramArea extends UdirTranslatedTitledObject {
    public String getKode();
    public List<UdirEducationalProgramRef> getEducationalProgramRefs();
    public UdirLevelRef getLevelRef();
    public List<UdirCurriculumRef> getCurricula();
}
