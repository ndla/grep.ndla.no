package org.mycurriculum.data.adapter.udir.abstracts.helpers;

import org.mycurriculum.data.adapter.WebFetcher;

public interface UdirRef {
    public String getDataUrl();

    public WebFetcher getWebFetcher();
}
