package org.mycurriculum.data.adapter.udir.domain;

public interface UdirRootReference {
    public UdirRoot getUdirRoot();
}
