package org.mycurriculum.data.adapter.udir.domain;

import org.mycurriculum.data.util.TranslatedText;

public interface UdirTranslatedTitledObject extends UdirObject {
    public TranslatedText getTranslatedTitle();
}
