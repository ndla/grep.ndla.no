package org.mycurriculum.data.adapter;

import java.util.Date;

public class ConcurrencyLimiterImpl implements ConcurrencyLimiter {
    private Slot[] slots;
    private Queued[] queue = null;
    private boolean flushing = false;
    private int idlePercentage = 0;

    public ConcurrencyLimiterImpl()
    {
        setConcurrency(1);
    }

    private void flush()
    {
        synchronized (this) {
            while (flushing) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
            if (queue != null && queue.length > 0) {
                flushing = true;
                do {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                    }
                } while (queue != null && queue.length > 0);
                flushing = false;
                notifyFirst();
            }
        }
    }

    @Override
    public void setConcurrency(int concurrency) {
        synchronized (this) {
            flush();
            slots = new Slot[concurrency];
            for (int i = 0; i < slots.length; i++)
                slots[i] = new Slot();
        }
    }

    @Override
    public int getConcurrency() {
        synchronized (this) {
            return slots.length;
        }
    }

    @Override
    public void setIdlePercentage(int idlePercentage) {
        this.idlePercentage = idlePercentage;
    }

    @Override
    public int getIdlePercentage() {
        return idlePercentage;
    }

    @Override
    public ConcurrencyLimiter.Handle acquire() {
        synchronized (this) {
            ConcurrencyLimiter.Handle handle;
            if (queue == null || queue.length == 0) {
                handle = tryToAquire();
                if (handle != null)
                    return handle;
            }
        }
        return waitInQueue();
    }

    private ConcurrencyLimiter.Handle tryToAquire()
    {
        synchronized (this) {
            if (flushing)
                return null;
            for (int i = 0; i < slots.length; i++) {
                if (slots[i].acquire()) {
                    return new Handle(slots[i]);
                }
            }
            return null;
        }
    }
    private ConcurrencyLimiter.Handle waitInQueue()
    {
        ConcurrencyLimiter.Handle handle;
        Queued queued = new Queued();

        synchronized (this) {
            addToQueue(queued);
            queued.armWaker();
        }
        do {
            queued.waitFor();
            synchronized (this) {
                handle = tryToAquire();
                if (handle != null) {
                    removeFromQueue(queued);
                    notifyFirst();
                    return handle;
                }
                queued.armWaker();
            }
        } while (true);
    }

    private void addToQueue(Queued queued)
    {
        synchronized (this) {
            if (queue == null) {
                queue = new Queued[1];
                queue[0] = queued;
                return;
            }
            Queued[] newQueue = new Queued[queue.length + 1];
            System.arraycopy(queue, 0, newQueue, 0, queue.length);
            newQueue[queue.length] = queued;
            queue = newQueue;
        }
    }
    private void removeFromQueue(Queued queued)
    {
        synchronized (this) {
            if (queue == null || queue.length == 0)
                throw new RuntimeException("Trying to unqueue while the object is not queued");
            if (queue.length == 1) {
                if (queue[0] != queued)
                    throw new RuntimeException("Trying to unqueue while the object is not queued");
                queue = null;
                return;
            }
            Queued[] newQueue = new Queued[queue.length - 1];
            int i;
            for (i = 0; i < queue.length && queue[i] != queued; i++) {
                if (i >= newQueue.length)
                    throw new RuntimeException("Trying to unqueue while the object is not queued");
                newQueue[i] = queue[i];
            }
            if (i < newQueue.length) {
                System.arraycopy(queue, i + 1, newQueue, i, newQueue.length - i);
            }
            queue = newQueue;
        }
    }
    private void notifyFirst()
    {
        synchronized (this) {
            if (queue != null && queue.length > 0)
                queue[0].release();
        }
    }
    private void released()
    {
        synchronized (this) {
            if (flushing)
                notifyAll();
            notifyFirst();
        }
    }

    private class Queued {
        boolean armed = false;
        boolean abortSleep = false;

        public Queued()
        {
        }
        public void armWaker()
        {
            synchronized (this) {
                abortSleep = false;
                armed = true;
            }
        }
        public void waitFor()
        {
            synchronized (this) {
                if (!armed)
                    throw new RuntimeException("The queue wait point is not armed! Arm the waker first!");
                armed = false;
                if (abortSleep)
                    return;
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
        public void release()
        {
            synchronized (this) {
                if (armed)
                    abortSleep = true;
                notify();
            }
        }
    }
    private class Slot {
        private Date before;
        private boolean aquired = false;
        public boolean acquire()
        {
            synchronized (this) {
                if (!aquired) {
                    aquired = true;
                    before = new Date();
                    return true;
                }
                return false;
            }
        }
        public void release()
        {
            synchronized (this) {
                if (aquired) {
                    Date after = new Date();
                    long elapsed = after.getTime() - before.getTime();
                    long idling = (elapsed * idlePercentage) / 100;
                    if (idling > 0) {
                        new Idler(this, idling);
                        return;
                    }
                    aquired = false;
                }
            }
            released();
        }
        public void idlerRelease()
        {
            synchronized (this) {
                aquired = false;
            }
            released();
        }
    }
    public static class Idler extends Thread {
        private Slot slot;
        private long ms;

        public Idler(Slot slot, long ms)
        {
            this.slot = slot;
            this.ms = ms;
            setDaemon(true);
            start();
        }
        public void run()
        {
            try {
                Thread.sleep(ms);
            } catch (InterruptedException e) {
            }
            slot.idlerRelease();
        }
    }
    public static class Handle implements ConcurrencyLimiter.Handle {
        private Slot slot;

        public Handle(Slot slot)
        {
            this.slot = slot;
        }

        @Override
        public void release() {
            Slot releaseSlot;
            synchronized (this) {
                releaseSlot = slot;
                slot = null;
            }
            if (releaseSlot != null)
                releaseSlot.release();
        }
    }
}
