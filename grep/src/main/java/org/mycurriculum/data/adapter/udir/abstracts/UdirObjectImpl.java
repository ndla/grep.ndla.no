package org.mycurriculum.data.adapter.udir.abstracts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.udir.UdirStickerRefImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirObject;
import org.mycurriculum.data.adapter.udir.domain.UdirRoot;
import org.mycurriculum.data.adapter.udir.domain.UdirRootReference;
import org.mycurriculum.data.adapter.udir.domain.UdirStickerRef;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class UdirObjectImpl implements UdirObject {
    private UdirRootReference rootReference;

    private WebFetcher webFetcher;

    @JsonProperty("id")
    private String id;

    @JsonProperty("url-psi")
    private String urlPsi;

    @JsonProperty("merkelapper")
    private List<UdirStickerRefImpl> stickers;

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public UdirRootReference getRootReference() {
        return rootReference;
    }

    public void setRootReference(UdirRootReference rootReference) {
        this.rootReference = rootReference;
    }

    public UdirRoot getUdirRoot() {
        return rootReference.getUdirRoot();
    }

    @Override
    public String getUrlPsi() {
        return urlPsi;
    }

    public void setUrlPsi(String urlPsi) {
        this.urlPsi = urlPsi;
    }

    public List<? extends UdirStickerRef> getStickers() {
        if (this.stickers == null)
            return null;
        List<UdirStickerRef> stickers = new ArrayList<UdirStickerRef>(this.stickers.size());
        for (UdirStickerRefImpl stickerRef : this.stickers) {
            stickerRef.setWebFetcher(getWebFetcher());
            stickers.add(stickerRef);
        }
        return stickers;
    }
    public void setStickers(List<UdirStickerRefImpl> stickers) {
        this.stickers = stickers;
    }

    @Override
    public WebFetcher getWebFetcher() {
        return webFetcher;
    }

    public void setWebFetcher(WebFetcher webFetcher) {
        this.webFetcher = webFetcher;
    }
}
