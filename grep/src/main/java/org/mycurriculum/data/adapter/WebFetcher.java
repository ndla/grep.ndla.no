package org.mycurriculum.data.adapter;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public interface WebFetcher {
    /**
     * Returns the total number of actual web requests done by this fetcher.
     *
     * @return Number of actual web requests
     */
    public int getNumberOfWebRequests();

    /**
     * Get a fetcher instance object for fetching the contents.
     *
     * @param url
     * @return A fetcher instance.
     */
    public WebFetcherInstance getFetcher(URL url);

    /**
     * Set a rateLimiter for this WebFetcher.
     *
     * @param rateLimiter
     */
    public void setRateLimiter(RateLimiter rateLimiter);

    /**
     * Set a concurrency limiter for this WebFetcher.
     *
     * @param concurrencyLimiter
     */
    public void setConcurrencyLimiter(ConcurrencyLimiter concurrencyLimiter);

    /**
     * Get the complete in memory cache serialized as a string. This is supposed
     * to be used for debugging to avoid the delay of fetching the data over and
     * over again.
     *
     * @param outputToFile
     */
    public void extractCache(File outputToFile) throws IOException;

    /**
     * Inject an in memory cache unserialized from a string. This is supposed to
     * be used for debugging to avoid the delay of fetching the data over and over
     * again.
     *
     * @param inputFromFile
     */
    public void injectCache(File inputFromFile) throws IOException, ClassNotFoundException;

    /**
     * This is not supposed to be essential, but it might sometimes be
     * useful to know that the object is being disposed.
     */
    public void close();
}
