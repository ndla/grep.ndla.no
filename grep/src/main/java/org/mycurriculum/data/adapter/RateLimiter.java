package org.mycurriculum.data.adapter;

/**
 * Created by janespen on 4/15/14.
 */
public interface RateLimiter {
    public void rateLimitWait(float weight);

    public void rateLimitWait();
}
