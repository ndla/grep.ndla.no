package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.udir.domain.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirLevelRefImpl implements UdirLevelRef {
    private UdirRootReference rootReference;

    @JsonProperty("noekkel")
    private String urlPsi;

    @Override
    public UdirRootReference getRootReference() {
        return rootReference;
    }

    @Override
    public WebFetcher getWebFetcher() {
        return null;
    }

    @Override
    public String getId() {
        return getUrlPsi();
    }

    @Override
    public String getUrlPsi() {
        return urlPsi;
    }

    @Override
    public List<? extends UdirStickerRef> getStickers() {
        return null;
    }

    @Override
    public UdirRoot getUdirRoot() {
        return getRootReference().getUdirRoot();
    }

    public void setUrlPsi(String urlPsi) {
        this.urlPsi = urlPsi;
    }

    public void setRootReference(UdirRootReference rootReference) {
        this.rootReference = rootReference;
    }

    @Override
    public UdirLevel getLevel() throws IOException {
        UdirLevelsRoot root = getUdirRoot().getLevelsRoot();
        List<UdirLevel> levels = root.getUdirLevels();
        for (UdirLevel level : levels) {
            if (level.getUrlPsi().equals(urlPsi))
                return level;
        }
        throw new IOException("Bad UdirLevel reference : "+urlPsi);
    }
}
