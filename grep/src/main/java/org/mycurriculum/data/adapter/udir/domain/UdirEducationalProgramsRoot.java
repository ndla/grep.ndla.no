package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;
import java.util.List;

public interface UdirEducationalProgramsRoot {
    public List<UdirEducationalProgramRef> getEducationalProgramReferences() throws IOException;
}
