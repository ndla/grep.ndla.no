package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTranslatedTitledObjectImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirAimRef;
import org.mycurriculum.data.adapter.udir.domain.UdirAimsByArea;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirAimsByAreaImpl extends UdirTranslatedTitledObjectImpl implements UdirAimsByArea {
    @JsonProperty(value = "kompetansemaal", required = true)
    private List<UdirAimRefImpl> aimRefList;

    @JsonProperty(value = "kode")
    private String kode;

    public List<UdirAimRefImpl> getAimRefList() {
        return aimRefList;
    }

    @Override
    public UdirAimRef[] getAimRefs() {
        List<UdirAimRefImpl> list = getAimRefList();
        UdirAimRef[] array = new UdirAimRef[list.size()];
        int i = 0;
        for (UdirAimRefImpl ref : list) {
            ref.setRootReference(getRootReference());
            ref.setWebFetcher(getWebFetcher());
            array[i] = ref;
            i++;
        }
        return array;
    }

    @Override
    public String getKode() {
        return kode;
    }

    @Override
    public Integer getSortingOrder() {
        String kode = getKode();
        if (kode.length() <= 1)
            return null;
        if (kode.startsWith("H")) {
            kode = kode.substring(1);
            while (kode.length() > 1 && kode.startsWith("0")) {
                kode = kode.substring(1);
            }
            try {
                return Integer.decode(kode);
            } catch (NumberFormatException e) {
                return null;
            }
        } else
            return null;
    }

    public void setAimRefList(List<UdirAimRefImpl> aimRefList) {
        this.aimRefList = aimRefList;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
}
