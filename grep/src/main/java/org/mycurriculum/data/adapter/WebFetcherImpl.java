package org.mycurriculum.data.adapter;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class WebFetcherImpl implements WebFetcher {
    private Map<String, InstanceImpl> instances = new Hashtable<String, InstanceImpl>();
    private volatile int fetchOps = 0;
    private RateLimiter rateLimiter = null;
    private ConcurrencyLimiter concurrencyLimiter = null;

    public int getNumberOfWebRequests() {
        return fetchOps;
    }

    public void incFetchOps() {
        fetchOps++;
    }

    @Override
    public WebFetcherInstance getFetcher(URL url) {
        synchronized (this) {
            String urlStr = url.toString();
            InstanceImpl instance = instances.get(urlStr);

            if (instance == null) {
                instance = new InstanceImpl(url);
                instances.put(urlStr, instance);
            }
            return instance;
        }
    }

    @Override
    public void setRateLimiter(RateLimiter rateLimiter) {
        this.rateLimiter = rateLimiter;
    }

    public ConcurrencyLimiter getConcurrencyLimiter() {
        return concurrencyLimiter;
    }

    @Override
    public void setConcurrencyLimiter(ConcurrencyLimiter concurrencyLimiter) {
        this.concurrencyLimiter = concurrencyLimiter;
    }

    private HashMap<String,String> getCacheMap() {
        HashMap<String,String> map = new HashMap<String,String>();
        for (Map.Entry<String,InstanceImpl> entry : instances.entrySet()) {
            String key = entry.getKey();
            InstanceImpl instance = entry.getValue();
            String cached = instance.getCachedContents();
            if (cached != null) {
                map.put(key, cached);
            }
        }
        return map;
    }

    @Override
    public void extractCache(File outputToFile) throws IOException {
        HashMap<String,String> map = getCacheMap();
        FileOutputStream outputStream = new FileOutputStream(outputToFile);
        try {
            ObjectOutputStream output = new ObjectOutputStream(outputStream);
            output.writeObject(map);
        } finally {
            outputStream.close();
        }
    }

    @Override
    public void injectCache(File inputFromFile) throws IOException {
        FileInputStream inputStream = new FileInputStream(inputFromFile);
        try {
            ObjectInputStream input = new ObjectInputStream(inputStream);
            Map<String, String> map;
            try {
                map = (HashMap<String, String>) input.readObject();
            } catch (ClassNotFoundException e) {
                throw new IOException("Failed to read serialized data from file", e);
            }
            for (Map.Entry<String, String> entry : map.entrySet()) {
                try {
                    instances.put(entry.getKey(), new InstanceImpl(new URL(entry.getKey()), entry.getValue()));
                } catch (MalformedURLException e) {
                    /* ignore invalid data */
                }
            }
        } finally {
            inputStream.close();
        }
    }

    @Override
    public void close() {
    }

    private class InstanceImpl implements WebFetcherInstance {
        URL url;
        String content = null;
        boolean fetching = false;
        int fetchers = 0;
        IOException ioException = null;

        public InstanceImpl(URL url) {
            this.url = url;
        }
        public InstanceImpl(URL url, String content) {
            this.url = url;
            this.content = content;
        }

        @Override
        public void fetch() throws IOException {
            boolean goingToFetch = false;
            String str;

            synchronized (this) {
                try {
                    if (fetching) {
                        while (content == null) {
                            if (ioException != null) {
                                if (fetchers == 1) {
                                    /* Previous failure: Redo the fetch */
                                    fetching = false;
                                    ioException = null;
                                    fetch();
                                    return;
                                }
                                throw ioException;
                            }
                            try {
                                this.wait();
                            } catch (InterruptedException e) {
                                /* Just ignore interruptions */
                            }
                        }
                        return;
                    }
                    fetching = true;
                    goingToFetch = true;
                } finally {
                    if (!goingToFetch)
                        fetchers--;
                }
            }
            if (rateLimiter != null)
                rateLimiter.rateLimitWait();
            ConcurrencyLimiter.Handle concurrencyHandle = null;
            if (concurrencyLimiter != null)
                concurrencyHandle = concurrencyLimiter.acquire();
            incFetchOps(); /* statistics */
            try {
                URLConnection connection = url.openConnection();
                InputStreamReader reader = new InputStreamReader(connection.getInputStream());
                char[] chunk = new char[128];
                str = "";
                while (true) {
                    int length = reader.read(chunk);
                    if (length <= 0) /* blocking read is not supposed to return 0 */
                        break;
                    int bufferSize = chunk.length;
                    if (length < bufferSize) {
                        char[] blk = new char[length];
                        System.arraycopy(chunk, 0, blk, 0, length);
                        chunk = blk;
                    }
                    str += new String(chunk);
                    /* Double the buffer size if the chunk was completely filled for speeding up large downloads */
                    if (length == bufferSize)
                        chunk = new char[chunk.length << 1];
                }

                synchronized (this) {
                    content = str;
                    this.notifyAll();
                }
            } catch (IOException e) {
                synchronized (this) {
                    ioException = e;
                    this.notifyAll();
                }
            } finally {
                if (concurrencyHandle != null)
                    concurrencyHandle.release();
                synchronized (this) {
                    fetchers--;
                }
            }
        }

        @Override
        public String getContents() throws IOException {
            synchronized (this) {
                if (content != null)
                    return content;
                if (ioException != null)
                    throw ioException;
            }
            fetch();
            return getContents();
        }

        public String getCachedContents() {
            synchronized (this) {
                return content;
            }
        }
    }
}
