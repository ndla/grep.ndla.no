package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTitledRefAbstract;
import org.mycurriculum.data.adapter.udir.domain.UdirAim;
import org.mycurriculum.data.adapter.udir.domain.UdirAimRef;

import java.io.IOException;

public class UdirAimRefImpl extends UdirTitledRefAbstract<UdirAimImpl> implements UdirAimRef {
    @JsonProperty("rekkefoelge")
    private int sortingOrder;

    public UdirAim getAim() throws IOException {
        UdirAimImpl aim = getRefObject(UdirAimImpl.class);
        aim.setRootReference(getRootReference());
        aim.setWebFetcher(getWebFetcher());
        return aim;
    }

    @Override
    public int getSortingOrder() {
        return sortingOrder;
    }

    public void setSortingOrder(int sortingOrder) {
        this.sortingOrder = sortingOrder;
    }
}
