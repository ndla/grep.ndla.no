package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTranslatedTitledObjectImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumRef;
import org.mycurriculum.data.adapter.udir.domain.UdirEducationalProgramRef;
import org.mycurriculum.data.adapter.udir.domain.UdirProgramArea;
import org.mycurriculum.data.adapter.udir.domain.UdirStickerRef;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirProgramAreaImpl extends UdirTranslatedTitledObjectImpl implements UdirProgramArea {
    @JsonProperty("kode")
    private String kode;

    @JsonProperty("aarstrinn")
    private UdirLevelRefImpl levelRef;

    @JsonProperty("tilknyttede-utdanningsprogram")
    private List<UdirEducationalProgramRefImpl> educationalProgramRefs;

    @JsonProperty("tilknyttede-laereplaner")
    private List<UdirCurriculumRefImpl> curricula;

    @Override
    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    @Override
    public List<UdirEducationalProgramRef> getEducationalProgramRefs() {
        List<UdirEducationalProgramRef> list = new ArrayList<UdirEducationalProgramRef>(educationalProgramRefs.size());
        for (UdirEducationalProgramRefImpl ref : educationalProgramRefs) {
            ref.setWebFetcher(getWebFetcher());
            list.add(ref);
        }
        return list;
    }

    public void setEducationalProgramRefs(List<UdirEducationalProgramRefImpl> educationalProgramRefs) {
        this.educationalProgramRefs = educationalProgramRefs;
    }

    @Override
    public UdirLevelRefImpl getLevelRef() {
        if (levelRef != null)
            levelRef.setRootReference(getRootReference());
        return levelRef;
    }

    public void setLevelRef(UdirLevelRefImpl levelRef) {
        this.levelRef = levelRef;
    }

    @Override
    public List<UdirCurriculumRef> getCurricula() {
        List<UdirCurriculumRef> curricula = new ArrayList<UdirCurriculumRef>(this.curricula.size());
        for (UdirCurriculumRefImpl ref : this.curricula) {
            ref.setRootReference(getRootReference());
            ref.setWebFetcher(getWebFetcher());
            curricula.add(ref);
        }
        return curricula;
    }

    public void setCurricula(List<UdirCurriculumRefImpl> curricula) {
        this.curricula = curricula;
    }

    @Override
    public List<? extends UdirStickerRef> getStickers() {
        List<? extends UdirStickerRef> stickerRefs = super.getStickers();
        for (UdirStickerRef stickerRef : stickerRefs) {
        }
        return stickerRefs;
    }
}
