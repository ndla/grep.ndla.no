package org.mycurriculum.data.adapter.udir.abstracts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.domain.UdirTitledObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirTitledObjectImpl extends UdirObjectImpl implements UdirTitledObject {
    @JsonProperty("tittel")
    private String title;

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
