package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;
import java.util.List;

public interface UdirLevelsRoot {
    public List<UdirLevel> getUdirLevels() throws IOException;
}
