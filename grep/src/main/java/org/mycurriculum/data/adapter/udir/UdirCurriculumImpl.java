package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTranslatedTitledObjectImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirAimSet;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculum;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirCurriculumImpl extends UdirTranslatedTitledObjectImpl implements UdirCurriculum {
    @JsonProperty("fagtype")
    private UdirCurriculumTypeData typeData;

    @JsonProperty("kode")
    private String kode;

    @JsonProperty("kompetansemaalsett")
    private List<UdirAimSetImpl> aimSetList;

    @JsonProperty("status")
    private UdirCurriculumStatusImpl status;

    @Override
    public String getCurriculumType() throws IOException {
        if (typeData == null)
            throw new IOException("Type data not found for "+getUrlPsi());
        return typeData.getTypePsi();
    }

    @Override
    public boolean isPublished() {
        if (getStatus() == null) {
            return false;
        }
        return getStatus().isPublished();
    }

    @Override
    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public UdirAimSet[] getAimSets() {
        List<UdirAimSetImpl> list = getAimSetList();
        UdirAimSet[] array = new UdirAimSet[list.size()];
        int i = 0;
        for (UdirAimSetImpl aimSet : list) {
            aimSet.setRootReference(getRootReference());
            aimSet.setWebFetcher(getWebFetcher());
            array[i] = aimSet;
            i++;
        }
        return array;
    }

    public List<UdirAimSetImpl> getAimSetList() {
        return aimSetList;
    }

    public void setAimSetList(List<UdirAimSetImpl> aimSetList) {
        this.aimSetList = aimSetList;
    }

    public UdirCurriculumTypeData getTypeData() {
        return typeData;
    }

    public void setTypeData(UdirCurriculumTypeData typeData) {
        this.typeData = typeData;
    }

    public UdirCurriculumStatusImpl getStatus() {
        return status;
    }

    public void setStatus(UdirCurriculumStatusImpl status) {
        this.status = status;
    }
}
