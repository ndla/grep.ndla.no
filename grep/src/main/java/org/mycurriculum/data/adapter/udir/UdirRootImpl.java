package org.mycurriculum.data.adapter.udir;

import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.udir.domain.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;

import java.net.MalformedURLException;
import java.net.URL;

class UdirRootImpl implements UdirRoot, UdirRootReference, BeanFactoryAware, DisposableBean {
    private BeanFactory beanFactory;
    private WebFetcher webFetcher;
    private URL rootUrl;

    private UdirCurriculumRootImpl curriculumRoot = null;
    private UdirProgramAreasRootImpl programAreasRoot = null;
    private UdirEducationalProgramsRootImpl educationalProgramsRoot = null;
    private UdirLevelsRootImpl levelsRoot = null;

    public UdirRootImpl(URL rootUrl) {
        this.rootUrl = rootUrl;
    }

    private String getUrlRootPath() {
        String pathPre = rootUrl.getPath();
        if (pathPre.length() == 0 || pathPre.charAt(pathPre.length() - 1) != '/')
            pathPre += "/";
        return pathPre;
    }

    public URL getCurriculumRootUrl() {
        try {
            return new URL(rootUrl, getUrlRootPath() + "laereplaner.json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public URL getProgramAreasRootUrl() {
        try {
            return new URL(rootUrl, getUrlRootPath() + "programomraader.json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public URL getEducationalProgramsRootUrl() {
        try {
            return new URL(rootUrl, getUrlRootPath() + "utdanningsprogram.json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public URL getLevelsRootUrl() {
        try {
            return new URL(rootUrl, getUrlRootPath() + "aarstrinn.json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public UdirCurriculumRoot getCurriculumRoot() {
        if (curriculumRoot == null) {
            curriculumRoot = new UdirCurriculumRootImpl(getCurriculumRootUrl());
            curriculumRoot.setWebFetcher(getWebFetcher());
            curriculumRoot.setUdirRootReference(this);
        }
        return curriculumRoot;
    }

    @Override
    public UdirProgramAreasRoot getProgramAreasRoot() {
        if (programAreasRoot == null) {
            programAreasRoot = new UdirProgramAreasRootImpl(getProgramAreasRootUrl());
            programAreasRoot.setWebFetcher(getWebFetcher());
            programAreasRoot.setUdirRootReference(this);
        }
        return programAreasRoot;
    }

    @Override
    public UdirEducationalProgramsRoot getEducationalProgramsRoot() {
        if (educationalProgramsRoot == null) {
            educationalProgramsRoot = new UdirEducationalProgramsRootImpl(getEducationalProgramsRootUrl());
            educationalProgramsRoot.setWebFetcher(getWebFetcher());
            educationalProgramsRoot.setUdirRootReference(this);
        }
        return educationalProgramsRoot;
    }

    @Override
    public UdirLevelsRoot getLevelsRoot() {
        synchronized (this) {
            if (levelsRoot == null) {
                levelsRoot = new UdirLevelsRootImpl(getLevelsRootUrl());
                levelsRoot.setWebFetcher(getWebFetcher());
                levelsRoot.setUdirRootReference(this);
            }
        }
        return levelsRoot;
    }

    @Override
    public void close() {
        if (webFetcher != null) {
            webFetcher.close();
            webFetcher = null;
        }
        curriculumRoot = null;
        programAreasRoot = null;
        educationalProgramsRoot = null;
        levelsRoot = null;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
        setWebFetcher((WebFetcher) beanFactory.getBean("webFetcher"));
    }

    public WebFetcher getWebFetcher() {
        return webFetcher;
    }

    public void setWebFetcher(WebFetcher webFetcher) {
        this.webFetcher = webFetcher;
    }

    @Override
    public UdirRoot getUdirRoot() {
        return this;
    }

    @Override
    public void destroy() throws Exception {
        close();
    }
}
