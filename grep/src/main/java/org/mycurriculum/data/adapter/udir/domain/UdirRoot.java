package org.mycurriculum.data.adapter.udir.domain;

public interface UdirRoot {
    public UdirCurriculumRoot getCurriculumRoot();

    public UdirProgramAreasRoot getProgramAreasRoot();

    public UdirEducationalProgramsRoot getEducationalProgramsRoot();

    public UdirLevelsRoot getLevelsRoot();

    /**
     * This is not supposed to be essential, but it might sometimes be
     * useful to know that the object is being disposed.
     */
    public void close();
}
