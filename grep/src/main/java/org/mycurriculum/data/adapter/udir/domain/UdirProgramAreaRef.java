package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;

public interface UdirProgramAreaRef extends UdirObject {
    public UdirProgramArea getProgramArea() throws IOException;
}
