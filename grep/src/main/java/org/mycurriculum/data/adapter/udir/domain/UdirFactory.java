package org.mycurriculum.data.adapter.udir.domain;

public interface UdirFactory {
    public UdirRoot createUdirRoot();
}
