package org.mycurriculum.data.adapter.udir;

import org.mycurriculum.data.adapter.udir.abstracts.UdirTranslatedTitledObjectImpl;
import org.mycurriculum.data.adapter.udir.domain.UdirLevel;

public class UdirLevelImpl extends UdirTranslatedTitledObjectImpl implements UdirLevel {
    @Override
    public String getUrlPsi()
    {
        return getId();
    }
}
