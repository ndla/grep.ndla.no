package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;

/**
 * Created by janespen on 4/14/14.
 */
public interface UdirCurriculumRef extends UdirObject {
    public UdirCurriculum getCurriculum() throws IOException;
}
