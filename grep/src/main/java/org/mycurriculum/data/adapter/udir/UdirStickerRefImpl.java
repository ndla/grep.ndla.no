package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.UdirTitledRefAbstract;
import org.mycurriculum.data.adapter.udir.domain.UdirSticker;
import org.mycurriculum.data.adapter.udir.domain.UdirStickerRef;

import java.io.IOException;

public class UdirStickerRefImpl extends UdirTitledRefAbstract<UdirStickerImpl> implements UdirStickerRef {
    @JsonProperty("kode")
    private String kode;

    @Override
    public String getKode() {
        return kode;
    }

    @Override
    public UdirSticker getSticker() throws IOException {
        return getRefObject(UdirStickerImpl.class);
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
}
