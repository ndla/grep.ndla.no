package org.mycurriculum.data.adapter.udir;

import org.mycurriculum.data.adapter.WebFetcherFactory;
import org.mycurriculum.data.adapter.udir.domain.UdirFactory;
import org.mycurriculum.data.adapter.udir.domain.UdirRoot;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

import java.net.URL;

public class UdirFactoryImpl implements UdirFactory, BeanFactoryAware {
    private WebFetcherFactory webFetcherFactory;
    private URL rootUrl;

    public UdirFactoryImpl(URL rootUrl) {
        this.rootUrl = rootUrl;
    }

    @Override
    public UdirRoot createUdirRoot() {
        UdirRootImpl root = new UdirRootImpl(rootUrl);
        root.setWebFetcher(webFetcherFactory.createWebFetcher());
        return root;
    }

    public WebFetcherFactory getWebFetcherFactory() {
        return webFetcherFactory;
    }

    public void setWebFetcherFactory(WebFetcherFactory webFetcherFactory) {
        this.webFetcherFactory = webFetcherFactory;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        setWebFetcherFactory((WebFetcherFactory) beanFactory.getBean("webFetcherFactory"));
    }
}
