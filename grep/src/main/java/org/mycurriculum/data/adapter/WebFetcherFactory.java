package org.mycurriculum.data.adapter;

import java.io.File;

public interface WebFetcherFactory {
    public void setRateLimiter(RateLimiter rateLimiter);
    public void setConcurrencyLimiter(ConcurrencyLimiter concurrencyLimiter);
    public WebFetcher createWebFetcher();

    /**
     * Intended for debugging, to get faster turnarounds when having to load the
     * same data over and over again.
     *
     * @param cacheFile
     */
    public void setDevelCacheFile(File cacheFile);
}
