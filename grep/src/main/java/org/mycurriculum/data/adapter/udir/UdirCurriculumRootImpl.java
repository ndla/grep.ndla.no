package org.mycurriculum.data.adapter.udir;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mycurriculum.data.adapter.WebFetcher;
import org.mycurriculum.data.adapter.WebFetcherInstance;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumRef;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumRoot;
import org.mycurriculum.data.adapter.udir.domain.UdirRoot;
import org.mycurriculum.data.adapter.udir.domain.UdirRootReference;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UdirCurriculumRootImpl implements UdirCurriculumRoot, BeanFactoryAware {
    private WebFetcher webFetcher;
    private UdirRootReference udirRootReference;

    private URL rootUrl;
    private List<UdirCurriculumRefImpl> curriculumRefs = null;

    public UdirCurriculumRootImpl(URL rootUrl) {
        this.rootUrl = rootUrl;
    }

    @Override
    public List<UdirCurriculumRef> getCurriculumReferences() throws IOException {
        WebFetcherInstance downloader;
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<UdirCurriculumRef> list;
        String data;

        if (curriculumRefs == null) {
            downloader = webFetcher.getFetcher(rootUrl);
            data = downloader.getContents();
            curriculumRefs = mapper.readValue(data, new TypeReference<List<UdirCurriculumRefImpl>>() {
            });
            for (UdirCurriculumRefImpl ref : curriculumRefs) {
                ref.setRootReference(udirRootReference);
                ref.setWebFetcher(webFetcher);
            }
        }
        list = new ArrayList<UdirCurriculumRef>();
        list.addAll(curriculumRefs);
        return list;
    }

    @Override
    public String toString() {
        String output = "{";
        String glue = "\n ";
        String end = "}";

        try {
            for (UdirCurriculumRef curRef : getCurriculumReferences()) {
                output += glue;
                output += curRef.toString();
                glue = ",\n ";
                end = "\n}";
            }
            output += end;
            return output;
        } catch (IOException e) {
            return "{ioerror}";
        }
    }

    public void setWebFetcher(WebFetcher fetcher) {
        this.webFetcher = fetcher;
    }

    public WebFetcher getWebFetcher() {
        return webFetcher;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        setWebFetcher((WebFetcher) beanFactory.getBean("webFetcher"));
    }

    @Override
    public UdirRoot getUdirRoot() {
        return getUdirRoot();
    }

    public void setUdirRootReference(UdirRootReference udirRootReference) {
        this.udirRootReference = udirRootReference;
    }
}
