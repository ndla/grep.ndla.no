package org.mycurriculum.data.adapter.udir.domain;

import org.mycurriculum.data.adapter.WebFetcher;

import java.util.List;

/**
 * Created by janespen on 4/15/14.
 */
public interface UdirObject extends UdirRootReference {
    public UdirRootReference getRootReference();

    public WebFetcher getWebFetcher();

    public String getId();

    public String getUrlPsi();

    public List<? extends UdirStickerRef> getStickers();
}
