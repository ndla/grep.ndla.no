package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;
import java.util.List;

public interface UdirCurriculumRoot extends UdirRootReference {
    public List<UdirCurriculumRef> getCurriculumReferences() throws IOException;
}
