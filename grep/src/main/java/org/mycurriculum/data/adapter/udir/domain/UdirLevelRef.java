package org.mycurriculum.data.adapter.udir.domain;

import java.io.IOException;

/**
 * Created by janespen on 5/19/14.
 */
public interface UdirLevelRef extends UdirObject {
    public UdirLevel getLevel() throws IOException;
}
