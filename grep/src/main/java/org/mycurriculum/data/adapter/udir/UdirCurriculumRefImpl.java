package org.mycurriculum.data.adapter.udir;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mycurriculum.data.adapter.udir.abstracts.UdirRefAbstract;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculum;
import org.mycurriculum.data.adapter.udir.domain.UdirCurriculumRef;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirCurriculumRefImpl extends UdirRefAbstract<UdirCurriculumImpl> implements UdirCurriculumRef {
    @Override
    public String toString() {
        return getId();
    }

    @Override
    public UdirCurriculum getCurriculum() throws IOException {
        UdirCurriculumImpl curriculum;

        curriculum = getRefObject(UdirCurriculumImpl.class);
        curriculum.setRootReference(getRootReference());
        curriculum.setWebFetcher(getWebFetcher());
        return curriculum;
    }
}
