package org.mycurriculum.data.adapter.udir.domain;

import java.util.Locale;

/**
 * Created by janespen on 4/14/14.
 */
public interface UdirTranslatedText {
    public String getLangId();

    public String getText();

    public Locale getLocale();
}
