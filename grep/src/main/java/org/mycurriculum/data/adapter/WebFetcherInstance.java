package org.mycurriculum.data.adapter;

import java.io.IOException;

/**
 * Created by janespen on 4/10/14.
 */
public interface WebFetcherInstance {
    public void fetch() throws IOException;

    public String getContents() throws IOException;
}
