package org.mycurriculum.data.adapter.udir.abstracts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mycurriculum.data.adapter.udir.abstracts.helpers.UdirRef;
import org.mycurriculum.data.adapter.udir.abstracts.helpers.UdirRefResolver;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdirTranslatedTitledRefAbstract<T> extends UdirTranslatedTitledObjectImpl implements UdirRef {
    private UdirRefResolver<T> resolver = new UdirRefResolver<T>(this);

    @JsonProperty("url-data")
    private String dataUrl;

    public String getDataUrl() {
        return dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    public T getRefObject(Class<T> cls) throws IOException {
        return resolver.getObject(cls);
    }
}
