/**
 * Created by Brett Kromkamp (brett@seria.no)
 * April 03, 2014
 */

package org.mycurriculum.ui.controller;

import org.mycurriculum.business.domain.Account;
import org.mycurriculum.business.domain.Course;
import org.mycurriculum.business.domain.User;
import org.mycurriculum.data.entityservice.AccountService;
import org.mycurriculum.data.entityservice.CourseService;
import org.mycurriculum.data.entityservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Controller
public class TestController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @Autowired
    private AccountService accountService;

    @RequestMapping("/test")
    public String test(@RequestParam(value="name", required=false, defaultValue="MyCurriculum.org") String name, Model model) {

        // Test to check that Spring Security is wired-up correctly.
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        User user = userService.findByUsername(username);
        Account userAccount = user.getAccount();

        Set<User> accountUsers = accountService.findByName(userAccount.getName()).getUsers();

        model.addAttribute("accountName", userAccount.getName());
        model.addAttribute("accountUsers", accountUsers);

        Collection<? extends GrantedAuthority> grantedAuthorities = authentication.getAuthorities();

        for (GrantedAuthority grantedAuthority : grantedAuthorities) {
            System.out.println(grantedAuthority.getAuthority());
        }

        model.addAttribute("username", username);
        model.addAttribute("authorities", grantedAuthorities);

        // Test to check that the (Hibernate) persistence layer (with multi-tenancy) is wired-up correctly.
        final Course course1 = new Course();
        course1.setName("controller-course-1");
        course1.setVersionNumber(1L);
        courseService.save(course1);

        final List<Course> courses = courseService.list(1L);
        model.addAttribute("courses", courses);

        return "test/test";
    }
}
