package org.mycurriculum.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StaticDocsController {
    public StaticDocsController() {
    }

    @RequestMapping(value = "/")
    public Object indexFileAction()
    {
        return "redirect:/index.html";
    }
}
