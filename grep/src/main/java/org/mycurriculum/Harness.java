/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 10, 2014
 */

package org.mycurriculum;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.domain.Course;
import org.mycurriculum.business.domain.EducationGroup;
import org.mycurriculum.business.domain.Version;
import org.mycurriculum.data.entityservice.CourseService;
import org.mycurriculum.data.entityservice.EducationGroupService;
import org.mycurriculum.data.entityservice.VersionService;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.topicmapservice.ParserService;
import org.mycurriculum.data.topicmapservice.TopicMapLinkService;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Harness {

    private static final String TEST_USER = "ndla";

    AbstractApplicationContext context;

    public static void main(String[] args) {
        Harness harness = new Harness();

        harness.execute();
        harness.close();
    }

    public void close() {
        context.close();
    }

    public void execute() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");

        // Get a user and add it into the security context (required for the multi-tenant services).
        UserDetailsService userService = (UserDetailsService) context.getBean("userDetailsService");
        UserDetails userDetails = userService.loadUserByUsername(TEST_USER);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        EducationGroupService educationGroupService = (EducationGroupService) context.getBean("educationGroupService");
        CourseService courseService = (CourseService) context.getBean("courseService");
        //CourseGroupService courseGroupService = (CourseGroupService) context.getBean("courseGroupService");
        VersionService versionService = (VersionService) context.getBean("versionService");

        // Read courses-course structures mapping (JSON) file
        String mappingString = null;
        try {
            mappingString = new String(Files.readAllBytes(Paths.get("/home/brettk/ndla.no/grep.ndla.no/mycurriculum/src/main/resources/coursestructures_mapping.json")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject mappingJson = new JSONObject(mappingString);
        Iterator<String> educationGroupsIterator = mappingJson.keys();

        ParserService parserService = (ParserService) context.getBean("parserService");
        TopicMapLinkService topicMapLinkService = (TopicMapLinkService) context.getBean("topicMapLinkService");

        try {
            String resourcesConnectionString = "db=org.mycurriculum.data.tms.connections.config.PostgresqlConnectionSetupImpl%3Ajdbc%3Apostgresql%3A%2F%2Flocalhost%2Ftestharness1403765851&tm=postgresql-1";

            Version version = versionService.findActive();
            String topicsmapConnectionString = version.getTopicMapConnectionInfo();
            /*
            String topicsmapConnectionString = parserService.createTopicmap("testharness" + (new Date().getTime() / 1000), "testharness");
            Version version = new Version();
            version.setTopicMapConnectionInfo(topicsmapConnectionString);
            version.setVersionNumber(new Date().getTime() / 1000);
            version.setName("v" + version.getVersionNumber());
            versionService.save(version);
            versionService.setActive(version);

            Parser.Context parser = parserService.importTopicmap(resourcesConnectionString, topicsmapConnectionString);
            final ProgressTracker progressTracker = parser.getProgressTracker();
            progressTracker.addListener(new MultithreadedTextProgressOutputter(progressTracker));
            parser.waitForCompletion();
            */

            // Create the "deling" educational group.
            /*
            EducationGroup ndlaRootEducationGroup = educationGroupService.findByExternalId("ndla-root");

            EducationGroup delingEducationGroup = new EducationGroup();
            delingEducationGroup.setExternalId("deling");
            delingEducationGroup.setName("DELING");
            delingEducationGroup.setParent(ndlaRootEducationGroup);

            educationGroupService.save(ndlaRootEducationGroup);
            */

            // ***** Map course structures to courses, and courses to education groups *****
            TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(topicsmapConnectionString);

            Map<String, Collection<String>> mappings = new HashMap<String, Collection<String>>();

            while (educationGroupsIterator.hasNext()) {
                String educationGroupExternalId = educationGroupsIterator.next();
                EducationGroup educationGroup = educationGroupService.findByExternalId(educationGroupExternalId);

                JSONArray educationGroupCourses = (JSONArray) mappingJson.get(educationGroupExternalId);
                for (int i = 0; i < educationGroupCourses.length(); i++) {
                    JSONObject courses = (JSONObject) educationGroupCourses.get(i);
                    Iterator<String> coursesIterator = courses.keys();

                    while (coursesIterator.hasNext()) {
                        String courseExternalId = coursesIterator.next();

                        JSONArray courseStructures = (JSONArray) courses.get(courseExternalId);
                        for (int j = 0; j < courseStructures.length(); j++) {
                            JSONObject jsonCourse = (JSONObject) courseStructures.get(j);

                            String courseTitle = jsonCourse.get("title").toString();
                            String courseStructureHumanId = jsonCourse.get("course_structure").toString();

                            Course course = new Course();
                            course.setVersionNumber(version.getVersionNumber());
                            course.setName(courseTitle);
                            course.setExternalId(courseExternalId);
                            course.setEducationGroup(educationGroup);
                            courseService.save(course);

                            OntopiaCourseStructure courseStructure = topicMapLinkService.getCourseStructureByHumanId(topicMapHandle, courseStructureHumanId);

                            List<String> courseStructurePsis = new ArrayList<String>();
                            if (courseStructure != null) {
                                courseStructurePsis.add(courseStructure.getPsi().toString());
                                mappings.put(course.getExternalId(), courseStructurePsis);
                            }
                        }
                    }
                }
            }
            parserService.mapCourseGroups(version.getVersionNumber(), topicsmapConnectionString, mappings);
            // ***** Map course structures to courses, and courses to education groups *****
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
