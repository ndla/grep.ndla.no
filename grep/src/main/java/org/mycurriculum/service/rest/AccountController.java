package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.SecurityBusinessException;
import org.mycurriculum.business.domain.Account;
import org.mycurriculum.business.domain.User;
import org.mycurriculum.service.rest.exception.EntityNotFoundServiceException;
import org.mycurriculum.service.rest.exception.InvalidUserServiceException;
import org.mycurriculum.service.rest.exception.SecurityServiceException;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("accountController")
@RequestMapping(value = "/v1/users/{userName}")
public class AccountController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/accounts/{id}",
            method = RequestMethod.GET)
    public Object getAccount(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") Long id) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        Account account = null;
        try {
            account = businessFacade.getAccount(userName, id);
            if (account == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (SecurityBusinessException e) {
            throw new SecurityServiceException(e.getMessage(), e);
        }

        Link link = linkTo(methodOn(AccountController.class).getAccount(null, userName, id)).withSelfRel();

        JSONArray usersBuilder = new JSONArray();
        for (User user : account.getUsers()) {
            link = linkTo(methodOn(UserController.class).getUser(null, userName, user.getId())).withSelfRel();

            JSONObject userBuilder = new JSONObject();
            userBuilder.put("userName", user.getUsername());
            userBuilder.put("link", link.getHref());
            usersBuilder.put(userBuilder);
        }

        JSONObject accountBuilder = new JSONObject();
        accountBuilder.put("id", account.getId());
        accountBuilder.put("name", account.getName());
        accountBuilder.put("isEnabled", account.isEnabled());
        accountBuilder.put("users", usersBuilder);

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("account", accountBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
}
