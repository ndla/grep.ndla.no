package org.mycurriculum.service.rest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BadRequestBusinessException;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.ConflictBusinessException;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.references.TopicMap;
import org.mycurriculum.data.util.MutableTranslatedText;
import org.mycurriculum.service.rest.exception.*;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("resourcesController")
@RequestMapping(value = "/v1/users/{userName}")
public class ResourcesController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/resources",
            method = RequestMethod.GET)
    public Object getResource(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam("psi") String psi,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "competence-aims", required = false) boolean resolveCompetenceAims) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        OntopiaResource resource = null;

        try {
            if (versionNumber != null) {
                resource = businessFacade.getResource(userName, versionNumber, psi);
            } else {
                resource = businessFacade.getResource(userName, psi);
            }
            if (resource == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        TopicMap topicMapReference = resource.getRef().getTopicMapReference();
        try {
            topicMapReference.openPersistentConnection();
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }
        try {
            JSONArray resourceNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : resource.getNameObjects()) {
                JSONArray nameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    nameScopesBuilder.put(scope);
                }
                JSONObject resourceNameBuilder = new JSONObject();
                resourceNameBuilder.put("name", name.getName());
                resourceNameBuilder.put("scopes", nameScopesBuilder);
                resourceNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                resourceNamesBuilder.put(resourceNameBuilder);
            }
            JSONArray resourcePsisBuilder = new JSONArray();
            for (URL jsonPsi : resource.getPsis()) {
                resourcePsisBuilder.put(jsonPsi.toString());
            }

            // ***** Ingress *****
            MutableTranslatedText translatedText = resource.getIngress();
            JSONObject ingressBuilder = new JSONObject();
            if (translatedText.getTextMap().size() > 0) {
                Locale defaultLocale = translatedText.getDefaultLocale();
                String defaultText = StringEscapeUtils.escapeJson(translatedText.getDefaultText()); // TODO: Escaping, encoding, etcetera.
                String defaultLocaleLanguage;
                String defaultLocaleDisplayLanguage;
                String defaultLocalePsi;
                if (defaultLocale != null) {
                    defaultLocaleLanguage = defaultLocale.getLanguage();
                    defaultLocaleDisplayLanguage = defaultLocale.getDisplayLanguage();
                    defaultLocalePsi = translatedText.getDefaultLocalePsi();
                } else {
                    defaultLocaleLanguage = null;
                    defaultLocaleDisplayLanguage = null;
                    defaultLocalePsi = null;
                }

                ingressBuilder.put("defaultText", defaultText);

                JSONObject localeBuilder = new JSONObject();
                localeBuilder.put("language", defaultLocaleLanguage);
                localeBuilder.put("displayLanguage", defaultLocaleDisplayLanguage);
                localeBuilder.put("psi", defaultLocalePsi);
                ingressBuilder.put("defaultLocale", localeBuilder);

                JSONArray languagesBuilder = new JSONArray();
                Locale[] languages = translatedText.getLanguages();
                for (int i = 0; i < languages.length; i++) {
                    Locale language = languages[i];
                    JSONObject languageBuilder = new JSONObject();
                    languageBuilder.put("language", language.getLanguage());
                    languageBuilder.put("displayLanguage", language.getDisplayLanguage());
                    languagesBuilder.put(languageBuilder);
                }
                ingressBuilder.put("languages", languagesBuilder);

                JSONArray textMapLocaleEntries = new JSONArray();
                for (Map.Entry<Locale, String> entry : translatedText.getTextMap().entrySet()) {
                    JSONObject textMapLocaleEntry = new JSONObject();
                    String key = entry.getKey().getLanguage();
                    String value = StringEscapeUtils.escapeJson(entry.getValue()); // TODO: Escaping, encoding, etcetera.
                    textMapLocaleEntry.put("language", key);
                    textMapLocaleEntry.put("text", value);

                    textMapLocaleEntries.put(textMapLocaleEntry);
                }
                ingressBuilder.put("localeTextEntries", textMapLocaleEntries);

                JSONArray textMapPsiEntries = new JSONArray();
                for (Map.Entry<String, String> entry : translatedText.getPsiTextMap().entrySet()) {
                    JSONObject textMapPsiEntry = new JSONObject();
                    String key = entry.getKey();
                    String value = StringEscapeUtils.escapeJson(entry.getValue()); // TODO: Escaping, encoding, etcetera.
                    textMapPsiEntry.put("psi", key);
                    textMapPsiEntry.put("text", value);

                    textMapPsiEntries.put(textMapPsiEntry);
                }
                ingressBuilder.put("psiTextEntries", textMapPsiEntries);
            }
            // ***** Ingress *****

            // ***** Resource - Aim - Associations *****
            JSONArray relationsBuilder = new JSONArray();
            Collection<OntopiaResourceAimAssociation> resourceAimAssociations = null;
            try {
                resourceAimAssociations = resource.getAimAssociations();
            } catch (IOException e) {
                e.printStackTrace();
                throw new GeneralServiceException(e.getMessage(), e);
            }
            if (resourceAimAssociations != null) {
                for (OntopiaResourceAimAssociation resourceAimAssociation : resourceAimAssociations) {

                    JSONObject relationBuilder = new JSONObject();

                    relationBuilder.put("humanId", resourceAimAssociation.getHumanId());

                    OntopiaAim competenceAim = null;
                    try {
                        competenceAim = resourceAimAssociation.getAim();
                    } catch (IOException e) {
                        e.printStackTrace();
                        throw new GeneralServiceException(e.getMessage(), e);
                    }
                    OntopiaCurriculumSet curriculumSet;
                    OntopiaLevel level;
                    try {
                        // ***** Curriculum set *****
                        curriculumSet = resourceAimAssociation.getCurriculumSet();
                        if (curriculumSet != null) {
                            JSONArray curriculumSetPsisBuilder = new JSONArray();
                            for (URL curriculumSetPsi : curriculumSet.getPsis()) {
                                curriculumSetPsisBuilder.put(curriculumSetPsi.toString());
                            }
                            JSONArray curriculumSetNamesBuilder = new JSONArray();
                            for (OntopiaTopic.Name name : curriculumSet.getNameObjects()) {
                                JSONArray curriculumSetNameScopesBuilder = new JSONArray();
                                for (String scope : name.getScopes()) {
                                    curriculumSetNameScopesBuilder.put(scope);
                                }
                                JSONObject curriculumSetNameBuilder = new JSONObject();
                                curriculumSetNameBuilder.put("name", name.getName());
                                curriculumSetNameBuilder.put("scopes", curriculumSetNameScopesBuilder);
                                curriculumSetNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                                curriculumSetNamesBuilder.put(curriculumSetNameBuilder);
                            }
                            String setType = curriculumSet.getSetType() != null ? curriculumSet.getSetType() : "";

                            JSONObject linksBuilder = new JSONObject();
                            Collection<OntopiaCourseStructure> courseStructures = curriculumSet.getCourseStructures();
                            JSONArray parentsBuilder = new JSONArray();
                            for (OntopiaCourseStructure courseStructure : courseStructures) {
                                Link parentLink = linkTo(methodOn(CourseStructuresController.class).getCourseStructure(null, userName, courseStructure.getHumanId(), versionNumber)).withSelfRel();
                                parentsBuilder.put(parentLink.getHref());
                            }
                            Link selfLink = linkTo(methodOn(CurriculumSetsController.class).getCurriculumSet(null, userName, curriculumSet.getHumanId(), versionNumber)).withSelfRel();
                            linksBuilder.put("parents", parentsBuilder);
                            linksBuilder.put("self", selfLink.getHref());

                            JSONObject curriculumSetBuilder = new JSONObject();
                            curriculumSetBuilder.put("id", curriculumSet.getHumanId());
                            curriculumSetBuilder.put("links", linksBuilder);
                            curriculumSetBuilder.put("names", curriculumSetNamesBuilder);
                            curriculumSetBuilder.put("setType", setType);
                            curriculumSetBuilder.put("psis", curriculumSetPsisBuilder);

                            relationBuilder.put("curriculumSet", curriculumSetBuilder);
                        }
                        // ***** Curriculum set *****

                        // ***** Level *****
                        level = resourceAimAssociation.getLevel();
                        if (level != null) {
                            JSONArray levelNamesBuilder = new JSONArray();
                            for (OntopiaTopic.Name name : level.getNameObjects()) {
                                JSONArray nameScopesBuilder = new JSONArray();
                                for (String scope : name.getScopes()) {
                                    nameScopesBuilder.put(scope);
                                }
                                JSONObject levelNameBuilder = new JSONObject();
                                levelNameBuilder.put("name", name.getName());
                                levelNameBuilder.put("scopes", nameScopesBuilder);
                                levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

                                levelNamesBuilder.put(levelNameBuilder);
                            }
                            JSONArray levelPsisBuilder = new JSONArray();
                            for (URL jsonPsi : level.getPsis()) {
                                levelPsisBuilder.put(jsonPsi.toString());
                            }

                            JSONObject levelLinksBuilder = new JSONObject();
                            Link levelSelfLink = linkTo(methodOn(LevelsController.class).getLevel(null, userName, level.getHumanId(), versionNumber, false)).withSelfRel();
                            levelLinksBuilder.put("self", levelSelfLink.getHref());

                            JSONObject levelBuilder = new JSONObject();
                            levelBuilder.put("id", level.getHumanId());
                            levelBuilder.put("links", levelLinksBuilder);
                            levelBuilder.put("names", levelNamesBuilder);
                            levelBuilder.put("psis", levelPsisBuilder);

                            relationBuilder.put("level", levelBuilder);
                        }
                        // ***** Level *****

                        // ***** Competence aim *****
                        JSONArray competenceAimNamesBuilder = new JSONArray();
                        for (OntopiaTopic.Name name : competenceAim.getNameObjects()) {
                            JSONArray nameScopesBuilder = new JSONArray();
                            for (String scope : name.getScopes()) {
                                nameScopesBuilder.put(scope);
                            }
                            JSONObject competenceAimNameBuilder = new JSONObject();
                            competenceAimNameBuilder.put("name", name.getName());
                            competenceAimNameBuilder.put("scopes", nameScopesBuilder);
                            competenceAimNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                            competenceAimNamesBuilder.put(competenceAimNameBuilder);
                        }
                        JSONArray competenceAimPsisBuilder = new JSONArray();
                        for (URL jsonPsi : competenceAim.getPsis()) {
                            competenceAimPsisBuilder.put(jsonPsi.toString());
                        }

                        JSONObject linksBuilder = new JSONObject();
                        Collection<OntopiaAimSet> competenceAimsSet = competenceAim.getAimSets();
                        JSONArray parentsBuilder = new JSONArray();
                        for (OntopiaAimSet competenceAimSet : competenceAimsSet) {
                            Link parentLink = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(null, userName, competenceAimSet.getHumanId(), versionNumber)).withSelfRel();
                            parentsBuilder.put(parentLink.getHref());
                        }
                        Link selfLink = linkTo(methodOn(CompetenceAimsController.class).getCompetenceAim(null, userName, competenceAim.getHumanId(), versionNumber, false)).withSelfRel();
                        linksBuilder.put("parents", parentsBuilder);
                        linksBuilder.put("self", selfLink.getHref());

                        JSONObject competenceAimBuilder = new JSONObject();
                        competenceAimBuilder.put("id", competenceAim.getHumanId());
                        competenceAimBuilder.put("links", linksBuilder);
                        competenceAimBuilder.put("names", competenceAimNamesBuilder);
                        competenceAimBuilder.put("psis", competenceAimPsisBuilder);

                        relationBuilder.put("competenceAim", competenceAimBuilder);
                        // ***** Competence aim *****

                        // ***** Miscellaneous occurrences *****
                        relationBuilder.put("apprenticeRelevant", resourceAimAssociation.getApprenticeRelevant());
                        String courseId = resourceAimAssociation.getCourseId();
                        if (courseId != null) {
                            relationBuilder.put("courseId", courseId);
                        }
                        String courseType = resourceAimAssociation.getCourseType();
                        if (courseType != null) {
                            relationBuilder.put("courseType", courseType);
                        }
                        // ***** Miscellaneous occurrences *****

                        // ***** Resource-competence aim relation metadata *****
                        OntopiaCurriculum curriculum = resourceAimAssociation.getCurriculum();
                        if (curriculum != null) {
                            relationBuilder.put("curriculumId", curriculum.getHumanId());
                        }
                        OntopiaAimSet competenceAimSet = resourceAimAssociation.getAimSet();
                        if (competenceAimSet != null) {
                            relationBuilder.put("competenceAimSetId", competenceAimSet.getHumanId());
                        }
                        // ***** Resource-competence aim relation metadata *****

                        // ***** Authors *****
                        String[] authorNames = resourceAimAssociation.getAuthors();
                        String[] authorUrls = resourceAimAssociation.getAuthorUrls();

                        JSONArray authorsBuilder = new JSONArray();
                        for (int i = 0; i < authorNames.length; i++) {
                            JSONObject authorBuilder = new JSONObject();
                            authorBuilder.put("name", authorNames[i]);
                            authorBuilder.put("url", authorUrls[i]);

                            authorsBuilder.put(authorBuilder);
                        }
                        relationBuilder.put("authors", authorsBuilder);
                        // ***** Authors *****

                        relationsBuilder.put(relationBuilder);
                    } catch (IOException e) {
                        e.printStackTrace();
                        throw new GeneralServiceException(e.getMessage(), e);
                    }
                }
            }
            // ***** Resource - Aim - Associations *****

            // ***** Links *****
            JSONObject linksBuilder = new JSONObject();
            Link selfLink = linkTo(methodOn(ResourcesController.class).getResource(null, userName, psi, versionNumber, resolveCompetenceAims)).withSelfRel();
            linksBuilder.put("self", selfLink.getHref());
            // ***** Links *****

            JSONObject resourceBuilder = new JSONObject();
            resourceBuilder.put("id", resource.getHumanId());
            resourceBuilder.put("status", resource.getStatus());
            resourceBuilder.put("links", linksBuilder);
            resourceBuilder.put("names", resourceNamesBuilder);
            //resourceBuilder.put("competenceAims", competenceAimsBuilder);
            resourceBuilder.put("relations", relationsBuilder);
            resourceBuilder.put("psis", resourcePsisBuilder);
            resourceBuilder.put("ingress", ingressBuilder);
            //resourceBuilder.put("authors", authorsBuilder);

            endTime = System.nanoTime();
            duration = endTime - startTime;

            JSONObject rootBuilder = new JSONObject();
            //rootBuilder.put("self", link.getHref());
            rootBuilder.put("processingTime", duration);
            rootBuilder.put("resource", resourceBuilder);
            ResponseUtils.respondWithJSON(response, rootBuilder);
            return null;
        } finally {
            topicMapReference.closePersistentConnection();
        }
    }

    @RequestMapping(
            value = "/resources",
            method = RequestMethod.POST)
    public Object createResource(
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestBody String payload,
            HttpServletResponse response) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        OntopiaResource resource = null;
        try {
            if (versionNumber != null) {
                resource = businessFacade.createResource(userName, versionNumber, payload);
            } else {
                resource = businessFacade.createResource(userName, payload);
            }
            if (resource == null) {
                throw new GeneralServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new ConflictServiceException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        Link link = linkTo(methodOn(ResourcesController.class).getResource(null, userName, resource.getPsi().toString(), versionNumber, false)).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", "Ok");
        rootBuilder.put("link", link.getHref());

        response.setStatus(HttpServletResponse.SC_CREATED);
        response.setHeader("Location", link.getHref());

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
}
