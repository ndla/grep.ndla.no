package org.mycurriculum.service.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "Invalid version") // HTTP Error Code: 406
public class InvalidVersionServiceException extends RuntimeException {

    public InvalidVersionServiceException() {
        super();
    }

    public InvalidVersionServiceException(String message) {
        super(message);
    }

    public InvalidVersionServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidVersionServiceException(Throwable cause) {
        super(cause);
    }
}