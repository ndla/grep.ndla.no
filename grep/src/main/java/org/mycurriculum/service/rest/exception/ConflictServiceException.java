package org.mycurriculum.service.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * User:    Brett Kromkamp from http://www.youprogramming.com
 * Date:    Aug 20, 2014
 */

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Conflict") // HTTP Error Code: 409
public class ConflictServiceException extends RuntimeException {

    public ConflictServiceException() {
        super();
    }

    public ConflictServiceException(String message) {
        super(message);
    }

    public ConflictServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflictServiceException(Throwable cause) {
        super(cause);
    }
}
