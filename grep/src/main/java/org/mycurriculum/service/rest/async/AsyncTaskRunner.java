package org.mycurriculum.service.rest.async;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.data.util.parser.MultithreadedTextProgressOutputter;
import org.mycurriculum.data.util.parser.ProgressBit;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class AsyncTaskRunner implements JsonAsyncCall, MultithreadedTextProgressOutputter.Listener {
    private AsyncTask asyncTask;
    private List<String> log = new ArrayList<String>();
    private String statusLine = "";
    private IOException ioException = null;

    public AsyncTaskRunner(AsyncTask searchIndexer) {
        this.asyncTask = searchIndexer;
    }

    @Override
    public JSONObject getStatus() {
        JSONObject status = new JSONObject();
        JSONArray log = new JSONArray();
        synchronized (this) {
            for (String logln : this.log) {
                log.put(logln);
            }
            status.put("log", log);
            status.put("status", statusLine);
            if (ioException != null) {
                JSONObject exceptionInfo = new JSONObject();
                exceptionInfo.put("message", ioException.getMessage());
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ioException.printStackTrace(printWriter);
                exceptionInfo.put("trace", writer.toString());
                status.put("error", exceptionInfo);
            } else {
                status.put("error", false);
            }
        }
        asyncTask.updateStatus(status);
        return status;
    }

    @Override
    public void run() {
        ProgressBit.Listener progressOutputter = new MultithreadedTextProgressOutputter(asyncTask.getProgressTracker(), this);
        asyncTask.getProgressTracker().addListener(progressOutputter);
        asyncTask.run();
        try {
            asyncTask.completionCheck();
        } catch (IOException e) {
            ioException = e;
        }
        progressOutputter.progressUpdate();
    }

    @Override
    public void newProgressText(String text) {
        synchronized (this) {
            statusLine = text;
        }
    }

    @Override
    public void newLogLine(String textLine) {
        synchronized (this) {
            log.add(textLine);
        }
    }
}
