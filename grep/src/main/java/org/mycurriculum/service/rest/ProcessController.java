package org.mycurriculum.service.rest;

import org.json.JSONObject;
import org.mycurriculum.business.api.AsyncImportFacade;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller("processController")
@RequestMapping(value = "/v1/users/{userName}")
public class ProcessController {

    @Autowired
    AsyncImportFacade importFacade;

    @RequestMapping(
            value = "/processes/start-{id}",
            method = RequestMethod.GET)
    public Object start(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") String id) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        String statusMessage;
        // Dispatch on process identifier (id).
        switch (id) {
            case "import":
                statusMessage = "Import process started";
                // TODO: importFacade.execute();
                break;
            // Additional processes.
            default:
                statusMessage = "Default process executed";
        }

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", statusMessage);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    /*
    @RequestMapping(
            value = "/processes/status-{id}",
            produces = "application/json",
            method = RequestMethod.GET)
    @ResponseBody
    public DeferredResult<Object> getStatus(
            @PathVariable("userName") String userName,
            @PathVariable("id") String id) {

        final DeferredResult<Object> result = new DeferredResult<Object>();
        // TODO: importFacade.getUpdate(result);
        return result;
    }
    */
}
