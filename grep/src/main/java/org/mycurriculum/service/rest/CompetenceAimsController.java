package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.domain.OntopiaAim;
import org.mycurriculum.data.tms.domain.OntopiaAimSet;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.service.rest.exception.EntityNotFoundServiceException;
import org.mycurriculum.service.rest.exception.GeneralServiceException;
import org.mycurriculum.service.rest.exception.InvalidUserServiceException;
import org.mycurriculum.service.rest.exception.InvalidVersionServiceException;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("competenceAimsController")
@RequestMapping(value = "/v1/users/{userName}")
public class CompetenceAimsController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/competence-aims/{id}",
            method = RequestMethod.GET)
    public Object getCompetenceAim(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") String id,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "resources", required = false) boolean resolveResources) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        OntopiaAim competenceAim = null;
        try {
            if (versionNumber != null) {
                competenceAim = businessFacade.getCompetenceAim(userName, versionNumber, id);
            } else {
                competenceAim = businessFacade.getCompetenceAim(userName, id);
            }
            if (competenceAim == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONArray resourcesBuilder = new JSONArray();
        if (resolveResources) {
            // ***** Resources *****
            Collection<OntopiaResource> resources = null;
            try {
                resources = competenceAim.getResources();
            } catch (IOException e) {
                e.printStackTrace();
                throw new GeneralServiceException(e.getMessage(), e);
            }

            for (OntopiaResource resource : resources) {

                JSONArray resourceNamesBuilder = new JSONArray();
                for (OntopiaTopic.Name name : resource.getNameObjects()) {
                    JSONArray nameScopesBuilder = new JSONArray();
                    for (String scope : name.getScopes()) {
                        nameScopesBuilder.put(scope);
                    }
                    JSONObject resourceNameBuilder = new JSONObject();
                    resourceNameBuilder.put("name", name.getName());
                    resourceNameBuilder.put("scopes", nameScopesBuilder);
                    resourceNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                    resourceNamesBuilder.put(resourceNameBuilder);
                }
                JSONArray resourcePsisBuilder = new JSONArray();
                for (URL jsonPsi : resource.getPsis()) {
                    resourcePsisBuilder.put(jsonPsi.toString());
                }

                // ***** Links *****
                JSONObject linksBuilder = new JSONObject();
                // TODO: Determine if appropriate to pass false as default for the resolving of competence aims.
                Link selfLink = linkTo(methodOn(ResourcesController.class).getResource(null, userName, resource.getPsi().toString(), versionNumber, false)).withSelfRel();
                linksBuilder.put("self", selfLink.getHref());
                // ***** Links *****

                JSONObject resourceBuilder = new JSONObject();
                resourceBuilder.put("names", resourceNamesBuilder);
                resourceBuilder.put("links", linksBuilder);
                resourceBuilder.put("psis", resourcePsisBuilder);
                resourcesBuilder.put(resourceBuilder);
            }
            // ***** Resources *****
        }

        JSONArray competenceAimNamesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : competenceAim.getNameObjects()) {
            JSONArray nameScopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                nameScopesBuilder.put(scope);
            }
            JSONObject competenceAimNameBuilder = new JSONObject();
            competenceAimNameBuilder.put("name", name.getName());
            competenceAimNameBuilder.put("scopes", nameScopesBuilder);
            competenceAimNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
            competenceAimNamesBuilder.put(competenceAimNameBuilder);
        }
        JSONArray competenceAimPsisBuilder = new JSONArray();
        for (URL psi : competenceAim.getPsis()) {
            competenceAimPsisBuilder.put(psi.toString());
        }

        // ***** Links *****
        JSONObject linksBuilder = new JSONObject();
        Collection<OntopiaAimSet> competenceAimsSet = competenceAim.getAimSets();
        //OntopiaAimSet parent = (OntopiaAimSet) competenceAimsSet.toArray()[0];
        //Link parentLink = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(userName, parent.getHumanId(), versionNumber)).withSelfRel();
        JSONArray parentsBuilder = new JSONArray();
        for (OntopiaAimSet competenceAimSet : competenceAimsSet) {
            Link parentLink = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(null, userName, competenceAimSet.getHumanId(), versionNumber)).withSelfRel();
            parentsBuilder.put(parentLink.getHref());
        }
        Link selfLink = linkTo(methodOn(CompetenceAimsController.class).getCompetenceAim(null, userName, competenceAim.getHumanId(), versionNumber, resolveResources)).withSelfRel();
        linksBuilder.put("parents", parentsBuilder);
        linksBuilder.put("self", selfLink.getHref());
        // ***** Links *****

        JSONObject competenceAimBuilder = new JSONObject();
        competenceAimBuilder.put("id", competenceAim.getHumanId());
        competenceAimBuilder.put("links", linksBuilder);
        competenceAimBuilder.put("names", competenceAimNamesBuilder);
        competenceAimBuilder.put("resources", resourcesBuilder);
        competenceAimBuilder.put("psis", competenceAimPsisBuilder);
        competenceAimBuilder.put("sortingOrder", competenceAim.getSortingOrder());

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        //rootBuilder.put("self", selfLink.getHref());
        //rootBuilder.put("links", linksBuilder);
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("competenceAim", competenceAimBuilder);
        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
}