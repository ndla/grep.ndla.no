package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaCurriculum;
import org.mycurriculum.data.tms.domain.OntopiaCurriculumSet;
import org.mycurriculum.service.rest.exception.EntityNotFoundServiceException;
import org.mycurriculum.service.rest.exception.GeneralServiceException;
import org.mycurriculum.service.rest.exception.InvalidUserServiceException;
import org.mycurriculum.service.rest.exception.InvalidVersionServiceException;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("curriculumSetsController")
@RequestMapping(value = "/v1/users/{userName}")
public class CurriculumSetsController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/curriculum-sets/{id}",
            method = RequestMethod.GET)
    public Object getCurriculumSet(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") String id,
            @RequestParam(value = "version", required = false) Long versionNumber) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        OntopiaCurriculumSet curriculumSet = null;
        try {
            if (versionNumber != null) {
                curriculumSet = businessFacade.getCurriculumSet(userName, versionNumber, id);
            } else {
                curriculumSet = businessFacade.getCurriculumSet(userName, id);
            }
            if (curriculumSet == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONArray curriculumSetPsisBuilder = new JSONArray();
        for (URL curriculumSetPsi : curriculumSet.getPsis()) {
            curriculumSetPsisBuilder.put(curriculumSetPsi.toString());
        }
        JSONArray curriculumSetNamesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : curriculumSet.getNameObjects()) {
            JSONArray curriculumSetNameScopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                curriculumSetNameScopesBuilder.put(scope);
            }
            JSONObject curriculumSetNameBuilder = new JSONObject();
            curriculumSetNameBuilder.put("name", name.getName());
            curriculumSetNameBuilder.put("scopes", curriculumSetNameScopesBuilder);
            curriculumSetNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
            curriculumSetNamesBuilder.put(curriculumSetNameBuilder);
        }

        // ***** Curriculums *****
        Collection<OntopiaCurriculum> curriculums = curriculumSet.getCurricula();
        JSONArray curriculumsBuilder = new JSONArray();
        for (OntopiaCurriculum curriculum : curriculums) {
            JSONArray curriculumPsisBuilder = new JSONArray();
            for (URL curriculumPsi : curriculum.getPsis()) {
                curriculumPsisBuilder.put(curriculumPsi.toString());
            }

            JSONArray curriculumNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : curriculum.getNameObjects()) {
                JSONArray curriculumNameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    curriculumNameScopesBuilder.put(scope);
                }
                JSONObject curriculumNameBuilder = new JSONObject();
                curriculumNameBuilder.put("name", name.getName());
                curriculumNameBuilder.put("scopes", curriculumNameScopesBuilder);
                curriculumNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                curriculumNamesBuilder.put(curriculumNameBuilder);
            }

            /*
            // ***** Links *****
            JSONObject linksBuilder = new JSONObject();
            // TODO: Implement parent link.
            Link selfLink = linkTo(methodOn(CurriculumsController.class).getCurriculum(userName, curriculum.getHumanId(), versionNumber)).withSelfRel();
            linksBuilder.put("self", selfLink.getHref());
            // ***** Links *****
            */
            // ***** Links *****
            JSONObject linksBuilder = new JSONObject();
            Collection<OntopiaCurriculumSet> curriculumSets = curriculum.getCurriculumSets();
            JSONArray parentsBuilder = new JSONArray();
            for (OntopiaCurriculumSet cs : curriculumSets) {
                Link parentLink = linkTo(methodOn(CurriculumSetsController.class).getCurriculumSet(null, userName, cs.getHumanId(), versionNumber)).withSelfRel();
                parentsBuilder.put(parentLink.getHref());
            }
            Link selfLink = linkTo(methodOn(CurriculumsController.class).getCurriculum(null, userName, curriculum.getHumanId(), versionNumber)).withSelfRel();
            linksBuilder.put("parents", parentsBuilder);
            linksBuilder.put("self", selfLink.getHref());
            // ***** Links *****

            JSONObject curriculumBuilder = new JSONObject();
            curriculumBuilder.put("id", curriculum.getHumanId());
            curriculumBuilder.put("links", linksBuilder);
            curriculumBuilder.put("names", curriculumNamesBuilder);
            curriculumBuilder.put("psis", curriculumPsisBuilder);
            curriculumsBuilder.put(curriculumBuilder);
        }
        // ***** Curriculums *****

        String setType = curriculumSet.getSetType() != null ? curriculumSet.getSetType() : "";

        // ***** Links *****
        JSONObject linksBuilder = new JSONObject();
        Collection<OntopiaCourseStructure> courseStructures = curriculumSet.getCourseStructures();
        JSONArray parentsBuilder = new JSONArray();
        for (OntopiaCourseStructure courseStructure : courseStructures) {
            Link parentLink = linkTo(methodOn(CourseStructuresController.class).getCourseStructure(null, userName, courseStructure.getHumanId(), versionNumber)).withSelfRel();
            parentsBuilder.put(parentLink.getHref());
        }
        Link selfLink = linkTo(methodOn(CurriculumSetsController.class).getCurriculumSet(null, userName, id, versionNumber)).withSelfRel();
        linksBuilder.put("parents", parentsBuilder);
        linksBuilder.put("self", selfLink.getHref());
        // ***** Links *****

        JSONObject curriculumSetBuilder = new JSONObject();
        curriculumSetBuilder.put("id", curriculumSet.getHumanId());
        curriculumSetBuilder.put("links", linksBuilder);
        curriculumSetBuilder.put("names", curriculumSetNamesBuilder);
        curriculumSetBuilder.put("setType", setType);
        curriculumSetBuilder.put("psis", curriculumSetPsisBuilder);
        curriculumSetBuilder.put("curriculums", curriculumsBuilder);

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        //rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("curriculumSet", curriculumSetBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
}
