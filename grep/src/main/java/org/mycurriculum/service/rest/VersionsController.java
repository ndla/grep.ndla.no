package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.domain.Version;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Controller("versionsController")
@RequestMapping(value = "/v1/users/{userName}")
public class VersionsController {
    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping("/versions")
    public Object getVersions(HttpServletResponse response, @PathVariable("userName") String userName) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        JSONArray versionsBuilder = new JSONArray();

        Collection<Version> versions;
        versions = businessFacade.getVersions(userName);
        for (Version version : versions) {
            JSONObject versionBuilder = new JSONObject();
            versionBuilder.put("id", version.getVersionNumber());
            Date createdDate = version.getDateCreated();
            if (createdDate != null) {
                versionBuilder.put("created", createdDate.toString());
            }
            versionBuilder.put("name", version.getName());
            versionBuilder.put("active", version.getActive());
            versionsBuilder.put(versionBuilder);
        }

        int count = 0;

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        //rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("versions", versionsBuilder);
        rootBuilder.put("numberOfItems", count);
        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
}
