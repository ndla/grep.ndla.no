package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BadRequestBusinessException;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.ConflictBusinessException;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.data.tms.domain.OntopiaResourceAimAssociation;
import org.mycurriculum.service.rest.exception.*;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("relationController")
@RequestMapping(value = "/v1/users/{userName}")
public class RelationController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/relations",
            method = RequestMethod.POST)
    public Object createRelations(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestBody String payload) {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        Collection<OntopiaResourceAimAssociation> resourceAimAssociations = null;
        try {
            if (versionNumber != null) {
                resourceAimAssociations = businessFacade.createRelations(userName, versionNumber, payload);
            } else {
                resourceAimAssociations = businessFacade.createRelations(userName, payload);
            }
            if (resourceAimAssociations == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new ConflictServiceException(e.getMessage(), e);
        }

        try {
            JSONArray resourcesBuilder = new JSONArray();
            for (OntopiaResourceAimAssociation resourceAimAssoc : resourceAimAssociations) {
                Link aimLink = linkTo(methodOn(CompetenceAimsController.class).getCompetenceAim(null, userName, resourceAimAssoc.getAim().getHumanId(), versionNumber, true)).withSelfRel();
                Link resourceLink = linkTo(methodOn(ResourcesController.class).getResource(null, userName, resourceAimAssoc.getResource().getPsi().toString(), versionNumber, true)).withSelfRel();
                JSONObject association = new JSONObject();
                association.put("humanId", resourceAimAssoc.getHumanId());
                association.put("aim", aimLink);
                association.put("resource", resourceLink);
                resourcesBuilder.put(association);
            }

            endTime = System.nanoTime();
            duration = endTime - startTime;

            JSONObject rootBuilder = new JSONObject();
            rootBuilder.put("processingTime", duration);
            rootBuilder.put("status", "Ok");
            rootBuilder.put("associations", resourcesBuilder);

            ResponseUtils.respondWithJSON(response, rootBuilder);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }
    }

    @RequestMapping(
            value = "/relations",
            method = RequestMethod.PUT)
    public Object updateRelations(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestBody String payload) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        System.out.println(payload);

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/relations",
            method = RequestMethod.DELETE)
    public Object deleteRelations(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestBody String payload) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        try {
            if (versionNumber != null) {
                businessFacade.deleteRelations(userName, versionNumber, payload);
            } else {
                businessFacade.deleteRelations(userName, payload);
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new ConflictServiceException(e.getMessage(), e);
        }

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", "Ok");

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
}
