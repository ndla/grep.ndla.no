package org.mycurriculum.service.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such domain entity") // HTTP Error Code: 404
public class EntityNotFoundServiceException extends RuntimeException {

    public EntityNotFoundServiceException() {
        super();
    }

    public EntityNotFoundServiceException(String message) {
        super(message);
    }

    public EntityNotFoundServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotFoundServiceException(Throwable cause) {
        super(cause);
    }
}
