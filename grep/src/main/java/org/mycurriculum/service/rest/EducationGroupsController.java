package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BadRequestBusinessException;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.ConflictBusinessException;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.business.domain.Course;
import org.mycurriculum.business.domain.EducationGroup;
import org.mycurriculum.service.rest.exception.*;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.mycurriculum.service.rest.tree.Tree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("groupsController")
@RequestMapping(value = "/v1/users/{userName}")
public class EducationGroupsController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/education-groups",
            method = RequestMethod.POST)
    public Object createGroup(
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestBody String payload,
            HttpServletResponse response) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        EducationGroup educationGroup = null;
        try {
            educationGroup = businessFacade.createEducationGroup(userName, payload);
            if (educationGroup == null) {
                throw new GeneralServiceException();
            }
            if (versionNumber == null) {
                versionNumber = businessFacade.getActiveVersionNumber();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new ConflictServiceException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        }

        Link link = linkTo(methodOn(EducationGroupsController.class).getGroup(null, userName, educationGroup.getExternalId(), versionNumber, false, false)).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", "Ok");
        rootBuilder.put("link", link.getHref());

        response.setStatus(HttpServletResponse.SC_CREATED);
        response.setHeader("Location", link.getHref());

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/education-groups",
            method = RequestMethod.GET)
    public Object getGroups(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "tree", required = false) boolean resolveTree,
            @RequestParam(value = "resources", required = false) boolean resolveResources) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        List<EducationGroup> educationGroups = null;

        try {
            educationGroups = businessFacade.getEducationGroups(userName);
            if (educationGroups == null) {
                throw new EntityNotFoundServiceException();
            }
            if (versionNumber == null) {
                versionNumber = businessFacade.getActiveVersionNumber();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        }

        JSONArray educationGroupsBuilder = new JSONArray();

        for (EducationGroup educationGroup : educationGroups) {
            Map<String, JSONObject> educationGroupsJson = new HashMap<String, JSONObject>();
            Tree educationGroupsTree = buildEducationGroupsTree(educationGroup);
            buildEducationGroupsJson(userName, versionNumber, educationGroup, educationGroupsJson, educationGroupsTree, resolveTree, resolveResources);

            educationGroupsBuilder.put(educationGroupsJson.get(educationGroup.getExternalId()));
        }

        endTime = System.nanoTime();
        duration = endTime - startTime;

        Link link = linkTo(methodOn(EducationGroupsController.class).getGroups(null, userName, versionNumber, resolveTree, resolveResources)).withSelfRel();

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("educationGroups", educationGroupsBuilder);
        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/education-groups/{externalId}",
            method = RequestMethod.GET)
    public Object getGroup(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("externalId") String externalId,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "tree", required = false) boolean resolveTree,
            @RequestParam(value = "resources", required = false) boolean resolveResources) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        EducationGroup rootEducationGroup = null;

        try {
            rootEducationGroup = businessFacade.getEducationGroup(userName, externalId);
            if (rootEducationGroup == null) {
                throw new EntityNotFoundServiceException();
            }
            if (versionNumber == null) {
                versionNumber = businessFacade.getActiveVersionNumber();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        }

        Map<String, JSONObject> educationGroupsJson = new HashMap<String, JSONObject>();
        Tree educationGroupsTree = buildEducationGroupsTree(rootEducationGroup);
        buildEducationGroupsJson(userName, versionNumber, rootEducationGroup, educationGroupsJson, educationGroupsTree, resolveTree, resolveResources);

        endTime = System.nanoTime();
        duration = endTime - startTime;

        Link link = linkTo(methodOn(EducationGroupsController.class).getGroup(null, userName, externalId, versionNumber, resolveTree, resolveResources)).withSelfRel();

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("educationGroup", educationGroupsJson.get(rootEducationGroup.getExternalId()));
        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    private Tree buildEducationGroupsTree(EducationGroup educationGroup) {
        return this.buildEducationGroupsTree(educationGroup, null);
    }

    private Tree buildEducationGroupsTree(EducationGroup educationGroup, Tree tree) {
        Tree result;
        if (tree == null) {
            result = new Tree();
            result.addNode(educationGroup.getExternalId(), null);
        } else {
            result = tree;
        }
        Collection<EducationGroup> children = educationGroup.getChildren();

        for (EducationGroup child : children) {
            result.addNode(child.getExternalId(), educationGroup.getExternalId());
            buildEducationGroupsTree(child, result);
        }
        return result;
    }

    private void buildEducationGroupsJson(
            String userName,
            Long versionNumber,
            EducationGroup educationGroup,
            Map<String, JSONObject> educationGroupsJson,
            Tree educationGroupsTree,
            boolean resolveTree,
            boolean resolveResources) {
        Collection<EducationGroup> children = educationGroup.getChildren();

        // ***** Parent *****
        EducationGroup parentEducationGroup = educationGroup.getParent();
        String parentExternalId = parentEducationGroup != null ? parentEducationGroup.getExternalId() : "";
        String parentName = parentEducationGroup != null ? parentEducationGroup.getName() : "";

        Link link;
        try {
            link = linkTo(methodOn(EducationGroupsController.class).getGroup(null, userName, parentExternalId, versionNumber, resolveTree, resolveResources)).withSelfRel();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        String parentLink = parentEducationGroup != null ? link.getHref() : "";

        JSONObject parentEducationGroupBuilder = new JSONObject();
        parentEducationGroupBuilder.put("externalId", parentExternalId);
        parentEducationGroupBuilder.put("link", parentLink);
        parentEducationGroupBuilder.put("name", parentName);
        // ***** Parent *****

        // ***** Courses *****

        // Course filtering by version.
        Collection<Course> unfilteredCourses = educationGroup.getCourses();
        Collection<Course> courses = new HashSet<Course>();
        for (Course course : unfilteredCourses) {
            if (course.getVersionNumber().equals(versionNumber)) {
                courses.add(course);
            }
        }

        JSONArray coursesBuilder = new JSONArray();

        for (Course course : courses) {
            JSONObject courseBuilder = new JSONObject();

            try {
                link = linkTo(methodOn(CoursesController.class).getCourse(null, userName, course.getExternalId(), versionNumber, resolveTree, resolveResources)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            courseBuilder.put("externalId", course.getExternalId());
            courseBuilder.put("link", link.getHref());
            courseBuilder.put("name", course.getName());
            courseBuilder.put("versionNumber", course.getVersionNumber());

            coursesBuilder.put(courseBuilder);
        }
        // ***** Courses *****

        try {
            link = linkTo(methodOn(EducationGroupsController.class).getGroup(null, userName, educationGroup.getExternalId(), versionNumber, resolveTree, resolveResources)).withSelfRel();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        JSONObject builder = new JSONObject();
        builder.put("externalId", educationGroup.getExternalId());
        builder.put("link", link.getHref());
        builder.put("name", educationGroup.getName());
        builder.put("parent", parentEducationGroupBuilder);
        builder.put("educationGroups", new JSONArray());
        builder.put("courses", coursesBuilder);

        if (!educationGroupsJson.isEmpty()) { // Non-root.
            String parentId = educationGroupsTree.getNodes().get(educationGroup.getExternalId()).getParent(); // parentExternalId
            JSONObject parentBuilder = educationGroupsJson.get(parentId);
            JSONArray parentChildren = parentBuilder.getJSONArray("educationGroups");
            parentChildren.put(builder);
            parentBuilder.put("educationGroups", parentChildren);
        }

        educationGroupsJson.put(educationGroup.getExternalId(), builder);

        for (EducationGroup child : children) {

            // Recursive call.
            buildEducationGroupsJson(userName, versionNumber, child, educationGroupsJson, educationGroupsTree, resolveTree, resolveResources);
        }
    }
}