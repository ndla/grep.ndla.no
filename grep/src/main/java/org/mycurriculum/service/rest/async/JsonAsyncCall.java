package org.mycurriculum.service.rest.async;

import org.json.JSONObject;

public interface JsonAsyncCall extends Runnable {
    public JSONObject getStatus();
}
