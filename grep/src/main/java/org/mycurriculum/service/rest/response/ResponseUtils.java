package org.mycurriculum.service.rest.response;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class ResponseUtils {
    public static void respondWithByteArray(HttpServletResponse response, byte[] data) throws IOException {
        ServletOutputStream stream = response.getOutputStream();
        stream.write(data);
        stream.close();
    }
    public static void respondWithString(HttpServletResponse response, String str, String charsetName) throws IOException {
        response.setCharacterEncoding(charsetName);
        byte[] data = str.getBytes(charsetName);
        respondWithByteArray(response, data);
    }
    public static void respondWithString(HttpServletResponse response, String str) throws IOException {
        try {
            respondWithString(response, str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Missing UTF-8 support", e);
        }
    }
    public static void respondWithJSON(HttpServletResponse response, JSONObject jsonObject) throws IOException {
        response.setContentType("application/json");
        respondWithString(response, jsonObject.toString());
    }
    public static void respondWithJSON(HttpServletResponse response, JSONArray jsonObject) throws IOException {
        response.setContentType("application/json");
        respondWithString(response, jsonObject.toString());
    }
}
