package org.mycurriculum.service.rest.tree;

import java.util.ArrayList;

public class Node {

    private String identifier;
    private ArrayList<String> children;
    private String parent;

    public Node(String identifier) {
        this(identifier, null); // Root node's parent is null.
    }

    // Constructor
    public Node(String identifier, String parent) {
        this.identifier = identifier;
        children = new ArrayList<String>();
        this.parent = parent;
    }

    // Properties
    public String getIdentifier() {
        return identifier;
    }

    public ArrayList<String> getChildren() {
        return children;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    // Public interface
    public void addChild(String identifier) {
        children.add(identifier);
    }
}