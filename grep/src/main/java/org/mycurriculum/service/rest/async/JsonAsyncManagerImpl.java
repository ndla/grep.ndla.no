package org.mycurriculum.service.rest.async;

import org.json.JSONObject;

import java.util.*;

public class JsonAsyncManagerImpl implements JsonAsyncManager {
    private static Object lock = new Object();
    private static long asyncSerial = 0;
    private Map<String,JsonAsyncCall> ongoingCalls = new HashMap<String,JsonAsyncCall>();
    private RemoveQueue removeQueue = new RemoveQueue();

    public String run(JsonAsyncCall async) {
        final JsonAsyncManagerCallWrapper wrapper = new JsonAsyncManagerCallWrapper(async);
        addNewCall(wrapper);
        Thread thread = new Thread() {
            @Override
            public void run() {
                wrapper.run();
            }
        };
        thread.start();
        return wrapper.getId();
    }

    @Override
    public JSONObject getStatus(String id) {
        removeQueue.gc();
        JsonAsyncCall call = ongoingCalls.get(id);
        if (call != null) {
            return call.getStatus();
        }
        return null;
    }

    @Override
    public Collection<String> getAsyncs() {
        removeQueue.gc();
        Collection<String> calls = ongoingCalls.keySet();
        Set<String> output = new HashSet<String>();
        output.addAll(calls);
        return output;
    }

    private void addNewCall(JsonAsyncManagerCallWrapper call) {
        ongoingCalls.put(call.getId(), call);
    }
    private void removeCall(JsonAsyncManagerCallWrapper call) {
        ongoingCalls.remove(call.getId());
    }

    private static String generateId() {
        Date dateObj = new Date();
        synchronized (lock) {
            return "" + dateObj.getTime() + "-" + (asyncSerial++);
        }
    }

    public class JsonAsyncManagerCallWrapper implements JsonAsyncCall {
        private String id;
        private JsonAsyncCall async;
        private volatile boolean finished;
        private volatile JSONObject status;
        private volatile Date started = null;

        public JsonAsyncManagerCallWrapper(String id, JsonAsyncCall async) {
            this.id = id;
            this.async = async;
            this.finished = false;
        }
        public JsonAsyncManagerCallWrapper(JsonAsyncCall async) {
            this(generateId(), async);
        }

        public String getId() {
            return id;
        }

        @Override
        public JSONObject getStatus() {
            JSONObject statusCall = new JSONObject();
            statusCall.put("id", id);
            boolean finished;
            finished = this.finished;
            Date started = this.started;
            if (started != null) {
                statusCall.put("started", started.getTime() / 1000);
            } else {
                statusCall.put("started", false);
            }
            JSONObject status;
            synchronized (this) {
                if (async == null && finished) {
                    statusCall.put("status", this.status);
                    statusCall.put("finished", true);
                    return statusCall;
                }
                status = async.getStatus();
                this.status = status;
                if (finished) {
                    removeQueue.add(this);
                    async = null;
                }
            }
            statusCall.put("status", status);
            statusCall.put("finished", finished);
            return statusCall;
        }

        @Override
        public void run() {
            this.started = new Date();
            async.run();
            this.finished = true;
        }
    }
    private class RemoveQueueItem {
        public Date removeAt;
        public JsonAsyncManagerCallWrapper object;
        public RemoveQueueItem next;
    }
    private class RemoveQueue {
        public final static long TIME_DELAY = 604800000;
        public RemoveQueueItem head = null;
        public RemoveQueueItem tail = null;

        public void add(JsonAsyncManagerCallWrapper object) {
            RemoveQueueItem item = new RemoveQueueItem();
            synchronized (this) {
                Date now = new Date();
                item.removeAt = new Date(now.getTime() + TIME_DELAY);
                item.object = object;
                item.next = null;
                if (head == null) {
                    head = tail = item;
                } else {
                    head.next = item;
                    head = item;
                }
            }
        }
        public JsonAsyncManagerCallWrapper getRemove() {
            Date now = new Date();
            synchronized (this) {
                if (tail != null && now.getTime() >= tail.removeAt.getTime()) {
                    RemoveQueueItem item = tail;
                    tail = tail.next;
                    if (tail == null) {
                        head = null;
                    }
                    return item.object;
                }
            }
            return null;
        }
        public void gc() {
            JsonAsyncManagerCallWrapper item;
            while ((item = getRemove()) != null) {
                removeCall(item);
            }
        }
    }
}
