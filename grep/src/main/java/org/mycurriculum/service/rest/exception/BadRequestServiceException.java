package org.mycurriculum.service.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * User:    Brett Kromkamp from http://www.youprogramming.com
 * Date:    Aug 20, 2014
 */

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad request") // HTTP Error Code: 400
public class BadRequestServiceException extends RuntimeException {

    public BadRequestServiceException() {
        super();
    }

    public BadRequestServiceException(String message) {
        super(message);
    }

    public BadRequestServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestServiceException(Throwable cause) {
        super(cause);
    }
}
