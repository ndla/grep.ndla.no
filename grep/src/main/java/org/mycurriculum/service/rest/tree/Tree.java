package org.mycurriculum.service.rest.tree;

import java.util.ArrayList;
import java.util.HashMap;

public class Tree {

    private final static int ROOT = 0;

    private HashMap<String, Node> nodes;

    public Tree() {
        this.nodes = new HashMap<String, Node>();
    }

    // Properties
    public HashMap<String, Node> getNodes() {
        return nodes;
    }

    // Public interface
    public Node addNode(String identifier) {
        return this.addNode(identifier, null);
    }

    public Node addNode(String identifier, String parent) {
        Node node = new Node(identifier);
        node.setParent(parent);
        nodes.put(identifier, node);

        if (parent != null) {
            nodes.get(parent).addChild(identifier);
        }

        return node;
    }

    public void display(String identifier) {
        this.display(identifier, ROOT);
    }

    public void display(String identifier, int depth) {
        ArrayList<String> children = nodes.get(identifier).getChildren();

        if (depth == ROOT) {
            System.out.println(String.format("%s", nodes.get(identifier).getIdentifier()));
        } else {
            String tabs = String.format("%0" + depth + "d", 0).replace("0", "\t");
            System.out.println(tabs + String.format("%s", nodes.get(identifier).getIdentifier()));
        }
        depth++;
        for (String child : children) {

            // Recursive call
            this.display(child, depth);
        }
    }

}