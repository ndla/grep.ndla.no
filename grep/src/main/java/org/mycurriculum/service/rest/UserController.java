package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BadRequestBusinessException;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.SecurityBusinessException;
import org.mycurriculum.business.domain.Role;
import org.mycurriculum.business.domain.User;
import org.mycurriculum.service.rest.exception.*;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("userController")
@RequestMapping(value = "/v1/users/{userName}")
public class UserController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/users",
            method = RequestMethod.POST)
    public Object createUser(
            @PathVariable("userName") String userName,
            @RequestBody String payload,
            HttpServletResponse response) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        User user = null;
        try {
            user = businessFacade.createUser(userName, payload);
            if (user == null) {
                throw new GeneralServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        }

        Link link = linkTo(methodOn(UserController.class).getUser(null, userName, user.getId())).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", "Ok");
        rootBuilder.put("link", link.getHref());

        response.setStatus(HttpServletResponse.SC_CREATED);
        response.setHeader("Location", link.getHref());

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/users",
            method = RequestMethod.GET)
    public Object getUsers(
            HttpServletResponse response,
            @PathVariable("userName") String userName) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        Collection<User> users = null;
        try {
            users = businessFacade.getUsers(userName);
            if (users == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        }

        JSONArray usersBuilder = new JSONArray();
        for (User user : users) {
            Link link = linkTo(methodOn(AccountController.class).getAccount(null, userName, user.getAccount().getId())).withSelfRel();

            JSONObject accountBuilder = new JSONObject();
            accountBuilder.put("name", user.getAccount().getName());
            accountBuilder.put("link", link.getHref());

            link = linkTo(methodOn(UserController.class).getUser(null, userName, user.getId())).withSelfRel();

            JSONObject userBuilder = new JSONObject();
            userBuilder.put("userName", user.getUsername());
            userBuilder.put("link", link.getHref());
            userBuilder.put("account", accountBuilder);
            usersBuilder.put(userBuilder);
        }

        Link link = linkTo(methodOn(UserController.class).getUsers(null, userName)).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("users", usersBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/users/{id}",
            method = RequestMethod.GET)
    public Object getUser(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") Long id) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        User user = null;
        try {
            user = businessFacade.getUser(userName, id);
            if (user == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (SecurityBusinessException e) {
            throw new SecurityServiceException(e.getMessage(), e);
        }

        Link link = linkTo(methodOn(AccountController.class).getAccount(null, userName, user.getAccount().getId())).withSelfRel();

        JSONObject accountBuilder = new JSONObject();
        accountBuilder.put("name", user.getAccount().getName());
        accountBuilder.put("link", link.getHref());

        JSONArray rolesBuilder = new JSONArray();
        for (Role role : user.getRoles()) {
            rolesBuilder.put(role.getName());
        }

        JSONObject userBuilder = new JSONObject();
        userBuilder.put("id", user.getId());
        userBuilder.put("userName", user.getUsername());
        userBuilder.put("firstName", user.getFirstName());
        userBuilder.put("lastName", user.getLastName());
        userBuilder.put("email", user.getEmail());
        userBuilder.put("isEnabled", user.isEnabled());
        userBuilder.put("roles", rolesBuilder);
        userBuilder.put("account", accountBuilder);

        link = linkTo(methodOn(UserController.class).getUser(null, userName, id)).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("self", link.getHref());
        rootBuilder.put("user", userBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/users/{id}",
            method = RequestMethod.PUT)
    public Object updateUser(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") Long id,
            @RequestBody String payload) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        // TODO: Implement.
        System.out.println(payload);

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/users/{id}",
            method = RequestMethod.DELETE)
    public Object deleteUser(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") Long id) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        // TODO: Implement.

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
}
