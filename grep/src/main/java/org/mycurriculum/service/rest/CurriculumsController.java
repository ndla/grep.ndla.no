package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.service.rest.exception.EntityNotFoundServiceException;
import org.mycurriculum.service.rest.exception.GeneralServiceException;
import org.mycurriculum.service.rest.exception.InvalidUserServiceException;
import org.mycurriculum.service.rest.exception.InvalidVersionServiceException;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.mycurriculum.service.rest.tree.Tree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("curriculumsController")
@RequestMapping(value = "/v1/users/{userName}")
public class CurriculumsController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/curriculums",
            method = RequestMethod.GET)
    public Object getCurriculums(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        JSONArray curriculumsBuilder = new JSONArray();

        Collection<OntopiaCurriculum> curriculums;
        try {
            curriculums = businessFacade.getCurriculums(userName, versionNumber);
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            throw new GeneralServiceException(e.getMessage(), e);
        }

        int count = 0;
        for (OntopiaCurriculum curriculum : curriculums) {
            JSONObject curriculumJson = buildSimpleCurriculumJson(curriculum, userName, versionNumber);
            curriculumsBuilder.put(curriculumJson);
            count++;
        }

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        //rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("curriculums", curriculumsBuilder);
        rootBuilder.put("numberOfItems", count);
        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
    @RequestMapping(
            value = "/curriculums/{id}",
            method = RequestMethod.GET)
    public Object getCurriculum(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") String id,
            @RequestParam(value = "version", required = false) Long versionNumber) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        OntopiaCurriculum curriculum = null;
        try {
            if (versionNumber != null) {
                curriculum = businessFacade.getCurriculum(userName, versionNumber, id);
            } else {
                curriculum = businessFacade.getCurriculum(userName, id);
            }
            if (curriculum == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONObject curriculumBuilder = buildCurriculumJson(curriculum, userName, versionNumber);

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        //rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("curriculum", curriculumBuilder);
        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    private URI getCurriculumUri(String userName, Long versionNumber, OntopiaCurriculum curriculum) {
        String id = curriculum.getHumanId();
        try {
            return linkTo(methodOn(CurriculumsController.class).getCurriculum(null, userName, id, versionNumber)).toUri();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    private JSONObject buildSimpleCurriculumJson(OntopiaCurriculum curriculum, String userName, Long versionNumber) {
        JSONObject curriculumBuilder = new JSONObject();
        curriculumBuilder.put("link", getCurriculumUri(userName, versionNumber, curriculum).toString());
        curriculumBuilder.put("id", curriculum.getHumanId());

        JSONArray curriculumNamesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : curriculum.getNameObjects()) {
            JSONArray curriculumNameScopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                curriculumNameScopesBuilder.put(scope);
            }
            JSONObject curriculumNameBuilder = new JSONObject();
            curriculumNameBuilder.put("name", name.getName());
            curriculumNameBuilder.put("scopes", curriculumNameScopesBuilder);
            curriculumNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
            curriculumNamesBuilder.put(curriculumNameBuilder);
        }
        curriculumBuilder.put("names", curriculumNamesBuilder);
        return curriculumBuilder;
    }
    private JSONObject buildCurriculumJson(OntopiaCurriculum curriculum, String userName, Long versionNumber) {
        JSONArray curriculumPsisBuilder = new JSONArray();
        for (URL curriculumPsi : curriculum.getPsis()) {
            curriculumPsisBuilder.put(curriculumPsi.toString());
        }

        JSONArray curriculumNamesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : curriculum.getNameObjects()) {
            JSONArray curriculumNameScopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                curriculumNameScopesBuilder.put(scope);
            }
            JSONObject curriculumNameBuilder = new JSONObject();
            curriculumNameBuilder.put("name", name.getName());
            curriculumNameBuilder.put("scopes", curriculumNameScopesBuilder);
            curriculumNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
            curriculumNamesBuilder.put(curriculumNameBuilder);
        }

        // ***** Competence Aim Sets *****
        JSONArray competenceAimSetsBuilder = new JSONArray();
        Collection<OntopiaAimSet> competenceAimSets = curriculum.getAimSets();
        for (OntopiaAimSet competenceAimSet : competenceAimSets) {
            Map<String, JSONObject> competenceAimSetsJson = new HashMap<String, JSONObject>();

            Tree competenceAimSetsTree = buildCompetenceAimSetsTree(competenceAimSet);
            buildCompetenceAimSetsJson(userName, versionNumber, competenceAimSet, competenceAimSetsJson, competenceAimSetsTree);

            competenceAimSetsBuilder.put(competenceAimSetsJson.get(competenceAimSet.getHumanId()));
        }
        // ***** Competence Aim Sets *****

        // ***** Links *****
        JSONObject linksBuilder = new JSONObject();
        Collection<OntopiaCurriculumSet> curriculumSets = curriculum.getCurriculumSets();
        JSONArray parentsBuilder = new JSONArray();
        for (OntopiaCurriculumSet curriculumSet : curriculumSets) {
            Link parentLink;
            try {
                parentLink = linkTo(methodOn(CurriculumSetsController.class).getCurriculumSet(null, userName, curriculumSet.getHumanId(), versionNumber)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
            parentsBuilder.put(parentLink.getHref());
        }
        Link selfLink;
        try {
            selfLink = linkTo(methodOn(CurriculumsController.class).getCurriculum(null, userName, curriculum.getHumanId(), versionNumber)).withSelfRel();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        linksBuilder.put("parents", parentsBuilder);
        linksBuilder.put("self", selfLink.getHref());
        // ***** Links *****

        JSONObject curriculumBuilder = new JSONObject();
        curriculumBuilder.put("id", curriculum.getHumanId());
        curriculumBuilder.put("links", linksBuilder);
        curriculumBuilder.put("names", curriculumNamesBuilder);
        curriculumBuilder.put("psis", curriculumPsisBuilder);
        curriculumBuilder.put("competenceAimSets", competenceAimSetsBuilder);
        return curriculumBuilder;
    }

    private Tree buildCompetenceAimSetsTree(OntopiaAimSet competenceAimsSet) {
        return this.buildCompetenceAimSetsTree(competenceAimsSet, null);
    }

    private Tree buildCompetenceAimSetsTree(OntopiaAimSet competenceAimsSet, Tree tree) {
        Tree result;
        if (tree == null) {
            result = new Tree();
            result.addNode(competenceAimsSet.getHumanId(), null);
        } else {
            result = tree;
        }
        Collection<OntopiaAimSet> children = competenceAimsSet.getAimSets();

        for (OntopiaAimSet child : children) {
            result.addNode(child.getHumanId(), competenceAimsSet.getHumanId());
            buildCompetenceAimSetsTree(child, result);
        }
        return result;
    }

    private void buildCompetenceAimSetsJson(
            String userName,
            Long versionNumber,
            OntopiaAimSet competenceAimSet,
            Map<String, JSONObject> competenceAimSetsJson,
            Tree competenceAimSetsTree) {
        Collection<OntopiaAimSet> children = competenceAimSet.getAimSets();

        JSONArray namesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : competenceAimSet.getNameObjects()) {
            JSONObject nameBuilder = new JSONObject();
            JSONArray scopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                scopesBuilder.put(scope);
            }
            nameBuilder.put("name", name.getName());
            nameBuilder.put("scopes", scopesBuilder);
            nameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

            namesBuilder.put(nameBuilder);
        }

        JSONArray psisBuilder = new JSONArray();
        for (URL psi : competenceAimSet.getPsis()) {
            psisBuilder.put(psi.toString());
        }

        JSONArray competenceAimsBuilder = new JSONArray();
        for (OntopiaAim competenceAim : competenceAimSet.getAims()) {

            JSONArray competenceAimNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : competenceAim.getNameObjects()) {
                JSONArray nameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    nameScopesBuilder.put(scope);
                }
                JSONObject competenceAimNameBuilder = new JSONObject();
                competenceAimNameBuilder.put("name", name.getName());
                competenceAimNameBuilder.put("scopes", nameScopesBuilder);
                competenceAimNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                competenceAimNamesBuilder.put(competenceAimNameBuilder);
            }

            /*
            // ***** Parent *****
            Collection<OntopiaAimSet> competenceAimsSet = competenceAim.getAimSets();
            OntopiaAimSet parent = (OntopiaAimSet) competenceAimsSet.toArray()[0];
            Link parentLink = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(userName, parent.getHumanId(), versionNumber)).withSelfRel();
            // ***** Parent *****

            // TODO: Determine if appropriate to pass false as default for the resolving of resources.
            Link link = linkTo(methodOn(CompetenceAimsController.class).getCompetenceAim(userName, competenceAim.getHumanId(), versionNumber, false)).withSelfRel();
            */

            // ***** Links *****
            JSONObject linksBuilder = new JSONObject();
            Collection<OntopiaAimSet> competenceAimsSet = competenceAim.getAimSets();
            //OntopiaAimSet parent = (OntopiaAimSet) competenceAimsSet.toArray()[0];
            //Link parentLink = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(userName, parent.getHumanId(), versionNumber)).withSelfRel();
            JSONArray parentsBuilder = new JSONArray();
            for (OntopiaAimSet cas : competenceAimsSet) {
                Link parentLink;
                try {
                    parentLink = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(null, userName, cas.getHumanId(), versionNumber)).withSelfRel();
                } catch (IOException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
                parentsBuilder.put(parentLink.getHref());
            }
            Link selfLink;
            try {
                selfLink = linkTo(methodOn(CompetenceAimsController.class).getCompetenceAim(null, userName, competenceAim.getHumanId(), versionNumber, false)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
            linksBuilder.put("parents", parentsBuilder);
            linksBuilder.put("self", selfLink.getHref());
            // ***** Links *****

            JSONObject competenceAimBuilder = new JSONObject();
            competenceAimBuilder.put("id", competenceAim.getHumanId());
            competenceAimBuilder.put("links", linksBuilder);
            //competenceAimBuilder.put("parent", parentLink.getHref());
            competenceAimBuilder.put("names", competenceAimNamesBuilder);
            //competenceAimBuilder.put("link", link.getHref());
            competenceAimBuilder.put("sortingOrder", competenceAim.getSortingOrder());

            competenceAimsBuilder.put(competenceAimBuilder);
        }

        JSONArray levelsBuilder = new JSONArray();
        for (OntopiaLevel level : competenceAimSet.getLevels()) {
            JSONArray levelNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : level.getNameObjects()) {
                JSONArray nameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    nameScopesBuilder.put(scope);
                }
                JSONObject levelNameBuilder = new JSONObject();
                levelNameBuilder.put("name", name.getName());
                levelNameBuilder.put("scopes", nameScopesBuilder);
                levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                levelNamesBuilder.put(levelNameBuilder);
            }
            JSONArray levelPsisBuilder = new JSONArray();
            for (URL jsonPsi : level.getPsis()) {
                levelPsisBuilder.put(jsonPsi.toString());
            }

            // TODO: Determine if passing in false for the resolveCourseStructures parameter is the right thing to do.
            Link levelLink;
            try {
                levelLink = linkTo(methodOn(LevelsController.class).getLevel(null, userName, level.getHumanId(), versionNumber, false)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            JSONObject levelBuilder =  new JSONObject();
            levelBuilder.put("id", level.getHumanId());
            levelBuilder.put("names", levelNamesBuilder);
            levelBuilder.put("link", levelLink.getHref());
            levelBuilder.put("psis", levelPsisBuilder);

            levelsBuilder.put(levelBuilder);
        }

        // ***** Links *****
        JSONObject linksBuilder = new JSONObject();
        String curriculumHumanId = competenceAimSet.getHumanId().split("_")[0];
        Link parentLink;
        Link selfLink;
        try {
            parentLink = linkTo(methodOn(CurriculumsController.class).getCurriculum(null, userName, curriculumHumanId, versionNumber)).withSelfRel();
            selfLink = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(null, userName, competenceAimSet.getHumanId(), versionNumber)).withSelfRel();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        linksBuilder.put("parent", parentLink.getHref());
        linksBuilder.put("self", selfLink.getHref());
        // ***** Links *****

        JSONObject builder = new JSONObject();
        builder.put("id", competenceAimSet.getHumanId());
        builder.put("links", linksBuilder);
        builder.put("names", namesBuilder); // TODO: Needs to display appropriate title property.
        builder.put("psis", psisBuilder);
        builder.put("levels", levelsBuilder);
        builder.put("sortingOrder", competenceAimSet.getSortingOrder());
        builder.put("competenceAimSets", new JSONArray());
        builder.put("competenceAims", competenceAimsBuilder);

        if (!competenceAimSetsJson.isEmpty()) { // Non-root.
            String parentHumanId = competenceAimSetsTree.getNodes().get(competenceAimSet.getHumanId()).getParent();
            JSONObject parentBuilder = competenceAimSetsJson.get(parentHumanId);
            JSONArray parentChildren = parentBuilder.getJSONArray("competenceAimSets");
            parentChildren.put(builder);
            parentBuilder.put("competenceAimSets", parentChildren);
        }

        competenceAimSetsJson.put(competenceAimSet.getHumanId(), builder);

        for (OntopiaAimSet child : children) {

            // Recursive call.
            buildCompetenceAimSetsJson(userName, versionNumber, child, competenceAimSetsJson, competenceAimSetsTree);
        }
    }
}
