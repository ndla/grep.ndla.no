package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.service.rest.async.AsyncTaskRunner;
import org.mycurriculum.service.rest.async.JsonAsyncManager;
import org.mycurriculum.service.rest.async.JsonAsyncManagerImpl;
import org.mycurriculum.service.rest.exception.BadRequestServiceException;
import org.mycurriculum.service.rest.exception.EntityNotFoundServiceException;
import org.mycurriculum.service.rest.exception.GeneralServiceException;
import org.mycurriculum.service.rest.exception.SecurityServiceException;
import org.mycurriculum.service.rest.payloads.PayloadConverter;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("grepIndexerController")
@RequestMapping(value = "/v1")
public class GrepImportController {
    private JsonAsyncManager asyncManager = new JsonAsyncManagerImpl();

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/users/{userName}/grepimport/start",
            method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public Object startImport(HttpServletRequest request, HttpServletResponse response, @PathVariable("userName") String userName, @RequestBody String payload) {
        if (request.getContentType().startsWith("application/x-www-form-urlencoded")) {
            payload = PayloadConverter.urlQueryStringToJsonObject(payload, request.getCharacterEncoding()).toString();
        }
        JSONObject jsonPayload = new JSONObject(payload);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedInUsername;
        if (authentication != null) {
            loggedInUsername = authentication.getName();
        } else {
            loggedInUsername = null;
        }
        if (loggedInUsername != null) {
            if (!loggedInUsername.equals(userName)) {
                throw new SecurityServiceException("Authenticated with a different username");
            }
        } else {
            System.out.println("WARNING: Spring security is disabled!");
        }
        JSONObject data = new JSONObject();
        try {
            JSONObject courseMappings;
            if (jsonPayload.has("courseMappings")) {
                Object courseMappingsObject = jsonPayload.get("courseMappings");
                if (courseMappingsObject instanceof JSONObject) {
                    courseMappings = (JSONObject) courseMappingsObject;
                } else if (courseMappingsObject instanceof String) {
                    String str = (String) courseMappingsObject;
                    courseMappings = new JSONObject(str);
                } else
                    throw new BadRequestServiceException("Invalid courseMappings");
            } else {
                courseMappings = null;
            }
            /* Get the importer */
            AsyncTask importer = businessFacade.createGrepImporter(userName, courseMappings);
            /* Start the importer */
            AsyncTaskRunner runner = new AsyncTaskRunner(importer);
            String id = asyncManager.run(runner);
            data.put("id", id);
            Link link = linkTo(methodOn(GrepImportController.class).getStatus(null, id)).withSelfRel();
            data.put("link", link.getHref());
            ResponseUtils.respondWithJSON(response, data);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }
    }

    @RequestMapping(
            value = "/grepimport/{id}",
            method = RequestMethod.GET)
    public Object getStatus(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        JSONObject status = asyncManager.getStatus(id);
        if (status != null) {
            ResponseUtils.respondWithJSON(response, status);
            return null;
        }
        throw new EntityNotFoundServiceException("Running grepimport with this id was not found");
    }

    @RequestMapping(
            value = "/grepimports",
            method = RequestMethod.GET)
    public Object getImports(HttpServletResponse response) throws IOException {
        JSONArray arr = new JSONArray();
        for (String id : asyncManager.getAsyncs()) {
            JSONObject info = new JSONObject();
            info.put("id", id);
            Link link = linkTo(methodOn(GrepImportController.class).getStatus(null, id)).withSelfRel();
            info.put("link", link.getHref());
            arr.put(info);
        }
        ResponseUtils.respondWithJSON(response, arr);
        return null;
    }
}
