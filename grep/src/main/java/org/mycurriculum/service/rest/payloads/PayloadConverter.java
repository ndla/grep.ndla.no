package org.mycurriculum.service.rest.payloads;

import org.json.JSONObject;
import org.mycurriculum.service.rest.exception.BadRequestServiceException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class PayloadConverter {
    public static Map<String,String> urlQueryStringToMap(String payload, String encoding) {
        Map<String,String> map = new HashMap<String,String>();
        try {
            String[] params = payload.split("\\&");
            for (String param : params) {
                String[] kv = param.split("=");
                if (kv.length > 0) {
                    String key;
                    String value;
                    if (kv.length > 1) {
                        key = kv[0];
                        value = kv[1];
                    } else {
                        key = kv[0];
                        value = "";
                    }
                    key = URLDecoder.decode(key, encoding);
                    value = URLDecoder.decode(value, encoding);
                    map.put(key, value);
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        }
        return map;
    }
    public static JSONObject mapToJsonObject(Map<String,String> map) {
        JSONObject payloadGenerator = new JSONObject();
        for (Map.Entry<String,String> mapEntry : map.entrySet()) {
            payloadGenerator.put(mapEntry.getKey(), mapEntry.getValue());
        }
        return payloadGenerator;
    }
    public static JSONObject urlQueryStringToJsonObject(String queryString, String encoding) {
        return mapToJsonObject(urlQueryStringToMap(queryString, encoding));
    }
}
