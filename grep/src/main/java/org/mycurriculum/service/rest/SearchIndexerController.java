package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BadRequestBusinessException;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.service.rest.async.AsyncTaskRunner;
import org.mycurriculum.service.rest.async.JsonAsyncManager;
import org.mycurriculum.service.rest.async.JsonAsyncManagerImpl;
import org.mycurriculum.service.rest.exception.*;
import org.mycurriculum.service.rest.payloads.PayloadConverter;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("searchIndexerController")
@RequestMapping(value = "/v1")
public class SearchIndexerController {
    private JsonAsyncManager asyncManager = new JsonAsyncManagerImpl();

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/searchindexer/types",
            method = RequestMethod.GET
    )
    public Object getIndexerTypes(HttpServletResponse response) throws IOException {
        JSONArray json = new JSONArray();
        Set<String> types = businessFacade.getSearchIndexerTypes();
        for (String type : types) {
            json.put(type);
        }
        ResponseUtils.respondWithJSON(response, json);
        return null;
    }

    @RequestMapping(
            value = "/users/{userName}/searchindexer/start",
            method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public Object startIndexer(HttpServletRequest request, HttpServletResponse response, @PathVariable("userName") String userName, @RequestBody String payload) {
        if (request.getContentType().startsWith("application/x-www-form-urlencoded")) {
            payload = PayloadConverter.urlQueryStringToJsonObject(payload, request.getCharacterEncoding()).toString();
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedInUsername;
        if (authentication != null) {
            loggedInUsername = authentication.getName();
        } else {
            loggedInUsername = null;
        }
        if (loggedInUsername != null) {
            if (!loggedInUsername.equals(userName)) {
                throw new SecurityServiceException("Authenticated with a different username");
            }
        } else {
            System.out.println("WARNING: Spring security is disabled!");
        }
        JSONObject data = new JSONObject();
        try {
            /* Get the searchindexer */
            AsyncTask indexer = businessFacade.createSearchIndexer(userName, payload);
            /* Start the searchindexer */
            AsyncTaskRunner runner = new AsyncTaskRunner(indexer);
            String id = asyncManager.run(runner);
            data.put("id", id);
            Link link = linkTo(methodOn(SearchIndexerController.class).getStatus(null, id)).withSelfRel();
            data.put("link", link.getHref());
            ResponseUtils.respondWithJSON(response, data);
            return null;
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        }
    }

    @RequestMapping(
            value = "/searchindexer/{id}",
            method = RequestMethod.GET)
    public Object getStatus(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        JSONObject status = asyncManager.getStatus(id);
        if (status != null) {
            ResponseUtils.respondWithJSON(response, status);
            return null;
        }
        throw new EntityNotFoundServiceException("Running searchindexer with this id was not found");
    }

    @RequestMapping(
            value = "/searchindexers",
            method = RequestMethod.GET)
    public Object getImports(HttpServletResponse response) throws IOException {
        JSONArray arr = new JSONArray();
        for (String id : asyncManager.getAsyncs()) {
            JSONObject info = new JSONObject();
            info.put("id", id);
            Link link = linkTo(methodOn(SearchIndexerController.class).getStatus(null, id)).withSelfRel();
            info.put("link", link.getHref());
            arr.put(info);
        }
        ResponseUtils.respondWithJSON(response, arr);
        return null;
    }
}
