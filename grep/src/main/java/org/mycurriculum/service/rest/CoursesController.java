package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.*;
import org.mycurriculum.business.domain.Course;
import org.mycurriculum.business.domain.CourseGroup;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.service.rest.exception.*;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.mycurriculum.service.rest.tree.Tree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("courseController")
@RequestMapping(value = "/v1/users/{userName}")
public class CoursesController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/courses",
            method = RequestMethod.POST)
    public Object createCourse(
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestBody String payload,
            HttpServletResponse response) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        Course course = null;
        try {
            if (versionNumber != null) {
                course = businessFacade.createCourse(userName, versionNumber, payload);
            } else {
                course = businessFacade.createCourse(userName, payload);
            }
            if (course == null) {
                throw new GeneralServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new ConflictServiceException(e.getMessage(), e);
        } catch (BadRequestBusinessException e) {
            throw new BadRequestServiceException(e.getMessage(), e);
        } catch (GeneralBusinessException | IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        Link link = linkTo(methodOn(CoursesController.class).getCourse(null, userName, course.getExternalId(), versionNumber, false, false)).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", "Ok");
        rootBuilder.put("link", link.getHref());

        response.setStatus(HttpServletResponse.SC_CREATED);
        response.setHeader("Location", link.getHref());

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/courses",
            method = RequestMethod.GET)
    public Object getCourses(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "education-group", required = false) String educationGroupExternalId,
            @RequestParam(value = "tree", required = false) boolean resolveTree,
            @RequestParam(value = "resources", required = false) boolean resolveResources) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        List<Course> courses = null;
        try {
            if (versionNumber != null) {
                if (educationGroupExternalId != null) {
                    courses = businessFacade.getCoursesForEducationGroup(userName, versionNumber, educationGroupExternalId);
                } else {
                    courses = businessFacade.getCourses(userName, versionNumber);
                }
            } else {
                // Get courses for active version.
                if (educationGroupExternalId != null) {
                    courses = businessFacade.getCoursesForEducationGroup(userName, educationGroupExternalId);
                } else {
                    courses = businessFacade.getCourses(userName);
                }
            }
            if (courses == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONArray coursesBuilder = new JSONArray();
        for (Course course : courses) {
            JSONArray courseStructuresBuilder = new JSONArray();
            for (CourseGroup courseGroup : course.getCourseGroups()) {

                // Resolve course structure (from the Ontopia topics map engine/backing store.
                OntopiaCourseStructure courseStructure = courseGroup.getOntopiaCourseStructure();

                JSONArray courseStructurePsisBuilder = new JSONArray();
                for (URL psi : courseStructure.getPsis()) {
                    courseStructurePsisBuilder.put(psi.toString());
                }

                // ***** Levels *****
                Collection<OntopiaLevel> levels = null;
                try {
                    levels = courseStructure.getLevels();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new GeneralServiceException(e.getMessage(), e);
                }

                JSONArray levelsBuilder = new JSONArray();
                for (OntopiaLevel level : levels) {
                    JSONArray levelNamesBuilder = new JSONArray();
                    for (OntopiaTopic.Name name : level.getNameObjects()) {
                        JSONArray levelNameScopesBuilder = new JSONArray();
                        for (String scope : name.getScopes()) {
                            levelNameScopesBuilder.put(scope);
                        }
                        JSONObject levelNameBuilder = new JSONObject();
                        levelNameBuilder.put("name", name.getName());
                        levelNameBuilder.put("scopes", levelNameScopesBuilder);
                        levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                        levelNamesBuilder.put(levelNameBuilder);
                    }
                    JSONArray levelPsisBuilder = new JSONArray();
                    for (URL psi : level.getPsis()) {
                        levelPsisBuilder.put(psi.toString());
                    }
                    JSONObject levelBuilder = new JSONObject();
                    levelBuilder.put("id", level.getHumanId());
                    levelBuilder.put("names", levelNamesBuilder);
                    levelBuilder.put("psis", levelPsisBuilder);
                    levelsBuilder.put(levelBuilder);
                }
                // ***** Levels *****

                Link link = linkTo(methodOn(CourseStructuresController.class).getCourseStructure(null, userName, courseStructure.getHumanId(), versionNumber)).withSelfRel();

                // ***** Course structure names *****
                JSONArray courseStructureNamesBuilder = new JSONArray();
                for (OntopiaTopic.Name name : courseStructure.getNameObjects()) {
                    JSONArray courseStructureNameScopesBuilder = new JSONArray();
                    for (String scope : name.getScopes()) {
                        courseStructureNameScopesBuilder.put(scope);
                    }

                    JSONObject courseStructureNameBuilder = new JSONObject();
                    courseStructureNameBuilder.put("name", name.getName());
                    courseStructureNameBuilder.put("scopes", courseStructureNameScopesBuilder);
                    courseStructureNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

                    courseStructureNamesBuilder.put(courseStructureNameBuilder);
                }

                String setType = courseStructure.getSetType() != null ? courseStructure.getSetType() : "";

                JSONObject courseStructureBuilder = new JSONObject();
                courseStructureBuilder.put("id", courseStructure.getHumanId());
                courseStructureBuilder.put("link", link.getHref());
                courseStructureBuilder.put("names", courseStructureNamesBuilder);
                courseStructureBuilder.put("setType", setType);
                courseStructureBuilder.put("psis", courseStructurePsisBuilder);
                courseStructureBuilder.put("levels", levelsBuilder);
                //courseStructureBuilder.put("curriculumSets", curriculumSetsBuilder);
                courseStructuresBuilder.put(courseStructureBuilder);
            }

            Link link = linkTo(methodOn(CoursesController.class).getCourse(null, userName, course.getExternalId(), versionNumber, resolveTree, resolveResources)).withSelfRel();

            JSONObject courseBuilder = new JSONObject();
            courseBuilder.put("externalId", course.getExternalId());
            courseBuilder.put("link", link.getHref());
            courseBuilder.put("name", course.getName());
            courseBuilder.put("versionNumber", String.valueOf(course.getVersionNumber()));
            courseBuilder.put("courseStructures", courseStructuresBuilder);
            coursesBuilder.put(courseBuilder);
        }
        Link link = linkTo(methodOn(CoursesController.class).getCourses(null, userName, versionNumber, educationGroupExternalId, resolveTree, resolveResources)).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("courses", coursesBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/courses/{externalId}",
            method = RequestMethod.GET)
    public Object getCourse(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("externalId") String externalId,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "tree", required = false) boolean resolveTree,
            @RequestParam(value = "resources", required = false) boolean resolveResources) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        Course course = null;
        try {
            if (versionNumber != null) {
                course = businessFacade.getCourse(userName, versionNumber, externalId);
            } else {
                // Get course for active version.
                course = businessFacade.getCourse(userName, externalId);
            }
            if (course == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONArray courseStructuresBuilder = new JSONArray();
        for (CourseGroup courseGroup : course.getCourseGroups()) {
            OntopiaCourseStructure rootCourseStructure = courseGroup.getOntopiaCourseStructure();

            // ***** Build course structure tree *****
            Map<String, JSONObject> courseStructuresJson = new HashMap<String, JSONObject>();

            Tree courseStructuresTree = buildCourseStructureTree(rootCourseStructure);
            buildCourseStructuresJson(userName, versionNumber, rootCourseStructure, courseStructuresJson, courseStructuresTree, resolveTree, resolveResources);
            // ***** Build course structure tree *****

            Link link = linkTo(methodOn(CourseStructuresController.class).getCourseStructure(null, userName, rootCourseStructure.getHumanId(), versionNumber)).withSelfRel();

            endTime = System.nanoTime();
            duration = endTime - startTime;
            JSONObject courseStructureBuilder = new JSONObject();
            courseStructureBuilder.put("self", link.getHref());
            courseStructureBuilder.put("processingTime", duration);
            courseStructureBuilder.put("courseStructure", courseStructuresJson.get(rootCourseStructure.getHumanId()));

            courseStructuresBuilder.put(courseStructureBuilder);
        }

        JSONObject courseBuilder = new JSONObject();
        courseBuilder.put("externalId", course.getExternalId());
        courseBuilder.put("name", course.getName());
        courseBuilder.put("versionNumber", String.valueOf(course.getVersionNumber()));
        courseBuilder.put("courseStructures", courseStructuresBuilder);

        Link link = linkTo(methodOn(CoursesController.class).getCourse(null, userName, externalId, versionNumber, resolveTree, resolveResources)).withSelfRel();

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("self", link.getHref());
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("course", courseBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/courses/{externalId}/mappings",
            method = RequestMethod.POST
    )
    public Object addCourseMapping(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("externalId") String externalId,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestBody String payload
    ) throws IOException {
        JsonObject jsonPayload = parsePayload(payload);
        long startTime = System.nanoTime();
        String courseStructureId = jsonPayload.getString("courseStructureId");
        CourseGroup result;
        try {
            if (versionNumber != null) {
                result = businessFacade.addCourseMapping(userName, versionNumber, externalId, courseStructureId);
            } else {
                result = businessFacade.addCourseMapping(userName, externalId, courseStructureId);
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new GeneralServiceException(e.getMessage(), e);
        }

        long endTime = System.nanoTime();
        long duration = endTime - startTime;

        Link link = linkTo(methodOn(CoursesController.class).getCourse(null, userName, externalId, versionNumber, false, false)).withSelfRel();

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", "ok");

        response.setStatus(HttpServletResponse.SC_CREATED);
        response.setHeader("Location", link.getHref());

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }
    @RequestMapping(
            value = "/courses/{externalId}/mappings/{courseStructureId}",
            method = RequestMethod.DELETE
    )
    public Object deleteCourseMapping(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("externalId") String externalId,
            @PathVariable("courseStructureId") String courseStructureId,
            @RequestParam(value = "version", required = false) Long versionNumber
    ) throws IOException {
        long startTime = System.nanoTime();
        try {
            if (versionNumber != null) {
                businessFacade.deleteCourseMapping(userName, versionNumber, externalId, courseStructureId);
            } else {
                businessFacade.deleteCourseMapping(userName, externalId, courseStructureId);
            }
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (ConflictBusinessException e) {
            throw new GeneralServiceException(e.getMessage(), e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        long endTime = System.nanoTime();
        long duration = endTime - startTime;

        Link link = linkTo(methodOn(CoursesController.class).getCourse(null, userName, externalId, versionNumber, false, false)).withSelfRel();

        JSONObject rootBuilder = new JSONObject();
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("status", "ok");

        response.setStatus(HttpServletResponse.SC_OK);
        response.setHeader("Location", link.getHref());

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    private Tree buildCourseStructureTree(OntopiaCourseStructure courseStructure) {
        return this.buildCourseStructuresTree(courseStructure, null);
    }

    private Tree buildCourseStructuresTree(OntopiaCourseStructure courseStructure, Tree tree) {
        Tree result;
        if (tree == null) {
            result = new Tree();
            result.addNode(courseStructure.getHumanId(), null);
        } else {
            result = tree;
        }
        Collection<OntopiaCourseStructure> children = courseStructure.getCourseStructures();

        for (OntopiaCourseStructure child : children) {
            result.addNode(child.getHumanId(), courseStructure.getHumanId());
            buildCourseStructuresTree(child, result);
        }
        return result;
    }

    private void buildCourseStructuresJson(
            String userName,
            Long versionNumber,
            OntopiaCourseStructure courseStructure,
            Map<String, JSONObject> courseStructuresJson,
            Tree courseStructuresTree,
            boolean resolveTree,
            boolean resolveResources) {
        Collection<OntopiaCourseStructure> children = courseStructure.getCourseStructures();

        JSONArray psisBuilder = new JSONArray();
        for (URL psi : courseStructure.getPsis()) {
            psisBuilder.put(psi.toString());
        }

        // ***** Levels *****
        Collection<OntopiaLevel> levels = null;
        try {
            levels = courseStructure.getLevels();
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONArray levelsBuilder = new JSONArray();
        for (OntopiaLevel level : levels) {
            JSONArray levelNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : level.getNameObjects()) {
                JSONArray levelNameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    levelNameScopesBuilder.put(scope);
                }
                JSONObject levelNameBuilder = new JSONObject();
                levelNameBuilder.put("name", name.getName());
                levelNameBuilder.put("scopes", levelNameScopesBuilder);
                levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

                levelNamesBuilder.put(levelNameBuilder);
            }

            JSONArray levelPsisBuilder = new JSONArray();
            for (URL psi : level.getPsis()) {
                levelPsisBuilder.put(psi.toString());
            }

            JSONObject levelBuilder = new JSONObject();
            levelBuilder.put("id", level.getHumanId());
            levelBuilder.put("names", levelNamesBuilder);
            levelBuilder.put("psis", levelPsisBuilder);

            levelsBuilder.put(levelBuilder);

        }
        // ***** Levels *****

        // ***** Curriculum sets *****
        Collection<OntopiaCurriculumSet> curriculumSets = courseStructure.getCurriculumSets();
        JSONArray curriculumSetsBuilder = new JSONArray();
        for (OntopiaCurriculumSet curriculumSet : curriculumSets) {

            // PSIs
            JSONArray curriculumSetPsisBuilder = new JSONArray();
            for (URL curriculumSetPsi : curriculumSet.getPsis()) {
                curriculumSetPsisBuilder.put(curriculumSetPsi.toString());
            }

            // Names
            JSONArray curriculumSetNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : curriculumSet.getNameObjects()) {
                JSONArray curriculumSetNameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    curriculumSetNameScopesBuilder.put(scope);
                }

                JSONObject curriculumSetNameBuilder = new JSONObject();
                curriculumSetNameBuilder.put("name", name.getName());
                curriculumSetNameBuilder.put("scopes", curriculumSetNameScopesBuilder);
                curriculumSetNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

                curriculumSetNamesBuilder.put(curriculumSetNameBuilder);
            }

            // ***** Resolve full tree structure *****
            JSONArray curriculumsBuilder = new JSONArray();
            if (resolveTree) {
                // ***** Curriculums *****
                Collection<OntopiaCurriculum> curriculums = curriculumSet.getCurricula();

                for (OntopiaCurriculum curriculum : curriculums) {
                    JSONArray curriculumPsisBuilder = new JSONArray();
                    for (URL curriculumPsi : curriculum.getPsis()) {
                        curriculumPsisBuilder.put(curriculumPsi.toString());
                    }

                    JSONArray curriculumNamesBuilder = new JSONArray();
                    for (OntopiaTopic.Name name : curriculum.getNameObjects()) {
                        JSONArray curriculumNameScopesBuilder = new JSONArray();
                        for (String scope : name.getScopes()) {
                            curriculumNameScopesBuilder.put(scope);
                        }
                        JSONObject curriculumNameBuilder = new JSONObject();
                        curriculumNameBuilder.put("name", name.getName());
                        curriculumNameBuilder.put("scopes", curriculumNameScopesBuilder);
                        curriculumNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                        curriculumNamesBuilder.put(curriculumNameBuilder);
                    }
                    Link link;
                    try {
                        link = linkTo(methodOn(CurriculumsController.class).getCurriculum(null, userName, curriculum.getHumanId(), versionNumber)).withSelfRel();
                    } catch (IOException e) {
                        throw new RuntimeException(e.getMessage(), e);
                    }

                    // ***** Competence aim sets *****
                    JSONArray competenceAimSetsBuilder = new JSONArray();
                    Collection<OntopiaAimSet> competenceAimSets = curriculum.getAimSets();
                    for (OntopiaAimSet competenceAimSet : competenceAimSets) {

                        Map<String, JSONObject> competenceAimSetsJson = new HashMap<String, JSONObject>();

                        Tree competenceAimSetsTree = buildCompetenceAimSetsTree(competenceAimSet);
                        buildCompetenceAimSetsJson(userName, versionNumber, competenceAimSet, competenceAimSetsJson, competenceAimSetsTree, resolveResources);

                        try {
                            link = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(null, userName, competenceAimSet.getHumanId(), versionNumber)).withSelfRel();
                        } catch (IOException e) {
                            throw new RuntimeException(e.getMessage(), e);
                        }

                        JSONObject competenceAimSetBuilder = new JSONObject();
                        competenceAimSetBuilder.put("self", link.getHref());
                        competenceAimSetBuilder.put("competenceAimSet", competenceAimSetsJson.get(competenceAimSet.getHumanId()));

                        competenceAimSetsBuilder.put(competenceAimSetBuilder);
                    }
                    // ***** Competence aim sets *****

                    JSONObject curriculumBuilder = new JSONObject();
                    curriculumBuilder.put("id", curriculum.getHumanId());
                    curriculumBuilder.put("link", link.getHref());
                    curriculumBuilder.put("names", curriculumNamesBuilder);
                    curriculumBuilder.put("psis", curriculumPsisBuilder);
                    curriculumBuilder.put("competenceAimSets", competenceAimSetsBuilder);
                    curriculumsBuilder.put(curriculumBuilder);
                }
                // ***** Curriculums *****
            }
            // ***** Resolve full tree structure *****

            // Link
            Link link;
            try {
                link = linkTo(methodOn(CurriculumSetsController.class).getCurriculumSet(null, userName, curriculumSet.getHumanId(), versionNumber)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            // Curriculum set
            String setType = curriculumSet.getSetType() != null ? curriculumSet.getSetType() : "";

            JSONObject curriculumSetBuilder = new JSONObject();
            curriculumSetBuilder.put("id", curriculumSet.getHumanId());
            curriculumSetBuilder.put("link", link.getHref());
            curriculumSetBuilder.put("setType", setType);
            curriculumSetBuilder.put("names", curriculumSetNamesBuilder);
            curriculumSetBuilder.put("psis", curriculumSetPsisBuilder);
            curriculumSetBuilder.put("curriculums", curriculumsBuilder);

            curriculumSetsBuilder.put(curriculumSetBuilder);
        }
        // ***** Curriculum sets *****

        // ***** Names *****
        JSONArray courseStructureNamesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : courseStructure.getNameObjects()) {
            JSONArray courseStructureNameScopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                courseStructureNameScopesBuilder.put(scope);
            }

            JSONObject courseStructureNameBuilder = new JSONObject();
            courseStructureNameBuilder.put("name", name.getName());
            courseStructureNameBuilder.put("scopes", courseStructureNameScopesBuilder);
            courseStructureNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

            courseStructureNamesBuilder.put(courseStructureNameBuilder);
        }

        Link link;
        try {
            link = linkTo(methodOn(CourseStructuresController.class).getCourseStructure(null, userName, courseStructure.getHumanId(), versionNumber)).withSelfRel();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        String setType = courseStructure.getSetType() != null ? courseStructure.getSetType() : "";

        JSONObject builder = new JSONObject();
        builder.put("id", courseStructure.getHumanId());
        builder.put("link", link.getHref());
        builder.put("names", courseStructureNamesBuilder);
        builder.put("setType", setType);
        builder.put("psis", psisBuilder);
        builder.put("levels", levelsBuilder);
        builder.put("curriculumSets", curriculumSetsBuilder);
        builder.put("courseStructures", new JSONArray());

        if (!courseStructuresJson.isEmpty()) { // Non-root.
            String parentHumanId = courseStructuresTree.getNodes().get(courseStructure.getHumanId()).getParent();
            JSONObject parentBuilder = courseStructuresJson.get(parentHumanId);
            JSONArray parentChildren = parentBuilder.getJSONArray("courseStructures");
            parentChildren.put(builder);
            parentBuilder.put("courseStructures", parentChildren);
        }

        courseStructuresJson.put(courseStructure.getHumanId(), builder);

        for (OntopiaCourseStructure child : children) {

            // Recursive call.
            buildCourseStructuresJson(userName, versionNumber, child, courseStructuresJson, courseStructuresTree, resolveTree, resolveResources);
        }
    }

    private Tree buildCompetenceAimSetsTree(OntopiaAimSet competenceAimsSet) {
        return this.buildCompetenceAimSetsTree(competenceAimsSet, null);
    }

    private Tree buildCompetenceAimSetsTree(OntopiaAimSet competenceAimsSet, Tree tree) {
        Tree result;
        if (tree == null) {
            result = new Tree();
            result.addNode(competenceAimsSet.getHumanId(), null);
        } else {
            result = tree;
        }
        Collection<OntopiaAimSet> children = competenceAimsSet.getAimSets();

        for (OntopiaAimSet child : children) {
            result.addNode(child.getHumanId(), competenceAimsSet.getHumanId());
            buildCompetenceAimSetsTree(child, result);
        }
        return result;
    }

    private void buildCompetenceAimSetsJson(
            String userName,
            Long versionNumber,
            OntopiaAimSet competenceAimSet,
            Map<String, JSONObject> competenceAimSetsJson,
            Tree competenceAimSetsTree,
            boolean resolveResources) {
        Collection<OntopiaAimSet> children = competenceAimSet.getAimSets();

        JSONArray namesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : competenceAimSet.getNameObjects()) {
            JSONObject nameBuilder = new JSONObject();
            JSONArray scopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                scopesBuilder.put(scope);
            }
            nameBuilder.put("name", name.getName());
            nameBuilder.put("scopes", scopesBuilder);
            nameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

            namesBuilder.put(nameBuilder);
        }

        JSONArray psisBuilder = new JSONArray();
        for (URL psi : competenceAimSet.getPsis()) {
            psisBuilder.put(psi.toString());
        }

        JSONArray competenceAimsBuilder = new JSONArray();
        for (OntopiaAim competenceAim : competenceAimSet.getAims()) {

            JSONArray resourcesBuilder = new JSONArray();
            if (resolveResources) {
                // ***** Resources *****
                Collection<OntopiaResource> resources = null;
                try {
                    resources = competenceAim.getResources();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new GeneralServiceException(e.getMessage(), e);
                }

                for (OntopiaResource resource : resources) {
                    JSONArray resourceNamesBuilder = new JSONArray();
                    for (OntopiaTopic.Name name : resource.getNameObjects()) {
                        JSONArray nameScopesBuilder = new JSONArray();
                        for (String scope : name.getScopes()) {
                            nameScopesBuilder.put(scope);
                        }
                        JSONObject resourceNameBuilder = new JSONObject();
                        resourceNameBuilder.put("name", name.getName());
                        resourceNameBuilder.put("scopes", nameScopesBuilder);
                        resourceNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                        resourceNamesBuilder.put(resourceNameBuilder);
                    }
                    JSONArray resourcePsisBuilder = new JSONArray();
                    for (URL jsonPsi : resource.getPsis()) {
                        resourcePsisBuilder.put(jsonPsi.toString());
                    }

                    // TODO: Determine if appropriate to pass false as default for the resolving of competence aims.
                    Link link;
                    try {
                        link = linkTo(methodOn(ResourcesController.class).getResource(null, userName, resource.getPsi().toString(), versionNumber, false)).withSelfRel();
                    } catch (IOException e) {
                        throw new RuntimeException(e.getMessage(), e);
                    }

                    JSONObject resourceBuilder = new JSONObject();
                    resourceBuilder.put("names", resourceNamesBuilder);
                    resourceBuilder.put("link", link.getHref());
                    resourceBuilder.put("psis", resourcePsisBuilder);
                    resourcesBuilder.put(resourceBuilder);
                }
                // ***** Resources *****
            }

            JSONArray competenceAimNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : competenceAim.getNameObjects()) {
                JSONArray nameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    nameScopesBuilder.put(scope);
                }
                JSONObject competenceAimNameBuilder = new JSONObject();
                competenceAimNameBuilder.put("name", name.getName());
                competenceAimNameBuilder.put("scopes", nameScopesBuilder);
                competenceAimNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                competenceAimNamesBuilder.put(competenceAimNameBuilder);
            }
            JSONArray competenceAimPsisBuilder = new JSONArray();
            for (URL psi : competenceAim.getPsis()) {
                competenceAimPsisBuilder.put(psi.toString());
            }

            JSONObject competenceAimBuilder = new JSONObject();
            competenceAimBuilder.put("id", competenceAim.getHumanId());
            competenceAimBuilder.put("names", competenceAimNamesBuilder);
            competenceAimBuilder.put("resources", resourcesBuilder);
            competenceAimBuilder.put("psis", competenceAimPsisBuilder);

            Link link;
            try {
                link = linkTo(methodOn(CompetenceAimsController.class).getCompetenceAim(null, userName, competenceAim.getHumanId(), versionNumber, resolveResources)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            JSONObject rootBuilder = new JSONObject();
            rootBuilder.put("self", link.getHref());
            rootBuilder.put("competenceAim", competenceAimBuilder);

            competenceAimsBuilder.put(competenceAimBuilder);
        }

        JSONArray levelsBuilder = new JSONArray();
        for (OntopiaLevel level : competenceAimSet.getLevels()) {
            JSONArray levelNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : level.getNameObjects()) {
                JSONArray nameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    nameScopesBuilder.put(scope);
                }
                JSONObject levelNameBuilder = new JSONObject();
                levelNameBuilder.put("name", name.getName());
                levelNameBuilder.put("scopes", nameScopesBuilder);
                levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());
                levelNamesBuilder.put(levelNameBuilder);
            }
            JSONArray levelPsisBuilder = new JSONArray();
            for (URL jsonPsi : level.getPsis()) {
                levelPsisBuilder.put(jsonPsi.toString());
            }

            // TODO: Determine if passing in false for the resolveCourseStructures parameter is the right thing to do.
            Link link;
            try {
                link = linkTo(methodOn(LevelsController.class).getLevel(null, userName, level.getHumanId(), versionNumber, false)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            JSONObject levelBuilder =  new JSONObject();
            levelBuilder.put("id", level.getHumanId());
            levelBuilder.put("names", levelNamesBuilder);
            levelBuilder.put("link", link.getHref());
            levelBuilder.put("psis", levelPsisBuilder);

            levelsBuilder.put(levelBuilder);
        }

        Link link;
        try {
            link = linkTo(methodOn(CompetenceAimSetsController.class).getCompetenceAimSet(null, userName, competenceAimSet.getHumanId(), versionNumber)).withSelfRel();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        JSONObject builder = new JSONObject();
        builder.put("id", competenceAimSet.getHumanId());
        builder.put("link", link.getHref());
        builder.put("names", namesBuilder);
        builder.put("psis", psisBuilder);
        builder.put("levels", levelsBuilder);
        builder.put("sortingOrder", competenceAimSet.getSortingOrder());
        builder.put("competenceAimSets", new JSONArray());
        builder.put("competenceAims", competenceAimsBuilder);

        if (!competenceAimSetsJson.isEmpty()) { // Non-root.
            String parentHumanId = competenceAimSetsTree.getNodes().get(competenceAimSet.getHumanId()).getParent();
            JSONObject parentBuilder = competenceAimSetsJson.get(parentHumanId);
            JSONArray parentChildren = parentBuilder.getJSONArray("competenceAimSets");
            parentChildren.put(builder);
            parentBuilder.put("competenceAimSets", parentChildren);
        }

        competenceAimSetsJson.put(competenceAimSet.getHumanId(), builder);

        for (OntopiaAimSet child : children) {

            // Recursive call.
            buildCompetenceAimSetsJson(userName, versionNumber, child, competenceAimSetsJson, competenceAimSetsTree, resolveResources);
        }
    }
    private static JsonObject parsePayload(String payload) {
        InputStream stream = new ByteArrayInputStream(payload.getBytes(StandardCharsets.UTF_8));
        JsonObject result;
        try (JsonReader jsonReader = Json.createReader(stream)) {
            result = jsonReader.readObject();
        }
        return result;
    }
}
