package org.mycurriculum.service.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "Invalid user") // HTTP Error Code: 406
public class InvalidUserServiceException extends RuntimeException {

    public InvalidUserServiceException() {
        super();
    }

    public InvalidUserServiceException(String message) {
        super(message);
    }

    public InvalidUserServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidUserServiceException(Throwable cause) {
        super(cause);
    }
}