package org.mycurriculum.service.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.api.BusinessFacade;
import org.mycurriculum.business.api.InvalidVersionBusinessException;
import org.mycurriculum.data.tms.abstracts.OntopiaTopic;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaCurriculumSet;
import org.mycurriculum.data.tms.domain.OntopiaLevel;
import org.mycurriculum.service.rest.exception.EntityNotFoundServiceException;
import org.mycurriculum.service.rest.exception.GeneralServiceException;
import org.mycurriculum.service.rest.exception.InvalidUserServiceException;
import org.mycurriculum.service.rest.exception.InvalidVersionServiceException;
import org.mycurriculum.service.rest.response.ResponseUtils;
import org.mycurriculum.service.rest.tree.Tree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller("levelsController")
@RequestMapping(value = "/v1/users/{userName}")
public class LevelsController {

    @Autowired
    BusinessFacade businessFacade;

    @RequestMapping(
            value = "/levels",
            method = RequestMethod.GET)
    public Object getLevels(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "course-structures", required = false) boolean resolveCourseStructures) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        Collection<OntopiaLevel> levels = null;
        try {
            if (versionNumber != null) {
                levels = businessFacade.getLevels(userName, versionNumber);
            } else {
                levels = businessFacade.getLevels(userName);
            }
            if (levels == null || levels.isEmpty()) { // TODO: Check logic.
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONArray levelsBuilder = new JSONArray();
        for (OntopiaLevel level : levels) {

            /*
            Collection<OntopiaCourseStructure> courseStructures = null;
            if (resolveCourseStructures == true) {
                try {
                    courseStructures = level.getCourseStructures();
                } catch (NullPointerException e) {
                    throw new EntityNotFoundServiceException();
                } catch (IOException e) {
                    throw new GeneralServiceException();
                }
            }
            */

            JSONArray levelNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : level.getNameObjects()) {
                JSONArray nameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    nameScopesBuilder.put(scope);
                }
                JSONObject levelNameBuilder = new JSONObject();
                levelNameBuilder.put("name", name.getName());
                levelNameBuilder.put("scopes", nameScopesBuilder);
                levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

                levelNamesBuilder.put(levelNameBuilder);
            }
            JSONArray levelPsisBuilder = new JSONArray();
            for (URL jsonPsi : level.getPsis()) {
                levelPsisBuilder.put(jsonPsi.toString());
            }

            // ***** Links *****
            JSONObject linksBuilder = new JSONObject();
            Link selfLink = linkTo(methodOn(LevelsController.class).getLevel(null, userName, level.getHumanId(), versionNumber, resolveCourseStructures)).withSelfRel();
            linksBuilder.put("self", selfLink.getHref());
            // ***** Links *****

            JSONObject levelBuilder = new JSONObject();
            levelBuilder.put("id", level.getHumanId());
            levelBuilder.put("links", linksBuilder);
            levelBuilder.put("names", levelNamesBuilder);
            levelBuilder.put("psis", levelPsisBuilder);

            levelsBuilder.put(levelBuilder);
        }

        // ***** Links *****
        /*
        JSONObject linksBuilder = new JSONObject();
        Link selfLink = linkTo(methodOn(LevelsController.class).getLevels(userName, versionNumber, resolveCourseStructures)).withSelfRel();
        linksBuilder.put("self", selfLink.getHref());
        */
        // ***** Links *****

        endTime = System.nanoTime();
        duration = endTime - startTime;
        JSONObject rootBuilder = new JSONObject();
        //rootBuilder.put("links", linksBuilder);
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("levels", levelsBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    @RequestMapping(
            value = "/levels/{id}",
            method = RequestMethod.GET)
    public Object getLevel(
            HttpServletResponse response,
            @PathVariable("userName") String userName,
            @PathVariable("id") String id,
            @RequestParam(value = "version", required = false) Long versionNumber,
            @RequestParam(value = "course-structures", required = false) boolean resolveCourseStructures) throws IOException {
        long endTime;
        long duration;
        long startTime = System.nanoTime();

        OntopiaLevel level = null;
        try {
            if (versionNumber != null) {
                level = businessFacade.getLevel(userName, versionNumber, id);
            } else {
                level = businessFacade.getLevel(userName, id);
            }
            if (level == null) {
                throw new EntityNotFoundServiceException();
            }
        } catch (UsernameNotFoundException e) {
            throw new InvalidUserServiceException(e.getMessage(), e);
        } catch (NullPointerException e) {
            throw new EntityNotFoundServiceException(e.getMessage(), e);
        } catch (InvalidVersionBusinessException e) {
            throw new InvalidVersionServiceException(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException(e.getMessage(), e);
        }

        JSONArray courseStructuresBuilder = new JSONArray();
        if (resolveCourseStructures == true) {
            Collection<OntopiaCourseStructure> courseStructures = null;
            try {
                courseStructures = level.getCourseStructures();

                for (OntopiaCourseStructure courseStructure : courseStructures) {
                    // ***** Build course structure tree *****
                    Map<String, JSONObject> courseStructuresJson = new HashMap<String, JSONObject>();

                    Tree courseStructuresTree = buildCourseStructureTree(courseStructure);
                    buildCourseStructuresJson(userName, versionNumber, courseStructure, courseStructuresJson, courseStructuresTree);
                    // ***** Build course structure tree *****

                    Link link = linkTo(methodOn(CourseStructuresController.class).getCourseStructure(null, userName, courseStructure.getHumanId(), versionNumber)).withSelfRel();

                    JSONObject courseStructureBuilder = new JSONObject();
                    courseStructureBuilder.put("self", link.getHref());
                    courseStructureBuilder.put("courseStructure", courseStructuresJson.get(courseStructure.getHumanId()));

                    courseStructuresBuilder.put(courseStructureBuilder);
                }
            } catch (NullPointerException e) {
                throw new EntityNotFoundServiceException(e.getMessage(), e);
            } catch (IOException e) {
                e.printStackTrace();
                throw new GeneralServiceException(e.getMessage(), e);
            }
        }

        JSONArray levelNamesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : level.getNameObjects()) {
            JSONArray nameScopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                nameScopesBuilder.put(scope);
            }
            JSONObject levelNameBuilder = new JSONObject();
            levelNameBuilder.put("name", name.getName());
            levelNameBuilder.put("scopes", nameScopesBuilder);
            levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

            levelNamesBuilder.put(levelNameBuilder);
        }
        JSONArray levelPsisBuilder = new JSONArray();
        for (URL jsonPsi : level.getPsis()) {
            levelPsisBuilder.put(jsonPsi.toString());
        }

        // ***** Links *****
        JSONObject linksBuilder = new JSONObject();
        Link selfLink = linkTo(methodOn(LevelsController.class).getLevel(null, userName, level.getHumanId(), versionNumber, resolveCourseStructures)).withSelfRel();
        linksBuilder.put("self", selfLink.getHref());
        // ***** Links *****

        JSONObject levelBuilder = new JSONObject();
        levelBuilder.put("id", level.getHumanId());
        levelBuilder.put("links", linksBuilder);
        levelBuilder.put("names", levelNamesBuilder);
        levelBuilder.put("courseStructures", courseStructuresBuilder);
        levelBuilder.put("psis", levelPsisBuilder);

        endTime = System.nanoTime();
        duration = endTime - startTime;

        JSONObject rootBuilder = new JSONObject();
        //rootBuilder.put("links", linksBuilder);
        rootBuilder.put("processingTime", duration);
        rootBuilder.put("level", levelBuilder);

        ResponseUtils.respondWithJSON(response, rootBuilder);
        return null;
    }

    private Tree buildCourseStructureTree(OntopiaCourseStructure courseStructure) {
        return this.buildCourseStructuresTree(courseStructure, null);
    }

    private Tree buildCourseStructuresTree(OntopiaCourseStructure courseStructure, Tree tree) {
        Tree result;
        if (tree == null) {
            result = new Tree();
            result.addNode(courseStructure.getHumanId(), null);
        } else {
            result = tree;
        }
        Collection<OntopiaCourseStructure> children = courseStructure.getCourseStructures();

        for (OntopiaCourseStructure child : children) {
            result.addNode(child.getHumanId(), courseStructure.getHumanId());
            buildCourseStructuresTree(child, result);
        }
        return result;
    }

    private void buildCourseStructuresJson(
            String userName,
            Long versionNumber,
            OntopiaCourseStructure courseStructure,
            Map<String, JSONObject> courseStructuresJson,
            Tree courseStructuresTree) {
        Collection<OntopiaCourseStructure> children = courseStructure.getCourseStructures();

        JSONArray psisBuilder = new JSONArray();
        for (URL psi : courseStructure.getPsis()) {
            psisBuilder.put(psi.toString());
        }

        // ***** Levels *****
        Collection<OntopiaLevel> levels = null;
        try {
            levels = courseStructure.getLevels();
        } catch (IOException e) {
            e.printStackTrace();
            throw new GeneralServiceException();
        }

        JSONArray levelsBuilder = new JSONArray();
        for (OntopiaLevel level : levels) {
            JSONArray levelNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : level.getNameObjects()) {
                JSONArray levelNameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    levelNameScopesBuilder.put(scope);
                }
                JSONObject levelNameBuilder = new JSONObject();
                levelNameBuilder.put("name", name.getName());
                levelNameBuilder.put("scopes", levelNameScopesBuilder);
                levelNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

                levelNamesBuilder.put(levelNameBuilder);
            }

            JSONArray levelPsisBuilder = new JSONArray();
            for (URL psi : level.getPsis()) {
                levelPsisBuilder.put(psi.toString());
            }

            // ***** Links *****
            JSONObject linksBuilder = new JSONObject();
            Link selfLink;
            try {
                selfLink = linkTo(methodOn(LevelsController.class).getLevel(null, userName, level.getHumanId(), versionNumber, false)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
            linksBuilder.put("self", selfLink.getHref());
            // ***** Links *****

            JSONObject levelBuilder = new JSONObject();
            levelBuilder.put("id", level.getHumanId());
            levelBuilder.put("links", linksBuilder);
            levelBuilder.put("names", levelNamesBuilder);
            levelBuilder.put("psis", levelPsisBuilder);

            levelsBuilder.put(levelBuilder);
        }
        // ***** Levels *****

        // ***** Curriculum sets *****
        Collection<OntopiaCurriculumSet> curriculumSets = courseStructure.getCurriculumSets();
        JSONArray curriculumSetsBuilder = new JSONArray();
        for (OntopiaCurriculumSet curriculumSet : curriculumSets) {

            // PSIs
            JSONArray curriculumSetPsisBuilder = new JSONArray();
            for (URL curriculumSetPsi : curriculumSet.getPsis()) {
                curriculumSetPsisBuilder.put(curriculumSetPsi.toString());
            }

            // Names
            JSONArray curriculumSetNamesBuilder = new JSONArray();
            for (OntopiaTopic.Name name : curriculumSet.getNameObjects()) {
                JSONArray curriculumSetNameScopesBuilder = new JSONArray();
                for (String scope : name.getScopes()) {
                    curriculumSetNameScopesBuilder.put(scope);
                }

                JSONObject curriculumSetNameBuilder = new JSONObject();
                curriculumSetNameBuilder.put("name", name.getName());
                curriculumSetNameBuilder.put("scopes", curriculumSetNameScopesBuilder);
                curriculumSetNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

                curriculumSetNamesBuilder.put(curriculumSetNameBuilder);
            }

            // ***** Links *****
            JSONObject linksBuilder = new JSONObject();
            Link selfLink;
            try {
                selfLink = linkTo(methodOn(CurriculumSetsController.class).getCurriculumSet(null, userName, curriculumSet.getHumanId(), versionNumber)).withSelfRel();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
            linksBuilder.put("self", selfLink.getHref());
            // ***** Links *****

            // Curriculum set
            JSONObject curriculumSetBuilder = new JSONObject();
            curriculumSetBuilder.put("id", curriculumSet.getHumanId());
            curriculumSetBuilder.put("links", linksBuilder);
            curriculumSetBuilder.put("names", curriculumSetNamesBuilder);
            curriculumSetBuilder.put("psis", curriculumSetPsisBuilder);

            curriculumSetsBuilder.put(curriculumSetBuilder);
        }
        // ***** Curriculum sets *****

        // ***** Names *****
        JSONArray courseStructureNamesBuilder = new JSONArray();
        for (OntopiaTopic.Name name : courseStructure.getNameObjects()) {
            JSONArray courseStructureNameScopesBuilder = new JSONArray();
            for (String scope : name.getScopes()) {
                courseStructureNameScopesBuilder.put(scope);
            }

            JSONObject courseStructureNameBuilder = new JSONObject();
            courseStructureNameBuilder.put("name", name.getName());
            courseStructureNameBuilder.put("scopes", courseStructureNameScopesBuilder);
            courseStructureNameBuilder.put("isLanguageNeutral", name.isLanguageNeutral());

            courseStructureNamesBuilder.put(courseStructureNameBuilder);
        }
        // ***** Names *****

        // ***** Links *****
        JSONObject linksBuilder = new JSONObject();
        Link selfLink;
        try {
            selfLink = linkTo(methodOn(CourseStructuresController.class).getCourseStructure(null, userName, courseStructure.getHumanId(), versionNumber)).withSelfRel();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        linksBuilder.put("self", selfLink.getHref());
        // ***** Links *****

        String setType = courseStructure.getSetType() != null ? courseStructure.getSetType() : "";

        JSONObject builder = new JSONObject();
        builder.put("id", courseStructure.getHumanId());
        builder.put("links", linksBuilder);
        builder.put("names", courseStructureNamesBuilder);
        builder.put("setType", setType);
        builder.put("psis", psisBuilder);
        builder.put("levels", levelsBuilder);
        builder.put("curriculumSets", curriculumSetsBuilder);
        builder.put("courseStructures", new JSONArray());

        if (!courseStructuresJson.isEmpty()) { // Non-root.
            String parentHumanId = courseStructuresTree.getNodes().get(courseStructure.getHumanId()).getParent();
            JSONObject parentBuilder = courseStructuresJson.get(parentHumanId);
            JSONArray parentChildren = parentBuilder.getJSONArray("courseStructures");
            parentChildren.put(builder);
            parentBuilder.put("courseStructures", parentChildren);
        }

        courseStructuresJson.put(courseStructure.getHumanId(), builder);

        for (OntopiaCourseStructure child : children) {

            // Recursive call.
            buildCourseStructuresJson(userName, versionNumber, child, courseStructuresJson, courseStructuresTree);
        }
    }
}
