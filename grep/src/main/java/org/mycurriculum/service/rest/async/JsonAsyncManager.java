package org.mycurriculum.service.rest.async;

import org.json.JSONObject;

import java.util.Collection;

public interface JsonAsyncManager {
    /**
     *
     * @param async
     * @return Id of the async operation.
     */
    public String run(JsonAsyncCall async);
    public JSONObject getStatus(String id);
    public Collection<String> getAsyncs();
}
