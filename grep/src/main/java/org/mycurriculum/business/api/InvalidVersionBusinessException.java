package org.mycurriculum.business.api;

public class InvalidVersionBusinessException extends Exception {

    public InvalidVersionBusinessException() {
        super();
    }

    public InvalidVersionBusinessException(String message) {
        super(message);
    }

    public InvalidVersionBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidVersionBusinessException(Throwable cause) {
        super(cause);
    }
}
