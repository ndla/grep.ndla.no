package org.mycurriculum.business.api;

/**
 * User:    Brett Kromkamp from http://www.youprogramming.com
 * Date:    Aug 20, 2014
 */

public class ConflictBusinessException extends Exception {

    public ConflictBusinessException() {
        super();
    }

    public ConflictBusinessException(String message) {
        super(message);
    }

    public ConflictBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflictBusinessException(Throwable cause) {
        super(cause);
    }
}
