package org.mycurriculum.business.api;

/**
 * User:    Brett Kromkamp from http://www.youprogramming.com
 * Date:    Aug 20, 2014
 */

public class BadRequestBusinessException extends Exception {

    public BadRequestBusinessException() {
        super();
    }

    public BadRequestBusinessException(String message) {
        super(message);
    }

    public BadRequestBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestBusinessException(Throwable cause) {
        super(cause);
    }
}