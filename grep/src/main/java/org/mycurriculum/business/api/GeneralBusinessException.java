package org.mycurriculum.business.api;

public class GeneralBusinessException extends Exception {

    public GeneralBusinessException() {
        super();
    }

    public GeneralBusinessException(String message) {
        super(message);
    }

    public GeneralBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralBusinessException(Throwable cause) {
        super(cause);
    }
}
