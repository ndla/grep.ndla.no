package org.mycurriculum.business.api;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mycurriculum.business.domain.*;
import org.mycurriculum.data.entityservice.*;
import org.mycurriculum.data.tms.domain.*;
import org.mycurriculum.data.tms.exceptions.InvalidDataException;
import org.mycurriculum.data.topicmapservice.ParserService;
import org.mycurriculum.data.topicmapservice.TopicMapLinkService;
import org.mycurriculum.data.util.MutableTranslatedText;
import org.mycurriculum.data.util.TranslatedText;
import org.mycurriculum.data.util.multicore.AsyncTask;
import org.mycurriculum.data.util.parser.Parser;
import org.mycurriculum.data.util.parser.ProgressBit;
import org.mycurriculum.data.util.parser.ProgressTracker;
import org.mycurriculum.data.util.parser.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.json.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;

public class BusinessFacade {

    // Constants

    // User fields
    public static final String USER_USER_NAME = "userName";
    public static final String USER_FIRST_NAME = "firstName";
    public static final String USER_LAST_NAME = "lastName";
    public static final String USER_EMAIL = "email";
    public static final String USER_IS_ENABLED = "isEnabled";
    public static final String USER_ROLES = "roles";

    // Resource fields
    public static final String RESOURCE_ID = "id";
    public static final String RESOURCE_PSI = "psi";
    public static final String RESOURCE_STATUS = "status";
    public static final String RESOURCE_TYPE = "type";
    public static final String RESOURCE_LICENSES = "licenses";
    public static final String RESOURCE_NAMES = "names";
    public static final String RESOURCE_INGRESS = "ingress";
    public static final String RESOURCE_AUTHORS = "authors";

    // Course fields
    public static final String COURSE_EXTERNAL_ID = "externalId";
    public static final String COURSE_NAME = "name";
    public static final String COURSE_EDUCATION_GROUP_ID = "educationGroupId";

    // ***** Services *****

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private VersionService versionService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseGroupService courseGroupService;

    @Autowired
    private EducationGroupService educationGroupService;

    @Autowired
    private TopicMapLinkService topicMapLinkService;

    @Autowired
    private ParserService parserService;

    // ***** Internal methods *****

    private void registerUserWithSecurityContext(String userName) throws UsernameNotFoundException {
        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    // ***** Versions *****
    public Collection<Version> getVersions() {
        return versionService.list();
    }
    public Collection<Version> getVersions(String userName) {
        registerUserWithSecurityContext(userName);
        return getVersions();
    }
    public Long getActiveVersionNumber() throws InvalidVersionBusinessException {
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }
        return version.getVersionNumber();
    }

    // ***** Education Groups *****

    public List<EducationGroup> getEducationGroups(String userName) throws UsernameNotFoundException {
        registerUserWithSecurityContext(userName);

        List<EducationGroup> educationGroups = educationGroupService.list();

        return educationGroups;
    }

    public EducationGroup getEducationGroup(String userName, String externalId) throws UsernameNotFoundException {
        registerUserWithSecurityContext(userName);

        EducationGroup educationGroup = educationGroupService.findByExternalId(externalId);
        return educationGroup;
    }

    public EducationGroup createEducationGroup(String userName, String payload) throws UsernameNotFoundException, BadRequestBusinessException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);

        JsonObject payloadObject = parsePayload(payload);

        String externalId = payloadObject.getString("externalId");
        if (externalId == null || externalId.isEmpty()) {
            throw new BadRequestBusinessException();
        }
        EducationGroup checkEducationGroup = educationGroupService.findByExternalId(externalId);
        if (checkEducationGroup != null) {
            throw new ConflictBusinessException("An education group with the same identifier already exists.");
        }

        String name = payloadObject.getString("name");
        if (name == null || name.isEmpty()) {
            throw new BadRequestBusinessException();
        }
        String parentExternalId = payloadObject.getString("parentExternalId");
        if (parentExternalId == null || parentExternalId.isEmpty()) {
            throw new BadRequestBusinessException();
        }

        // Get the parent education group.
        EducationGroup parentEducationGroup = educationGroupService.findByExternalId(parentExternalId);

        EducationGroup educationGroup = new EducationGroup();
        educationGroup.setExternalId(externalId);
        educationGroup.setName(name);
        educationGroup.setParent(parentEducationGroup);

        educationGroupService.save(educationGroup);
        return educationGroup;
    }

    // ***** Courses *****

    public List<Course> getCourses(String userName, Long versionNumber) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        List<Course> courses = courseService.list(version.getVersionNumber());

        for (Course course : courses) {
            for (CourseGroup courseGroup : course.getCourseGroups()) {
                courseGroup.setTopicMapHandle(topicMapHandle);
                courseGroup.setTopicMapLinkService(topicMapLinkService);
            }
        }
        return courses;
    }

    // Retrieves courses for the active version.
    public List<Course> getCourses(String userName) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        List<Course> courses = courseService.list(version.getVersionNumber());

        for (Course course : courses) {
            for (CourseGroup courseGroup : course.getCourseGroups()) {
                courseGroup.setTopicMapHandle(topicMapHandle);
                courseGroup.setTopicMapLinkService(topicMapLinkService);
            }
        }
        return courses;
    }

    public List<Course> getCoursesForEducationGroup(String userName, Long versionNumber, String educationGroupExternalId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        Set<Course> courseSet = educationGroupService.findByExternalId(educationGroupExternalId).getCourses();

        // Filter courses by version number.
        List<Course> courses = new ArrayList();
        for (Course course : courseSet) {
            if (course.getVersionNumber().equals(version.getVersionNumber())) {
                courses.add(course);
            }
        }

        //List<Course> courses = new ArrayList(courseSet);
        for (Course course : courses) {
            for (CourseGroup courseGroup : course.getCourseGroups()) {
                courseGroup.setTopicMapHandle(topicMapHandle);
                courseGroup.setTopicMapLinkService(topicMapLinkService);
            }
        }

        return courses;
    }

    // Retrieves courses for an education group for the active version.
    public List<Course> getCoursesForEducationGroup(String userName, String educationGroupExternalId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        Set<Course> courseSet = educationGroupService.findByExternalId(educationGroupExternalId).getCourses();

        // Filter courses by version number.
        List<Course> courses = new ArrayList();
        for (Course course : courseSet) {
            if (course.getVersionNumber().equals(version.getVersionNumber())) {
                courses.add(course);
            }
        }

        //List<Course> courses = new ArrayList(courseSet);
        for (Course course : courses) {
            for (CourseGroup courseGroup : course.getCourseGroups()) {
                courseGroup.setTopicMapHandle(topicMapHandle);
                courseGroup.setTopicMapLinkService(topicMapLinkService);
            }
        }

        return courses;
    }

    public Course getCourseForCourseStructure(String userName, Long versionNumber, String courseGroupPsi) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        CourseGroup courseGroup = courseGroupService.findCourseGroupByPsi(courseGroupPsi, version.getVersionNumber());
        Course course = courseGroup.getCourse();

        return course;
    }

    public Course getCourseForCourseStructure(String userName, String courseGroupPsi) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        CourseGroup courseGroup = courseGroupService.findCourseGroupByPsi(courseGroupPsi, version.getVersionNumber());
        Course course = courseGroup.getCourse();

        return course;
    }

    public Course getCourse(String userName, Long versionNumber, String externalId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        Course course = courseService.findByExternalId(externalId, version.getVersionNumber());

        for (CourseGroup courseGroup : course.getCourseGroups()) {
            courseGroup.setTopicMapHandle(topicMapHandle);
            courseGroup.setTopicMapLinkService(topicMapLinkService);
        }
        return course;
    }

    // Retrieves a course for the active version.
    public Course getCourse(String userName, String externalId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        Course course = courseService.findByExternalId(externalId, version.getVersionNumber());

        for (CourseGroup courseGroup : course.getCourseGroups()) {
            courseGroup.setTopicMapHandle(topicMapHandle);
            courseGroup.setTopicMapLinkService(topicMapLinkService);
        }
        return course;
    }

    public Course createCourse(String userName, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, GeneralBusinessException, ConflictBusinessException, BadRequestBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        JsonObject payloadObject = parsePayload(payload);

        String externalId = payloadObject.getString(COURSE_EXTERNAL_ID);
        if (externalId == null) {
            throw new BadRequestBusinessException();
        }
        String name = payloadObject.getString(COURSE_NAME);
        if (name == null) {
            throw new BadRequestBusinessException();
        }
        String educationGroupId = payloadObject.getString(COURSE_EDUCATION_GROUP_ID);
        if (educationGroupId == null) {
            throw new BadRequestBusinessException();
        }

        // Check to ensure that a course with the same external identifier and version (number) does not already exist.
        Course checkCourse = courseService.findByExternalId(externalId, version.getVersionNumber());
        if (checkCourse != null) {
            throw new ConflictBusinessException("Course with the same external identifier already exists for this version");
        }

        EducationGroup educationGroup = educationGroupService.findByExternalId(educationGroupId);
        if (educationGroup == null) {
            throw new GeneralBusinessException("Education group does not exist");
        }

        Course course = new Course();
        course.setExternalId(externalId);
        course.setName(name);
        course.setVersionNumber(version.getVersionNumber());
        course.setEducationGroup(educationGroup);

        courseService.save(course);

        return course;
    }

    public Course createCourse(String userName, Long versionNumber, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, GeneralBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        // TODO: Implement.
        return null;
    }

    public void deleteCourseMapping(CourseGroup courseGroup) {
        courseGroupService.delete(courseGroup);
    }
    public void deleteCourseMapping(Course course, OntopiaCourseStructure courseStructure) throws ConflictBusinessException {
        Collection<CourseGroup> courseGroups = course.getCourseGroups();
        for (CourseGroup courseGroup : courseGroups) {
            if (courseGroup.getPsi().equals(courseStructure.getPsi().toString())) {
                deleteCourseMapping(courseGroup);
                return;
            }
        }
        throw new ConflictBusinessException("The mapping does not exist");
    }
    public void deleteCourseMapping(Version version, String externalId, String courseStructureId) throws IOException, ConflictBusinessException {
        Course course = courseService.findByExternalId(externalId, version.getVersionNumber());

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCourseStructure courseStructure = topicMapLinkService.getCourseStructureByHumanId(topicMapHandle, courseStructureId);

        deleteCourseMapping(course, courseStructure);
    }
    public void deleteCourseMapping(String userName, Long versionNumber, String externalId, String courseStructureId) throws InvalidVersionBusinessException, IOException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        deleteCourseMapping(version, externalId, courseStructureId);
    }
    public void deleteCourseMapping(String userName, String externalId, String courseStructureId) throws InvalidVersionBusinessException, IOException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        deleteCourseMapping(version, externalId, courseStructureId);
    }
    public CourseGroup addCourseMapping(Course course, OntopiaCourseStructure courseStructure) throws ConflictBusinessException {
        Collection<CourseGroup> courseGroups = course.getCourseGroups();
        for (CourseGroup courseGroup : courseGroups) {
            if (courseGroup.getPsi().equals(courseStructure.getPsi().toString())) {
                throw new ConflictBusinessException("The mapping does already exist");
            }
        }
        CourseGroup courseGroup = new CourseGroup();
        courseGroup.setCourse(course);
        courseGroup.setPsi(courseStructure.getPsi().toString());
        courseGroup.setDateCreated(new Date());
        courseGroupService.save(courseGroup);
        return courseGroup;
    }
    public CourseGroup addCourseMapping(Version version, String courseId, String courseStructureId) throws IOException, ConflictBusinessException {

        Course course = courseService.findByExternalId(courseId, version.getVersionNumber());

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCourseStructure courseStructure = topicMapLinkService.getCourseStructureByHumanId(topicMapHandle, courseStructureId);

        return addCourseMapping(course, courseStructure);
    }
    public CourseGroup addCourseMapping(String userName, Long versionNumber, String courseId, String courseStructureId) throws InvalidVersionBusinessException, IOException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        return addCourseMapping(version, courseId, courseStructureId);
    }
    public CourseGroup addCourseMapping(String userName, String courseId, String courseStructureId) throws IOException, ConflictBusinessException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }
        return addCourseMapping(version, courseId, courseStructureId);
    }

    // ***** Course Structures *****

    public OntopiaCourseStructure getCourseStructure(String userName, Long versionNumber, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCourseStructure courseStructure = topicMapLinkService.getCourseStructureByHumanId(topicMapHandle, humanId);

        return courseStructure;
    }

    public OntopiaCourseStructure getCourseStructure(String userName, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        /*
        String decodedPsi = null;
        try {
            decodedPsi = java.net.URLDecoder.decode(encodedPsi, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        */

        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCourseStructure courseStructure = topicMapLinkService.getCourseStructureByHumanId(topicMapHandle, humanId);

        return courseStructure;
    }

    // ***** Curriculum Sets *****

    public OntopiaCurriculumSet getCurriculumSet(String userName, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCurriculumSet curriculumSet = topicMapLinkService.getCurriculumSetByHumanId(topicMapHandle, humanId);

        return curriculumSet;
    }

    public OntopiaCurriculumSet getCurriculumSet(String userName, Long versionNumber, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCurriculumSet curriculumSet = topicMapLinkService.getCurriculumSetByHumanId(topicMapHandle, humanId);

        return curriculumSet;
    }

    // ***** Curriculums *****

    public Collection<OntopiaCurriculum> getCurriculums(Version version) throws IOException {
        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        Collection<OntopiaCurriculum> curriculums = topicMapLinkService.getCurricula(topicMapHandle);
        return curriculums;
    }
    public Collection<OntopiaCurriculum> getCurriculums(String userName) throws InvalidVersionBusinessException, IOException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }
        return getCurriculums(version);
    }
    public Collection<OntopiaCurriculum> getCurriculums(String userName, Long versionNumber) throws InvalidVersionBusinessException, IOException {
        if (versionNumber == null) {
            return getCurriculums(userName);
        }

        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        return getCurriculums(version);
    }

    public OntopiaCurriculum getCurriculum(String userName, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCurriculum curriculum = topicMapLinkService.getCurriculumByHumanId(topicMapHandle, humanId);

        return curriculum;
    }

    public OntopiaCurriculum getCurriculum(String userName, Long versionNumber, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaCurriculum curriculum = topicMapLinkService.getCurriculumByHumanId(topicMapHandle, humanId);

        return curriculum;
    }

    // ***** Competence Aim Sets *****

    public OntopiaAimSet getCompetenceAimSet(String userName, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaAimSet aimSet = topicMapLinkService.getAimSetByHumanId(topicMapHandle, humanId);

        return aimSet;
    }

    public OntopiaAimSet getCompetenceAimSet(String userName, Long versionNumber, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaAimSet aimSet = topicMapLinkService.getAimSetByHumanId(topicMapHandle, humanId);

        return aimSet;
    }

    // ***** Competence Aims *****

    public OntopiaAim getCompetenceAim(String userName, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaAim aim = topicMapLinkService.getAimByHumanId(topicMapHandle, humanId);

        return aim;
    }

    public OntopiaAim getCompetenceAim(String userName, Long versionNumber, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaAim aim = topicMapLinkService.getAimByHumanId(topicMapHandle, humanId);

        return aim;
    }

    // ***** Resources *****

    public OntopiaResource getResource(String userName, Long versionNumber, String psi) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaResource resource = topicMapLinkService.getResource(topicMapHandle, psi);

        return resource;
    }

    public OntopiaResource getResource(String userName, String psi) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaResource resource = topicMapLinkService.getResource(topicMapHandle, psi);

        return resource;
    }

    public OntopiaResource createResource(String userName, Long versionNumber, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        // TODO: Implement.
        return null;
    }

    public OntopiaResource createResource(String userName, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, ConflictBusinessException, BadRequestBusinessException {
        registerUserWithSecurityContext(userName);

        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        JsonObject payloadObject = parsePayload(payload);

        String jsonPsi = payloadObject.getString(RESOURCE_PSI);
        if (jsonPsi == null) {
            throw new BadRequestBusinessException();
        }
        String jsonStatus = payloadObject.getString(RESOURCE_STATUS);
        if (jsonStatus == null) {
            throw new BadRequestBusinessException();
        }
        String jsonType = payloadObject.getString(RESOURCE_TYPE);
        if (jsonType == null) {
            throw new BadRequestBusinessException();
        }
        JsonArray jsonLicenses = payloadObject.getJsonArray(RESOURCE_LICENSES);
        if (jsonLicenses == null) {
            throw new BadRequestBusinessException();
        }
        JsonArray jsonNames = payloadObject.getJsonArray(RESOURCE_NAMES);
        if (jsonNames == null) {
            throw new BadRequestBusinessException();
        }
        JsonObject jsonIngress = payloadObject.getJsonObject(RESOURCE_INGRESS);
        if (jsonIngress == null) {
            throw new BadRequestBusinessException();
        }
        JsonArray jsonAuthors = payloadObject.getJsonArray(RESOURCE_AUTHORS);
        if (jsonAuthors == null) {
            throw new BadRequestBusinessException();
        }

        // Check to ensure that a resource with the same PSI does not already exist.
        OntopiaResource checkResource = topicMapLinkService.getResource(topicMapHandle, jsonPsi);
        if (checkResource != null) {
            throw new ConflictBusinessException("Resource with the same PSI already exists");
        }

        OntopiaResource.Builder resourceBuilder = topicMapLinkService.resourceBuilder(topicMapHandle);

        // Build the initial resource.
        OntopiaResource resource = resourceBuilder
                .psi(jsonPsi)
                .status(jsonStatus)
                .nodeType(jsonType)
                .build();

        // Add name objects to the resource.
        for (int i = 0; i < jsonNames.size(); i++) {
            JsonObject jsonName = jsonNames.getJsonObject(i);
            String name = jsonName.getString("name").replace("\"", "");
            if (name == null) {
                throw new BadRequestBusinessException();
            }
            String languageCode = jsonName.getString("languageCode").replace("\"", "");
            if (languageCode == null) {
                throw new BadRequestBusinessException();
            }
            Boolean isLanguageNeutral = null;
            try {
                isLanguageNeutral = jsonName.getBoolean("isLanguageNeutral");
            } catch (ClassCastException e) {
                String isLanguageNeutralString = jsonName.getString("isLanguageNeutral");
                if (isLanguageNeutralString.equals("true")) {
                    isLanguageNeutral = true;
                } else {
                    isLanguageNeutral = false;
                }
            }

            Locale nameLocale = new Locale(languageCode);
            resource.setName(nameLocale, name);
            resource.getNameObject(nameLocale).setLanguageNeutral(isLanguageNeutral);
        }

        // Add ingress to the resource.
        if (jsonIngress.size() > 0) {
            String ingressText = jsonIngress.getString("text").replace("\"", "");
            if (ingressText == null) {
                throw new BadRequestBusinessException();
            }
            String ingressLanguageCode = jsonIngress.getString("languageCode").replace("\"", "");
            if (ingressLanguageCode == null) {
                throw new BadRequestBusinessException();
            }
            Locale ingressLocale = new Locale(ingressLanguageCode);

            MutableTranslatedText translatedText = resource.getIngress();
            translatedText.addText(ingressLocale, ingressText);
            translatedText.setDefaultLocale(ingressLocale);

            resource.setIngress(translatedText);
        }

        // Add authors to the resource.
        String[] authorNames = new String[jsonAuthors.size()];
        String[] authorUrls = new String[jsonAuthors.size()];

        for (int j = 0; j < jsonAuthors.size(); j++) {
            JsonObject jsonAuthor = jsonAuthors.getJsonObject(j);
            authorNames[j] = jsonAuthor.getString("name").replace("\"", "");
            authorUrls[j] = jsonAuthor.getString("url").replace("\"", "");
        }
        try {
            resource.setAuthors(authorNames);
            resource.setAuthorUrls(authorUrls);
        } catch (InvalidDataException e) {
            throw new BadRequestBusinessException(e.getMessage(), e);
        }

        // Add licenses to the resource.
        // TODO: Implement.

        resource.save(); // Persist resource to backing store.
        return resource;
    }

    public void updateResource(String userName, Long versionNumber, String payload, String id) {
        // TODO: Implement.
    }

    public void updateResource(String userName, String payload, String id) {
        // TODO: Implement.
    }

    // ***** Levels *****

    public Collection<OntopiaLevel> getLevels(String userName, Long versionNumber) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        Collection<OntopiaLevel> levels = topicMapLinkService.getLevels(topicMapHandle);

        return levels;
    }

    public Collection<OntopiaLevel> getLevels(String userName) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        Collection<OntopiaLevel> levels = topicMapLinkService.getLevels(topicMapHandle);

        return levels;
    }

    public OntopiaLevel getLevel(String userName, Long versionNumber, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaLevel level = topicMapLinkService.getLevelByHumanId(topicMapHandle, humanId);
        return level;
    }

    public OntopiaLevel getLevel(String userName, String humanId) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        OntopiaLevel level = topicMapLinkService.getLevelByHumanId(topicMapHandle, humanId);
        return level;
    }

    // ***** Competence Aims - Resources (Mapping) *****

    public Collection<OntopiaResourceAimAssociation> createRelations(String userName, Long versionNumber, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, BadRequestBusinessException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }
        return createRelations(version, payload);
    }
    public Collection<OntopiaResourceAimAssociation> createRelations(String userName, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, BadRequestBusinessException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }
        return createRelations(version, payload);
    }
    public Collection<OntopiaResourceAimAssociation> createRelations(Version version, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, BadRequestBusinessException, ConflictBusinessException {
        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        JsonObject payloadObject = parsePayload(payload);

        JsonObject author = payloadObject.getJsonObject("author");
        if (author == null) {
            throw new BadRequestBusinessException();
        }
        List<String> authorNames = new ArrayList<String>();
        String authorName = author.getString("name").replace("\"", "");
        if (authorName == null) {
            throw new BadRequestBusinessException();
        }
        authorNames.add(authorName);
        String[] authorNamesArray = new String[authorNames.size()];
        authorNames.toArray(authorNamesArray);

        List<String> authorUrls = new ArrayList<>();
        String authorUrl = author.getString("url").replace("\"", "");
        if (authorUrl == null) {
            throw new BadRequestBusinessException();
        }
        authorUrls.add(authorUrl);
        String[] authorUrlsArray = new String[authorUrls.size()];
        authorUrls.toArray(authorUrlsArray);

        String subjectMatterId = payloadObject.getString("subjectMatterId").replace("\"", "");
        if (subjectMatterId == null) {
            throw new BadRequestBusinessException();
        }
        //String timestamp = payloadObject.getString("timestamp").replace("\"", "");

        JsonArray relations = payloadObject.getJsonArray("relations");
        if (relations == null) {
            throw new BadRequestBusinessException();
        }

        Collection<OntopiaResourceAimAssociation> aimResourceAssociations = new ArrayList<OntopiaResourceAimAssociation>();
        for (int i = 0; i < relations.size(); i++) {
            JsonObject relation = relations.getJsonObject(i);

            String competenceAimId = relation.getString("competenceAimId").replace("\"", "");
            if (competenceAimId == null) {
                throw new BadRequestBusinessException();
            }
            String relationType = relation.getString("relationType").replace("\"", "");
            if (relationType == null) {
                throw new BadRequestBusinessException();
            }
            String psi = relation.getString("resourcePsi").replace("\"", "");
            if (psi == null) {
                throw new BadRequestBusinessException();
            }

            /*
            Boolean apprenticeRelevance = relation.getBoolean("apprenticeRelevance");
            if (apprenticeRelevance == null) {
                throw new BadRequestBusinessException();
            }
            */

            Boolean apprenticeRelevance = null;
            try {
                apprenticeRelevance = relation.getBoolean("apprenticeRelevance");
            } catch (ClassCastException e) {
                String apprenticeRelevanceString = relation.getString("apprenticeRelevance");
                if (apprenticeRelevanceString.equals("true")) {
                    apprenticeRelevance = true;
                } else {
                    apprenticeRelevance = false;
                }
            }

            String curriculumSetHumanId;
            if (!relation.isNull("curriculumSetId")) {
                curriculumSetHumanId = relation.getString("curriculumSetId");
            } else {
                curriculumSetHumanId = null;
            }
            String levelHumanId;
            if (!relation.isNull("level")) {
                levelHumanId = relation.getString("level");
            } else {
                levelHumanId = null;
            }

            OntopiaResource resource = topicMapLinkService.getResource(topicMapHandle, psi);

            OntopiaAim competenceAim = topicMapLinkService.getAimByHumanId(topicMapHandle, competenceAimId);

            OntopiaResourceAimAssociation aimResourceAssoc;
            if (relation.containsKey("humanId")) {
                JsonString humanIdObject = relation.getJsonString("humanId");
                // Find the relationship object
                aimResourceAssoc = topicMapLinkService.getResourceAimsAssociation(topicMapHandle, humanIdObject.getString());
                // Sanity check
                if (!aimResourceAssoc.getAim().getHumanId().equals(competenceAim.getHumanId()) ||
                    !aimResourceAssoc.getResource().getPsi().equals(resource.getPsi())) {
                    throw new ConflictBusinessException("Assoc humanId, aim and resource mismatch/conflict in update");
                }
            } else {
                // Create relationship between resource and competence aim and save metadata on the (reified) relationship object/topic.
                aimResourceAssoc = topicMapLinkService.createResourceAimsAssociation(resource, competenceAim);
            }
            aimResourceAssociations.add(aimResourceAssoc);

            // Set curriculum set identifier and level on the reifier. First get the appropriate
            // curriculum set and level objects.
            if (curriculumSetHumanId != null && (!curriculumSetHumanId.equals(""))) {
                OntopiaCurriculumSet relationCurriculumSet = topicMapLinkService.getCurriculumSetByHumanId(topicMapHandle, curriculumSetHumanId);
                aimResourceAssoc.setCurriculumSet(relationCurriculumSet);
            }
            if (levelHumanId != null && (!levelHumanId.equals(""))) {
                OntopiaLevel relationLevel = topicMapLinkService.getLevelByHumanId(topicMapHandle, levelHumanId);
                aimResourceAssoc.setLevel(relationLevel);
            }

            String curriculumId;
            if (!relation.isNull("curriculumId")) {
                curriculumId = relation.getString("curriculumId");
            } else {
                curriculumId = null;
            }
            String competenceAimSetId;
            if (!relation.isNull("competenceAimSetId")) {
                competenceAimSetId = relation.getString("competenceAimSetId");
            } else {
                competenceAimSetId = null;
            }
            if (curriculumId != null && (!curriculumId.equals(""))) {
                OntopiaCurriculum curriculum = topicMapLinkService.getCurriculumByHumanId(topicMapHandle, curriculumId);
                aimResourceAssoc.setCurriculum(curriculum);
            }
            if (competenceAimSetId != null && (!competenceAimSetId.equals(""))) {
                OntopiaAimSet competenceAimSet = topicMapLinkService.getAimSetByHumanId(topicMapHandle, competenceAimSetId);
                aimResourceAssoc.setAimSet(competenceAimSet);
            }

            String courseId;
            if (!relation.isNull("courseId")) {
                courseId = relation.getString("courseId");
            } else {
                courseId = null;
            }
            String courseType;
            if (!relation.isNull("courseType")) {
                courseType = relation.getString("courseType");
            } else {
                courseType = null;
            }

            if (courseId != null && (!courseId.equals(""))) {
                aimResourceAssoc.setCourseId(courseId);
            }
            if (courseType != null && (!courseType.equals(""))) {
                aimResourceAssoc.setCourseType(courseType);
            }

            aimResourceAssoc.setAuthors(authorNamesArray);
            aimResourceAssoc.setAuthorUrls(authorUrlsArray);
            aimResourceAssoc.setApprenticeRelevant(apprenticeRelevance);
            aimResourceAssoc.setTimestampDate(new Date());
            aimResourceAssoc.save();
        }
        return aimResourceAssociations;
    }

    public void updateRelations(String userName, Long versionNumber, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        JsonObject payloadObject = parsePayload(payload);

        // TODO: Implement.
    }

    public void updateRelations(String userName, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        JsonObject payloadObject = parsePayload(payload);

        // TODO: Implement.
    }

    public void deleteRelations(Version version, String payload) throws IOException, ConflictBusinessException {
        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        JsonObject payloadObject = parsePayload(payload);

        JsonArray relations = payloadObject.getJsonArray("relations");
        Collection<OntopiaResourceAimAssociation> assocs = new ArrayList<OntopiaResourceAimAssociation>();
        Collection<JsonObject> objects = relations.getValuesAs(JsonObject.class);
        for (JsonObject object : objects) {
            String assocId = object.getJsonString("humanId").getString();
            String resourcePsi = object.getJsonString("resourcePsi").getString();
            String aimId = object.getJsonString("competenceAimId").getString();
            OntopiaResourceAimAssociation assoc = topicMapLinkService.getResourceAimsAssociation(topicMapHandle, assocId);
            if (!assoc.getAim().getHumanId().equals(aimId) ||
                !assoc.getResource().getPsi().toString().equals(resourcePsi)) {
                throw new ConflictBusinessException("Assoc humanId, aim and resource mismatch/conflict in update");
            }
            assocs.add(assoc);
        }
        for (OntopiaResourceAimAssociation assoc : assocs) {
            assoc.remove();
        }
    }
    public void deleteRelations(String userName, Long versionNumber, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, BadRequestBusinessException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findVersionByVersionNumber(versionNumber);
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }
        deleteRelations(version, payload);
    }

    public void deleteRelations(String userName, String payload) throws UsernameNotFoundException, IOException, InvalidVersionBusinessException, BadRequestBusinessException, ConflictBusinessException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }
        deleteRelations(version, payload);
    }

    // ***** Search indexers *****
    public Set<String> getSearchIndexerTypes() {
        return topicMapLinkService.getSearchIndexerTypes();
    }
    public AsyncTask createSearchIndexer(String userName, String payload) throws InvalidVersionBusinessException, BadRequestBusinessException, IOException {
        registerUserWithSecurityContext(userName);
        JsonObject payloadObject = parsePayload(payload);
        String typeStr;
        if (payloadObject.containsKey("type")) {
            typeStr = payloadObject.getString("type");
        } else {
            throw new BadRequestBusinessException("Missing type of search indexer (type)");
        }
        Long versionId;
        if (payloadObject.containsKey("version")) {
            String versionIdStr;
            versionIdStr = payloadObject.getString("version");
            versionId = Long.decode(versionIdStr);
        } else {
            versionId = null;
        }
        Version version;
        if (versionId != null) {
            version = versionService.findVersionByVersionNumber(versionId);
        } else {
            version = versionService.findActive();
        }
        if (version == null) {
            throw new InvalidVersionBusinessException();
        }

        String tmConnectionString = version.getTopicMapConnectionInfo();
        TopicMapLinkService.TopicMapHandle topicMapHandle = topicMapLinkService.getTopicMapHandle(tmConnectionString);

        return topicMapLinkService.createSearchIndexer(topicMapHandle, typeStr);
    }

    private JSONArray getTranslatedTextJSONObject(TranslatedText translatedText) {
        JSONArray result = new JSONArray();
        Locale defaultLocale = translatedText.getDefaultLocale();
        Map<Locale,String> map = translatedText.getTextMap();
        if (map != null) {
            for (Map.Entry<Locale, String> entry : map.entrySet()) {
                Locale loc = entry.getKey();
                String text = entry.getValue();
                boolean isLanguageNeutral = false;
                if (defaultLocale != null && defaultLocale.equals(loc)) {
                    isLanguageNeutral = true;
                }
                JSONObject json = new JSONObject();
                json.put("text", text);
                json.put("languageCode", loc.getISO3Language());
                json.put("isLanguageNeutral", isLanguageNeutral);
                result.put(json);
            }
        }
        return result;
    }

    // ***** Importer *****
    private AsyncTask createGrepImporter(final Parser.Context parserContext, final CreateNewVersion newVersion) {
        final Version version = newVersion.getVersion();
        final String connectionString = newVersion.getTopicmapConnectionString();
        final ProgressTracker progressTracker = parserContext.getProgressTracker();
        final ProgressBit afterImportProgress = progressTracker.createBit("After import");
        afterImportProgress.setExpected(2);
        progressTracker.addSubBit(afterImportProgress);
        final IOException[] ioException = new IOException[1];
        ioException[0] = null;
        final JSONObject[] report = new JSONObject[1];
        report[0] = null;
        AsyncTask importerFacade = new AsyncTask() {
            @Override
            public void updateStatus(JSONObject status) {
                if (report[0] != null) {
                    status.put("report", report[0]);
                }
            }

            @Override
            public ProgressTracker getProgressTracker() {
                return progressTracker;
            }

            @Override
            public void completionCheck() throws IOException {
                if (ioException[0] != null) {
                    throw new IOException(ioException[0].getMessage(), ioException[0]);
                }
            }

            @Override
            public void run() {
                try {
                    parserContext.waitForCompletion();
                    Report parserReport = parserContext.getReport();
                    Report.AimsAssociations aimsReport = parserReport.getAimsAssociationsReport();
                    JSONObject jsonReport = new JSONObject();
                    JSONArray aimsReportArray = new JSONArray();
                    JSONArray curriculumSetsReportArray = new JSONArray();
                    JSONArray curriculaReportArray = new JSONArray();
                    JSONArray aimSetsReportArray = new JSONArray();
                    jsonReport.put("aimsNotFound", aimsReportArray);
                    jsonReport.put("curriculumSetsNotFound", curriculumSetsReportArray);
                    jsonReport.put("curriculaNotFound", curriculaReportArray);
                    jsonReport.put("aimSetsNotFound", aimSetsReportArray);
                    if (aimsReport != null) {
                        Map<String,Collection<String>> aimsMap = aimsReport.getAimsNotFound();
                        Map<String,Collection<String>> curriculumSetsMap = aimsReport.getCurriculumSetsNotFound();
                        Map<String,Collection<String>> curriculaMap = aimsReport.getCurriculaNotFound();
                        Map<String,Collection<String>> aimSetsMap = aimsReport.getAimSetsNotFound();
                        for (Map.Entry<String,Collection<String>> aimNotFoundEntry : aimsMap.entrySet()) {
                            String aimId = aimNotFoundEntry.getKey();
                            TranslatedText name = aimsReport.getAimName(aimId);
                            JSONArray jsonName = getTranslatedTextJSONObject(name);
                            JSONObject json = new JSONObject();
                            JSONArray jsonResources = new JSONArray();
                            json.put("id", aimId);
                            json.put("name", jsonName);
                            json.put("resources", jsonResources);
                            for (String resourcePsi : aimNotFoundEntry.getValue()) {
                                JSONObject resourceJson = new JSONObject();
                                resourceJson.put("psi", resourcePsi);
                                jsonResources.put(resourceJson);
                            }
                            aimsReportArray.put(json);
                        }
                        for (Map.Entry<String,Collection<String>> curriculumSetNotFoundEntry : curriculumSetsMap.entrySet()) {
                            String curriculumSetId = curriculumSetNotFoundEntry.getKey();
                            TranslatedText name = aimsReport.getCurriculumSetName(curriculumSetId);
                            JSONArray jsonName = getTranslatedTextJSONObject(name);
                            JSONObject json = new JSONObject();
                            JSONArray jsonResources = new JSONArray();
                            json.put("id", curriculumSetId);
                            json.put("name", jsonName);
                            json.put("resources", jsonResources);
                            for (String resourcePsi : curriculumSetNotFoundEntry.getValue()) {
                                JSONObject resourceJson = new JSONObject();
                                resourceJson.put("psi", resourcePsi);
                                jsonResources.put(resourceJson);
                            }
                            curriculumSetsReportArray.put(json);
                        }
                        for (Map.Entry<String,Collection<String>> curriculumNotFoundEntry : curriculaMap.entrySet()) {
                            String curriculumId = curriculumNotFoundEntry.getKey();
                            TranslatedText name = aimsReport.getCurriculumName(curriculumId);
                            JSONArray jsonName = getTranslatedTextJSONObject(name);
                            JSONObject json = new JSONObject();
                            JSONArray jsonResources = new JSONArray();
                            json.put("id", curriculumId);
                            json.put("name", jsonName);
                            json.put("resources", jsonResources);
                            for (String resourcePsi : curriculumNotFoundEntry.getValue()) {
                                JSONObject resourceJson = new JSONObject();
                                resourceJson.put("psi", resourcePsi);
                                jsonResources.put(resourceJson);
                            }
                            curriculaReportArray.put(json);
                        }
                        for (Map.Entry<String,Collection<String>> aimSetNotFoundEntry : aimSetsMap.entrySet()) {
                            String aimSetId = aimSetNotFoundEntry.getKey();
                            TranslatedText name = aimsReport.getAimSetName(aimSetId);
                            JSONArray jsonName = getTranslatedTextJSONObject(name);
                            JSONObject json = new JSONObject();
                            JSONArray jsonResources = new JSONArray();
                            json.put("id", aimSetId);
                            json.put("name", jsonName);
                            json.put("resources", jsonResources);
                            for (String resourcePsi : aimSetNotFoundEntry.getValue()) {
                                JSONObject resourceJson = new JSONObject();
                                resourceJson.put("psi", resourcePsi);
                                jsonResources.put(resourceJson);
                            }
                            aimSetsReportArray.put(json);
                        }
                    }
                    report[0] = jsonReport;
                    afterImportProgress.setStartTime();
                    afterImportProgress.setExpected(100);
                    afterImportProgress.setDone(1);
                    newVersion.setupHibernateSession();
                    afterImportProgress.setDone(2);
                    newVersion.afterImport();
                    afterImportProgress.setDone(100);
                } catch (IOException e) {
                    ioException[0] = new IOException(e.getMessage(), e);
                } catch (InterruptedException e) {
                    ioException[0] = new IOException(e.getMessage(), e);
                }
            }
        };
        return importerFacade;
    }
    private AsyncTask createGrepImporter(TopicMapLinkService.TopicMapHandle source, CreateNewVersion newVersion) throws IOException {
        Parser.Context parserContext = topicMapLinkService.createGrepImporter(source, newVersion.getTopicMapHandle());
        return createGrepImporter(parserContext, newVersion);
    }
    private AsyncTask createGrepImporter(CreateNewVersion newVersion) throws IOException {
        Parser.Context parserContext = topicMapLinkService.createGrepImporter(newVersion.getTopicMapHandle());
        return createGrepImporter(parserContext, newVersion);
    }
    private class CreateNewVersion {
        private String userName;
        private Version version;
        private String topicmapConnectionString;

        public CreateNewVersion(String userName) throws IOException, SQLException {
            this.userName = userName;
            topicmapConnectionString = parserService.createTopicmap("mc_topicmap_" + (new Date().getTime() / 1000), "mycurriculum");
            version = new Version();
            version.setTopicMapConnectionInfo(topicmapConnectionString);
            version.setVersionNumber(new Date().getTime() / 1000);
            version.setName("v" + version.getVersionNumber());
            versionService.save(version);
        }

        public void setupHibernateSession() {
            registerUserWithSecurityContext(userName);
        }

        public Version getVersion() {
            return version;
        }
        public String getTopicmapConnectionString() {
            return topicmapConnectionString;
        }
        public TopicMapLinkService.TopicMapHandle getTopicMapHandle() throws IOException {
            return topicMapLinkService.getTopicMapHandle(getTopicmapConnectionString());
        }

        public void activateVersion() {
            versionService.setActive(version);
        }

        public void afterImport() throws IOException {
            activateVersion();
        }
    }
    private class CreateNewVersionWithCourses extends CreateNewVersion {
        private JSONObject courseMappings;
        private Map<String, Collection<String>> mappings;

        public CreateNewVersionWithCourses(String userName, JSONObject courseMappings) throws IOException, SQLException {
            super(userName);
            this.courseMappings = courseMappings;
        }
        private void prepareMappings() throws IOException {
            mappings = new HashMap<String, Collection<String>>();
            if (courseMappings == null) {
                return;
            }
            Iterator<String> educationGroupsIterator = courseMappings.keys();
            while (educationGroupsIterator.hasNext()) {
                String educationGroupExternalId = educationGroupsIterator.next();
                EducationGroup educationGroup = educationGroupService.findByExternalId(educationGroupExternalId);

                JSONArray educationGroupCourses = (JSONArray) courseMappings.get(educationGroupExternalId);
                for (int i = 0; i < educationGroupCourses.length(); i++) {
                    JSONObject courses = (JSONObject) educationGroupCourses.get(i);
                    Iterator<String> coursesIterator = courses.keys();

                    while (coursesIterator.hasNext()) {
                        String courseExternalId = coursesIterator.next();

                        JSONArray courseStructures = (JSONArray) courses.get(courseExternalId);
                        for (int j = 0; j < courseStructures.length(); j++) {
                            JSONObject jsonCourse = (JSONObject) courseStructures.get(j);

                            String courseTitle = jsonCourse.get("title").toString();
                            String courseStructureHumanId = jsonCourse.get("course_structure").toString();

                            Course course = new Course();
                            course.setVersionNumber(getVersion().getVersionNumber());
                            course.setName(courseTitle);
                            course.setExternalId(courseExternalId);
                            course.setEducationGroup(educationGroup);
                            courseService.save(course);

                            OntopiaCourseStructure courseStructure = topicMapLinkService.getCourseStructureByHumanId(getTopicMapHandle(), courseStructureHumanId);

                            List<String> courseStructurePsis = new ArrayList<String>();
                            if (courseStructure != null) {
                                courseStructurePsis.add(courseStructure.getPsi().toString());
                                mappings.put(course.getExternalId(), courseStructurePsis);
                            }
                        }
                    }
                }
            }
        }
        public Map<String,Collection<String>> getMappings() {
            return mappings;
        }
        @Override
        public void afterImport() throws IOException {
            prepareMappings();
            parserService.mapCourseGroups(getVersion().getVersionNumber(), getTopicmapConnectionString(), mappings);
            super.afterImport();
        }
    }
    private AsyncTask createGrepImporter(Version previous, JSONObject courseMappings, CreateNewVersion newVersion) throws IOException, SQLException {
        if (previous != null) {
            return createGrepImporter(topicMapLinkService.getTopicMapHandle(previous.getTopicMapConnectionInfo()), newVersion);
        } else {
            return createGrepImporter(newVersion);
        }
    }
    public AsyncTask createGrepImporter(String userName, Version previous, JSONObject courseMappings) throws IOException, SQLException {
        registerUserWithSecurityContext(userName);
        CreateNewVersionWithCourses newVersion = new CreateNewVersionWithCourses(userName, courseMappings);
        return createGrepImporter(previous, courseMappings, newVersion);
    }
    public AsyncTask createGrepImporter(String userName, JSONObject courseMappings) throws IOException, SQLException {
        registerUserWithSecurityContext(userName);
        Version version = versionService.findActive();
        CreateNewVersionWithCourses newVersion = new CreateNewVersionWithCourses(userName, courseMappings);
        return createGrepImporter(version, courseMappings, newVersion);
    }

    // ***** Accounts *****

    public Account getAccount(String userName, Long accountId) throws UsernameNotFoundException, SecurityBusinessException, NullPointerException {
        registerUserWithSecurityContext(userName);

        User securityUser = userService.findByUsername(userName);
        Account securityAccount = securityUser.getAccount();

        Account account = accountService.find(accountId);

        if (!securityAccount.getId().equals(account.getId())) {
            throw new SecurityBusinessException();
        }

        return account;
    }

    // ***** Users *****

    // Get users belonging to the user's account.
    public Collection<User> getUsers(String userName) throws UsernameNotFoundException, NullPointerException {
        registerUserWithSecurityContext(userName);

        User securityUser = userService.findByUsername(userName);
        Account securityAccount = securityUser.getAccount();

        Collection<User> users = securityAccount.getUsers();
        return users;
    }

    public User getUser(String userName, Long userId) throws UsernameNotFoundException, SecurityBusinessException, NullPointerException {
        registerUserWithSecurityContext(userName);

        User securityUser = userService.findByUsername(userName);
        Account securityAccount = securityUser.getAccount();

        User user = userService.find(userId);

        Collection<User> accountUsers = securityAccount.getUsers();

        boolean isAccountUser = false;
        for (User accountUser : accountUsers) {
            if (accountUser.getUsername().equals(user.getUsername())) {
                isAccountUser = true;
                break;
            }
        }

        if (!isAccountUser) {
            throw new SecurityBusinessException();
        }

        return user;
    }

    public User createUser(String userName, String payload) throws UsernameNotFoundException, BadRequestBusinessException {
        registerUserWithSecurityContext(userName);

        User securityUser = userService.findByUsername(userName);
        Account securityAccount = securityUser.getAccount();

        JsonObject payloadObject = parsePayload(payload);

        String userNameParam = payloadObject.getString(USER_USER_NAME);
        if (userNameParam == null) {
            throw new BadRequestBusinessException();
        }
        String firstNameParam = payloadObject.getString(USER_FIRST_NAME);
        if (firstNameParam == null) {
            throw new BadRequestBusinessException();
        }
        String lastNameParam = payloadObject.getString(USER_LAST_NAME);
        if (lastNameParam == null) {
            throw new BadRequestBusinessException();
        }
        String emailParam = payloadObject.getString(USER_EMAIL);
        if (emailParam == null) {
            throw new BadRequestBusinessException();
        }
        boolean isEnabledParam = payloadObject.getBoolean(USER_IS_ENABLED);
        JsonArray rolesParam = payloadObject.getJsonArray(USER_ROLES);
        if (rolesParam == null) {
            throw new BadRequestBusinessException();
        }

        Set<Role> roles = new HashSet<Role>();
        for (JsonValue roleParam : rolesParam) {
            Role role = roleService.findByName(roleParam.toString().replace("\"", ""));
            roles.add(role);
        }

        User user = new User();
        user.setUsername(userNameParam);
        user.setFirstName(firstNameParam);
        user.setLastName(lastNameParam);
        user.setEmail(emailParam);
        user.setEnabled(isEnabledParam);
        user.setAccount(securityAccount);
        user.setRoles(roles);

        userService.save(user);

        return user;
    }

    public void updateUser() {
        // TODO: Implement.
    }

    public void deleteUser() {
        // TODO: Implement.
    }

    // ***** Roles *****

    // ***** Internal methods *****

    private static JsonObject parsePayload(String payload) {
        InputStream stream = new ByteArrayInputStream(payload.getBytes(StandardCharsets.UTF_8));
        JsonObject result;
        try (JsonReader jsonReader = Json.createReader(stream)) {
            result = jsonReader.readObject();
        }
        return result;
    }
}
