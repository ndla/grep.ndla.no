package org.mycurriculum.business.api;

public class SecurityBusinessException extends Exception {

    public SecurityBusinessException() {
        super();
    }

    public SecurityBusinessException(String message) {
        super(message);
    }

    public SecurityBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityBusinessException(Throwable cause) {
        super(cause);
    }
}