package org.mycurriculum.business.api;

import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class AsyncImportFacade implements Runnable {

    private final BlockingQueue<DeferredResult<Object>> resultQueue = new LinkedBlockingQueue<>();
    private Thread thread;
    private volatile boolean start = true;

    @Override
    public void run() {

    }
}