/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 10, 2014
 * This exception is included to test if Spring is wired-up correctly.
 * TODO: Remove.
 */

package org.mycurriculum.business.domain.springtest;

public class TestException extends Exception {
    public TestException(String message) {
        super(message);
    }

    public TestException(Throwable throwable) {
        super(throwable);
    }

    public TestException(String message, Throwable throwable) {
        super(message, throwable);
    }
}