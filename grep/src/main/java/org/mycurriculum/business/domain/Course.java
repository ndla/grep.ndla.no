/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 10, 2014
 */

package org.mycurriculum.business.domain;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@NamedQueries({
        @NamedQuery(
                name = "findCourseByName",
                query = "from Course where name = :name"),
        @NamedQuery(
                name = "findCourseByExternalId",
                query = "from Course where externalId = :externalId")
})
@Entity
@FilterDef(name = "courseVersionNumberFilter", parameters = @ParamDef(name = "courseVersionNumberFilterCode", type = "java.lang.Long"))
@Table(name = "course")
@Filter(name = "courseVersionNumberFilter", condition = "version_number = :courseVersionNumberFilterCode")
public class Course implements Serializable {

    // ***** Constants *****
    private static final String DEFAULT_NAME = "";

    private Long id;
    private String externalId;
    private String name;
    private Long versionNumber;
    private Date dateCreated;
    private EducationGroup educationGroup;

    // Course group is the generic name for 'educational program' (from NDLA terminology).
    private Set<CourseGroup> courseGroups;

    // ***** Constructors *****
    public Course() {
        this(DEFAULT_NAME);
    }

    public Course(String name) {
        this.name = name;
    }

    // ***** Properties *****
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    @Column(name = "external_id")
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "version_number")
    public Long getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Long versionNumber) {
        this.versionNumber = versionNumber;
    }

    @Column(name = "date_created")
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @ManyToOne
    @JoinColumn(name = "educationgroup_id")
    public EducationGroup getEducationGroup() {
        return educationGroup;
    }

    public void setEducationGroup(EducationGroup educationGroup) {
        this.educationGroup = educationGroup;
    }

    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    public Set<CourseGroup> getCourseGroups() {
        return courseGroups;
    }

    public void setCourseGroups(Set<CourseGroup> courseGroups) {
        this.courseGroups = courseGroups;
    }

    @Override
    public String toString() {
        return String.format("[Course externalId=%s, name of=%s]", externalId, name);
    }
}
