/**
 * Created by Brett Kromkamp (brett@seria.no)
 * April 11, 2014
 */

package org.mycurriculum.business.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "psi")
public class Psi {

    private Long id;
    private String ref;
}
