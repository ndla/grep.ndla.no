/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 10, 2014
 */

package org.mycurriculum.business.domain;

import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.topicmapservice.TopicMapLinkService;

import javax.persistence.*;
import java.io.IOException;
import java.util.Date;
import java.util.Set;

@NamedQuery(
        name = "findCourseGroupByPsi",
        query = "from CourseGroup cg left join cg.course c where psi = :psi and c.versionNumber = :versionNumber")
@Entity
@Table(name = "coursegroup")
public class CourseGroup {

    private Long id;
    private String psi;
    private Date dateCreated;

    private TopicMapLinkService topicMapLinkService;
    private TopicMapLinkService.TopicMapHandle topicMapHandle;

    // ***** Recursive collection mapping *****
    private CourseGroup parent;
    private Set<CourseGroup> children;
    // ***** Recursive collection mapping *****

    private Course course;

    public CourseGroup() {
    }

    // ***** Properties *****
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    @Column(name = "psi")
    public String getPsi() {
        return psi;
    }

    public void setPsi(String psi) {
        this.psi = psi;
    }

    // ***** Recursive collection mapping *****
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "coursegroup_id")
    public CourseGroup getParent() {
        return parent;
    }

    @SuppressWarnings("unused")
    public void setParent(CourseGroup parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent")
    public Set<CourseGroup> getChildren() {
        return children;
    }

    public void setChildren(Set<CourseGroup> children) {
        this.children = children;
    }
    // ***** Recursive collection mapping *****

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "course_id")
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Column(name = "date_created")
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Transient
    public TopicMapLinkService getTopicMapLinkService() {
        return topicMapLinkService;
    }

    public void setTopicMapLinkService(TopicMapLinkService topicMapLinkService) {
        this.topicMapLinkService = topicMapLinkService;
    }

    @Transient
    public TopicMapLinkService.TopicMapHandle getTopicMapHandle() {
        return topicMapHandle;
    }

    public void setTopicMapHandle(TopicMapLinkService.TopicMapHandle topicMapHandle) {
        this.topicMapHandle = topicMapHandle;
    }

    @Transient
    public OntopiaCourseStructure getOntopiaCourseStructure() {
        OntopiaCourseStructure ontopiaCourseStructure = null;
        try {
            ontopiaCourseStructure = topicMapLinkService.getCourseStructure(topicMapHandle, psi);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ontopiaCourseStructure;
    }
}
