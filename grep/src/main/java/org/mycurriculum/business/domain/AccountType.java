package org.mycurriculum.business.domain;

public enum AccountType {
    BUSINESS_ACCOUNT,
    PRIVATE_ACCOUNT
}
