package org.mycurriculum.business.domain;

import javax.persistence.*;
import java.util.Date;

@NamedQueries({
        @NamedQuery(
                name = "findActiveVersion",
                query = "from Version where active = true"),
        @NamedQuery(
                name = "findVersionByVersionNumber",
                query = "from Version where versionNumber = :versionNumber"),
        @NamedQuery(
                name = "deactivateAllVersions",
                query = "update Version set active = false where active = true")
})
@Entity
@Table(name = "version")
public class Version {

    private Long id;
    private String name;
    private Long versionNumber;
    private String description;
    private String topicMapConnectionInfo;
    private boolean active = false;
    private Date dateCreated;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "version_number")
    public Long getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Long versionNumber) {
        this.versionNumber = versionNumber;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "conn_topicmap")
    public String getTopicMapConnectionInfo() {
        return topicMapConnectionInfo;
    }

    public void setTopicMapConnectionInfo(String topicMapConnectionInfo) {
        this.topicMapConnectionInfo = topicMapConnectionInfo;
    }

    @Column(name = "active")
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name = "date_created")
    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
