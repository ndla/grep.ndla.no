/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 10, 2014
 */

package org.mycurriculum.business.domain;

public class Name {

    private static final String DEFAULT_NAME = "";

    private Long id;
    private String name;

    public Name() {
        this(DEFAULT_NAME);
    }

    public Name(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
