/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 10, 2014
 */

package org.mycurriculum.business.domain;

public class LearningResource {

    private String psi;

    public String getPsi() {
        return psi;
    }

    public void setPsi(String psi) {
        this.psi = psi;
    }
}
