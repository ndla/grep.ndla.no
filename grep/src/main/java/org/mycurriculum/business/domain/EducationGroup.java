/**
 * Created by Brett Kromkamp (brett@seria.no)
 * March 10, 2014
 */

package org.mycurriculum.business.domain;

import javax.persistence.*;
import java.util.Set;

@NamedQuery(
        name = "findEducationGroupByExternalId",
        query = "from EducationGroup where externalId = :externalId")
@Entity
@Table(name = "educationgroup")
public class EducationGroup {

    private static final String DEFAULT_NAME = "";

    private Long id;
    private String externalId;
    private String name;

    // ***** Recursive collection mapping *****
    private EducationGroup parent;
    private Set<EducationGroup> children;
    // ***** Recursive collection mapping *****

    private Set<Course> courses;

    public EducationGroup() {
        this(DEFAULT_NAME);
    }

    public EducationGroup(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "external_id")
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    // ***** Recursive collection mapping *****
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "educationgroup_id")
    public EducationGroup getParent() {
        return parent;
    }

    @SuppressWarnings("unused")
    public void setParent(EducationGroup parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
    public Set<EducationGroup> getChildren() {
        return children;
    }

    public void setChildren(Set<EducationGroup> children) {
        this.children = children;
    }
    // ***** Recursive collection mapping *****

    @OneToMany(mappedBy = "educationGroup", fetch = FetchType.EAGER)
    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
}
