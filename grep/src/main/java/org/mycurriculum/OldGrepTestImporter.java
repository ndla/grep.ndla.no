package org.mycurriculum;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.util.oldgrep.OldGrepResourceImporter;
import org.mycurriculum.data.util.parser.MultithreadedTextProgressOutputter;
import org.mycurriculum.data.util.parser.ProgressTracker;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class OldGrepTestImporter {
    AbstractApplicationContext context;

    public static void main(String[] args) {
        OldGrepTestImporter test = new OldGrepTestImporter();
        test.context();
        try {
            test.execute();
        } finally {
            test.destroyContext();
        }
    }

    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public void destroyContext() {
        context.close();
    }
    public void execute() {
        OldGrepResourceImporter resourceImporter = (OldGrepResourceImporter) context.getBean("oldGrepResourceImporter");
        TopicMapLocator tmLocator = (TopicMapLocator) context.getBean("testTopicMapLocator");
        ProgressTracker progress = resourceImporter.getProgress();
        progress.addListener(new MultithreadedTextProgressOutputter(progress));
        System.out.println("Importing old resources");
        try {
            resourceImporter.parse(tmLocator);
            System.out.println("Resource import is done.");
            resourceImporter.getFilter().printSummary();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
