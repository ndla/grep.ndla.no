package org.mycurriculum;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaCourseStructure;
import org.mycurriculum.data.tms.domain.OntopiaCurriculum;
import org.mycurriculum.data.tms.domain.OntopiaLevel;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;

public class OntopiaList {
    AbstractApplicationContext context;

    public static void main(String[] args) {
        OntopiaList test = new OntopiaList();

        test.context();
        try {
            test.execute();
        } finally {
            test.destroyContext();
        }
    }

    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public void destroyContext() {
        context.close();
    }
    public void execute() {
        TopicMapLocator tmLocator = (TopicMapLocator) context.getBean("testTopicMapLocator");
        try {
            TopicMapRoot root = tmLocator.getTopicMapRoot();
            System.out.println("Curricula:");
            Collection<OntopiaCurriculum> curricula = root.getCurricula();
            for (OntopiaCurriculum curriculum : curricula) {
                String curriculumType = "";
                Collection<String> typePsis = curriculum.getType();
                for (String typePsi : typePsis) {
                    if (curriculumType.length() > 0)
                        curriculumType += ", ";
                    curriculumType += typePsi;
                }
                System.out.println(" * "+curriculum.getPsi()+" "+curriculum.getName()+" ("+curriculum.getHumanId()+" " + curriculumType + ")");
            }
            System.out.println("Course structures:");
            Collection<URL> psis = new HashSet<URL>();
            Collection<OntopiaCourseStructure> courseStructures = root.getCourseStructures();
            for (OntopiaCourseStructure courseStructure : courseStructures) {
                System.out.println(" * "+courseStructure.getPsi()+" "+courseStructure.getName()+" (id="+courseStructure.getId()+", humanid="+courseStructure.getHumanId()+")");
                psis.add(courseStructure.getPsi());
            }
            System.out.println("Course structures by psi:");
            for (URL psi : psis) {
                OntopiaCourseStructure courseStructure = root.getCourseStructure(psi);
                System.out.println(" * "+courseStructure.getName()+" (id="+courseStructure.getId()+", humanid="+courseStructure.getHumanId()+")");
            }
            System.out.println("Levels:");
            Collection<OntopiaLevel> levels = root.getLevels();
            for (OntopiaLevel level : levels) {
                System.out.println(" * "+level.getPsi()+" "+level.getName()+" ("+level.getHumanId()+")");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
