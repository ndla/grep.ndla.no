package org.mycurriculum;

import org.mycurriculum.business.domain.springtest.TestException;
import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaAim;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.data.tms.domain.OntopiaResourceAimAssociation;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

public class OntopiaListResources {
    AbstractApplicationContext context;

    public static void main(String[] args) {
        OntopiaListResources test = new OntopiaListResources();
        try {
            test.context();
            test.execute();
            test.destroyContext();
        } catch (TestException e) {
            throw new RuntimeException("Failed", e);
        }
    }

    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public void destroyContext() {
        context.close();
    }
    public void execute() throws TestException {
        Date now = new Date();
        Date deadline = new Date(now.getTime() + 120000);
        TopicMapLocator tmLocator = (TopicMapLocator) context.getBean("testTopicMapLocator");
        TopicMapRoot root;
        try {
            root = tmLocator.getTopicMapRoot();
            root.openPersistentConnection();
        } catch (IOException e) {
            throw new TestException("IO error", e);
        }
        try {
            System.out.println("Resources:");
            Collection<OntopiaResource> resources = root.getResources();
            for (OntopiaResource resource : resources) {
                now = new Date();
                if (now.getTime() >= deadline.getTime()) {
                    System.out.println("Maximum time of two minutes expired");
                    break;
                }
                System.out.println(" * "+resource.getName());
                for (OntopiaResourceAimAssociation assoc : resource.getAimAssociations()) {
                    OntopiaAim aim = assoc.getAim();
                    System.out.println("  - "+aim.getName());
                }
            }
        } catch (IOException e) {
            throw new TestException("IO error", e);
        } finally {
            root.closePersistentConnection();
        }
    }
}
