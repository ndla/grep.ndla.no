package org.mycurriculum;

import org.mycurriculum.data.tms.connections.TopicMapLocator;
import org.mycurriculum.data.tms.domain.OntopiaResource;
import org.mycurriculum.data.tms.references.TopicMapRoot;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Collection;

public class RemoveResources {
    AbstractApplicationContext context;

    public static void main(String[] args) {
        RemoveResources test = new RemoveResources();
        test.context();
        try {
            test.execute();
        } finally {
            test.destroyContext();
        }
    }

    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public void destroyContext() {
        context.close();
    }
    public void execute() {
        TopicMapLocator tmLocator = (TopicMapLocator) context.getBean("testTopicMapLocator");
        System.out.println("Finding the resources");
        try {
            TopicMapRoot tmRoot = tmLocator.getTopicMapRoot();
            Collection<OntopiaResource> resources = tmRoot.getResources();
            System.out.println("Deleting "+resources.size()+" resources");
            tmRoot.deleteTopics(resources);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
