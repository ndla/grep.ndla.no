package org.mycurriculum;

import org.mycurriculum.data.topicmapservice.ParserService;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.sql.SQLException;

public class UdirTestParserCreatedb {
    AbstractApplicationContext context;

    public static void main(String[] args) {
        UdirTestParserCreatedb test = new UdirTestParserCreatedb();
        test.context();
        try {
            test.execute();
        } finally {
            test.destroyContext();
        }
    }

    public void context() {
        context = new ClassPathXmlApplicationContext("classpath*:spring.xml");
    }
    public void destroyContext() {
        context.close();
    }
    public void execute() {
        ParserService parserService = (ParserService) context.getBean("parserService");
        try {
            String connectionString = parserService.createTopicmap("testudirimport", "testudirimport");
            System.out.println("Created: " + connectionString);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}
