<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/tx
        http://www.springframework.org/schema/tx/spring-tx.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">

    <!-- Hibernate DAO components -->
    <context:component-scan base-package="org.mycurriculum.data.dao"/>

    <context:component-scan base-package="org.mycurriculum.data.adapter"/>
    <context:component-scan base-package="org.mycurriculum.data.adapter.udir"/>

    <!-- Entity service components -->
    <context:component-scan base-package="org.mycurriculum.data.entityservice"/>

    <!-- Translates Hibernate exceptions to Spring Data Access Exceptions -->
    <bean class="org.springframework.orm.hibernate4.HibernateExceptionTranslator"/>

    <!-- External database configuration file -->
    <context:property-placeholder
        location="file:${user.home}/etc/mycurriculum.properties"
        order="1" ignore-unresolvable="true" ignore-resource-not-found="true" />
    <context:property-placeholder
        location="classpath:/db.properties"
        order="2" ignore-unresolvable="true" />
    <context:property-placeholder
        location="classpath:/db.topicmap.properties"
        order="3" ignore-unresolvable="true" />
    <context:property-placeholder
            location="classpath:/mycurriculum.properties"
            order="4" ignore-unresolvable="true" />

    <!-- Security (Users / Roles / Accounts) database configuration -->

    <!-- Pooled data source bean -->
    <bean id="securityDataSource"
          class="org.apache.commons.dbcp.BasicDataSource"
          destroy-method="close">
    <property name="driverClassName" value="${jdbc.driverClassName}"/>
        <property name="url" value="${jdbc.url}"/>
        <property name="username" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
        <property name="initialSize" value="5"/>
        <property name="maxActive" value="10"/>
    </bean>

    <!-- Transaction manager -->
    <tx:annotation-driven transaction-manager="securityTransactionManager"/>

    <!-- Inject the session factory into the transaction manager -->
    <bean id="securityTransactionManager" class="org.springframework.orm.hibernate4.HibernateTransactionManager">
        <property name="sessionFactory" ref="securitySessionFactory"/>
        <qualifier value="security"/>
    </bean>

    <!-- Session factory bean -->
    <bean id="securitySessionFactory"
          class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
        <property name="dataSource">
            <ref bean="securityDataSource"/>
        </property>
        <property name="hibernateProperties">
            <props>
                <prop key="hibernate.dialect">${hibernate.dialect}</prop>
                <prop key="hibernate.show_sql">false</prop>
                <prop key="hibernate.format_sql">true</prop>
            </props>
        </property>
        <property name="annotatedClasses">
            <list>
                <value>org.mycurriculum.business.domain.User</value>
                <value>org.mycurriculum.business.domain.Role</value>
                <value>org.mycurriculum.business.domain.Account</value>
            </list>
        </property>
    </bean>

    <!-- JdbcTemplate -->
    <bean class="org.springframework.jdbc.core.JdbcTemplate"
          p:dataSource-ref="securityDataSource" />

    <!-- Business data (multi-tenancy) database configuration -->

    <!-- Transaction manager -->
    <tx:annotation-driven transaction-manager="businessTransactionManager"/>

    <!-- Inject the session factory into the transaction manager -->
    <bean id="businessTransactionManager" class="org.springframework.orm.hibernate4.HibernateTransactionManager">
        <property name="sessionFactory" ref="businessSessionFactory"/>
        <property name="autodetectDataSource" value="false"/>
        <qualifier value="business"/>
    </bean>

    <bean id="multiTenantProvider" class="org.mycurriculum.data.util.MultiTenantConnectionProviderImpl"
          lazy-init="false">
        <property name="hibernateDialect" value="${hibernate.dialect}"/>
        <property name="hibernateDriver" value="${jdbc.driverClassName}"/>
        <property name="hibernateUrl" value="${jdbc.url}"/>
        <property name="hibernateUsername" value="${jdbc.username}"/>
        <property name="hibernatePassword" value="${jdbc.password}"/>
        <property name="hibernatePoolInitialSize" value="${hibernate.hibernatePoolInitialSize}"/>
        <property name="hibernatePoolMaxActive" value="${hibernate.hibernatePoolMaxActive}"/>
    </bean>

    <bean id="tenantIdResolver" class="org.mycurriculum.data.util.CurrentTenantIdentifierResolverImpl"/>

    <bean id="businessSessionFactory"
          class="org.mycurriculum.data.util.CustomLocalSessionFactoryBean"
          depends-on="multiTenantProvider">
        <property name="multiTenantConnectionProvider" ref="multiTenantProvider"/>
        <property name="tenantIdResolver" ref="tenantIdResolver"/>
        <property name="hibernateProperties">
            <props>
                <prop key="hibernate.dialect">${hibernate.dialect}</prop>
                <prop key="hibernate.show_sql">false</prop>
                <prop key="hibernate.format_sql">true</prop>
                <prop key="hibernate.multiTenancy">DATABASE</prop>
                <!-- TODO: Test -->
                <!--
                <prop key="hibernate.cache.region.factory_class">org.hibernate.cache.ehcache.EhCacheRegionFactory</prop>
                <prop key="hibernate.cache.use_query_cache">true</prop>
                <prop key="hibernate.cache.use_second_level_cache">true</prop>
                -->
                <!-- TODO: Test -->
            </props>
        </property>
        <property name="annotatedClasses">
            <list>
                <value>org.mycurriculum.business.domain.EducationGroup</value>
                <value>org.mycurriculum.business.domain.Course</value>
                <value>org.mycurriculum.business.domain.CourseGroup</value>
                <value>org.mycurriculum.business.domain.Version</value>
            </list>
        </property>
    </bean>

    <!-- http://docs.spring.io/spring/docs/3.1.x/javadoc-api/org/springframework/orm/hibernate4/support/OpenSessionInViewInterceptor.html -->
    <!--
    <bean name="openSessionInViewInterceptor"
          class="org.springframework.orm.hibernate4.support.OpenSessionInViewInterceptor">
        <property name="sessionFactory" ref="sessionFactory"/>
    </bean>
    -->

    <!-- Udir root bean -->
    <bean id="udirTestRateLimiter" class="org.mycurriculum.data.adapter.PerSecondRateLimiterImpl">
        <property name="maxPerSecond" value="3.0f"/>
        <property name="averagingSeconds" value="3.0f"/>
    </bean>
    <bean id="udirTestConcurrencyLimiter" class="org.mycurriculum.data.adapter.ConcurrencyLimiterImpl">
        <property name="concurrency" value="1"/>
        <property name="idlePercentage" value="250"/>
    </bean>
    <bean id="udirFactory" class="org.mycurriculum.data.adapter.udir.UdirFactoryImpl">
        <constructor-arg type="java.net.URL" value="http://data.udir.no/kl06" />
    </bean>
    <bean name="webFetcherFactory" class="org.mycurriculum.data.adapter.WebFetcherFactoryImpl">
        <property name="rateLimiter" ref="udirTestRateLimiter"/>
        <property name="concurrencyLimiter" ref="udirTestConcurrencyLimiter"/>
    </bean>

    <!-- Postgresql template database and creators -->
    <bean id="tmSourcesPostgresql" class="org.mycurriculum.data.tms.connections.config.TmSourcesSetupImpl">
        <property name="id" value="postgresql"/>
        <property name="title" value="Postgresql connection"/>
    </bean>
    <bean id="tmPostgresqlStoreTemplate" class="org.mycurriculum.data.tms.connections.config.PostgresqlConnectionSetupImpl">
        <property name="connectionString" value="${topicmap.db.template}" />
        <property name="username" value="${topicmap.db.username}" />
        <property name="password" value="${topicmap.db.password}" />
        <property name="minimumSize" value="10" />
        <property name="maximumSize" value="30" />
    </bean>
    <bean id="ontopiaConnectionFactory" class="org.mycurriculum.data.tms.connections.ontopia.OntopiaConnectionFactoryImpl">
        <constructor-arg name="cacheDao" ref="cacheDao"/>
    </bean>
    <bean id="postgresqlTemplateTopicMapStoreLocator" class="org.mycurriculum.data.tms.connections.ontopia.TopicMapStoreLocatorImpl">
        <constructor-arg name="connectionFactory" ref="ontopiaConnectionFactory"/>
        <constructor-arg name="cacheDao" ref="cacheDao"/>
        <property name="dbConnectionSetup" ref="tmPostgresqlStoreTemplate"/>
        <property name="tmSourcesSetup" ref="tmSourcesPostgresql"/>
    </bean>
    <bean id="topicMapStoreCreator" class="org.mycurriculum.data.tms.connections.topicmapstore.postgresql.PostgresqlTopicMapStoreCreator">
        <constructor-arg name="connectionFactory" ref="ontopiaConnectionFactory"/>
        <constructor-arg name="cacheDao" ref="cacheDao"/>
        <property name="topicMapStoreLocator" ref="postgresqlTemplateTopicMapStoreLocator"/>
    </bean>
    <bean id="topicMapCreator" class="org.mycurriculum.data.tms.TopicMapCreatorImpl">
        <constructor-arg type="java.net.URL" name="ltmFile" value="classpath:my_curriculum_org_ontologi.ltm"/>
        <constructor-arg type="org.mycurriculum.data.tms.connections.topicmapstore.TopicMapStoreCreator" name="storeCreator" ref="topicMapStoreCreator"/>
        <constructor-arg name="connectionFactory" ref="ontopiaConnectionFactory"/>
    </bean>
    <bean id="parserFactory" class="org.mycurriculum.data.util.parser.ParserFactoryImpl">
        <constructor-arg type="org.mycurriculum.data.adapter.udir.domain.UdirFactory" name="udirFactory" ref="udirFactory"/>
    </bean>
    <bean id="topicmapService" class="org.mycurriculum.data.topicmapservice.data.TopicMapServiceImpl">
        <constructor-arg type="org.mycurriculum.data.util.parser.ParserFactory" name="parserFactory" ref="parserFactory"/>
        <constructor-arg type="org.mycurriculum.data.tms.TopicMapCreator" name="topicMapCreator" ref="topicMapCreator"/>
    </bean>
    <alias alias="parserService" name="topicmapService"/>
    <alias alias="topicMapLinkService" name="topicmapService"/>

    <!-- Test beans for testing udir import -->
    <bean id="testTopicMapResourcesLocator" class="org.mycurriculum.data.tms.TopicMapCreatorImpl$TopicMapLocator" lazy-init="true">
        <constructor-arg name="storeCreator" ref="topicMapStoreCreator"/>
        <constructor-arg name="storeName" value="testresources"/>
        <constructor-arg name="name" value="testresources"/>
    </bean>
    <bean id="testTopicMapLocator" class="org.mycurriculum.data.tms.TopicMapCreatorImpl$TopicMapLocator" lazy-init="true">
        <constructor-arg name="storeCreator" ref="topicMapStoreCreator"/>
        <constructor-arg name="storeName" value="testudirimport"/>
        <constructor-arg name="name" value="mycurriculum"/>
    </bean>

    <!-- old grep database for initial resources import -->
    <bean id="tmOldGrep" class="org.mycurriculum.data.tms.connections.config.PostgresqlConnectionSetupImpl" lazy-init="true">
        <property name="connectionString" value="${oldgrep.topicmap.db.connect}" />
        <property name="username" value="${oldgrep.topicmap.db.username}" />
        <property name="password" value="${oldgrep.topicmap.db.password}" />
    </bean>
    <bean id="oldGrepTopicMapStoreLocator" class="org.mycurriculum.data.tms.connections.ontopia.TopicMapStoreLocatorImpl" lazy-init="true">
        <constructor-arg name="connectionFactory" ref="ontopiaConnectionFactory"/>
        <constructor-arg name="cacheDao" ref="cacheDao"/>
        <property name="dbConnectionSetup" ref="tmOldGrep"/>
        <property name="tmSourcesSetup" ref="tmSourcesPostgresql"/>
    </bean>
    <bean id="oldGrepAllTopicsFilter" class="org.mycurriculum.data.util.oldgrep.OldGrepAllTopicsFilterImpl" lazy-init="true"/>
    <bean id="oldGrepFyrFilter" class="org.mycurriculum.data.util.oldgrep.OldGrepUrlHostFilterImpl" lazy-init="true">
        <constructor-arg name="hostnames">
            <set>
                <value>fyr.ndla.no</value>
                <value>nygiv.ndla.no</value>
            </set>
        </constructor-arg>
    </bean>
    <bean id="oldGrepNotFyrFilter" class="org.mycurriculum.data.util.oldgrep.OldGrepUrlHostFilterImpl" lazy-init="true">
        <constructor-arg name="hostnames">
            <set>
                <value>fyr.ndla.no</value>
                <value>nygiv.ndla.no</value>
            </set>
        </constructor-arg>
        <constructor-arg name="reverse" value="true"/>
    </bean>
    <bean id="oldGrepNdlaFilter" class="org.mycurriculum.data.util.oldgrep.OldGrepUrlHostFilterImpl" lazy-init="true">
        <constructor-arg name="hostnames">
            <set>
                <value>ndla.no</value>
            </set>
        </constructor-arg>
    </bean>
    <bean id="relationImportHack20141118" class="org.mycurriculum.data.util.oldgrep.RelationImportHack20141118" lazy-init="true">
        <constructor-arg name="logfile" value="${oldgrep.relationimporthack20141118.logfile}"/>
        <constructor-arg name="mappingFile" value="classpath:/oldgrep_mappingHack.json"/>
    </bean>
    <bean id="oldGrepResourceImporter" class="org.mycurriculum.data.util.oldgrep.OldGrepResourceImporter" lazy-init="true">
        <constructor-arg name="filter" ref="oldGrepAllTopicsFilter"/>
        <constructor-arg name="storeLocator" ref="oldGrepTopicMapStoreLocator"/>
        <property name="relationImportHack" ref="relationImportHack20141118"/>
    </bean>
    <bean id="oldGrepFyrResourceImporter" class="org.mycurriculum.data.util.oldgrep.OldGrepResourceImporter" lazy-init="true">
        <constructor-arg name="filter" ref="oldGrepFyrFilter"/>
        <constructor-arg name="storeLocator" ref="oldGrepTopicMapStoreLocator"/>
        <property name="relationImportHack" ref="relationImportHack20141118"/>
    </bean>
    <bean id="oldGrepNotFyrResourceImporter" class="org.mycurriculum.data.util.oldgrep.OldGrepResourceImporter" lazy-init="true">
        <constructor-arg name="filter" ref="oldGrepNotFyrFilter"/>
        <constructor-arg name="storeLocator" ref="oldGrepTopicMapStoreLocator"/>
        <property name="relationImportHack" ref="relationImportHack20141118"/>
    </bean>
    <bean id="oldGrepNdlaResourceImporter" class="org.mycurriculum.data.util.oldgrep.OldGrepResourceImporter" lazy-init="true">
        <constructor-arg name="filter" ref="oldGrepNdlaFilter"/>
        <constructor-arg name="storeLocator" ref="oldGrepTopicMapStoreLocator"/>
        <property name="relationImportHack" ref="relationImportHack20141118"/>
    </bean>

    <!-- Search indexers -->
    <bean id="searchIndexerFactory" class="org.mycurriculum.data.util.searchindexer.SearchIndexerFactoryImpl">
    </bean>
    <bean id="ndlaSearchIndexerType" class="org.mycurriculum.data.util.searchindexer.ndla.NdlaSearchIndexerTypeImpl">
        <constructor-arg name="factory" ref="searchIndexerFactory"/>
        <constructor-arg name="name" value="ndla"/>
        <constructor-arg name="serviceUri" type="java.net.URI" value="${ndla.search.uri}"/>
        <constructor-arg name="username" value="${ndla.search.username}"/>
        <constructor-arg name="password" value="${ndla.search.password}"/>
        <property name="httpUsername" value="${ndla.search.http.username}"/>
        <property name="httpPassword" value="${ndla.search.http.password}"/>
    </bean>

    <!-- Aspect declarations go here -->

    <bean id="ehCacheManager" class="org.springframework.cache.ehcache.EhCacheCacheManager">
        <property name="cacheManager">
            <bean class="org.springframework.cache.ehcache.EhCacheManagerFactoryBean">
                <property name="configLocation" value="classpath:/ehcache.xml"/>
            </bean>
        </property>
    </bean>
    <bean id="ehcacheDao" class="org.mycurriculum.data.cache.CacheDaoEhcacheImpl">
        <constructor-arg name="cacheManager" ref="ehCacheManager"/>
        <constructor-arg name="cacheName" value="mycurriculum"/>
    </bean>
    <alias name="ehcacheDao" alias="cacheDao"/>
    <!--
    <bean id="cacheDao" class="org.mycurriculum.data.cache.CacheDaoDebuggerImpl">
        <constructor-arg name="cacheDao" ref="ehcacheDao"/>
    </bean>
    -->
    <!--
    <bean id="cacheDao" class="org.mycurriculum.data.cache.CacheDaoNullImpl">
    </bean>
    -->
</beans>